﻿using dbChange.Models.VanPhongSo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public interface IVanBanRepository
    {
        public List<VanBan> GetDanhSach(long IdNguoiTao, string TrangThai);
        public List<VanBan> GetDanhSach_TruongPhong(long IdTruongPhong, string TrangThai);
        public VanBan TaoMoi(VanBan VanBan);
        public VanBan CapNhap(VanBan VanBan, long IdNhanVien, string GhiChu);
        //public void HuyPhieu(long IdVanBan);
        public VanBan GetById(long Id);
        public VanBan GetByMaVanBan(string MaVanBan);
        //public void KhoaPhieu(long IdVanBan);
        //public void MoKhoaPhieu(long IdVanBan);
        public DataTable GetBaoCaoVanBanTheoThoiGian(string TenDangNhap, long IdDonVi, DateTime TuNgay, DateTime DenNgay);
        public DataTable GetBaoCaoDanhSachVanBanTheoThoiGian(string TenDangNhap, long IdDonVi, DateTime TuNgay, DateTime DenNgay);
    }
}
