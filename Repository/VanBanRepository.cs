﻿using dbChange.Models;
using dbChange.Models.VanPhongSo;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public class VanBanRepository : IVanBanRepository
    {
        string connectionString = "";
        string Domain = "";
        IAccountRepository _accountRepository;
        public VanBanRepository(IConfiguration configuration, IAccountRepository accountRepository)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            Domain = configuration.GetConnectionString("Domain");
            _accountRepository = accountRepository;
        }
        public VanBan CapNhap(VanBan VanBan, long IdNhanVien, string GhiChu)
        {
            VanBan vanban = new VanBan();
            VanBan vanbanTemp = new VanBan();
            vanbanTemp = GetById(VanBan.Id);
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("VPS_UpdateVanBan", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Id", VanBan.Id));
                    cmd.Parameters.Add(new SqlParameter("@MaVanBan", VanBan.MaVanBan));
                    cmd.Parameters.Add(new SqlParameter("@MaQuyenVanBan", VanBan.MaQuyenVanBan));
                    cmd.Parameters.Add(new SqlParameter("@TrangThai", VanBan.TrangThai));
                    cmd.Parameters.Add(new SqlParameter("@MaQR", VanBan.MaQR));
                    cmd.Parameters.Add(new SqlParameter("@TieuDe", VanBan.TieuDe));
                    cmd.Parameters.Add(new SqlParameter("@NoiDung", VanBan.NoiDung));
                    cmd.Parameters.Add(new SqlParameter("@GhiChu", VanBan.GhiChu));
                    cmd.Parameters.Add(new SqlParameter("@IdDonVi", VanBan.IdDonVi));
                    cmd.Parameters.Add(new SqlParameter("@IdNguoiTao", VanBan.IdNguoiTao));
                    cmd.Parameters.Add(new SqlParameter("@IdLoaiVanBan", VanBan.IdLoaiVanban));
                    cmd.ExecuteNonQuery();
                    vanban = GetById(VanBan.Id);

                    cmd = new SqlCommand("GhiLogVanBan", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@MaVanBan", VanBan.MaVanBan));
                    cmd.Parameters.Add(new SqlParameter("@IdNhanVien", IdNhanVien));
                    cmd.Parameters.Add(new SqlParameter("@NoiDungGoc", Newtonsoft.Json.JsonConvert.SerializeObject(vanbanTemp)));
                    cmd.Parameters.Add(new SqlParameter("@NoiDungThayDoi", Newtonsoft.Json.JsonConvert.SerializeObject(vanban)));
                    cmd.Parameters.Add(new SqlParameter("@GhiChu",GhiChu));
                    cmd.ExecuteNonQuery();
                    return vanban;
                }
            }
            catch (Exception ex)
            {
                return vanban;
            }
        }

        public VanBan GetById(long Id)
        {
            VanBan phieu = new VanBan();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                //string commandText = "select * from VPS_VanBan where Id= " + Id;
                string commandText = "SELECT a.*,b.TenDonVi,c.TenDangNhap as TenNguoiTao, d.TenLoaiVanBan" +
                    " from VPS_VanBan a " +
                    " inner join DonViChiTiet b on a.IdDonVi = b.Id " +
                    " inner join Account c on a.IdNguoiTao = c.Id " +
                    " inner join VPS_LoaiVanBan d on a.IdLoaiVanBan=d.Id " +
                    " where a.Id=" + Id + "";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    phieu = new VanBan
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MaVanBan = reader["MaVanBan"] != null && reader["MaVanBan"] != DBNull.Value ? Convert.ToString(reader["MaVanBan"]) : "",
                        MaQuyenVanBan = reader["MaQuyenVanBan"] != null && reader["MaQuyenVanBan"] != DBNull.Value ? Convert.ToString(reader["MaQuyenVanBan"]) :"",
                        TrangThai = reader["TrangThai"] != null && reader["TrangThai"] != DBNull.Value ? Convert.ToString(reader["TrangThai"]) : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? Convert.ToString(reader["MaQR"]) : "",
                        TieuDe = reader["TieuDe"] != null && reader["TieuDe"] != DBNull.Value ? Convert.ToString(reader["TieuDe"]) : "",
                        NoiDung = reader["NoiDung"] != null && reader["NoiDung"] != DBNull.Value ? Convert.ToString(reader["NoiDung"]) : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? Convert.ToString(reader["GhiChu"]) : "",
                        TenDonVi = reader["TenDonVi"] != null && reader["TenDonVi"] != DBNull.Value ? Convert.ToString(reader["TenDonVi"]) : "",
                        IdDonVi = reader["IdDonVi"] != null && reader["IdDonVi"] != DBNull.Value ? Convert.ToInt64(reader["IdDonVi"]) : 0,
                        IdNguoiTao = reader["IdNguoiTao"] != null && reader["IdNguoiTao"] != DBNull.Value ? Convert.ToInt64(reader["IdNguoiTao"]) : 0,
                        TenNguoiTao = reader["TenNguoiTao"] != null && reader["TenNguoiTao"] != DBNull.Value ? Convert.ToString(reader["TenNguoiTao"]) : "",
                        NgayTao = reader["NgayTao"] != null && reader["NgayTao"] != DBNull.Value ? Convert.ToDateTime(reader["NgayTao"]) : Utils.Utils.GetTimeServer(),
                        IdLoaiVanban = reader["IdLoaiVanban"] != null && reader["IdLoaiVanban"] != DBNull.Value ? Convert.ToInt64(reader["IdLoaiVanban"]) : 0,
                        TenLoaiVanban = reader["TenLoaiVanban"] != null && reader["TenLoaiVanban"] != DBNull.Value ? Convert.ToString(reader["TenLoaiVanban"]) : "",


                    };
                }
            }
            return phieu;
        }

        public VanBan GetByMaVanBan(string MaVanBan)
        {
            VanBan phieu = new VanBan();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                //string commandText = "select * from VPS_VanBan where MaVanBan='" + MaVanBan + "'";
                string commandText = "SELECT a.*,b.TenDonVi,c.TenDangNhap as TenNguoiTao, d.TenLoaiVanBan" +
                    " from VPS_VanBan a " +
                    " inner join DonViChiTiet b on a.IdDonVi = b.Id " +
                    " inner join Account c on a.IdNguoiTao = c.Id " +
                    " inner join VPS_LoaiVanBan d on a.IdLoaiVanBan=d.Id " +
                    " where MaVanBan='" + MaVanBan + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    phieu = new VanBan
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MaVanBan = reader["MaVanBan"] != null && reader["MaVanBan"] != DBNull.Value ? Convert.ToString(reader["MaVanBan"]) : "",
                        MaQuyenVanBan = reader["MaQuyenVanBan"] != null && reader["MaQuyenVanBan"] != DBNull.Value ? Convert.ToString(reader["MaQuyenVanBan"]) : "",
                        TrangThai = reader["TrangThai"] != null && reader["TrangThai"] != DBNull.Value ? Convert.ToString(reader["TrangThai"]) : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? Convert.ToString(reader["MaQR"]) : "",
                        TieuDe = reader["TieuDe"] != null && reader["TieuDe"] != DBNull.Value ? Convert.ToString(reader["TieuDe"]) : "",
                        NoiDung = reader["NoiDung"] != null && reader["NoiDung"] != DBNull.Value ? Convert.ToString(reader["NoiDung"]) : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? Convert.ToString(reader["GhiChu"]) : "",
                        TenDonVi = reader["TenDonVi"] != null && reader["TenDonVi"] != DBNull.Value ? Convert.ToString(reader["TenDonVi"]) : "",
                        IdDonVi = reader["IdDonVi"] != null && reader["IdDonVi"] != DBNull.Value ? Convert.ToInt64(reader["IdDonVi"]) : 0,
                        IdNguoiTao = reader["IdNguoiTao"] != null && reader["IdNguoiTao"] != DBNull.Value ? Convert.ToInt64(reader["IdNguoiTao"]) : 0,
                        TenNguoiTao = reader["TenNguoiTao"] != null && reader["TenNguoiTao"] != DBNull.Value ? Convert.ToString(reader["TenNguoiTao"]) : "",
                        NgayTao = reader["NgayTao"] != null && reader["NgayTao"] != DBNull.Value ? Convert.ToDateTime(reader["NgayTao"]) : Utils.Utils.GetTimeServer(),
                        IdLoaiVanban = reader["IdLoaiVanban"] != null && reader["IdLoaiVanban"] != DBNull.Value ? Convert.ToInt64(reader["IdLoaiVanban"]) : 0,
                        TenLoaiVanban = reader["TenLoaiVanban"] != null && reader["TenLoaiVanban"] != DBNull.Value ? Convert.ToString(reader["TenLoaiVanban"]) : "",
                    };
                }
            }
            return phieu;
        }

        public List<VanBan> GetDanhSach(long IdNguoiTao,string TrangThai)
        {
            List<VanBan> lsResult = new List<VanBan>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                
                if(TrangThai == "")
                    commandText = "SELECT a.*,b.TenDonVi,c.TenDangNhap as TenNguoiTao, d.TenLoaiVanBan from VPS_VanBan a " +
                    " inner join DonViChiTiet b on a.IdDonVi = b.Id " +
                    " inner join Account c on a.IdNguoiTao = c.Id inner join VPS_LoaiVanBan d on a.IdLoaiVanBan=d.Id where a.TrangThai!='DaHuy' and IdNguoiTao=" + IdNguoiTao + " order by NgayTao desc";
                else
                    commandText = "SELECT a.*,b.TenDonVi,c.TenDangNhap as TenNguoiTao, d.TenLoaiVanBan from VPS_VanBan a " +
                    " inner join DonViChiTiet b on a.IdDonVi = b.Id " +
                    " inner join Account c on a.IdNguoiTao = c.Id inner join VPS_LoaiVanBan d on a.IdLoaiVanBan=d.Id where a.TrangThai='" + TrangThai+"' and IdNguoiTao=" + IdNguoiTao + " order by NgayTao desc";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    VanBan phieu = new VanBan
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MaVanBan = reader["MaVanBan"] != null && reader["MaVanBan"] != DBNull.Value ? Convert.ToString(reader["MaVanBan"]) : "",
                        MaQuyenVanBan = reader["MaQuyenVanBan"] != null && reader["MaQuyenVanBan"] != DBNull.Value ? Convert.ToString(reader["MaQuyenVanBan"]) : "",
                        TrangThai = reader["TrangThai"] != null && reader["TrangThai"] != DBNull.Value ? Convert.ToString(reader["TrangThai"]) : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? String.Format("data:image/png;base64,{0}", Convert.ToBase64String(Utils.Utils.BitmapToBytes(Utils.Utils.GetQRCode(Domain + "/VanPhongSo/ShareVanBan?code=" + Utils.Utils.Encrypt(reader["MaQR"].ToString()) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString()) )))) : "",
                        Link = Domain + "/VanPhongSo/ShareVanBan?code=" + Utils.Utils.Encrypt(reader["MaQR"].ToString()) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString()),
                        TieuDe = reader["TieuDe"] != null && reader["TieuDe"] != DBNull.Value ? Convert.ToString(reader["TieuDe"]) : "",
                        NoiDung = reader["NoiDung"] != null && reader["NoiDung"] != DBNull.Value ? Convert.ToString(reader["NoiDung"]) : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? Convert.ToString(reader["GhiChu"]) : "",
                        TenDonVi = reader["TenDonVi"] != null && reader["TenDonVi"] != DBNull.Value ? Convert.ToString(reader["TenDonVi"]) : "",
                        IdDonVi = reader["IdDonVi"] != null && reader["IdDonVi"] != DBNull.Value ? Convert.ToInt64(reader["IdDonVi"]) : 0,
                        IdNguoiTao = reader["IdNguoiTao"] != null && reader["IdNguoiTao"] != DBNull.Value ? Convert.ToInt64(reader["IdNguoiTao"]) : 0,
                        TenNguoiTao = reader["TenNguoiTao"] != null && reader["TenNguoiTao"] != DBNull.Value ? Convert.ToString(reader["TenNguoiTao"]) : "",
                        NgayTao = reader["NgayTao"] != null && reader["NgayTao"] != DBNull.Value ? Convert.ToDateTime(reader["NgayTao"]) : Utils.Utils.GetTimeServer(),
                        IdLoaiVanban = reader["IdLoaiVanban"] != null && reader["IdLoaiVanban"] != DBNull.Value ? Convert.ToInt64(reader["IdLoaiVanban"]) : 0,
                        TenLoaiVanban = reader["TenLoaiVanban"] != null && reader["TenLoaiVanban"] != DBNull.Value ? Convert.ToString(reader["TenLoaiVanban"]) : "",


                    };
                    lsResult.Add(phieu);
                }
            }
            return lsResult;
        }

        public List<VanBan> GetDanhSach_TruongPhong(long IdTruongPhong,string TrangThai)
        {
            List<VanBan> lsResult = new List<VanBan>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";

                if (TrangThai == "")
                    commandText = "SELECT a.*,b.TenDonVi,c.TenDangNhap as TenNguoiTao, d.TenLoaiVanBan from VPS_VanBan a " +
                    " inner join DonViChiTiet b on a.IdDonVi = b.Id " +
                    " inner join Account c on a.IdNguoiTao = c.Id inner join VPS_LoaiVanBan d on a.IdLoaiVanBan=d.Id where a.TrangThai!='DaHuy' " +
                    " and (IdDonVi in (select DonViChiTietId from Account_DonViChiTiet where TenDangNhap=(select TenDangNhap from Account where id="+ IdTruongPhong +")))" +
                    " order by NgayTao desc";
                else
                    commandText = "SELECT a.*,b.TenDonVi,c.TenDangNhap as TenNguoiTao, d.TenLoaiVanBan from VPS_VanBan a " +
                    " inner join DonViChiTiet b on a.IdDonVi = b.Id " +
                    " inner join Account c on a.IdNguoiTao = c.Id inner join VPS_LoaiVanBan d on a.IdLoaiVanBan=d.Id where a.TrangThai='" + TrangThai + "'" +
                    " and (IdDonVi in (select DonViChiTietId from Account_DonViChiTiet where TenDangNhap=(select TenDangNhap from Account where id=" + IdTruongPhong + ")))" +
                    " order by NgayTao desc";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    VanBan phieu = new VanBan
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MaVanBan = reader["MaVanBan"] != null && reader["MaVanBan"] != DBNull.Value ? Convert.ToString(reader["MaVanBan"]) : "",
                        MaQuyenVanBan = reader["MaQuyenVanBan"] != null && reader["MaQuyenVanBan"] != DBNull.Value ? Convert.ToString(reader["MaQuyenVanBan"]) : "",
                        TrangThai = reader["TrangThai"] != null && reader["TrangThai"] != DBNull.Value ? Convert.ToString(reader["TrangThai"]) : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? String.Format("data:image/png;base64,{0}", Convert.ToBase64String(Utils.Utils.BitmapToBytes(Utils.Utils.GetQRCode(Domain + "/VanPhongSo/ShareVanBan?code=" + Utils.Utils.Encrypt(reader["MaQR"].ToString()) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString()))))) : "",
                        Link = Domain + "/VanPhongSo/ShareVanBan?code=" + Utils.Utils.Encrypt(reader["MaQR"].ToString()) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString()),
                        TieuDe = reader["TieuDe"] != null && reader["TieuDe"] != DBNull.Value ? Convert.ToString(reader["TieuDe"]) : "",
                        NoiDung = reader["NoiDung"] != null && reader["NoiDung"] != DBNull.Value ? Convert.ToString(reader["NoiDung"]) : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? Convert.ToString(reader["GhiChu"]) : "",
                        TenDonVi = reader["TenDonVi"] != null && reader["TenDonVi"] != DBNull.Value ? Convert.ToString(reader["TenDonVi"]) : "",
                        IdDonVi = reader["IdDonVi"] != null && reader["IdDonVi"] != DBNull.Value ? Convert.ToInt64(reader["IdDonVi"]) : 0,
                        IdNguoiTao = reader["IdNguoiTao"] != null && reader["IdNguoiTao"] != DBNull.Value ? Convert.ToInt64(reader["IdNguoiTao"]) : 0,
                        TenNguoiTao = reader["TenNguoiTao"] != null && reader["TenNguoiTao"] != DBNull.Value ? Convert.ToString(reader["TenNguoiTao"]) : "",
                        NgayTao = reader["NgayTao"] != null && reader["NgayTao"] != DBNull.Value ? Convert.ToDateTime(reader["NgayTao"]) : Utils.Utils.GetTimeServer(),
                        IdLoaiVanban = reader["IdLoaiVanban"] != null && reader["IdLoaiVanban"] != DBNull.Value ? Convert.ToInt64(reader["IdLoaiVanban"]) : 0,
                        TenLoaiVanban = reader["TenLoaiVanban"] != null && reader["TenLoaiVanban"] != DBNull.Value ? Convert.ToString(reader["TenLoaiVanban"]) : "",


                    };
                    lsResult.Add(phieu);
                }
            }
            return lsResult;
        }
        public VanBan TaoMoi(VanBan VanBan)
        {
            VanBan vanban = new VanBan();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("VPS_CreateVanBan", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@MaVanBan", VanBan.MaVanBan));
                    cmd.Parameters.Add(new SqlParameter("@MaQuyenVanBan", VanBan.MaQuyenVanBan));
                    cmd.Parameters.Add(new SqlParameter("@TrangThai", VanBan.TrangThai));
                    cmd.Parameters.Add(new SqlParameter("@MaQR", VanBan.MaQR));
                    cmd.Parameters.Add(new SqlParameter("@TieuDe", VanBan.TieuDe));
                    cmd.Parameters.Add(new SqlParameter("@NoiDung", VanBan.NoiDung));
                    cmd.Parameters.Add(new SqlParameter("@GhiChu", VanBan.GhiChu));
                    cmd.Parameters.Add(new SqlParameter("@IdDonVi", VanBan.IdDonVi));
                    cmd.Parameters.Add(new SqlParameter("@IdNguoiTao", VanBan.IdNguoiTao));
                    cmd.Parameters.Add(new SqlParameter("@IdLoaiVanBan", VanBan.IdLoaiVanban));
                    SqlParameter outputIdParam = new SqlParameter("@new_identity", SqlDbType.BigInt)
                    {
                        Direction = ParameterDirection.Output
                    };
                    cmd.Parameters.Add(outputIdParam);
                    cmd.ExecuteNonQuery();
                    long Id = long.Parse(outputIdParam.Value.ToString());
                    if (Id > 0) vanban = GetById(Id);
                    return vanban;
                }
            }
            catch (Exception ex)
            {
                return vanban;
            }
        }

        //public void HuyPhieu(long IdVanBan)
        //{
        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(connectionString))
        //        {
        //            conn.Open();
        //            string commandText = "update VPS_VanBan set TrangThai='DaHuy' where Id=" + IdVanBan;
        //            SqlCommand cmd = new SqlCommand(commandText, conn);
        //            cmd.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        //public void KhoaPhieu(long IdVanBan)
        //{
        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(connectionString))
        //        {
        //            conn.Open();
        //            string commandText = "update VPS_VanBan set TrangThai='DaKhoa' where Id=" + IdVanBan;
        //            SqlCommand cmd = new SqlCommand(commandText, conn);
        //            cmd.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        //public void MoKhoaPhieu(long IdVanBan)
        //{
        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(connectionString))
        //        {
        //            conn.Open();
        //            string commandText = "update VPS_VanBan set TrangThai='MoiTao' where Id=" + IdVanBan;
        //            SqlCommand cmd = new SqlCommand(commandText, conn);
        //            cmd.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        public DataTable GetBaoCaoVanBanTheoThoiGian(string TenDangNhap,long IdDonVi, DateTime TuNgay, DateTime DenNgay)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("VPS_BaoCaoSoLuongVanBanTheoThoiGian", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@TenDangNhap", TenDangNhap));
                cmd.Parameters.Add(new SqlParameter("@IdDonVi", IdDonVi));
                cmd.Parameters.Add(new SqlParameter("@TuNgay", TuNgay));
                cmd.Parameters.Add(new SqlParameter("@DenNgay", DenNgay));
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetBaoCaoDanhSachVanBanTheoThoiGian(string TenDangNhap,long IdDonVi, DateTime TuNgay, DateTime DenNgay)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("VPS_BaoCaoDanhSachVanBanTheoThoiGian", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@TenDangNhap", TenDangNhap));
                cmd.Parameters.Add(new SqlParameter("@IdDonVi", IdDonVi));
                cmd.Parameters.Add(new SqlParameter("@TuNgay", TuNgay));
                cmd.Parameters.Add(new SqlParameter("@DenNgay", DenNgay));
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
    }
}
