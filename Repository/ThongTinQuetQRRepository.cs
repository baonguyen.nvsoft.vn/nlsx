﻿using dbChange.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public class ThongTinQuetQRRepository : IThongTinQuetQRRepository
    {
        private readonly IHubContext<SignalServer> _context;
        string connectionString = "";
        LibRepository lib;
        public ThongTinQuetQRRepository(IConfiguration configuration,
                                    IHubContext<SignalServer> context)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            _context = context;
            lib = new LibRepository(configuration, context);
        }
        public List<ThongTinQuetQR> GetThongTinQuetQRsByNgay(DateTime NgayCheckIn)
        {
            List<ThongTinQuetQR> thongtins = new List<ThongTinQuetQR>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
          
                SqlCommand cmd = new SqlCommand("GetThongTinQuetQRsByNgay", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@NgayCheckIn", NgayCheckIn));


                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ThongTinQuetQR thongtin = new ThongTinQuetQR();
                        thongtin.Id = reader["Id"] != null && reader["Id"] != DBNull.Value ? Convert.ToInt64(reader["Id"]) : -1;

                        thongtin.MaQR= reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? Convert.ToString(reader["MaQR"]) : "";
                        thongtin.GioQuet= reader["GioQuet"] != null && reader["GioQuet"] != DBNull.Value ? Convert.ToDateTime(reader["GioQuet"]) : new DateTime(1,1,1);
                        thongtin.HoTen= reader["HoTen"] != null && reader["HoTen"] != DBNull.Value ? Convert.ToString(reader["HoTen"]) : "";
                        thongtin.NamSinh= reader["NamSinh"] != null && reader["NamSinh"] != DBNull.Value ? Convert.ToInt32(reader["NamSinh"]) : 0;
                        thongtin.DonViChiTiet= reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? Convert.ToString(reader["DonViChiTiet"]) : "";
                        thongtin.DonViChiTietId= reader["DonViChiTietId"] != null && reader["DonViChiTietId"] != DBNull.Value ? Convert.ToInt32(reader["DonViChiTietId"]) : 0;
                        thongtin.BoPhan= reader["BoPhan"] != null && reader["BoPhan"] != DBNull.Value ? Convert.ToString(reader["BoPhan"]) : "";
                        thongtin.BoPhanId= reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]) : 0;
                        thongtin.LinkTKYT= reader["LinkTKYT"] != null && reader["LinkTKYT"] != DBNull.Value ? Convert.ToString(reader["LinkTKYT"]) : "";
                        thongtin.TrangThai= reader["TrangThai"] != null && reader["TrangThai"] != DBNull.Value ? Convert.ToString(reader["TrangThai"]) : "";
                        thongtin.Color= reader["Color"] != null && reader["Color"] != DBNull.Value ? Convert.ToString(reader["Color"]) : "";
                        thongtin.NguoiQuet= reader["NguoiQuet"] != null && reader["NguoiQuet"] != DBNull.Value ? Convert.ToString(reader["NguoiQuet"]) : "";            
                    
                    thongtins.Add( thongtin);
                }
            }

            return thongtins;
        }

        public ThongTinQuetQR CreteThongTinQuetQRs(ThongTinQuetQR newQR)
        {
           
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("CreteThongTinQuetQRs", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@MaQR", newQR.MaQR));
                cmd.Parameters.Add(new SqlParameter("@GioQuet", newQR.GioQuet));
                cmd.Parameters.Add(new SqlParameter("@HoTen", newQR.HoTen));
                cmd.Parameters.Add(new SqlParameter("@NamSinh", newQR.NamSinh));
                cmd.Parameters.Add(new SqlParameter("@DonViChiTiet", newQR.DonViChiTiet));
                cmd.Parameters.Add(new SqlParameter("@DonViChiTietId", newQR.DonViChiTietId));
                cmd.Parameters.Add(new SqlParameter("@BoPhan", newQR.BoPhan));
                cmd.Parameters.Add(new SqlParameter("@BoPhanId", newQR.BoPhanId));
                cmd.Parameters.Add(new SqlParameter("@LinkTKYT", newQR.LinkTKYT));
                cmd.Parameters.Add(new SqlParameter("@TrangThai", newQR.TrangThai));
                cmd.Parameters.Add(new SqlParameter("@Color", newQR.Color));
                cmd.Parameters.Add(new SqlParameter("@NguoiQuet", newQR.NguoiQuet));


                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ThongTinQuetQR thongtin = new ThongTinQuetQR();
                    thongtin.Id = reader["Id"] != null && reader["Id"] != DBNull.Value ? Convert.ToInt64(reader["Id"]) : -1;

                    thongtin.MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? Convert.ToString(reader["MaQR"]) : "";
                    thongtin.GioQuet = reader["GioQuet"] != null && reader["GioQuet"] != DBNull.Value ? Convert.ToDateTime(reader["GioQuet"]) : new DateTime(1, 1, 1);
                    thongtin.HoTen = reader["HoTen"] != null && reader["HoTen"] != DBNull.Value ? Convert.ToString(reader["HoTen"]) : "";
                    thongtin.NamSinh = reader["NamSinh"] != null && reader["NamSinh"] != DBNull.Value ? Convert.ToInt32(reader["NamSinh"]) : 0;
                    thongtin.DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? Convert.ToString(reader["DonViChiTiet"]) : "";
                    thongtin.DonViChiTietId = reader["DonViChiTietId"] != null && reader["DonViChiTietId"] != DBNull.Value ? Convert.ToInt32(reader["DonViChiTietId"]) : 0;
                    thongtin.BoPhan = reader["BoPhan"] != null && reader["BoPhan"] != DBNull.Value ? Convert.ToString(reader["BoPhan"]) : "";
                    thongtin.BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]) : 0;
                    thongtin.LinkTKYT = reader["LinkTKYT"] != null && reader["LinkTKYT"] != DBNull.Value ? Convert.ToString(reader["LinkTKYT"]) : "";
                    thongtin.TrangThai = reader["TrangThai"] != null && reader["TrangThai"] != DBNull.Value ? Convert.ToString(reader["TrangThai"]) : "";
                    thongtin.Color = reader["Color"] != null && reader["Color"] != DBNull.Value ? Convert.ToString(reader["Color"]) : "";
                    thongtin.NguoiQuet = reader["NguoiQuet"] != null && reader["NguoiQuet"] != DBNull.Value ? Convert.ToString(reader["NguoiQuet"]) : "";

                    return thongtin;
                }
            }

            return null;
        }
    }
}
