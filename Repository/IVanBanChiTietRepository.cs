﻿using dbChange.Models.VanPhongSo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public interface IVanBanChiTietRepository
    {
        public List<VanBan_ChiTiet> GetDanhSach(string MaVanBan);
        public VanBan_ChiTiet TaoMoi(VanBan_ChiTiet VanBanChiTiet);
        public VanBan_ChiTiet CapNhap(VanBan_ChiTiet VanBanChiTiet, long IdNhanVien, string GhiChu);
        public void Xoa(long IdVanBanChiTiet,long IdNhanVien, string GhiChu);
        public void Huy(long IdVanBanChiTiet, long IdNhanVien, string GhiChu);
        public VanBan_ChiTiet GetById(long IdVanBanChiTiet);
        public DataTable GetAllChungTuChiTiet(long IdNguoiTao, string TrangThai);
        public DataTable GetAllChungTuChiTiet_Search(string Search,int Option, long IdNhanVien, DateTime TuNgay, DateTime DenNgay, string IdDonVi, string IdLoaiVanBan);
        public DataTable GetAllChungTuChiTiet_Search_TruongPhong(string Search, int Option, long IdNhanVien, DateTime TuNgay, DateTime DenNgay, string IdDonVi, string IdLoaiVanBan);
    }
}
