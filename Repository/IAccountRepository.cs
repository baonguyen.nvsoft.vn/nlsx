using System.Collections.Generic;
using System.Threading.Tasks;
using dbChange.Models;
using dbChange.Models.VanPhongSo;

namespace dbChange.Repository
{
    public interface IAccountRepository
    {
      
        List<Account> GetAllAccounts();
        List<Account> GetAllAccountsPhanQuyen(Account account);
        public List<Staff> GetAllStaffByDonViBoPhan(string TenDangNhap, string DonVi, string BoPhan);
        public List<Staff> GetAllStaffByDonViBoPhan2(string TenDangNhap, int[] DonVi, string BoPhan);
        public Dictionary<string, int> GetDataVungCovidChart(string TenDangNhap, string DonVi, string BoPhan);
        public Dictionary<string, int> GetDataVungCovidChart2(string TenDangNhap, int[] DonVi, string BoPhan);
        Account CheckLogin(Account acc);
        bool Forgot(Account acc);
        bool SendEmailForgot(Account acc);
        void SendEmailTest();
        public bool SendEmailHoanTatVanBan(long IdNguoiNhan, VanBan vanBan);
        bool UpdatePassword(Account acc, string matkhaumoi);
        Staff GetStaffByCMND(string CMND);
        Staff GetStaffByUserName(string UserName);
        Account GetAccount(string UserName);
        Account GetAccount(long Id);
        public void UpdateTaiKhoan(Account acc);
        public void CreateTaiKhoan(Account acc);
        public bool UpdateStaffThongTinNhanVien(Staff staff);
        public bool UpdateStaffThongTinNhanVien_Temp(Staff staff);
        public bool UpdateStaffDiacChiNhanVien(Staff staff);
        public bool UpdateStaffDiacChiNhanVien_Temp(Staff staff);
        public bool UpdateMui1Staff(Staff staff);
        public bool UpdateMui1Staff_Temp(Staff staff);
        public bool UpdateMui2Staff(Staff staff);
        public bool UpdateMui2Staff_Temp(Staff staff);
        public bool UpdateThongTinKhacStaff(Staff staff);
        public void CreateStaff(Staff staff);
        public bool isAdmin(Account acc);
        public void UpdatePhanQuyenDonViChiTiet(Account acc, string lsDonViChiTiet);
        public string GetPhanQuyenListIdDonViChiTiet(Account acc);
        public bool isScan(Account acc);
        public void SetPhanQuyenDonViTaiKhoan(Account acc, int IdDonViChiTiet, int isCheck);
    }
}