﻿using dbChange.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public interface IPhieuDangKyRaVaoCongRepository 
    {
        public void CreateChiTietPhieuNgay(int IdChiTiet, DateTime Ngay);
        public DataTable GetAllPhieuDangKyRaVaoCong(string UserName);
        public DataTable GetAllPhieuDangKyRaVaoCongDangChoDuyet(Account acc, DateTime TuNgay, DateTime DenNgay);
        public DataTable GetAllPhieuDangKyRaVaoCongDaDuyet(Account acc, DateTime TuNgay, DateTime DenNgay);
        public DataTable GetAllNhanVienPhieuDangKyRaVaoCong(int IdPhieu);
        public DataTable GetAllNhanVienPhieuDangKyRaVaoCong2(int IdPhieu);
        public int CreatePhieu(PhieuDangKyRaVaoCong phieu);
        public void UpdatePhieu(PhieuDangKyRaVaoCong phieu);
        public PhieuDangKyRaVaoCong GetPhieu(int IdPhieu);
        public List<Staff> GetDanhSachNhanVienByIdDonVi(string TenDonVi);
        public DataTable GetDanhSachNhanVienByIdDonViIdPhieu(string TenDonVi, int IdPhieu);
        public void SaveNhanVienVaoPhieu(int IdNhanVien, int IdPhieu, string LyDo);
        public void UpdateChiTiet(string ThoiGianLamViec, string LyDo, int IdChiTiet, int SoMuiTiem, DateTime NgayTestGanNhat, bool Trong14Ngay_TiepXuc, bool Trong14Ngay_XungQuanhNha, bool NguoiOCung, int ViTriVPLamViecId, int PhanNhomId, int ViTriLamViecId, int ViTriDeXeId, int ViTriVeSinhId, int ViTriNhaAnId);
        public void UpdateChiTietImport(string LyDo, string ThoiGianLamViec, string MaNS, int SoMuiTiem, DateTime NgayTestGanNhat,bool Trong14Ngay_HoSot, bool Trong14Ngay_TiepXuc, bool Trong14Ngay_XungQuanhNha, bool NguoiOCung, long ViTriVPLamViecId, long PhanNhomId, long ViTriLamViecId, long ViTriDeXeId, long ViTriVeSinhId, long ViTriNhaAnId);
        public void XoaNhanVienTrongPhieu(int IdChiTiet);
        public void XoaPhieu(int IdPhieu);
        public void XetDuyetPhieuMaster(int IdPhieu, int Type);
        public void XetDuyetPhieu(int IdPhieu, int IdChiTiet, bool IsDuyet, string LyDo, int IdNguoiDuyet, int Type);
        public void UpdateNhanVienYTeDuyet(int IdNhanVienYTeDuyet, string TenNhanVienYTeDuyet, int IdPhieu);
        public List<Staff> GetDanhSachNhanVienYTeDuyet();
        public void ThemThuChoNhanVien(int IdPhieu,int IdChiTiet, string LsThu);
        public void ThemThuChoNhanVienImport(int IdPhieu,string MaQR, string LsThu);
        public void ThemNgayChoNhanVien(int IdPhieu,int IdChiTiet, DateTime Ngay);
        public DataTable DanhSachNgayDiLamGroupByNhanVien(string lsIdChiTiet);
        public DataTable DanhSachNgayDiLamTrenPhieu(int IdPhieu);
        public void XoaChiTietNgay(int IdChiTietNgay);
        public void XoaChiTietNgayTheoIdChiTiet(int IdChiTiet);
        public void UpdateTrangThaiPhieu(int IdPhieu, string TrangThai);
        public void CapNhatCacDongChiTiet(int IdChiTiet, string Loai, string GiaTri);
        public void CopyChiTietPhieu(int IdPhieuCopy, int IdPhieu);
        public bool IsBoSung();
        public int SoLuongNhanVienTrongPhieu(int IdPhieu, string TinhTrang);
        public DataTable GetSoLuongNhanVienChiTietPhieuTheoThu(int IdPhieu, string TinhTrang);
    }
}
