﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using dbChange.Models;
using dbChange.Models.LIB;
using dbChange.Models.VanPhongSo;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace dbChange.Repository{
    public class AccountRepository : IAccountRepository
    {
        private readonly IHubContext<SignalServer> _context;
        string connectionString = "";
        private IHostingEnvironment _env;
        IStaffRepository _staffRepository;
        ILibRepository _libRepository;
        string Domain = "";
        public AccountRepository(IConfiguration configuration,IStaffRepository staffRepository, ILibRepository libRepository,
                                    IHubContext<SignalServer> context, IHostingEnvironment env)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            Domain = configuration.GetConnectionString("Domain");
            _context = context;
            _env = env;
            _staffRepository = staffRepository;
            _libRepository = libRepository;
        }
        public bool isAdmin(Account acc)
        {
            bool result = false;
            try
            {
                if (acc != null)
                {
                    using (SqlConnection conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        string commandText = "select isnull(isAdmin,0) as isAdmin from Account where TenDangNhap='"+acc.TenDangNhap+"'";
                        SqlCommand cmd = new SqlCommand(commandText, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            if (Convert.ToBoolean(reader["isAdmin"]))
                                return true;
                            else 
                                return false;
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool isScan(Account acc)
        {
            bool result = false;
            try
            {
                if (acc != null)
                {
                    using (SqlConnection conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        string commandText = "select isnull(isScan,0) as isScan from Account where TenDangNhap='" + acc.TenDangNhap + "'";
                        SqlCommand cmd = new SqlCommand(commandText, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            if (Convert.ToBoolean(reader["isScan"]))
                                return true;
                            else
                                return false;
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public List<Account> GetAllAccounts()
        {
            var accounts = new List<Account>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "SELECT a.*,b.DonViChiTiet  FROM Account a left join Staff b on a.MaNhanVien=b.MaNS";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var account = new Account
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        TenDangNhap = reader["TenDangNhap"].ToString(),
                        HoTen = reader["HoTen"].ToString(),
                        DonViChiTiet = reader["DonViChiTiet"].ToString(),
                        MatKhau = reader["MatKhau"].ToString(),
                        MaNhanVien = reader["MaNhanVien"].ToString(),
                        Email = reader["Email"].ToString(),
                        isYTeDuyet = reader["isYTeDuyet"] != null && reader["isYTeDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isYTeDuyet"]) : false,
                        isDuyet = reader["isDuyet"] != null && reader["isDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isDuyet"]) : false,
                        isTaoPhieu = reader["isTaoPhieu"] != null && reader["isTaoPhieu"] != DBNull.Value ? Convert.ToBoolean(reader["isTaoPhieu"]) : false,
                        isAdmin = reader["isAdmin"] != null && reader["isAdmin"] != DBNull.Value ? Convert.ToBoolean(reader["isAdmin"]) : false,
                        isScan = reader["isScan"] != null && reader["isScan"] != DBNull.Value ? Convert.ToBoolean(reader["isScan"]) : false,
                        isTruongPhong = reader["isTruongPhong"] != null && reader["isTruongPhong"] != DBNull.Value ? Convert.ToBoolean(reader["isTruongPhong"]) : false,
                        isVPS = reader["isVPS"] != null && reader["isVPS"] != DBNull.Value ? Convert.ToBoolean(reader["isVPS"]) : false,
                    };

                    accounts.Add(account);
                }
            }

            return accounts;
        }

        List<Account> IAccountRepository.GetAllAccountsPhanQuyen(Account acc)
        {
            var accounts = new List<Account>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);
                string commandText = "";
                if (acc.isAdmin!=null && acc.isAdmin == true)
                {
                    commandText = "SELECT a.*,b.DonViChiTiet  " +
                        "FROM Account a " +
                        "left join Staff b on a.MaNhanVien = b.MaNS " +
                        "where b.DonViChiTiet in (select DonViChiTiet from Account_DonViChiTiet where TenDangNhap = '" + acc.TenDangNhap + "') or isnull(b.DonViChiTiet,'')=''";
                }
                else
                {
                    commandText = "SELECT a.*,b.DonViChiTiet  " +
                        "FROM Account a " +
                        "left join Staff b on a.MaNhanVien = b.MaNS " +
                        "where b.DonViChiTiet in (select DonViChiTiet from Account_DonViChiTiet where TenDangNhap = '" + acc.TenDangNhap + "')";
                }
                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var account = new Account
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        TenDangNhap = reader["TenDangNhap"].ToString(),
                        HoTen = reader["HoTen"].ToString(),
                        DonViChiTiet = reader["DonViChiTiet"].ToString(),
                        MatKhau = reader["MatKhau"].ToString(),
                        MaNhanVien = reader["MaNhanVien"].ToString(),
                        Email = reader["Email"].ToString(),
                        isYTeDuyet = reader["isYTeDuyet"] != null && reader["isYTeDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isYTeDuyet"]) : false,
                        isDuyet = reader["isDuyet"] != null && reader["isDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isDuyet"]) : false,
                        isTaoPhieu = reader["isTaoPhieu"] != null && reader["isTaoPhieu"] != DBNull.Value ? Convert.ToBoolean(reader["isTaoPhieu"]) : false,
                        isAdmin = reader["isAdmin"] != null && reader["isAdmin"] != DBNull.Value ? Convert.ToBoolean(reader["isAdmin"]) : false,
                        isScan = reader["isScan"] != null && reader["isScan"] != DBNull.Value ? Convert.ToBoolean(reader["isScan"]) : false,
                        isTruongPhong = reader["isTruongPhong"] != null && reader["isTruongPhong"] != DBNull.Value ? Convert.ToBoolean(reader["isTruongPhong"]) : false,
                        isVPS = reader["isVPS"] != null && reader["isVPS"] != DBNull.Value ? Convert.ToBoolean(reader["isVPS"]) : false,
                    };

                    accounts.Add(account);
                }
            }

            return accounts;
        }
        public void UpdateTaiKhoan(Account acc)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "update account set isVPS="+(acc.isVPS ? 1 : 0) +",isTruongPhong=" + (acc.isTruongPhong ? 1 : 0) + ",isScan=" + (acc.isScan? 1 :0)+", Hoten=N'"+acc.HoTen+"', MaNhanVien=N'"+acc.MaNhanVien+ "', TenDangNhap=N'" + acc.TenDangNhap + "', Email=N'" + acc.Email + "' where Id=" + acc.Id;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteNonQuery();
            };
        } 
        public void CreateTaiKhoan(Account acc)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "insert into account (Hoten, MaNhanVien,TenDangNhap, Email, MatKhau) values(N'"+acc.HoTen+ "',N'" + acc.MaNhanVien + "',N'" + acc.TenDangNhap + "',N'" + acc.Email + "','"+Utils.Utils.Encrypt(acc.MatKhau)+"')";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteNonQuery();
            };
        }
        public string GetPhanQuyenListIdDonViChiTiet(Account acc)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "SELECT abc = STUFF((SELECT ',' + cast(DonViChiTietId as nvarchar(10)) FROM Account_DonViChiTiet WHERE tendangnhap='"+acc.TenDangNhap+"' FOR XML PATH('') ), 1, 1, '') " +
                    " FROM Account_DonViChiTiet " +
                    " WHERE tendangnhap='"+ acc.TenDangNhap + "' " +
                    " group by tendangnhap";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    return reader["abc"].ToString();
                }
            }

            return "";
        }
        public Account CheckLogin(Account acc)
        {
            Account account = new Account();
           
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "SELECT a.*,b.DonViChiTiet  FROM Account a left join Staff b on a.MaNhanVien=b.MaNS where TenDangNhap = '" + acc.TenDangNhap+"' and MatKhau = '"+ Utils.Utils.Encrypt(acc.MatKhau) + "'";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                     account = new Account
                    {
                         Id = Convert.ToInt32(reader["Id"]),
                         TenDangNhap = reader["TenDangNhap"].ToString(),
                         HoTen = reader["HoTen"].ToString(),
                         DonViChiTiet = reader["DonViChiTiet"].ToString(),
                         MatKhau = reader["MatKhau"].ToString(),
                         MaNhanVien = reader["MaNhanVien"].ToString(),
                         Email = reader["Email"].ToString(),
                         isYTeDuyet = reader["isYTeDuyet"] != null && reader["isYTeDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isYTeDuyet"]) : false,
                         isDuyet = reader["isDuyet"] != null && reader["isDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isDuyet"]) : false,
                         isTaoPhieu = reader["isTaoPhieu"] != null && reader["isTaoPhieu"] != DBNull.Value ? Convert.ToBoolean(reader["isTaoPhieu"]) : false,
                         isAdmin = reader["isAdmin"] != null && reader["isAdmin"] != DBNull.Value ? Convert.ToBoolean(reader["isAdmin"]) : false,
                         isScan = reader["isScan"] != null && reader["isScan"] != DBNull.Value ? Convert.ToBoolean(reader["isScan"]) : false,
                         isTruongPhong = reader["isTruongPhong"] != null && reader["isTruongPhong"] != DBNull.Value ? Convert.ToBoolean(reader["isTruongPhong"]) : false,
                         isVPS = reader["isVPS"] != null && reader["isVPS"] != DBNull.Value ? Convert.ToBoolean(reader["isVPS"]) : false,

                     };

                   
                }
            }

            return account;
        }
        public bool UpdatePassword(Account acc, string Password)
        {
            bool result = false;
            try
            {
                if (acc != null)
                {
                    using (SqlConnection conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        //SqlDependency.Start(connectionString);
                        string commandText = "Update Account set MatKhau='" + Utils.Utils.Encrypt(Password) + "' where TenDangNhap='" + acc.TenDangNhap + "'";
                        SqlCommand cmd = new SqlCommand(commandText, conn);
                        //SqlDependency dependency = new SqlDependency(cmd);
                        //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool Forgot(Account acc)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    //SqlDependency.Start(connectionString);

                    string commandText = "select * from Account where TenDangNhap='" + acc.TenDangNhap + "' and Email='"+ acc.Email + "'";

                    SqlCommand cmd = new SqlCommand(commandText, conn);

                    //SqlDependency dependency = new SqlDependency(cmd);

                    //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        private void dbChangeNotification(object sender, SqlNotificationEventArgs e)
        {
            Console.WriteLine(e.Info);
          //  _context.Clients.All.SendAsync("refreshEmployees");
        }
        
        public bool SendEmailForgot(Account acc)
        {
            bool result = false;
            string verify = Utils.Utils.Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(acc));
            string exp = Utils.Utils.Encrypt(Utils.Utils.GetTimeServer().ToString());
            string fullname = acc.TenDangNhap;

            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("itsc@nhabe.com.vn","Intranet NBC");
                message.To.Add(new MailAddress(acc.Email));
                message.Subject = "Lấy lại mật khẩu đăng nhập Intranet NBC";
                message.IsBodyHtml = true; //to make message body as html  
                string body = @"<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                body += "<tr>" +
                            "<td align='center' bgcolor='#e9ecef'>" +
                                "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>" +
                                    "<tr>" +
                                        "<td align='center' valign='top' style='padding: 36px 24px;'>" +
                                            "<a href='https://intranet.nhabe.com.vn' target='_blank' style='display: inline-block;'>" +
                                                "<img src='https://www.nhabe.com.vn/assets/img/logo.png' alt='Logo' border='0' width='48' style='display: block; width: 48px; max-width: 48px; min-width: 48px;' />" +
                                            "</a>" +
                                        "</td>" +
                                      "</tr>" +
                                 "</table>" +
                             "</td>" +
                         "</tr>" +
                         "<tr>" +
                            "<td align='center' bgcolor='#e9ecef'>" +
                                "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>" +
                                    "<tr>" +
                                        "<td align='left' bgcolor='#ffffff' style='padding: 36px 24px 0; border-top: 3px solid #d4dadf;'>" +
                                            "<h1 style='margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;'>Thay đổi mật khẩu</h1>" +
                                        "</td>" +
                                    "</tr>" +
                                "</table>" +
                            "</td>" +
                         "</tr>" +
                          "<tr>" +
                            "<td align='center' bgcolor='#e9ecef'>" +
                                "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>" +
                                    "<tr>" +
                                        "<td align='left' bgcolor='#ffffff' style='padding: 24px;  font-size: 16px; line-height: 24px;'>" +
                                            "<p>Xin chào <strong>"+ fullname +"</strong></p><p style='margin: 0;'>Click vào nút bên dưới để tiến hành đổi mật khẩu. Nếu không phải bạn yêu cầu thì bỏ qua email này.</p>" +
                                        "</td>" +
                                    "</tr>" +
                                "</table>" +
                            "</td>" +
                         "</tr>" +
                          "<tr>" +
                            "<td align='center' bgcolor='#e9ecef'>" +
                                "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>" +
                                    "<tr>" +
                                        "<td align='center' bgcolor='white' style='border-radius: 6px;'>" +
                                            "<a href='https://intranet.nhabe.com.vn/Account/ChangePassword?exp=" + exp + "&verify=" + verify + "' target='_blank' style='background-color:#1387d9;display: inline-block; padding: 16px 36px;  font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;'>Đổi mật khẩu</a>" +
                                         "</td>" +
                                    "</tr>" +
                                "</table>" +
                            "</td>" +
                         "</tr>" +
                          "<tr>" +
                            "<td align='center' bgcolor='#e9ecef'>" +
                                "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>" +
                                    "<tr>" +
                                        "<td align='left' bgcolor='#ffffff' style='padding: 24px;  font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf'>" +
                                            "<p style='margin: 0;'>Trung Tâm Hệ Thống Công Nghệ Thông Tin - ITSC</p>" +
                                        "</td>" +
                                    "</tr>" +
                                "</table>" +
                            "</td>" +
                         "</tr>";
                body += "</table>";
                message.Body = body;
                smtp.Port = 587;
                smtp.Host = "mail.nhabe.com.vn"; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("itsc@nhabe.com.vn", "M*&$01Hpe2@");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
                result = true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        public void SendEmailTest()
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("nhansuct@congtien.com.vn", "CT");
                message.To.Add(new MailAddress("nguyenduybaotn@gmail.com"));
                message.Subject = "Email Test CT";
                message.IsBodyHtml = true; //to make message body as html  
                string body = @"<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                body += "</table>";
                message.Body = body;
                smtp.Port = 465;
                smtp.Host = "mail9040.maychuemail.com"; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("nhansuct@congtien.com.vn", "******");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }
        public bool SendEmailHoanTatVanBan(long IdNguoiNhan, VanBan vanBan)
        {
            Account acc = GetAccount(IdNguoiNhan);
            bool result = false;
            string fullname = acc.TenDangNhap;
            
            string maqr = String.Format("data:image/png;base64,{0}", Convert.ToBase64String(Utils.Utils.BitmapToBytes(Utils.Utils.GetQRCode(Domain + "/VanPhongSo/ShareVanBan?code=" + Utils.Utils.Encrypt(vanBan.MaQR) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString())))));
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("itsc@nhabe.com.vn", "Intranet NBC - Văn phòng số NBC");
                message.To.Add(new MailAddress(acc.Email));
                message.Subject = "Văn phòng số NBC - Hoàn tất phiếu đăng ký số hóa online";
                message.IsBodyHtml = true; //to make message body as html  
                string body = @"<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                body += "<tr>" +
                            "<td align='center' bgcolor='#e9ecef'>" +
                                "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>" +
                                    "<tr>" +
                                        "<td align='center' valign='top' style='padding: 36px 24px;'>" +
                                            "<a href='https://intranet.nhabe.com.vn' target='_blank' style='display: inline-block;'>" +
                                                "<img src='https://www.nhabe.com.vn/assets/img/logo.png' alt='Logo' border='0' width='48' style='display: block; width: 48px; max-width: 48px; min-width: 48px;' />" +
                                            "</a>" +
                                        "</td>" +
                                      "</tr>" +
                                 "</table>" +
                             "</td>" +
                         "</tr>" +
                         "<tr>" +
                            "<td align='center' bgcolor='#e9ecef'>" +
                                "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>" +
                                    "<tr>" +
                                        "<td align='left' bgcolor='#ffffff' style='padding: 36px 0px 0; border-top: 3px solid #d4dadf;'>" +
                                            "<h1 style='margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;'>Phiếu đăng ký số hóa chứng từ online đã hoàn tất</h1>" +
                                        "</td>" +
                                    "</tr>" +
                                "</table>" +
                            "</td>" +
                         "</tr>" +
                          "<tr>" +
                            "<td align='center' bgcolor='#e9ecef'>" +
                                "<table border='1' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>" +
                                    "<tr style='paadding:5px;'>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>Mã văn bản</td>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>" + vanBan.MaVanBan+"</td>" +
                                    "</tr>" +
                                     "<tr style='paadding:5px;'>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>Tiêu đề</td>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>" + vanBan.TieuDe + "</td>" +
                                    "</tr>" +
                                     "<tr style='paadding:5px;'>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>Nội dung</td>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>" + vanBan.NoiDung + "</td>" +
                                    "</tr>" +
                                     "<tr style='paadding:5px;'>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>Ngày tạo</td>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>" + vanBan.NgayTao.ToString("dd/MM/yyyy") + "</td>" +
                                    "</tr>" +
                                    "<tr style='paadding:5px;'>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>" +
                                            "Liên kết để chia sẻ" +
                                         "</td>" +
                                        "<td align='left' bgcolor='white' style='border-radius: 6px;padding:5px;'>" +
                                            "<a href='" + Domain + "/VanPhongSo/ShareVanBan?code=" + Utils.Utils.Encrypt(vanBan.MaQR) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString()) + "' target='_blank' style='background-color:#1387d9;display: bock; padding: 16px 36px;  font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;'>Link chia sẻ</a>" +
                                         "</td>" +
                                    "</tr>" +
                                "</table>" +
                            "</td>" +
                         "</tr>" +
                          "<tr>" +
                            "<td align='center' bgcolor='#e9ecef'>" +
                                "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='padding-top:20px;max-width: 600px;'>" +
                                    "<tr>" +
                                        "<td align='left' bgcolor='#ffffff' style='font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf'>" +
                                            "<hr></hr><p style='margin: 0;'>Trung Tâm Hệ Thống Công Nghệ Thông Tin - ITSC</p>" +
                                        "</td>" +
                                    "</tr>" +
                                "</table>" +
                            "</td>" +
                         "</tr>";
                body += "</table>";
                message.Body = body;
                
                smtp.Port = 587;
                smtp.Host = "mail.nhabe.com.vn"; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("itsc@nhabe.com.vn", "M*&$01Hpe2@");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                string base64encodedstring = Convert.ToBase64String(Utils.Utils.BitmapToBytes(Utils.Utils.GetQRCode(Domain + "/VanPhongSo/ShareVanBan?code=" + Utils.Utils.Encrypt(vanBan.MaQR) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString()))));

                byte[] bytes = Convert.FromBase64String(base64encodedstring);
                System.Net.Mail.Attachment att = new System.Net.Mail.Attachment(new MemoryStream(bytes), "Barcode_"+vanBan.MaVanBan + ".png");
                message.Attachments.Add(att);

                smtp.Send(message);
                result = true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        public static Image LoadBase64(string base64)
        {
            byte[] bytes = Convert.FromBase64String(base64);
            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }
            return image;
        }
        public Staff GetStaffByUserName(string UserName)
        {
            Account acc = GetAccount(UserName);
            Staff staff = new Staff();
            //staff = _staffRepository.GetStaff(acc.Id.ToString());
            staff = _staffRepository.GetStaff(acc.MaNhanVien);
            return staff;
        }
        public List<Staff> GetAllStaffByDonViBoPhan(string TenDangNhap, string DonVi, string BoPhan)
        {
            List<Staff> result = new List<Staff>();
            result = _staffRepository.GetAllStaffByDonViBoPhan(TenDangNhap,DonVi, BoPhan);
            return result;
        }
        public List<Staff> GetAllStaffByDonViBoPhan2(string TenDangNhap, int[] DonVi, string BoPhan)
        {
            List<Staff> result = new List<Staff>();
            result = _staffRepository.GetAllStaffByDonViBoPhan2(TenDangNhap,DonVi, BoPhan);
            return result;
        }
        public Dictionary<string, int> GetDataVungCovidChart(string TenDangNhap, string DonVi, string BoPhan)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            result = _staffRepository.GetDataVungCovidChart(TenDangNhap,DonVi, BoPhan);
            return result;
        }
        public Dictionary<string, int> GetDataVungCovidChart2(string TenDangNhap, int[] DonVi, string BoPhan)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            result = _staffRepository.GetDataVungCovidChart2(TenDangNhap,DonVi, BoPhan);
            return result;
        }
        public Staff GetStaffByCMND(string CMND)
        {
            Staff staff = new Staff();
            staff = _staffRepository.GetStaffByCMND(CMND);
            return staff;
        }
        public void UpdatePhanQuyenDonViChiTiet(Account acc, string lsDonViChiTiet)
        {
            deletePhanQuyenDonViChiTiet(acc.TenDangNhap);
            string[] ids = lsDonViChiTiet.Split(',');
            foreach(string s in ids)
            {
                int id = Convert.ToInt32(s);
                if (!checkPhanQuyenDonViChiTiet(acc.TenDangNhap, id))
                {
                    DM_BoPhanChiTiet dv = _libRepository.GetDonViChiTietById(id);
                    insertPhanQuyenDonViChiTiet(acc.TenDangNhap,dv.TenDonVi, id);
                }
            }
        }
        public void SetPhanQuyenDonViTaiKhoan(Account acc, int IdDonViChiTiet, int isCheck)
        {
            if (!checkPhanQuyenDonViChiTiet(acc.TenDangNhap, IdDonViChiTiet))
            {
                DM_BoPhanChiTiet dv = _libRepository.GetDonViChiTietById(IdDonViChiTiet);
                insertPhanQuyenDonViChiTiet(acc.TenDangNhap, dv.TenDonVi, IdDonViChiTiet);
            }
            else
            {
                updatePhanQuyenDonViChiTiet(acc.TenDangNhap,IdDonViChiTiet, isCheck);
            }

        }
        public bool checkPhanQuyenDonViChiTiet(string TenDangNhap, int idDonVi)
        {
            bool result = false;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from Account_DonViChiTiet where TenDangNhap = '" + TenDangNhap + "' and DonViChiTietId=" + idDonVi;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    return true;
                }
            }
            return result;
        }
        public void insertPhanQuyenDonViChiTiet(string TenDangNhap,string DonViChiTiet, int idDonVi)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "insert into Account_DonViChiTiet (TenDangNhap,DonViChiTiet,isEnable,DonViChiTietId) values ('"+TenDangNhap+"',N'"+ DonViChiTiet + "',1,"+idDonVi+")";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteNonQuery();
            };
        }
        public void updatePhanQuyenDonViChiTiet(string TenDangNhap,int DonViChiTiet, int isCheck)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "update Account_DonViChiTiet set isEnable=" + isCheck + " where TenDangNhap='"+ TenDangNhap +"' and DonViChiTietId="+DonViChiTiet;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteNonQuery();
            };
        }
        public void deletePhanQuyenDonViChiTiet(string TenDangNhap)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "delete Account_DonViChiTiet where TenDangNhap='"+TenDangNhap+"'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteNonQuery();
            };
        }
        public Account GetAccount(string UserName)
        {
            Account account = new Account();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "SELECT a.*,b.DonViChiTiet  FROM Account a left join Staff b on a.MaNhanVien=b.MaNS where TenDangNhap = '" + UserName + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    account = new Account
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        TenDangNhap = reader["TenDangNhap"].ToString(),
                        HoTen = reader["HoTen"].ToString(),
                        DonViChiTiet = reader["DonViChiTiet"].ToString(),
                        MatKhau = reader["MatKhau"].ToString(),
                        MaNhanVien = reader["MaNhanVien"].ToString(),
                        Email = reader["Email"].ToString(),
                        isYTeDuyet = reader["isYTeDuyet"] != null && reader["isYTeDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isYTeDuyet"]) : false,
                        isDuyet = reader["isDuyet"] != null && reader["isDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isDuyet"]) : false,
                        isTaoPhieu = reader["isTaoPhieu"] != null && reader["isTaoPhieu"] != DBNull.Value ? Convert.ToBoolean(reader["isTaoPhieu"]) : false,
                        isAdmin = reader["isAdmin"] != null && reader["isAdmin"] != DBNull.Value ? Convert.ToBoolean(reader["isAdmin"]) : false,
                        isScan = reader["isScan"] != null && reader["isScan"] != DBNull.Value ? Convert.ToBoolean(reader["isScan"]) : false,
                        isTruongPhong = reader["isTruongPhong"] != null && reader["isTruongPhong"] != DBNull.Value ? Convert.ToBoolean(reader["isTruongPhong"]) : false,
                        isVPS = reader["isVPS"] != null && reader["isVPS"] != DBNull.Value ? Convert.ToBoolean(reader["isVPS"]) : false,

                    };


                }
            }

            return account;
        }
        public Account GetAccount(long Id)
        {
            Account account = new Account();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "SELECT a.*,b.DonViChiTiet  FROM Account a inner join Staff b on a.MaNhanVien=b.MaNS where a.Id = " + Id ;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    account = new Account
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        TenDangNhap = reader["TenDangNhap"].ToString(),
                        HoTen = reader["HoTen"].ToString(),
                        DonViChiTiet = reader["DonViChiTiet"].ToString(),
                        MatKhau = reader["MatKhau"].ToString(),
                        MaNhanVien = reader["MaNhanVien"].ToString(),
                        Email = reader["Email"].ToString(),
                        isYTeDuyet = reader["isYTeDuyet"] != null && reader["isYTeDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isYTeDuyet"]) : false,
                        isDuyet = reader["isDuyet"] != null && reader["isDuyet"] != DBNull.Value ? Convert.ToBoolean(reader["isDuyet"]) : false,
                        isTaoPhieu = reader["isTaoPhieu"] != null && reader["isTaoPhieu"] != DBNull.Value ? Convert.ToBoolean(reader["isTaoPhieu"]) : false,
                        isAdmin = reader["isAdmin"] != null && reader["isAdmin"] != DBNull.Value ? Convert.ToBoolean(reader["isAdmin"]) : false,
                        isScan = reader["isScan"] != null && reader["isScan"] != DBNull.Value ? Convert.ToBoolean(reader["isScan"]) : false,
                        isTruongPhong = reader["isTruongPhong"] != null && reader["isTruongPhong"] != DBNull.Value ? Convert.ToBoolean(reader["isTruongPhong"]) : false,
                        isVPS = reader["isVPS"] != null && reader["isVPS"] != DBNull.Value ? Convert.ToBoolean(reader["isVPS"]) : false,
                    };


                }
            }

            return account;
        }
        public bool UpdateStaffThongTinNhanVien(Staff staff) {
            return _staffRepository.UpdateStaffThongTinNhanVien(staff);
        }
        public bool UpdateStaffThongTinNhanVien_Temp(Staff staff)
        {
            return _staffRepository.UpdateStaffThongTinNhanVien_Temp(staff);
        }
        public bool UpdateStaffDiacChiNhanVien(Staff staff)
        {
            return _staffRepository.UpdateStaffDiacChiNhanVien(staff);
        }
        public bool UpdateStaffDiacChiNhanVien_Temp(Staff staff)
        {
            return _staffRepository.UpdateStaffDiacChiNhanVien_Temp(staff);
        }
        public bool UpdateMui1Staff(Staff staff)
        {
            return _staffRepository.UpdateMui1Staff(staff);
        }
        public bool UpdateMui1Staff_Temp(Staff staff)
        {
            return _staffRepository.UpdateMui1Staff_Temp(staff);
        }
        public bool UpdateMui2Staff(Staff staff)
        {
            return _staffRepository.UpdateMui2Staff(staff);
        }
        public bool UpdateMui2Staff_Temp(Staff staff)
        {
            return _staffRepository.UpdateMui2Staff_Temp(staff);
        }
        public bool UpdateThongTinKhacStaff(Staff staff)
        {
            return _staffRepository.UpdateThongTinKhacStaff(staff);
        }

        public void CreateStaff(Staff staff)
        {
            _staffRepository.CreateStaff(staff);
        }

        
    }
}