using System.Collections.Generic;
using dbChange.Models;

namespace dbChange.Repository
{
    public interface IEmployeeRepository
    {
        void AddEmployee(Employee emp);
        List<Employee> GetAllEmployees();

        List<ThongKe> GetThongKes();
      
    }
}