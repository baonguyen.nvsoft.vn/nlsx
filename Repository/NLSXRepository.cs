﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public class NLSXRepository : INLSXRepository
    {
        string connectionString = "";
        public NLSXRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public DataTable GetData_DonVi(long IdDonVi, string lsThang)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from NLSX_TAM where IdDonVi=" + IdDonVi + " or IdDonVi is null";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }

            return dt;
        }
       
        public DataTable GetData_Tong(string lsThang, int Tuan)
        {
            DataTable dt = new DataTable();
            //using (SqlConnection conn = new SqlConnection(connectionString))
            //{
            //    conn.Open();
            //    string commandText = "select *, TuanCapNhat as Tuan_1, Thang as Thang_1 from NLSX_BANG_MASTER_SOSANH";
            //    SqlCommand cmd = new SqlCommand(commandText, conn);
            //    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            //    adapter.Fill(dt);
            //}
            dt.Columns.Add("Id",typeof(long));
            dt.Columns.Add("DonVi");
            dt.Columns.Add("IdParent", typeof(long));
            dt.Columns.Add("ChungLoai");
            dt.Columns.Add("TenChungLoai");
            dt.Columns.Add("GhiChu");
            dt.Columns.Add("Color");
            dt.Columns.Add("Class");
            dt.Columns.Add("IsDetail");
            dt.Columns.Add("TuanCapNhat");
            dt.Columns.Add("NgayCapNhat",typeof(DateTime));

            string[] array_thang_nam = lsThang.Split(',');
            for(int i = 1; i <= array_thang_nam.Length; i++)
            {
                dt.Columns.Add("NguonHangBooking_PCS_" + i);
                dt.Columns.Add("NguonHangBooking_USD_" + i);
                dt.Columns.Add("NguonHangThucTe_PCS_" + i);
                dt.Columns.Add("NguonHangThucTe_USD_" + i);
                dt.Columns.Add("NangLuc_PCS_" + i);
                dt.Columns.Add("NangLuc_USD_" + i);
                dt.Columns.Add("ChenhLech_PCS_" + i);
                dt.Columns.Add("ChenhLech_USD_" + i);
                dt.Columns.Add("TuanTruoc_" + i);
                dt.Columns.Add("NguonHangBooking_PCS_SoSanh_" + i);
                dt.Columns.Add("NguonHangBooking_USD_SoSanh_" + i);
                dt.Columns.Add("NguonHangThucTe_PCS_SoSanh_" + i);
                dt.Columns.Add("NguonHangThucTe_USD_SoSanh_" + i);
                dt.Columns.Add("NangLuc_PCS_SoSanh_" + i);
                dt.Columns.Add("NangLuc_USD_SoSanh_" + i);
                dt.Columns.Add("ChenhLech_PCS_SoSanh_" + i);
                dt.Columns.Add("ChenhLech_USD_SoSanh_" + i);
                dt.Columns.Add("TuanTruoc_SoSanh_" + i);
                dt.Columns.Add("Thang_" + i);
                dt.Columns.Add("Nam_" + i);
                dt.Columns.Add("Tuan_" + i);
                if (i == 1)
                {
                    string[] thang_nam = array_thang_nam[i-1].Split('/');
                    DataTable dtTemp = GetData_Tong_Tuan_Thang(Tuan, Convert.ToInt32(thang_nam[0]), Convert.ToInt32(thang_nam[1]));
                    foreach(DataRow dr in dtTemp.Rows)
                    {
                        DataRow drNew = dt.NewRow();
                        drNew["Id"] = dr["Id"];
                        drNew["DonVi"] = dr["DonVi"];
                        drNew["IdParent"] = dr["IdParent"];
                        drNew["ChungLoai"] = dr["ChungLoai"];
                        drNew["TenChungLoai"] = dr["TenChungLoai"];
                        drNew["GhiChu"] = dr["GhiChu"];
                        drNew["Color"] = dr["Color"];
                        drNew["Class"] = dr["Class"];
                        drNew["IsDetail"] = dr["IsDetail"];
                        drNew["TuanCapNhat"] = dr["TuanCapNhat"];
                        drNew["NgayCapNhat"] = dr["NgayCapNhat"];
                        drNew["NguonHangBooking_PCS_" + i] = dr["NguonHangBooking_PCS"];
                        drNew["NguonHangBooking_USD_" + i] = dr["NguonHangBooking_USD"];
                        drNew["NguonHangThucTe_PCS_" + i] = dr["NguonHangThucTe_PCS"];
                        drNew["NguonHangThucTe_USD_" + i] = dr["NguonHangThucTe_USD"];
                        drNew["NangLuc_PCS_" + i] = dr["NangLuc_PCS"];
                        drNew["NangLuc_USD_" + i] = dr["NangLuc_USD"];
                        drNew["ChenhLech_PCS_" + i] = dr["ChenhLech_PCS"];
                        drNew["ChenhLech_USD_" + i] = dr["ChenhLech_USD"];
                        drNew["TuanTruoc_" + i] = dr["TuanTruoc"];
                        drNew["NguonHangBooking_PCS_SoSanh_" + i] = dr["NguonHangBooking_PCS_SoSanh"];
                        drNew["NguonHangBooking_USD_SoSanh_" + i] = dr["NguonHangBooking_USD_SoSanh"];
                        drNew["NguonHangThucTe_PCS_SoSanh_" + i] = dr["NguonHangThucTe_PCS_SoSanh"];
                        drNew["NguonHangThucTe_USD_SoSanh_" + i] = dr["NguonHangThucTe_USD_SoSanh"];
                        drNew["NangLuc_PCS_SoSanh_" + i] = dr["NangLuc_PCS_SoSanh"];
                        drNew["NangLuc_USD_SoSanh_" + i] = dr["NangLuc_USD_SoSanh"];
                        drNew["ChenhLech_PCS_SoSanh_" + i] = dr["ChenhLech_PCS_SoSanh"];
                        drNew["ChenhLech_USD_SoSanh_" + i] = dr["ChenhLech_USD_SoSanh"];
                        drNew["TuanTruoc_SoSanh_" + i] = dr["TuanTruoc_SoSanh"];
                        drNew["Thang_" + i] = dr["Thang"];
                        drNew["Nam_" + i] = dr["Nam"];
                        drNew["Tuan_" + i] = dr["TuanCapNhat"];
                        dt.Rows.Add(drNew);
                    }
                }
                else
                {
                    string[] thang_nam = array_thang_nam[i-1].Split('/');
                    DataTable dtTemp = GetData_Tong_Tuan_Thang(Tuan, Convert.ToInt32(thang_nam[0]), Convert.ToInt32(thang_nam[1]));
                    foreach (DataRow dr in dtTemp.Rows)
                    {
                        DataRow drNew = dt.NewRow();
                        drNew["Id"] = dr["Id"];
                        drNew["DonVi"] = dr["DonVi"];
                        drNew["IdParent"] = dr["IdParent"];
                        drNew["ChungLoai"] = dr["ChungLoai"];
                        drNew["TenChungLoai"] = dr["TenChungLoai"];
                        drNew["GhiChu"] = dr["GhiChu"];
                        drNew["Color"] = dr["Color"];
                        drNew["Class"] = dr["Class"];
                        drNew["IsDetail"] = dr["IsDetail"];
                        drNew["TuanCapNhat"] = dr["TuanCapNhat"];
                        drNew["NgayCapNhat"] = dr["NgayCapNhat"]!=null && dr["NgayCapNhat"]!= DBNull.Value ? dr["NgayCapNhat"] : Utils.Utils.GetTimeServer();
                        drNew["NguonHangBooking_PCS_" + i] = dr["NguonHangBooking_PCS"];
                        drNew["NguonHangBooking_USD_" + i] = dr["NguonHangBooking_USD"];
                        drNew["NguonHangThucTe_PCS_" + i] = dr["NguonHangThucTe_PCS"];
                        drNew["NguonHangThucTe_USD_" + i] = dr["NguonHangThucTe_USD"];
                        drNew["NangLuc_PCS_" + i] = dr["NangLuc_PCS"];
                        drNew["NangLuc_USD_" + i] = dr["NangLuc_USD"];
                        drNew["ChenhLech_PCS_" + i] = dr["ChenhLech_PCS"];
                        drNew["ChenhLech_USD_" + i] = dr["ChenhLech_USD"];
                        drNew["TuanTruoc_" + i] = dr["TuanTruoc"];
                        drNew["NguonHangBooking_PCS_SoSanh_" + i] = dr["NguonHangBooking_PCS_SoSanh"];
                        drNew["NguonHangBooking_USD_SoSanh_" + i] = dr["NguonHangBooking_USD_SoSanh"];
                        drNew["NguonHangThucTe_PCS_SoSanh_" + i] = dr["NguonHangThucTe_PCS_SoSanh"];
                        drNew["NguonHangThucTe_USD_SoSanh_" + i] = dr["NguonHangThucTe_USD_SoSanh"];
                        drNew["NangLuc_PCS_SoSanh_" + i] = dr["NangLuc_PCS_SoSanh"];
                        drNew["NangLuc_USD_SoSanh_" + i] = dr["NangLuc_USD_SoSanh"];
                        drNew["ChenhLech_PCS_SoSanh_" + i] = dr["ChenhLech_PCS_SoSanh"];
                        drNew["ChenhLech_USD_SoSanh_" + i] = dr["ChenhLech_USD_SoSanh"];
                        drNew["TuanTruoc_SoSanh_" + i] = dr["TuanTruoc_SoSanh"];
                        drNew["Thang_" + i] = dr["Thang"];
                        drNew["Nam_" + i] = dr["Nam"];
                        drNew["Tuan_" + i] = dr["TuanCapNhat"];
                        dt.Rows.Add(drNew);
                    }
                }
            }
            DataTable dtMain = dt;
            if (dt.Rows.Count > 0)
            {
                dtMain = dt.AsEnumerable()
                               .GroupBy(r => new
                               {
                                   Col1 = r["DonVi"],
                                   Col2 = r["ChungLoai"],
                                   Col3 = r["TenChungLoai"]
                               })
                               .Select(g =>
                                {
                                    var row = dt.NewRow();

                                    row["Id"] = g.Min(r => r.Field<long>("Id"));
                                    row["IdParent"] = g.Min(r => r.Field<long>("IdParent"));
                                    row["DonVi"] = g.Min(r => r.Field<string>("DonVi"));
                                    row["ChungLoai"] = g.Min(r => r.Field<string>("ChungLoai"));
                                    row["TenChungLoai"] = g.Min(r => r.Field<string>("TenChungLoai"));
                                    row["GhiChu"] = g.Min(r => r.Field<string>("GhiChu"));
                                    row["Color"] = g.Min(r => r.Field<string>("Color"));
                                    row["Class"] = g.Min(r => r.Field<string>("Class"));
                                    row["IsDetail"] = g.Min(r => r.Field<string>("IsDetail"));
                                    row["TuanCapNhat"] = g.Min(r => r.Field<string>("TuanCapNhat"));
                                //row["NgayCapNhat"] = g.Select(r=>r.Field<DateTime>("NgayCapNhat"));
                                for (int i = 1; i <= array_thang_nam.Length; i++)
                                    {
                                        row["NguonHangBooking_PCS_" + i] = g.Max(r => r.Field<string>("NguonHangBooking_PCS_" + i));
                                        row["NguonHangBooking_USD_" + i] = g.Max(r => r.Field<string>("NguonHangBooking_USD_" + i));
                                        row["NguonHangThucTe_PCS_" + i] = g.Max(r => r.Field<string>("NguonHangThucTe_PCS_" + i));
                                        row["NguonHangThucTe_USD_" + i] = g.Max(r => r.Field<string>("NguonHangThucTe_USD_" + i));
                                        row["NangLuc_PCS_" + i] = g.Max(r => r.Field<string>("NangLuc_PCS_" + i));
                                        row["NangLuc_USD_" + i] = g.Max(r => r.Field<string>("NangLuc_USD_" + i));
                                        row["ChenhLech_PCS_" + i] = g.Max(r => r.Field<string>("ChenhLech_PCS_" + i));
                                        row["ChenhLech_USD_" + i] = g.Max(r => r.Field<string>("ChenhLech_USD_" + i));
                                        row["TuanTruoc_" + i] = g.Max(r => r.Field<string>("TuanTruoc_" + i));
                                        row["NguonHangBooking_PCS_SoSanh_" + i] = g.Max(r => r.Field<string>("NguonHangBooking_PCS_SoSanh_" + i));
                                        row["NguonHangBooking_USD_SoSanh_" + i] = g.Max(r => r.Field<string>("NguonHangBooking_USD_SoSanh_" + i));
                                        row["NguonHangThucTe_PCS_SoSanh_" + i] = g.Max(r => r.Field<string>("NguonHangThucTe_PCS_SoSanh_" + i));
                                        row["NguonHangThucTe_USD_SoSanh_" + i] = g.Max(r => r.Field<string>("NguonHangThucTe_USD_SoSanh_" + i));
                                        row["NangLuc_PCS_SoSanh_" + i] = g.Max(r => r.Field<string>("NangLuc_PCS_SoSanh_" + i));
                                        row["NangLuc_USD_SoSanh_" + i] = g.Max(r => r.Field<string>("NangLuc_USD_SoSanh_" + i));
                                        row["ChenhLech_PCS_SoSanh_" + i] = g.Max(r => r.Field<string>("ChenhLech_PCS_SoSanh_" + i));
                                        row["ChenhLech_USD_SoSanh_" + i] = g.Max(r => r.Field<string>("ChenhLech_USD_SoSanh_" + i));
                                        row["TuanTruoc_SoSanh_" + i] = g.Max(r => r.Field<string>("TuanTruoc_SoSanh_" + i));
                                        row["Thang_" + i] = g.Max(r => r.Field<string>("Thang_" + i));
                                        row["Nam_" + i] = g.Max(r => r.Field<string>("Nam_" + i));
                                        row["Tuan_" + i] = g.Max(r => r.Field<string>("Tuan_" + i));
                                    }
                                    return row;

                                })
                               .CopyToDataTable();
            }
            return dtMain;
        }
        public DataTable GetData_Tong_Tuan_Thang(int Tuan, int Thang, int Nam=0)
        {
            if(Nam==0) Nam = Utils.Utils.GetTimeServer().Year;
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetData_Tong_Tuan_Thang", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Tuan", Tuan));
                cmd.Parameters.Add(new SqlParameter("@Thang", Thang));
                cmd.Parameters.Add(new SqlParameter("@Nam", Nam));
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            //using (SqlConnection conn = new SqlConnection(connectionString))
            //{
            //    conn.Open();
            //    string commandText = "select * from NLSX_BANG_MASTER_SOSANH_TEMP where TuanCapNhat=" + Tuan+" and Thang="+Thang+" and Nam=" + Nam;
            //    SqlCommand cmd = new SqlCommand(commandText, conn);
            //    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            //    adapter.Fill(dt);
            //}
            return dt;
        }
        public DataTable GetData_NguonHang_Tong(long IdRoot, string TenChungLoai, int TuanCapNhat, int Thang, string TenDonVi)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select *, isnull(KhachHang_Name,isnull(Sale_Name,GroupKhachHang_Name)) as KhachHang " +
                    " from NLSX_POPUP_CHITIET_NGUONHANG " +
                    " where TuanCapNhat=" + TuanCapNhat + " and Thang=" + Thang + " and ChungLoai_Name=N'"+TenChungLoai+ "' and isnull(TenDonVi,'')=N'" + TenDonVi + "'" +
                    " order by IdParent";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        public DataTable GetData_NguonHang_SoSanh(long IdRoot, string TenChungLoai, int TuanCapNhat, int TuanSoSanh, int Thang)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GET_SOSANH_NGUONHANG", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Tuan", TuanCapNhat));
                cmd.Parameters.Add(new SqlParameter("@TuanSoSanh", TuanSoSanh));
                cmd.Parameters.Add(new SqlParameter("@Thang", Thang));
                cmd.Parameters.Add(new SqlParameter("@TenChungLoai", TenChungLoai));
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetData_NangLuc_Tong(long IdRoot, string TenChungLoai, int TuanCapNhat, int Thang, int Nam)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SHOW_NANGLUC", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Tuan", TuanCapNhat));
                cmd.Parameters.Add(new SqlParameter("@Thang", Thang));
                cmd.Parameters.Add(new SqlParameter("@Nam", Nam));
                cmd.Parameters.Add(new SqlParameter("@TenChungLoai", TenChungLoai));
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

            //DataTable dt = new DataTable();
            //using (SqlConnection conn = new SqlConnection(connectionString))
            //{
            //    conn.Open();
            //    string commandText = "select *, isnull(Chuyen,isnull(XiNghiep_Name,DonVi_Name)) as DonVi_XN_Chuyen " +
            //        " from NLSX_POPUP_CHITIET_NANGLUC a where Tuan=" + TuanCapNhat + " and Thang=" + Thang + " and Nam="+Utils.Utils.GetTimeServer().Year+" " +
            //        " and ChungLoai_Name=N'" + TenChungLoai + "' order by IdParent,Id";
            //    SqlCommand cmd = new SqlCommand(commandText, conn);
            //    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            //    adapter.Fill(dt);
            //}
            //return dt;
        }
        public DataTable GetData_NangLuc_SoSanh(long IdRoot, string TenChungLoai, int TuanCapNhat, int TuanSoSanh, int Thang, int ThangSoSanh, int Nam, int NamSoSanh)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GET_SOSANH_NANGLUC", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Tuan", TuanCapNhat));
                cmd.Parameters.Add(new SqlParameter("@TuanSoSanh", TuanSoSanh));
                cmd.Parameters.Add(new SqlParameter("@Thang", Thang));
                cmd.Parameters.Add(new SqlParameter("@ThangSoSanh", ThangSoSanh));
                cmd.Parameters.Add(new SqlParameter("@Nam", Nam));
                cmd.Parameters.Add(new SqlParameter("@NamSoSanh", NamSoSanh));
                cmd.Parameters.Add(new SqlParameter("@TenChungLoai", TenChungLoai));
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public int GET_TUANCAPNHAT_MOINHAT()
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GET_TUANCAPNHAT_MOINHAT", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return Convert.ToInt32(dt.Rows[0]["Tuan"]);
        }
        public DataTable Get_List_Tuan_NangLuc()
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("Get_List_Tuan_NangLuc", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable Get_List_Tuan_ChenhLech()
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("Get_List_Tuan_ChenhLech", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetData_ChenhLech_Tong(long IdRoot, string TenChungLoai, int TuanCapNhat, int Thang)
        {
            int Nam = Utils.Utils.GetTimeServer().Year;
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from NLSX_POPUP_CHITIET_CHENHLECH where DonVi_Name != N'Hậu Giang' and ChungLoai_Name=N'" + TenChungLoai+"' and TuanCapNhat=" + TuanCapNhat + " and Thang=" + Thang + " and Nam=" + Nam + " and isnull(KhachHang_Name,'')!='' order by Id";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        public DataTable GetData_ChenhLech_SoSanh(long IdRoot, string TenChungLoai, int TuanCapNhat, int TuanSoSanh, int Thang, int ThangSoSanh, int Nam, int NamSoSanh)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GET_SOSANH_CHENHLECH", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Tuan", TuanCapNhat));
                cmd.Parameters.Add(new SqlParameter("@TuanSoSanh", TuanSoSanh));
                cmd.Parameters.Add(new SqlParameter("@Thang", Thang));
                cmd.Parameters.Add(new SqlParameter("@ThangSoSanh", ThangSoSanh));
                cmd.Parameters.Add(new SqlParameter("@Nam", Nam));
                cmd.Parameters.Add(new SqlParameter("@NamSoSanh", NamSoSanh));
                cmd.Parameters.Add(new SqlParameter("@TenChungLoai", TenChungLoai));
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
    }
}
