﻿using dbChange.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public interface IThongTinQuetQRRepository
    {
        public List<ThongTinQuetQR> GetThongTinQuetQRsByNgay(DateTime NgayCheckIn);
        public ThongTinQuetQR CreteThongTinQuetQRs(ThongTinQuetQR newQR);
    }
}
