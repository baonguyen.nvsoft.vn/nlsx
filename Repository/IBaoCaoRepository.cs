using System;
using System.Collections.Generic;
using System.Data;
using dbChange.Models;
using dbChange.Models.BaoCao;

namespace dbChange.Repository
{
    public interface IBaoCaoRepository
    {    
        List<ThongKeTiemChungTheoTaiKhoan> GetAll_ThongKeTiemChungTheoTaiKhoan(string TenDangNhap);

        List<ThongKeTiemChungTheoTaiKhoan_Theo_DonViTong> GetAll_ThongKeTiemChungTheoTaiKhoan_Theo_DonViTong(string TenDangNhap);

        List<ThongKeMuiTiem_Theo_DonViTong> GetAll_ThongKeMuiTiem_Theo_DonViTong(string TenDangNhap);

        List<ThongKeMuiTiem_Theo_DonViTong> GetAll_ThongKeMuiTiem_Tong_TheoTaiKhoan(string TenDangNhap);

        bool UpdateThongKe(int key,string values);
        DataTable GetDanhSachNhanVienDiLamTheoThangNam(long DonViChiTiet, long BoPhan, int Thang, int Nam);
        public DataTable GetBaoCaoDanhSachDiLamTuan(DateTime tungay, DateTime denngay, string lsDonViChiTietId, string lsBoPhanId);
        public DataTable GetBangTongHopNhanVienLamViec(DateTime tungay, DateTime denngay);
        public DataTable GetThongKePhieuDangKyRaVaoCong(DateTime tungay, DateTime denngay);
        public DataTable GetBaoCaoTang(DateTime tungay, DateTime denngay);
        public DataTable GetBaoCaoGiam(DateTime tungay, DateTime denngay);
        public DataTable GetBaoCaoThongTinYTe(DateTime tungay, DateTime denngay, string TenDonVi);
        public DataTable GetBaoCaoThongTinYTeTang(DateTime tungay, DateTime denngay, string TenDonVi);
        public DataTable GetBaoCaoRaVaoCongTheoDonVi(DateTime tungay, string TenDonVi);
        public DataTable GetBaoCaoDanhSachChuaKhaiBaoYTe(DateTime tungay, string TenDonVi);
    }
}