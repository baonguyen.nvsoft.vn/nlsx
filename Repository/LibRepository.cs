﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using dbChange.Models.LIB;
using dbChange.Models.VanPhongSo;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;

namespace dbChange.Repository{
    public class LibRepository : ILibRepository
    {
        private readonly IHubContext<SignalServer> _context;
        string connectionString = "";
        public LibRepository(IConfiguration configuration,
                                    IHubContext<SignalServer> context)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            _context = context;
        }
        public LoaiVanBan GetLoaiVanBan(long IdLoaiVanBan)
        {
            LoaiVanBan lvb = new LoaiVanBan();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from VPS_LoaiVanBan where Id=" + IdLoaiVanBan;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new LoaiVanBan
                    {
                        Id = reader["Id"] != null && reader["Id"] != DBNull.Value ? Convert.ToInt64(reader["Id"]) : 0,
                        TenLoaiVanBan = reader["TenLoaiVanBan"] != null && reader["TenLoaiVanBan"] != DBNull.Value ? reader["TenLoaiVanBan"].ToString() : ""
                    };

                    lvb = list;
                }
            }
            return lvb;
        }
        public List<LoaiVanBan> GetAllLoaiVanBan()
        {
            var lists = new List<LoaiVanBan>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from VPS_LoaiVanBan order by TenLoaiVanBan";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new LoaiVanBan
                    {
                        Id = reader["Id"]!=null && reader["Id"]!=DBNull.Value ? Convert.ToInt64(reader["Id"]) : 0,
                        TenLoaiVanBan = reader["TenLoaiVanBan"] != null && reader["TenLoaiVanBan"] != DBNull.Value ? reader["TenLoaiVanBan"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        public List<PL3_DanhMucHanhChinh> GetAllProvice()
        {
            var lists = new List<PL3_DanhMucHanhChinh>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select distinct Ma_TinhThanhPho,Ten_TinhThanhPho from PL3_DanhMucHanhChinh order by Ma_TinhThanhPho";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new PL3_DanhMucHanhChinh
                    {
                        Ma_TinhThanhPho = reader["Ma_TinhThanhPho"]!=null && reader["Ma_TinhThanhPho"]!=DBNull.Value ? reader["Ma_TinhThanhPho"].ToString() : "",
                        Ten_TinhThanhPho = reader["Ten_TinhThanhPho"] != null && reader["Ten_TinhThanhPho"] != DBNull.Value ? reader["Ten_TinhThanhPho"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        public List<ViTriNhaAn> GetAllViTriNhaAn()
        {
            var lists = new List<ViTriNhaAn>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from ViTriNhaAn";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new ViTriNhaAn
                    {
                        id = reader["id"]!=null && reader["id"] !=DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        TenViTriNhaAn = reader["TenViTriNhaAn"] != null && reader["TenViTriNhaAn"] != DBNull.Value ? reader["TenViTriNhaAn"].ToString() : ""
                    };
                    lists.Add(list);
                }
            }
            return lists;
        }
        public List<DonViChiTiet_ViTriLamViec> GetAllViTriLamViec(int IdDonViChiTiet)
        {
            var lists = new List<DonViChiTiet_ViTriLamViec>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_ViTriLamViec where IdDonViChiTiet="+ IdDonViChiTiet;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_ViTriLamViec
                    {
                        id = reader["id"]!=null && reader["id"] !=DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] !=null && reader["IdDonViChiTiet"] !=DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenViTriLamViec = reader["TenViTriLamViec"] != null && reader["TenViTriLamViec"] != DBNull.Value ? reader["TenViTriLamViec"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    lists.Add(list);
                }
            }
            return lists;
        }
        public List<DonViChiTiet_PhanNhom> GetAllPhanNhom(int IdDonViChiTiet)
        {
            var lists = new List<DonViChiTiet_PhanNhom>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_PhanNhom where IdDonViChiTiet=" + IdDonViChiTiet;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_PhanNhom
                    {
                        id = reader["id"]!=null && reader["id"] !=DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] !=null && reader["IdDonViChiTiet"] !=DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenPhanNhom = reader["TenPhanNhom"] != null && reader["TenPhanNhom"] != DBNull.Value ? reader["TenPhanNhom"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    lists.Add(list);
                }
            }
            return lists;
        }
        public List<DonViChiTiet_ViTriDeXe> GetAllViTriDeXe(int IdDonViChiTiet)
        {
            var lists = new List<DonViChiTiet_ViTriDeXe>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_ViTriDeXe where IdDonViChiTiet=" + IdDonViChiTiet;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_ViTriDeXe
                    {
                        id = reader["id"]!=null && reader["id"] !=DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] !=null && reader["IdDonViChiTiet"] !=DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenViTriDeXe = reader["TenViTriDeXe"] != null && reader["TenViTriDeXe"] != DBNull.Value ? reader["TenViTriDeXe"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    lists.Add(list);
                }
            }
            return lists;
        }
        public List<DonViChiTiet_ViTriVeSinh> GetAllViTriVeSinh(int IdDonViChiTiet)
        {
            var lists = new List<DonViChiTiet_ViTriVeSinh>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_ViTriVeSinh where IdDonViChiTiet=" + IdDonViChiTiet;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_ViTriVeSinh
                    {
                        id = reader["id"]!=null && reader["id"] !=DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] !=null && reader["IdDonViChiTiet"] !=DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenViTriVeSinh = reader["TenViTriVeSinh"] != null && reader["TenViTriVeSinh"] != DBNull.Value ? reader["TenViTriVeSinh"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    lists.Add(list);
                }
            }
            return lists;
        }
        public List<DonViChiTiet_ViTriVPLamViec> GetAllViTriVPLamViec(int IdDonViChiTiet)
        {
            var lists = new List<DonViChiTiet_ViTriVPLamViec>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_ViTriVPLamViec where IdDonViChiTiet=" + IdDonViChiTiet;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_ViTriVPLamViec
                    {
                        id = reader["id"]!=null && reader["id"] !=DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] !=null && reader["IdDonViChiTiet"] !=DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenViTriVPLamViec = reader["TenViTriVPLamViec"] != null && reader["TenViTriVPLamViec"] != DBNull.Value ? reader["TenViTriVPLamViec"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    lists.Add(list);
                }
            }
            return lists;
        }
        public List<DM_BoPhanChiTiet> GetAllDonViChiTiet()
        {
            var lists = new List<DM_BoPhanChiTiet>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "SELECT * FROM DonViChiTiet order by MaDonVi";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new DM_BoPhanChiTiet
                    {
                        Id = reader["Id"] != null && reader["Id"] != DBNull.Value ? Convert.ToInt64(reader["Id"]) : 0,
                        MaDonVi = reader["MaDonVi"] != null && reader["MaDonVi"] != DBNull.Value ? reader["MaDonVi"].ToString() : "",
                        TenDonVi = reader["TenDonVi"] != null && reader["TenDonVi"] != DBNull.Value ? reader["TenDonVi"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public DM_BoPhanChiTiet GetDonViChiTietByTen(string TenDonVi)
        {
            var result = new DM_BoPhanChiTiet();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "SELECT * FROM DonViChiTiet where TenDonVi=N'"+ TenDonVi +"'";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result = new DM_BoPhanChiTiet
                    {
                        Id = reader["Id"] != null && reader["Id"] != DBNull.Value ? Convert.ToInt32(reader["Id"]) : -1,
                        MaDonVi = reader["MaDonVi"] != null && reader["MaDonVi"] != DBNull.Value ? reader["MaDonVi"].ToString() : "",
                        TenDonVi = reader["TenDonVi"] != null && reader["TenDonVi"] != DBNull.Value ? reader["TenDonVi"].ToString() : ""
                    };
                }
            }

            return result;
        }
        public DM_BoPhanChiTiet GetDonViChiTietById(int IdDonVi)
        {
            var result = new DM_BoPhanChiTiet();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "SELECT * FROM DonViChiTiet where Id=" + IdDonVi;

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result = new DM_BoPhanChiTiet
                    {
                        Id = reader["Id"] != null && reader["Id"] != DBNull.Value ? Convert.ToInt32(reader["Id"]) : -1,
                        MaDonVi = reader["MaDonVi"] != null && reader["MaDonVi"] != DBNull.Value ? reader["MaDonVi"].ToString() : "",
                        TenDonVi = reader["TenDonVi"] != null && reader["TenDonVi"] != DBNull.Value ? reader["TenDonVi"].ToString() : ""
                    };
                }
            }

            return result;
        }

        public List<PL3_DanhMucHanhChinh> GetAllDistrictByProvice(string MaTinh)
        {
            var lists = new List<PL3_DanhMucHanhChinh>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select distinct Ma_QuanHuyen,Ten_QuanHuyen from PL3_DanhMucHanhChinh where Ma_TinhThanhPho='"+ MaTinh +"' order by Ma_QuanHuyen";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new PL3_DanhMucHanhChinh
                    {
                        Ma_QuanHuyen = reader["Ma_QuanHuyen"] !=null && reader["Ma_QuanHuyen"] !=DBNull.Value ? reader["Ma_QuanHuyen"].ToString() : "",
                        Ten_QuanHuyen = reader["Ten_QuanHuyen"] != null && reader["Ten_QuanHuyen"] != DBNull.Value ? reader["Ten_QuanHuyen"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        public List<PL3_DanhMucHanhChinh> GetAllDistrict()
        {
            var lists = new List<PL3_DanhMucHanhChinh>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select distinct Ma_QuanHuyen,Ten_QuanHuyen,Ma_TinhThanhPho from PL3_DanhMucHanhChinh order by Ma_QuanHuyen";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new PL3_DanhMucHanhChinh
                    {
                        Ma_QuanHuyen = reader["Ma_QuanHuyen"] !=null && reader["Ma_QuanHuyen"] !=DBNull.Value ? reader["Ma_QuanHuyen"].ToString() : "",
                        Ten_QuanHuyen = reader["Ten_QuanHuyen"] != null && reader["Ten_QuanHuyen"] != DBNull.Value ? reader["Ten_QuanHuyen"].ToString() : "",
                        Ma_TinhThanhPho = reader["Ma_TinhThanhPho"] != null && reader["Ma_TinhThanhPho"] != DBNull.Value ? reader["Ma_TinhThanhPho"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public List<PL3_DanhMucHanhChinh> GetAllWardByDistrict(string MaQuan)
        {
            var lists = new List<PL3_DanhMucHanhChinh>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select distinct Ma_PhuongXa,Ten_PhuongXa from PL3_DanhMucHanhChinh where Ma_QuanHuyen='" + MaQuan + "' order by Ma_PhuongXa";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new PL3_DanhMucHanhChinh
                    {
                        Ma_PhuongXa = reader["Ma_PhuongXa"] !=null && reader["Ma_PhuongXa"] !=DBNull.Value ? reader["Ma_PhuongXa"].ToString() : "",
                        Ten_PhuongXa = reader["Ten_PhuongXa"] != null && reader["Ten_PhuongXa"] != DBNull.Value ? reader["Ten_PhuongXa"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        public List<PL3_DanhMucHanhChinh> GetAllWard()
        {
            var lists = new List<PL3_DanhMucHanhChinh>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select distinct Ma_PhuongXa,Ten_PhuongXa,Ma_QuanHuyen from PL3_DanhMucHanhChinh order by Ma_PhuongXa";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new PL3_DanhMucHanhChinh
                    {
                        Ma_PhuongXa = reader["Ma_PhuongXa"] !=null && reader["Ma_PhuongXa"] !=DBNull.Value ? reader["Ma_PhuongXa"].ToString() : "",
                        Ten_PhuongXa = reader["Ten_PhuongXa"] != null && reader["Ten_PhuongXa"] != DBNull.Value ? reader["Ten_PhuongXa"].ToString() : "",
                        Ma_QuanHuyen = reader["Ma_QuanHuyen"] != null && reader["Ma_QuanHuyen"] != DBNull.Value ? reader["Ma_QuanHuyen"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public List<DM_ToKhuPho> GetAllToKhuPhoByWard(string MaPhuong)
        {
            var lists = new List<DM_ToKhuPho>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "SELECT Ma_ToKhuPho,Ten_ToKhuPho,TenKhuPho,IdWeb,MoTa FROM DM_ToKhuPho where Ma_Phuong='" + MaPhuong +"' order by Ma_ToKhuPho";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new DM_ToKhuPho
                    {
                        Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                        Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                        TenKhuPho = reader["TenKhuPho"] != null && reader["TenKhuPho"] != DBNull.Value ? reader["TenKhuPho"].ToString() : "",
                        IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",
                        MoTa = reader["MoTa"] != null && reader["MoTa"] != DBNull.Value ? reader["MoTa"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public List<PL2_NhomDoiTuong> GetAll_PL2_NhomDoiTuong()
        {
            var lists = new List<PL2_NhomDoiTuong>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from PL2_NhomDoiTuong";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new PL2_NhomDoiTuong
                    {
                        ID = Convert.ToInt32(reader["ID"]),
                        Nhom = Convert.ToInt32(reader["Nhom"]),
                        NgheNghiep = reader["NgheNghiep"].ToString(),                     
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public List<PL3_DanhMucHanhChinh> GetAll_PL3_DanhMucHanhChinh()
        {
            var lists = new List<PL3_DanhMucHanhChinh>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from PL3_DanhMucHanhChinh";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new PL3_DanhMucHanhChinh
                    {
                        ID = Convert.ToInt32(reader["ID"]),
                        Ma_PhuongXa = reader["Ma_PhuongXa"].ToString(),
                        Ma_QuanHuyen = reader["Ma_QuanHuyen"].ToString(),
                        Ma_TinhThanhPho = reader["Ma_TinhThanhPho"].ToString(),
                        Ten_PhuongXa = reader["Ten_PhuongXa"].ToString(),
                        Ten_QuanHuyen = reader["Ten_QuanHuyen"].ToString(),
                        Ten_TinhThanhPho = reader["Ten_TinhThanhPho"].ToString(),
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public List<PL4_DiaDiemTiem> GetAll_PL4_DiaDiemTiem()
        {
            var lists = new List<PL4_DiaDiemTiem>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from PL4_DiaDiemTiem";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new PL4_DiaDiemTiem
                    {
                        ID = Convert.ToInt32(reader["ID"]),
                        Ma_CoSo = reader["Ma_CoSo"].ToString(),
                        Ten_CoSo = reader["Ten_CoSo"].ToString(),
                        TinhThanhPho = reader["TinhThanhPho"].ToString(),
                        QuanHuyen = reader["QuanHuyen"].ToString(),
                        XaPhuong = reader["XaPhuong"].ToString(),
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        
       

        public List<PL5_DanhMucVacxin> GetAll_PL5_DanhMucVacxin()
        {
            var lists = new List<PL5_DanhMucVacxin>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from PL5_DanhMucVacxin";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new PL5_DanhMucVacxin
                    {
                        ID = Convert.ToInt32(reader["ID"]),
                        TenVacxin = reader["TenVacxin"].ToString(),
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        private void dbChangeNotification(object sender, SqlNotificationEventArgs e)
        {
            Console.WriteLine(e.Info);
          //  _context.Clients.All.SendAsync("refreshEmployees");
        }

        public List<DonViChiTiet_BoPhan> GetAllDonViChiTietBoPhanByDonViChiTietId(int DonViChiTietId)
        {
            var lists = new List<DonViChiTiet_BoPhan>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from DonViChiTiet_BoPhan where DonViChiTietId=" + DonViChiTietId;

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new DonViChiTiet_BoPhan
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MaBoPhan = reader["MaBoPhan"]!=null && reader["MaBoPhan"]!=DBNull.Value ? reader["MaBoPhan"].ToString() : "",
                        TenBoPhan = reader["TenBoPhan"] != null && reader["TenBoPhan"] != DBNull.Value ? reader["TenBoPhan"].ToString() : "",
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        
        public List<DonViChiTiet_BoPhan> GetAllBoPhan()
        {
            var lists = new List<DonViChiTiet_BoPhan>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from DonViChiTiet_BoPhan ";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new DonViChiTiet_BoPhan
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MaBoPhan = reader["MaBoPhan"]!=null && reader["MaBoPhan"]!=DBNull.Value ? reader["MaBoPhan"].ToString() : "",
                        TenBoPhan = reader["TenBoPhan"] != null && reader["TenBoPhan"] != DBNull.Value ? reader["TenBoPhan"].ToString() : "",
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public List<DM_BoPhanChiTiet> GetAllDonViChiTietByUser(string UserName)
        {
            var lists = new List<DM_BoPhanChiTiet>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from Account_DonViChiTiet  where TenDangNhap='"+ UserName +"'";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new DM_BoPhanChiTiet
                    {
                        Id = reader["DonViChiTietId"] != null && reader["DonViChiTietId"] != DBNull.Value ? Convert.ToInt64(reader["DonViChiTietId"]) : -1,
                        TenDonVi = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public DonViChiTiet_ViTriVeSinh GetViTriVeSinhByName(string name, int IdDonVi)
        {
            var lists = new DonViChiTiet_ViTriVeSinh();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_ViTriVeSinh where TenViTriVeSinh=N'" + name + "' and IdDonViChiTiet=" + IdDonVi;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_ViTriVeSinh
                    {
                        id = reader["id"] != null && reader["id"] != DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] != null && reader["IdDonViChiTiet"] != DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenViTriVeSinh = reader["TenViTriVeSinh"] != null && reader["TenViTriVeSinh"] != DBNull.Value ? reader["TenViTriVeSinh"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    return list;
                }
            }
            return lists;
        }

        public DonViChiTiet_ViTriVPLamViec GetViTriVPLamViecByName(string name, int IdDonVi)
        {
            var lists = new DonViChiTiet_ViTriVPLamViec();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_ViTriVPLamViec where TenViTriVPLamviec=N'" + name + "' and IdDonViChiTiet=" + IdDonVi;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_ViTriVPLamViec
                    {
                        id = reader["id"] != null && reader["id"] != DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] != null && reader["IdDonViChiTiet"] != DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenViTriVPLamViec = reader["TenViTriVPLamViec"] != null && reader["TenViTriVPLamViec"] != DBNull.Value ? reader["TenViTriVPLamViec"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    return list;
                }
            }
            return lists;
        }

        public DonViChiTiet_ViTriLamViec GetViTriLamViecByName(string name, int IdDonVi)
        {
            var lists = new DonViChiTiet_ViTriLamViec();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_ViTriLamViec where TenViTriLamviec=N'" + name + "' and IdDonViChiTiet=" + IdDonVi;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_ViTriLamViec
                    {
                        id = reader["id"] != null && reader["id"] != DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] != null && reader["IdDonViChiTiet"] != DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenViTriLamViec = reader["TenViTriLamViec"] != null && reader["TenViTriLamViec"] != DBNull.Value ? reader["TenViTriLamViec"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    return list;
                }
            }
            return lists;
        }

        public ViTriNhaAn GetViTriNhaAnByName(string name)
        {
            var lists = new ViTriNhaAn();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from ViTriNhaAn where TenViTriNhaAn=N'" + name + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new ViTriNhaAn
                    {
                        id = reader["id"] != null && reader["id"] != DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        TenViTriNhaAn = reader["TenViTriNhaAn"] != null && reader["TenViTriNhaAn"] != DBNull.Value ? reader["TenViTriNhaAn"].ToString() : ""
                    };
                    return list;
                }
            }
            return lists;
        }

        public DonViChiTiet_PhanNhom GetPhanNhomByName(string name, int IdDonVi)
        {
            var lists = new DonViChiTiet_PhanNhom();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_PhanNhom where TenPhanNhom=N'" + name + "' and IdDonViChiTiet=" + IdDonVi;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_PhanNhom
                    {
                        id = reader["id"] != null && reader["id"] != DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] != null && reader["IdDonViChiTiet"] != DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenPhanNhom = reader["TenPhanNhom"] != null && reader["TenPhanNhom"] != DBNull.Value ? reader["TenPhanNhom"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    return list;
                }
            }
            return lists;
        }

        public DonViChiTiet_ViTriDeXe GetViTriDeXeByName(string name, int IdDonVi)
        {
            var lists = new DonViChiTiet_ViTriDeXe();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from DonViChiTiet_ViTriDeXe where TenViTriDeXe=N'" + name + "' and IdDonViChiTiet=" + IdDonVi;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new DonViChiTiet_ViTriDeXe
                    {
                        id = reader["id"] != null && reader["id"] != DBNull.Value ? Convert.ToInt64(reader["id"]) : 0,
                        IdDonViChiTiet = reader["IdDonViChiTiet"] != null && reader["IdDonViChiTiet"] != DBNull.Value ? Convert.ToInt32(reader["IdDonViChiTiet"]) : 0,
                        TenViTriDeXe = reader["TenViTriDeXe"] != null && reader["TenViTriDeXe"] != DBNull.Value ? reader["TenViTriDeXe"].ToString() : "",
                        TenDonViChiTiet = reader["TenDonViChiTiet"] != null && reader["TenDonViChiTiet"] != DBNull.Value ? reader["TenDonViChiTiet"].ToString() : ""
                    };
                    return list;
                }
            }
            return lists;
        }

        public List<VanBan_Quyen> GetAllQuyenVanBan()
        {
            var lists = new List<VanBan_Quyen>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from VPS_VanBan_Quyen order by STT";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new VanBan_Quyen
                    {
                        Id = reader["Id"] != null && reader["Id"] != DBNull.Value ? Convert.ToInt64(reader["Id"]) : 0,
                        MaQuyenVanBan = reader["MaQuyenVanBan"] != null && reader["MaQuyenVanBan"] != DBNull.Value ? reader["MaQuyenVanBan"].ToString() : "",
                        TenQuyenVanBan = reader["TenQuyenVanBan"] != null && reader["TenQuyenVanBan"] != DBNull.Value ? reader["TenQuyenVanBan"].ToString() : "",
                        IsActive = reader["IsActive"] != null && reader["IsActive"] != DBNull.Value ? Convert.ToBoolean(reader["IsActive"].ToString()) : false,
                    };
                    lists.Add(list);
                }
            }
            return lists;
        }
        public DataTable GetDanhSachDonViPhanQuyen(string TenDangNhap)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * ,(select count(*) from account_donvichitiet where DonViChiTietId = donvichitiet.Id and TenDangNhap = '"+TenDangNhap+ "' and isEnable=1) as isCheck " +
                    "from donvichitiet order by tendonvi";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
    }
}