﻿using dbChange.Models;
using dbChange.Models.LIB;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public class StaffRepository: IStaffRepository
    {

        private readonly IHubContext<SignalServer> _context;
        string connectionString = "";
        LibRepository lib;
        public StaffRepository(IConfiguration configuration,
                                    IHubContext<SignalServer> context)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            _context = context;
            lib = new LibRepository(configuration, context);
        }
        public List<Staff> GetAllStaffs(string account)
        {
            var staffs = new List<Staff>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select Staff.* from Staff " +
                    "inner join Account_DonViChiTiet " +
                    "on Account_DonViChiTiet.DonViChiTiet  =  Staff.DonViChiTiet  " +
                    " and Account_DonViChiTiet.TenDangNhap = '" + account+"' "+
                    "order by Staff.Id";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var staff = new Staff
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        HoTen = reader["HoTen"].ToString(),
                        NgaySinh = reader["NgaySinh"] != null && reader["NgaySinh"] != DBNull.Value ? reader["NgaySinh"].ToString() : "",
                        NgayS = reader["NgayS"] != null && reader["NgayS"] != DBNull.Value ? Convert.ToInt32(reader["NgayS"].ToString()) : 1,
                        ThangS = reader["ThangS"] != null && reader["ThangS"] != DBNull.Value ? Convert.ToInt32(reader["ThangS"].ToString()) : 1,
                        NamS = reader["NamS"] != null && reader["NamS"] != DBNull.Value ? Convert.ToInt32(reader["NamS"].ToString()) : 1900,
                        GioiTinh = reader["GioiTinh"] != null && reader["GioiTinh"] != DBNull.Value ? reader["GioiTinh"].ToString() : "",
                        MaNhom = reader["MaNhom"] != null && reader["MaNhom"] != DBNull.Value ? reader["MaNhom"].ToString() : "",
                        DonVi = reader["DonVi"] != null && reader["DonVi"] != DBNull.Value ? reader["DonVi"].ToString() : "",
                        DienThoai = reader["DienThoai"] != null && reader["DienThoai"] != DBNull.Value ? reader["DienThoai"].ToString() : "",
                        CMND = reader["CMND"] != null && reader["CMND"] != DBNull.Value ? reader["CMND"].ToString() : "",
                        CCCD = reader["CCCD"] != null && reader["CCCD"] != DBNull.Value ? reader["CCCD"].ToString() : "",
                        BHYT = reader["BHYT"] != null && reader["BHYT"] != DBNull.Value ? reader["BHYT"].ToString() : "",
                        TenTinh = reader["TenTinh"] != null && reader["TenTinh"] != DBNull.Value ? reader["TenTinh"].ToString() : "",
                        MaTinh = reader["MaTinh"] != null && reader["MaTinh"] != DBNull.Value ? reader["MaTinh"].ToString() : "",
                        TenQuan = reader["TenQuan"] != null && reader["TenQuan"] != DBNull.Value ? reader["TenQuan"].ToString() : "",
                        MaQuan = reader["MaQuan"] != null && reader["MaQuan"] != DBNull.Value ? reader["MaQuan"].ToString() : "",
                        TenPhuong = reader["TenPhuong"] != null && reader["TenPhuong"] != DBNull.Value ? reader["TenPhuong"].ToString() : "",
                        MaPhuong = reader["MaPhuong"] != null && reader["MaPhuong"] != DBNull.Value ? reader["MaPhuong"].ToString() : "",
                        DiaChi = reader["DiaChi"] != null && reader["DiaChi"] != DBNull.Value ? reader["DiaChi"].ToString() : "",
                        Mui1_TenVacxin = reader["Mui1_TenVacxin"] != null && reader["Mui1_TenVacxin"] != DBNull.Value ? reader["Mui1_TenVacxin"].ToString() : "",
                        Mui1_NgayTiem = reader["Mui1_NgayTiem"] != null && reader["Mui1_NgayTiem"] != DBNull.Value ? reader["Mui1_NgayTiem"].ToString() : "",
                        Mui1_SoLo = reader["Mui1_SoLo"] != null && reader["Mui1_SoLo"] != DBNull.Value ? reader["Mui1_SoLo"].ToString() : "",
                        Mui1_DiaDiem = reader["Mui1_DiaDiem"] != null && reader["Mui1_DiaDiem"] != DBNull.Value ? reader["Mui1_DiaDiem"].ToString() : "",
                        Mui2_TenVacxin = reader["Mui2_TenVacxin"] != null && reader["Mui2_TenVacxin"] != DBNull.Value ? reader["Mui2_TenVacxin"].ToString() : "",
                        Mui2_NgayTiem = reader["Mui2_NgayTiem"] != null && reader["Mui2_NgayTiem"] != DBNull.Value ? reader["Mui2_NgayTiem"].ToString() : "",
                        Mui2_SoLo = reader["Mui2_SoLo"] != null && reader["Mui2_SoLo"] != DBNull.Value ? reader["Mui2_SoLo"].ToString() : "",
                        Mui2_DiaDiem = reader["Mui2_DiaDiem"] != null && reader["Mui2_DiaDiem"] != DBNull.Value ? reader["Mui2_DiaDiem"].ToString() : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? reader["GhiChu"].ToString() : "",
                        KetQuaImPort = reader["KetQuaImPort"] != null && reader["KetQuaImPort"] != DBNull.Value ? reader["KetQuaImPort"].ToString() : "",
                        DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : "",
                        NgayCapNhat = reader["NgayCapNhat"] != null && reader["NgayCapNhat"] != DBNull.Value ? reader["NgayCapNhat"].ToString() : "",
                        NguoiCapNhat = reader["NguoiCapNhat"] != null && reader["NguoiCapNhat"] != DBNull.Value ? reader["NguoiCapNhat"].ToString() : "",
                        UrlHinhGiayXN = reader["UrlHinhGiayXN"] != null && reader["UrlHinhGiayXN"] != DBNull.Value ? reader["UrlHinhGiayXN"].ToString() : "",
                        PhanNhom = reader["PhanNhom"] != null && reader["PhanNhom"] != DBNull.Value ? reader["PhanNhom"].ToString() : "",
                        TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? reader["TinhTrang"].ToString() : "",
                        BiF0 = reader["BiF0"] != null && reader["BiF0"] != DBNull.Value ? Convert.ToBoolean(reader["BiF0"]) : false,
                        NgayXuatVien = reader["NgayXuatVien"] != null && reader["NgayXuatVien"] != DBNull.Value ? reader["NgayXuatVien"].ToString() : "",
                        ISok = reader["ISok"] != null && reader["ISok"] != DBNull.Value ? Convert.ToBoolean(reader["ISok"]) : true,
                        Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                        Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                        VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString() : "chưa có tổ dân phố",
                        BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]) : -1,
                        IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? reader["MaQR"].ToString() : "",


                    };

                    staffs.Add(staff);
                }
            }

            return staffs;
        }
        public Dictionary<string,int> GetDataVungCovidChart(string TenDangNhap, string DonVi, string BoPhan)
        {
            var staffs = new Dictionary<string, int>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "";


                if(DonVi == "All")
                {
                    commandText = "SELECT count(isnull(VungCovid,'')) as SoLuong, isnull(VungCovid,'') as VungCovid " +
                        " FROM Staff a " +
                        " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                        " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                        " where c.TenDangNhap = '" + TenDangNhap + "' " +
                        " group by VungCovid order by SoLuong desc";
                }
                else
                {
                    if (BoPhan == "All")
                    {
                        commandText = "SELECT count(isnull(VungCovid,'')) as SoLuong, isnull(VungCovid,'') as VungCovid " +
                        " FROM Staff a " +
                        " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                        " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                        " where c.TenDangNhap = '" + TenDangNhap + "' and a.DonViChiTiet=N'" + DonVi + "'" +
                        " group by VungCovid order by SoLuong desc";
                    }
                    else
                    {
                        commandText = "SELECT count(isnull(VungCovid,'')) as SoLuong, isnull(VungCovid,'') as VungCovid " +
                        " FROM Staff a " +
                        " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                        " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                        " where c.TenDangNhap = '" + TenDangNhap + "' and  a.DonViChiTiet=N'" + DonVi + "' and a.BoPhanId=" + Convert.ToInt32(BoPhan) +
                        " group by VungCovid order by SoLuong desc";
                    }
                }
                

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int SoLuong = reader["SoLuong"] != null && reader["SoLuong"] != DBNull.Value ? Convert.ToInt32(reader["SoLuong"]) : 0;
                    string VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString()!="" ? reader["VungCovid"].ToString() : "chưa có tổ dân phố" : "chưa có tổ dân phố";
                    if(!staffs.ContainsKey(VungCovid)) staffs.Add(VungCovid, SoLuong);
                }
            }

            return staffs;
        }
        public Dictionary<string,int> GetDataVungCovidChart2(string TenDangNhap, int[] DonVi, string BoPhan)
        {
            var staffs = new Dictionary<string, int>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "";


                if(DonVi.Length==0)
                {
                    commandText = "select sum(SoLuong) as SoLuong,VungCovid from (SELECT count(isnull(VungCovid,'')) as SoLuong, isnull(VungCovid,'') as VungCovid " +
                        " FROM Staff a " +
                        " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                        " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                        " where c.TenDangNhap = '" + TenDangNhap + "' " +
                        " group by VungCovid ) a group by VungCovid order by SoLuong desc";
                }
                else
                {
                    string sqlDonVi = "";
                    int i = 0;
                    foreach (int a in DonVi)
                    {
                        if (i == 0) sqlDonVi = " b.Id= " + a;
                        else
                            sqlDonVi += " or b.Id= " + a;
                        i++;
                    }
                    if (BoPhan == "All")
                    {
                        commandText = "select sum(SoLuong) as SoLuong,VungCovid from (SELECT count(isnull(VungCovid,'')) as SoLuong, isnull(VungCovid,'') as VungCovid " +
                        " FROM Staff a " +
                        " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                        " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                        " where c.TenDangNhap = '" + TenDangNhap + "' and ( "+ sqlDonVi + " )" +
                        " group by VungCovid ) a group by VungCovid order by SoLuong desc";
                    }
                    else
                    {
                        commandText = "select sum(SoLuong) as SoLuong,VungCovid from (SELECT count(isnull(VungCovid,'')) as SoLuong, isnull(VungCovid,'') as VungCovid " +
                        " FROM Staff a " +
                        " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                        " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                        " where c.TenDangNhap = '" + TenDangNhap + "' and  ("+ sqlDonVi + ") and a.BoPhanId=" + Convert.ToInt32(BoPhan) +
                        " group by VungCovid ) a group by VungCovid order by SoLuong desc";
                    }
                }
                

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int SoLuong = reader["SoLuong"] != null && reader["SoLuong"] != DBNull.Value ? Convert.ToInt32(reader["SoLuong"]) : 0;
                    string VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString()!="" ? reader["VungCovid"].ToString() : "chưa có tổ dân phố" : "chưa có tổ dân phố";
                    staffs.Add(VungCovid, SoLuong);
                }
            }

            return staffs;
        }
        public List<Staff> GetAllStaffByDonViBoPhan(string TenDangNhap, string DonVi, string BoPhan)
        {
            var staffs = new List<Staff>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "";

                if(DonVi == "All")
                {
                    commandText = "SELECT ROW_NUMBER() OVER(ORDER BY a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho) as STT,a.*,d.TenBoPhan" +
                        ",case when a.Ma_ToKhuPho is null then 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Ward=' + MaPhuong else 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Square=' + a.Ma_ToKhuPho + '&Ward=' + MaPhuong end as LinkWeb " +
                        " ,e.MoTa FROM Staff a " +
                            " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                            " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                            " left join DonViChiTiet_BoPhan d on d.Id = a.BoPhanId " +
                            " left join DM_ToKhuPho e on e.IdWeb=a.IdWeb " +
                            " where c.TenDangNhap = '" + TenDangNhap + "'" +
                            " order by a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho";
                }
                else
                {
                    if (BoPhan == "All")
                    {
                        commandText = "SELECT ROW_NUMBER() OVER(ORDER BY a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho) as STT, a.*,d.TenBoPhan " +
                            ",case when a.Ma_ToKhuPho is null then 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Ward=' + MaPhuong else 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Square=' + a.Ma_ToKhuPho + '&Ward=' + MaPhuong end as LinkWeb " +
                            " ,e.MoTa FROM Staff a " +
                            " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                            " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                            " left join DonViChiTiet_BoPhan d on d.Id = a.BoPhanId " +
                            " left join DM_ToKhuPho e on e.IdWeb=a.IdWeb " +
                            " where c.TenDangNhap = '" + TenDangNhap + "' and a.DonViChiTiet=N'" + DonVi + "'" +
                            " order by a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho";
                    }
                    else
                    {
                        commandText = "SELECT ROW_NUMBER() OVER(ORDER BY a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho) as STT, a.*,d.TenBoPhan " +
                            ",case when a.Ma_ToKhuPho is null then 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Ward=' + MaPhuong else 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Square=' + a.Ma_ToKhuPho + '&Ward=' + MaPhuong end as LinkWeb " +
                            " ,e.MoTa FROM Staff a " +
                            " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                            " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                            " left join DonViChiTiet_BoPhan d on d.Id = a.BoPhanId " +
                            " left join DM_ToKhuPho e on e.IdWeb=a.IdWeb " +
                            " where c.TenDangNhap = '" + TenDangNhap + "' and  a.DonViChiTiet=N'"+ DonVi +"' and a.BoPhanId=" + Convert.ToInt32(BoPhan) +
                            " order by a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho";
                    }
                }
                

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var staff = new Staff
                    {
                        Id = Convert.ToInt32(reader["STT"]),
                        HoTen = reader["HoTen"].ToString(),
                        NgaySinh = reader["NgaySinh"] != null && reader["NgaySinh"] != DBNull.Value ? reader["NgaySinh"].ToString() : "",
                        NgayS = reader["NgayS"] != null && reader["NgayS"] != DBNull.Value ? Convert.ToInt32(reader["NgayS"].ToString()) : 1,
                        ThangS = reader["ThangS"] != null && reader["ThangS"] != DBNull.Value ? Convert.ToInt32(reader["ThangS"].ToString()) : 1,
                        NamS = reader["NamS"] != null && reader["NamS"] != DBNull.Value ? Convert.ToInt32(reader["NamS"].ToString()) : 1900,
                        GioiTinh = reader["GioiTinh"] != null && reader["GioiTinh"] != DBNull.Value ? reader["GioiTinh"].ToString() : "",
                        MaNhom = reader["MaNhom"] != null && reader["MaNhom"] != DBNull.Value ? reader["MaNhom"].ToString() : "",
                        DonVi = reader["DonVi"] != null && reader["DonVi"] != DBNull.Value ? reader["DonVi"].ToString() : "",
                        DienThoai = reader["DienThoai"] != null && reader["DienThoai"] != DBNull.Value ? reader["DienThoai"].ToString() : "",
                        CMND = reader["CMND"] != null && reader["CMND"] != DBNull.Value ? reader["CMND"].ToString() : "",
                        CCCD = reader["CCCD"] != null && reader["CCCD"] != DBNull.Value ? reader["CCCD"].ToString() : "",
                        BHYT = reader["BHYT"] != null && reader["BHYT"] != DBNull.Value ? reader["BHYT"].ToString() : "",
                        TenTinh = reader["TenTinh"] != null && reader["TenTinh"] != DBNull.Value ? reader["TenTinh"].ToString() : "",
                        MaTinh = reader["MaTinh"] != null && reader["MaTinh"] != DBNull.Value ? reader["MaTinh"].ToString() : "",
                        TenQuan = reader["TenQuan"] != null && reader["TenQuan"] != DBNull.Value ? reader["TenQuan"].ToString() : "",
                        MaQuan = reader["MaQuan"] != null && reader["MaQuan"] != DBNull.Value ? reader["MaQuan"].ToString() : "",
                        TenPhuong = reader["TenPhuong"] != null && reader["TenPhuong"] != DBNull.Value ? reader["TenPhuong"].ToString() : "",
                        MaPhuong = reader["MaPhuong"] != null && reader["MaPhuong"] != DBNull.Value ? reader["MaPhuong"].ToString() : "",
                        DiaChi = reader["DiaChi"] != null && reader["DiaChi"] != DBNull.Value ? reader["DiaChi"].ToString() : "",
                        Mui1_TenVacxin = reader["Mui1_TenVacxin"] != null && reader["Mui1_TenVacxin"] != DBNull.Value ? reader["Mui1_TenVacxin"].ToString() : "",
                        Mui1_NgayTiem = reader["Mui1_NgayTiem"] != null && reader["Mui1_NgayTiem"] != DBNull.Value ? reader["Mui1_NgayTiem"].ToString() : "",
                        Mui1_SoLo = reader["Mui1_SoLo"] != null && reader["Mui1_SoLo"] != DBNull.Value ? reader["Mui1_SoLo"].ToString() : "",
                        Mui1_DiaDiem = reader["Mui1_DiaDiem"] != null && reader["Mui1_DiaDiem"] != DBNull.Value ? reader["Mui1_DiaDiem"].ToString() : "",
                        Mui2_TenVacxin = reader["Mui2_TenVacxin"] != null && reader["Mui2_TenVacxin"] != DBNull.Value ? reader["Mui2_TenVacxin"].ToString() : "",
                        Mui2_NgayTiem = reader["Mui2_NgayTiem"] != null && reader["Mui2_NgayTiem"] != DBNull.Value ? reader["Mui2_NgayTiem"].ToString() : "",
                        Mui2_SoLo = reader["Mui2_SoLo"] != null && reader["Mui2_SoLo"] != DBNull.Value ? reader["Mui2_SoLo"].ToString() : "",
                        Mui2_DiaDiem = reader["Mui2_DiaDiem"] != null && reader["Mui2_DiaDiem"] != DBNull.Value ? reader["Mui2_DiaDiem"].ToString() : "",
                        GhiChu = reader["MoTa"] != null && reader["MoTa"] != DBNull.Value ? reader["MoTa"].ToString() : "",
                        KetQuaImPort = reader["KetQuaImPort"] != null && reader["KetQuaImPort"] != DBNull.Value ? reader["KetQuaImPort"].ToString() : "",
                        DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : "",
                        NgayCapNhat = reader["NgayCapNhat"] != null && reader["NgayCapNhat"] != DBNull.Value ? reader["NgayCapNhat"].ToString() : "",
                        NguoiCapNhat = reader["NguoiCapNhat"] != null && reader["NguoiCapNhat"] != DBNull.Value ? reader["NguoiCapNhat"].ToString() : "",
                        UrlHinhGiayXN = reader["UrlHinhGiayXN"] != null && reader["UrlHinhGiayXN"] != DBNull.Value ? reader["UrlHinhGiayXN"].ToString() : "",
                        PhanNhom = reader["PhanNhom"] != null && reader["PhanNhom"] != DBNull.Value ? reader["PhanNhom"].ToString() : "",
                        TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? reader["TinhTrang"].ToString() : "",
                        BiF0 = reader["BiF0"] != null && reader["BiF0"] != DBNull.Value ? Convert.ToBoolean(reader["BiF0"]) : false,
                        NgayXuatVien = reader["NgayXuatVien"] != null && reader["NgayXuatVien"] != DBNull.Value ? reader["NgayXuatVien"].ToString() : "",
                        ISok = reader["ISok"] != null && reader["ISok"] != DBNull.Value ? Convert.ToBoolean(reader["ISok"]) : true,
                        Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                        Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                        VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString() : "chưa có tổ dân phố",
                        BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]) : -1,
                        TenBoPhan = reader["TenBoPhan"] != null && reader["TenBoPhan"] != DBNull.Value ? reader["TenBoPhan"].ToString() : "",
                        IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",
                        LinkWeb = reader["LinkWeb"] != null && reader["LinkWeb"] != DBNull.Value ? reader["LinkWeb"].ToString() : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? reader["MaQR"].ToString() : "",


                    };

                    staffs.Add(staff);
                }
            }

            return staffs;
        }
        public List<Staff> GetAllStaffByDonViBoPhan2(string TenDangNhap, int[] DonVi, string BoPhan)
        {
            var staffs = new List<Staff>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "";

                if(DonVi.Length == 0)
                {
                    commandText = "SELECT ROW_NUMBER() OVER(ORDER BY a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho) as STT,a.*,d.TenBoPhan" +
                        ",case when a.Ma_ToKhuPho is null then 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Ward=' + MaPhuong else 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Square=' + a.Ma_ToKhuPho + '&Ward=' + MaPhuong end as LinkWeb " +
                        " ,e.MoTa FROM Staff a " +
                            " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                            " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                            " left join DonViChiTiet_BoPhan d on d.Id = a.BoPhanId " +
                            " left join DM_ToKhuPho e on e.IdWeb=a.IdWeb " +
                            " where c.TenDangNhap = '" + TenDangNhap + "'" +
                            " order by a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho";
                }
                else
                {
                    string sqlDonVi = "";
                    int i = 0;
                    foreach (int a in DonVi)
                    {
                        if (i == 0) sqlDonVi = " b.Id= " + a;
                        else
                            sqlDonVi += " or b.Id= " + a;
                        i++;
                    }
                    if (BoPhan == "All")
                    {
                        commandText = "SELECT ROW_NUMBER() OVER(ORDER BY a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho) as STT, a.*,d.TenBoPhan " +
                            ",case when a.Ma_ToKhuPho is null then 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Ward=' + MaPhuong else 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Square=' + a.Ma_ToKhuPho + '&Ward=' + MaPhuong end as LinkWeb " +
                            " ,e.MoTa FROM Staff a " +
                            " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                            " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                            " left join DonViChiTiet_BoPhan d on d.Id = a.BoPhanId " +
                            " left join DM_ToKhuPho e on e.IdWeb=a.IdWeb " +
                            " where c.TenDangNhap = '" + TenDangNhap + "' and ("+sqlDonVi+") " +
                            " order by a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho";
                    }
                    else
                    {
                        commandText = "SELECT ROW_NUMBER() OVER(ORDER BY a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho) as STT, a.*,d.TenBoPhan " +
                            ",case when a.Ma_ToKhuPho is null then 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Ward=' + MaPhuong else 'https://bando.tphcm.gov.vn/ogis/thongke?District=' + MaQuan + '&Province=' + MaTinh + '&Square=' + a.Ma_ToKhuPho + '&Ward=' + MaPhuong end as LinkWeb " +
                            " ,e.MoTa FROM Staff a " +
                            " inner join DonViChiTiet b on a.DonViChiTiet = b.TenDonVi " +
                            " inner join Account_DonViChiTiet c on c.DonViChiTietId = b.Id " +
                            " left join DonViChiTiet_BoPhan d on d.Id = a.BoPhanId " +
                            " left join DM_ToKhuPho e on e.IdWeb=a.IdWeb " +
                            " where c.TenDangNhap = '" + TenDangNhap + "' and  ("+sqlDonVi+") and a.BoPhanId=" + Convert.ToInt32(BoPhan) +
                            " order by a.VungCovid,a.DonViChiTiet,d.TenBoPhan, a.MaTinh,a.MaQuan,a.MaPhuong,a.Ma_ToKhuPho";
                    }
                }
                

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var staff = new Staff
                    {
                        Id = Convert.ToInt32(reader["STT"]),
                        HoTen = reader["HoTen"].ToString(),
                        NgaySinh = reader["NgaySinh"] != null && reader["NgaySinh"] != DBNull.Value ? reader["NgaySinh"].ToString() : "",
                        NgayS = reader["NgayS"] != null && reader["NgayS"] != DBNull.Value ? Convert.ToInt32(reader["NgayS"].ToString()) : 1,
                        ThangS = reader["ThangS"] != null && reader["ThangS"] != DBNull.Value ? Convert.ToInt32(reader["ThangS"].ToString()) : 1,
                        NamS = reader["NamS"] != null && reader["NamS"] != DBNull.Value ? Convert.ToInt32(reader["NamS"].ToString()) : 1900,
                        GioiTinh = reader["GioiTinh"] != null && reader["GioiTinh"] != DBNull.Value ? reader["GioiTinh"].ToString() : "",
                        MaNhom = reader["MaNhom"] != null && reader["MaNhom"] != DBNull.Value ? reader["MaNhom"].ToString() : "",
                        DonVi = reader["DonVi"] != null && reader["DonVi"] != DBNull.Value ? reader["DonVi"].ToString() : "",
                        DienThoai = reader["DienThoai"] != null && reader["DienThoai"] != DBNull.Value ? reader["DienThoai"].ToString() : "",
                        CMND = reader["CMND"] != null && reader["CMND"] != DBNull.Value ? reader["CMND"].ToString() : "",
                        CCCD = reader["CCCD"] != null && reader["CCCD"] != DBNull.Value ? reader["CCCD"].ToString() : "",
                        BHYT = reader["BHYT"] != null && reader["BHYT"] != DBNull.Value ? reader["BHYT"].ToString() : "",
                        TenTinh = reader["TenTinh"] != null && reader["TenTinh"] != DBNull.Value ? reader["TenTinh"].ToString() : "",
                        MaTinh = reader["MaTinh"] != null && reader["MaTinh"] != DBNull.Value ? reader["MaTinh"].ToString() : "",
                        TenQuan = reader["TenQuan"] != null && reader["TenQuan"] != DBNull.Value ? reader["TenQuan"].ToString() : "",
                        MaQuan = reader["MaQuan"] != null && reader["MaQuan"] != DBNull.Value ? reader["MaQuan"].ToString() : "",
                        TenPhuong = reader["TenPhuong"] != null && reader["TenPhuong"] != DBNull.Value ? reader["TenPhuong"].ToString() : "",
                        MaPhuong = reader["MaPhuong"] != null && reader["MaPhuong"] != DBNull.Value ? reader["MaPhuong"].ToString() : "",
                        DiaChi = reader["DiaChi"] != null && reader["DiaChi"] != DBNull.Value ? reader["DiaChi"].ToString() : "",
                        Mui1_TenVacxin = reader["Mui1_TenVacxin"] != null && reader["Mui1_TenVacxin"] != DBNull.Value ? reader["Mui1_TenVacxin"].ToString() : "",
                        Mui1_NgayTiem = reader["Mui1_NgayTiem"] != null && reader["Mui1_NgayTiem"] != DBNull.Value ? reader["Mui1_NgayTiem"].ToString() : "",
                        Mui1_SoLo = reader["Mui1_SoLo"] != null && reader["Mui1_SoLo"] != DBNull.Value ? reader["Mui1_SoLo"].ToString() : "",
                        Mui1_DiaDiem = reader["Mui1_DiaDiem"] != null && reader["Mui1_DiaDiem"] != DBNull.Value ? reader["Mui1_DiaDiem"].ToString() : "",
                        Mui2_TenVacxin = reader["Mui2_TenVacxin"] != null && reader["Mui2_TenVacxin"] != DBNull.Value ? reader["Mui2_TenVacxin"].ToString() : "",
                        Mui2_NgayTiem = reader["Mui2_NgayTiem"] != null && reader["Mui2_NgayTiem"] != DBNull.Value ? reader["Mui2_NgayTiem"].ToString() : "",
                        Mui2_SoLo = reader["Mui2_SoLo"] != null && reader["Mui2_SoLo"] != DBNull.Value ? reader["Mui2_SoLo"].ToString() : "",
                        Mui2_DiaDiem = reader["Mui2_DiaDiem"] != null && reader["Mui2_DiaDiem"] != DBNull.Value ? reader["Mui2_DiaDiem"].ToString() : "",
                        GhiChu = reader["MoTa"] != null && reader["MoTa"] != DBNull.Value ? reader["MoTa"].ToString() : "",
                        KetQuaImPort = reader["KetQuaImPort"] != null && reader["KetQuaImPort"] != DBNull.Value ? reader["KetQuaImPort"].ToString() : "",
                        DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : "",
                        NgayCapNhat = reader["NgayCapNhat"] != null && reader["NgayCapNhat"] != DBNull.Value ? reader["NgayCapNhat"].ToString() : "",
                        NguoiCapNhat = reader["NguoiCapNhat"] != null && reader["NguoiCapNhat"] != DBNull.Value ? reader["NguoiCapNhat"].ToString() : "",
                        UrlHinhGiayXN = reader["UrlHinhGiayXN"] != null && reader["UrlHinhGiayXN"] != DBNull.Value ? reader["UrlHinhGiayXN"].ToString() : "",
                        PhanNhom = reader["PhanNhom"] != null && reader["PhanNhom"] != DBNull.Value ? reader["PhanNhom"].ToString() : "",
                        TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? reader["TinhTrang"].ToString() : "",
                        BiF0 = reader["BiF0"] != null && reader["BiF0"] != DBNull.Value ? Convert.ToBoolean(reader["BiF0"]) : false,
                        NgayXuatVien = reader["NgayXuatVien"] != null && reader["NgayXuatVien"] != DBNull.Value ? reader["NgayXuatVien"].ToString() : "",
                        ISok = reader["ISok"] != null && reader["ISok"] != DBNull.Value ? Convert.ToBoolean(reader["ISok"]) : true,
                        Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                        Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                        VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString() : "chưa có tổ dân phố",
                        BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]) : -1,
                        TenBoPhan = reader["TenBoPhan"] != null && reader["TenBoPhan"] != DBNull.Value ? reader["TenBoPhan"].ToString() : "",
                        IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",
                        LinkWeb = reader["LinkWeb"] != null && reader["LinkWeb"] != DBNull.Value ? reader["LinkWeb"].ToString() : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? reader["MaQR"].ToString() : "",


                    };

                    staffs.Add(staff);
                }
            }

            return staffs;
        }
        public Staff GetStaff(string MaNhanVien)
        {
            var staff = new Staff();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                //SqlDependency.Start(connectionString);
                string commandText = "select * from Staff where MaNS ='"+ MaNhanVien + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                //SqlDependency dependency = new SqlDependency(cmd);
                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                     staff = new Staff
                    {
                         Id = Convert.ToInt32(reader["Id"]),
                         HoTen = reader["HoTen"].ToString(),
                         //NgayThangNamSinh = reader["NgayThangNamSinh"] != null && reader["NgayThangNamSinh"] != DBNull.Value ? reader["NgayThangNamSinh"].ToString() : "",
                         NgayS = reader["NgayS"] != null && reader["NgayS"] != DBNull.Value ? Convert.ToInt32(reader["NgayS"].ToString()) : 1,
                         ThangS = reader["ThangS"] != null && reader["ThangS"] != DBNull.Value ? Convert.ToInt32(reader["ThangS"].ToString()) : 1,
                         NamS = reader["NamS"] != null && reader["NamS"] != DBNull.Value ? Convert.ToInt32(reader["NamS"].ToString()) : 1900,
                         GioiTinh = reader["GioiTinh"] != null && reader["GioiTinh"] != DBNull.Value ? reader["GioiTinh"].ToString() : "",
                         MaNhom = reader["MaNhom"] != null && reader["MaNhom"] != DBNull.Value ? reader["MaNhom"].ToString() : "",
                         DonVi = reader["DonVi"] != null && reader["DonVi"] != DBNull.Value ? reader["DonVi"].ToString() : "",
                         DienThoai = reader["DienThoai"] != null && reader["DienThoai"] != DBNull.Value ? reader["DienThoai"].ToString() : "",
                         CMND = reader["CMND"] != null && reader["CMND"] != DBNull.Value ? reader["CMND"].ToString() : "",
                         CCCD = reader["CCCD"] != null && reader["CCCD"] != DBNull.Value ? reader["CCCD"].ToString() : "",
                         BHYT = reader["BHYT"] != null && reader["BHYT"] != DBNull.Value ? reader["BHYT"].ToString() : "",
                         TenTinh = reader["TenTinh"] != null && reader["TenTinh"] != DBNull.Value ? reader["TenTinh"].ToString() : "",
                         MaTinh = reader["MaTinh"] != null && reader["MaTinh"] != DBNull.Value ? reader["MaTinh"].ToString() : "",
                         TenQuan = reader["TenQuan"] != null && reader["TenQuan"] != DBNull.Value ? reader["TenQuan"].ToString() : "",
                         MaQuan = reader["MaQuan"] != null && reader["MaQuan"] != DBNull.Value ? reader["MaQuan"].ToString() : "",
                         TenPhuong = reader["TenPhuong"] != null && reader["TenPhuong"] != DBNull.Value ? reader["TenPhuong"].ToString() : "",
                         MaPhuong = reader["MaPhuong"] != null && reader["MaPhuong"] != DBNull.Value ? reader["MaPhuong"].ToString() : "",
                         DiaChi = reader["DiaChi"] != null && reader["DiaChi"] != DBNull.Value ? reader["DiaChi"].ToString() : "",
                         Mui1_TenVacxin = reader["Mui1_TenVacxin"] != null && reader["Mui1_TenVacxin"] != DBNull.Value ? reader["Mui1_TenVacxin"].ToString() : "",
                         Mui1_NgayTiem = reader["Mui1_NgayTiem"] != null && reader["Mui1_NgayTiem"] != DBNull.Value ? reader["Mui1_NgayTiem"].ToString() : "",
                         Mui1_SoLo = reader["Mui1_SoLo"] != null && reader["Mui1_SoLo"] != DBNull.Value ? reader["Mui1_SoLo"].ToString() : "",
                         Mui1_DiaDiem = reader["Mui1_DiaDiem"] != null && reader["Mui1_DiaDiem"] != DBNull.Value ? reader["Mui1_DiaDiem"].ToString() : "",
                         Mui2_TenVacxin = reader["Mui2_TenVacxin"] != null && reader["Mui2_TenVacxin"] != DBNull.Value ? reader["Mui2_TenVacxin"].ToString() : "",
                         Mui2_NgayTiem = reader["Mui2_NgayTiem"] != null && reader["Mui2_NgayTiem"] != DBNull.Value ? reader["Mui2_NgayTiem"].ToString() : "",
                         Mui2_SoLo = reader["Mui2_SoLo"] != null && reader["Mui2_SoLo"] != DBNull.Value ? reader["Mui2_SoLo"].ToString() : "",
                         Mui2_DiaDiem = reader["Mui2_DiaDiem"] != null && reader["Mui2_DiaDiem"] != DBNull.Value ? reader["Mui2_DiaDiem"].ToString() : "",
                         GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? reader["GhiChu"].ToString() : "",
                         KetQuaImPort = reader["KetQuaImPort"] != null && reader["KetQuaImPort"] != DBNull.Value ? reader["KetQuaImPort"].ToString() : "",
                         DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : "",
                         NgayCapNhat = reader["NgayCapNhat"] != null && reader["NgayCapNhat"] != DBNull.Value ? reader["NgayCapNhat"].ToString() : "",
                         NguoiCapNhat = reader["NguoiCapNhat"] != null && reader["NguoiCapNhat"] != DBNull.Value ? reader["NguoiCapNhat"].ToString() : "",
                         UrlHinhGiayXN = reader["UrlHinhGiayXN"] != null && reader["UrlHinhGiayXN"] != DBNull.Value ? reader["UrlHinhGiayXN"].ToString() : "",
                         PhanNhom = reader["PhanNhom"] != null && reader["PhanNhom"] != DBNull.Value ? reader["PhanNhom"].ToString() : "",
                         TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? reader["TinhTrang"].ToString() : "",
                         BiF0 = reader["BiF0"] != null && reader["BiF0"] != DBNull.Value ? Convert.ToBoolean(reader["BiF0"]) : false,
                         NgayXuatVien = reader["NgayXuatVien"] != null && reader["NgayXuatVien"] != DBNull.Value ? reader["NgayXuatVien"].ToString() : "",
                         ISok = reader["ISok"] != null && reader["ISok"] != DBNull.Value ? Convert.ToBoolean(reader["ISok"]) : true,
                         Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                         Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                         VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString() : "",
                         BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]) : -1,
                         IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",
                         MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? reader["MaQR"].ToString() : "",

                     };
                }
            }

            return staff;
        }
        public Staff GetStaffByCMND(string CMND)
        {
            var staff = new Staff();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                //SqlDependency.Start(connectionString);
                string commandText = "select * from Staff where CMND ='" + CMND + "' or CCCD='"+ CMND +"'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                //SqlDependency dependency = new SqlDependency(cmd);
                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    staff = new Staff
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        HoTen = reader["HoTen"].ToString(),
                        NgaySinh = reader["NgaySinh"] != null && reader["NgaySinh"] != DBNull.Value ? reader["NgaySinh"].ToString() : "",
                        NgayS = reader["NgayS"] != null && reader["NgayS"] != DBNull.Value ? Convert.ToInt32(reader["NgayS"].ToString()) : 1,
                        ThangS = reader["ThangS"] != null && reader["ThangS"] != DBNull.Value ? Convert.ToInt32(reader["ThangS"].ToString()) : 1,
                        NamS = reader["NamS"] != null && reader["NamS"] != DBNull.Value ? Convert.ToInt32(reader["NamS"].ToString()) : 1900,
                        GioiTinh = reader["GioiTinh"] != null && reader["GioiTinh"] != DBNull.Value ? reader["GioiTinh"].ToString() : "",
                        MaNhom = reader["MaNhom"] != null && reader["MaNhom"] != DBNull.Value ? reader["MaNhom"].ToString() : "",
                        DonVi = reader["DonVi"] != null && reader["DonVi"] != DBNull.Value ? reader["DonVi"].ToString() : "",
                        DienThoai = reader["DienThoai"] != null && reader["DienThoai"] != DBNull.Value ? reader["DienThoai"].ToString() : "",
                        CMND = reader["CMND"] != null && reader["CMND"] != DBNull.Value ? reader["CMND"].ToString() : "",
                        CCCD = reader["CCCD"] != null && reader["CCCD"] != DBNull.Value ? reader["CCCD"].ToString() : "",
                        BHYT = reader["BHYT"] != null && reader["BHYT"] != DBNull.Value ? reader["BHYT"].ToString() : "",
                        TenTinh = reader["TenTinh"] != null && reader["TenTinh"] != DBNull.Value ? reader["TenTinh"].ToString() : "",
                        MaTinh = reader["MaTinh"] != null && reader["MaTinh"] != DBNull.Value ? reader["MaTinh"].ToString() : "",
                        TenQuan = reader["TenQuan"] != null && reader["TenQuan"] != DBNull.Value ? reader["TenQuan"].ToString() : "",
                        MaQuan = reader["MaQuan"] != null && reader["MaQuan"] != DBNull.Value ? reader["MaQuan"].ToString() : "",
                        TenPhuong = reader["TenPhuong"] != null && reader["TenPhuong"] != DBNull.Value ? reader["TenPhuong"].ToString() : "",
                        MaPhuong = reader["MaPhuong"] != null && reader["MaPhuong"] != DBNull.Value ? reader["MaPhuong"].ToString() : "",
                        DiaChi = reader["DiaChi"] != null && reader["DiaChi"] != DBNull.Value ? reader["DiaChi"].ToString() : "",
                        Mui1_TenVacxin = reader["Mui1_TenVacxin"] != null && reader["Mui1_TenVacxin"] != DBNull.Value ? reader["Mui1_TenVacxin"].ToString() : "",
                        Mui1_NgayTiem = reader["Mui1_NgayTiem"] != null && reader["Mui1_NgayTiem"] != DBNull.Value ? reader["Mui1_NgayTiem"].ToString() : "",
                        Mui1_SoLo = reader["Mui1_SoLo"] != null && reader["Mui1_SoLo"] != DBNull.Value ? reader["Mui1_SoLo"].ToString() : "",
                        Mui1_DiaDiem = reader["Mui1_DiaDiem"] != null && reader["Mui1_DiaDiem"] != DBNull.Value ? reader["Mui1_DiaDiem"].ToString() : "",
                        Mui2_TenVacxin = reader["Mui2_TenVacxin"] != null && reader["Mui2_TenVacxin"] != DBNull.Value ? reader["Mui2_TenVacxin"].ToString() : "",
                        Mui2_NgayTiem = reader["Mui2_NgayTiem"] != null && reader["Mui2_NgayTiem"] != DBNull.Value ? reader["Mui2_NgayTiem"].ToString() : "",
                        Mui2_SoLo = reader["Mui2_SoLo"] != null && reader["Mui2_SoLo"] != DBNull.Value ? reader["Mui2_SoLo"].ToString() : "",
                        Mui2_DiaDiem = reader["Mui2_DiaDiem"] != null && reader["Mui2_DiaDiem"] != DBNull.Value ? reader["Mui2_DiaDiem"].ToString() : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? reader["GhiChu"].ToString() : "",
                        KetQuaImPort = reader["KetQuaImPort"] != null && reader["KetQuaImPort"] != DBNull.Value ? reader["KetQuaImPort"].ToString() : "",
                        DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : "",
                        NgayCapNhat = reader["NgayCapNhat"] != null && reader["NgayCapNhat"] != DBNull.Value ? reader["NgayCapNhat"].ToString() : "",
                        NguoiCapNhat = reader["NguoiCapNhat"] != null && reader["NguoiCapNhat"] != DBNull.Value ? reader["NguoiCapNhat"].ToString() : "",
                        UrlHinhGiayXN = reader["UrlHinhGiayXN"] != null && reader["UrlHinhGiayXN"] != DBNull.Value ? reader["UrlHinhGiayXN"].ToString() : "",
                        PhanNhom = reader["PhanNhom"] != null && reader["PhanNhom"] != DBNull.Value ? reader["PhanNhom"].ToString() : "",
                        TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? reader["TinhTrang"].ToString() : "",
                        BiF0 = reader["BiF0"]!=null && reader["BiF0"]!=DBNull.Value ? Convert.ToBoolean(reader["BiF0"]) : false,
                        NgayXuatVien = reader["NgayXuatVien"] != null && reader["NgayXuatVien"] != DBNull.Value ? reader["NgayXuatVien"].ToString() : "",
                        ISok = reader["ISok"] != null && reader["ISok"] != DBNull.Value ? Convert.ToBoolean(reader["ISok"]) : true,
                        Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                        Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                        VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString() : "",
                        BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]): -1,
                        IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? reader["MaQR"].ToString() : "",
                    };
                }
            }
            return staff;
        }
        public Staff GetStaffByMaNS(string MaNS)
        {
            var staff = new Staff();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                //SqlDependency.Start(connectionString);
                string commandText = "select * from Staff where MaNS ='" + MaNS + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                //SqlDependency dependency = new SqlDependency(cmd);
                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    staff = new Staff
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        HoTen = reader["HoTen"].ToString(),
                        NgaySinh = reader["NgaySinh"] != null && reader["NgaySinh"] != DBNull.Value ? reader["NgaySinh"].ToString() : "",
                        NgayS = reader["NgayS"] != null && reader["NgayS"] != DBNull.Value ? Convert.ToInt32(reader["NgayS"].ToString()) : 1,
                        ThangS = reader["ThangS"] != null && reader["ThangS"] != DBNull.Value ? Convert.ToInt32(reader["ThangS"].ToString()) : 1,
                        NamS = reader["NamS"] != null && reader["NamS"] != DBNull.Value ? Convert.ToInt32(reader["NamS"].ToString()) : 1900,
                        GioiTinh = reader["GioiTinh"] != null && reader["GioiTinh"] != DBNull.Value ? reader["GioiTinh"].ToString() : "",
                        MaNhom = reader["MaNhom"] != null && reader["MaNhom"] != DBNull.Value ? reader["MaNhom"].ToString() : "",
                        DonVi = reader["DonVi"] != null && reader["DonVi"] != DBNull.Value ? reader["DonVi"].ToString() : "",
                        DienThoai = reader["DienThoai"] != null && reader["DienThoai"] != DBNull.Value ? reader["DienThoai"].ToString() : "",
                        CMND = reader["CMND"] != null && reader["CMND"] != DBNull.Value ? reader["CMND"].ToString() : "",
                        CCCD = reader["CCCD"] != null && reader["CCCD"] != DBNull.Value ? reader["CCCD"].ToString() : "",
                        BHYT = reader["BHYT"] != null && reader["BHYT"] != DBNull.Value ? reader["BHYT"].ToString() : "",
                        TenTinh = reader["TenTinh"] != null && reader["TenTinh"] != DBNull.Value ? reader["TenTinh"].ToString() : "",
                        MaTinh = reader["MaTinh"] != null && reader["MaTinh"] != DBNull.Value ? reader["MaTinh"].ToString() : "",
                        TenQuan = reader["TenQuan"] != null && reader["TenQuan"] != DBNull.Value ? reader["TenQuan"].ToString() : "",
                        MaQuan = reader["MaQuan"] != null && reader["MaQuan"] != DBNull.Value ? reader["MaQuan"].ToString() : "",
                        TenPhuong = reader["TenPhuong"] != null && reader["TenPhuong"] != DBNull.Value ? reader["TenPhuong"].ToString() : "",
                        MaPhuong = reader["MaPhuong"] != null && reader["MaPhuong"] != DBNull.Value ? reader["MaPhuong"].ToString() : "",
                        DiaChi = reader["DiaChi"] != null && reader["DiaChi"] != DBNull.Value ? reader["DiaChi"].ToString() : "",
                        Mui1_TenVacxin = reader["Mui1_TenVacxin"] != null && reader["Mui1_TenVacxin"] != DBNull.Value ? reader["Mui1_TenVacxin"].ToString() : "",
                        Mui1_NgayTiem = reader["Mui1_NgayTiem"] != null && reader["Mui1_NgayTiem"] != DBNull.Value ? reader["Mui1_NgayTiem"].ToString() : "",
                        Mui1_SoLo = reader["Mui1_SoLo"] != null && reader["Mui1_SoLo"] != DBNull.Value ? reader["Mui1_SoLo"].ToString() : "",
                        Mui1_DiaDiem = reader["Mui1_DiaDiem"] != null && reader["Mui1_DiaDiem"] != DBNull.Value ? reader["Mui1_DiaDiem"].ToString() : "",
                        Mui2_TenVacxin = reader["Mui2_TenVacxin"] != null && reader["Mui2_TenVacxin"] != DBNull.Value ? reader["Mui2_TenVacxin"].ToString() : "",
                        Mui2_NgayTiem = reader["Mui2_NgayTiem"] != null && reader["Mui2_NgayTiem"] != DBNull.Value ? reader["Mui2_NgayTiem"].ToString() : "",
                        Mui2_SoLo = reader["Mui2_SoLo"] != null && reader["Mui2_SoLo"] != DBNull.Value ? reader["Mui2_SoLo"].ToString() : "",
                        Mui2_DiaDiem = reader["Mui2_DiaDiem"] != null && reader["Mui2_DiaDiem"] != DBNull.Value ? reader["Mui2_DiaDiem"].ToString() : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? reader["GhiChu"].ToString() : "",
                        KetQuaImPort = reader["KetQuaImPort"] != null && reader["KetQuaImPort"] != DBNull.Value ? reader["KetQuaImPort"].ToString() : "",
                        DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : "",
                        NgayCapNhat = reader["NgayCapNhat"] != null && reader["NgayCapNhat"] != DBNull.Value ? reader["NgayCapNhat"].ToString() : "",
                        NguoiCapNhat = reader["NguoiCapNhat"] != null && reader["NguoiCapNhat"] != DBNull.Value ? reader["NguoiCapNhat"].ToString() : "",
                        UrlHinhGiayXN = reader["UrlHinhGiayXN"] != null && reader["UrlHinhGiayXN"] != DBNull.Value ? reader["UrlHinhGiayXN"].ToString() : "",
                        PhanNhom = reader["PhanNhom"] != null && reader["PhanNhom"] != DBNull.Value ? reader["PhanNhom"].ToString() : "",
                        TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? reader["TinhTrang"].ToString() : "",
                        BiF0 = reader["BiF0"]!=null && reader["BiF0"]!=DBNull.Value ? Convert.ToBoolean(reader["BiF0"]) : false,
                        NgayXuatVien = reader["NgayXuatVien"] != null && reader["NgayXuatVien"] != DBNull.Value ? reader["NgayXuatVien"].ToString() : "",
                        ISok = reader["ISok"] != null && reader["ISok"] != DBNull.Value ? Convert.ToBoolean(reader["ISok"]) : true,
                        Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                        Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                        VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString() : "",
                        BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]): -1,
                        IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? reader["MaQR"].ToString() : "",
                    };
                }
            }
            return staff;
        }
        private void dbChangeNotification(object sender, SqlNotificationEventArgs e)
        {
            Console.WriteLine(e.Info);
            //  _context.Clients.All.SendAsync("refreshEmployees");
        }
        public void UpdateLinkGCN(string url, string id)
        {

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "update Staff set UrlHinhGiayXN='" + url + "' from Staff where Id ='" + id + "'";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteNonQuery();
            }
        }
        public void UpdateStaff(int id,string hoten)
        {

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "update Staff set HoTen=N'" + hoten + "' from Staff where Id ='" + id + "'";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteNonQuery();
            }
        }

        public List<PL2_NhomDoiTuong> GetAll_PL2_NhomDoiTuong()
        {
            return lib.GetAll_PL2_NhomDoiTuong();

        }

        public bool UpdateStaffThongTinNhanVien(Staff staff)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateStaffThongTinNhanVien", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@HoTen", staff.HoTen));
                    cmd.Parameters.Add(new SqlParameter("@NgaySinh", staff.NgayS));
                    cmd.Parameters.Add(new SqlParameter("@GioiTinh", staff.GioiTinh));
                    cmd.Parameters.Add(new SqlParameter("@MaNhom", staff.MaNhom));
                    cmd.Parameters.Add(new SqlParameter("@DonVi", staff.DonVi));
                    cmd.Parameters.Add(new SqlParameter("@DienThoai", staff.DienThoai));
                    cmd.Parameters.Add(new SqlParameter("@CMND", staff.CMND));
                    cmd.Parameters.Add(new SqlParameter("@BHYT", staff.BHYT));
                    cmd.Parameters.Add(new SqlParameter("@Id", staff.Id));
                    cmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool UpdateStaffThongTinNhanVien_Temp(Staff staff)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateStaffThongTinNhanVien_Temp", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@HoTen", staff.HoTen));
                    cmd.Parameters.Add(new SqlParameter("@NgaySinh", staff.NgayS));
                    cmd.Parameters.Add(new SqlParameter("@ThangSinh", staff.ThangS));
                    cmd.Parameters.Add(new SqlParameter("@NamSinh", staff.NamS));
                    cmd.Parameters.Add(new SqlParameter("@GioiTinh", staff.GioiTinh));
                    cmd.Parameters.Add(new SqlParameter("@MaNhom", staff.MaNhom));
                    cmd.Parameters.Add(new SqlParameter("@DonViChiTiet", staff.DonViChiTiet));
                    cmd.Parameters.Add(new SqlParameter("@DonVi", staff.DonVi));
                    cmd.Parameters.Add(new SqlParameter("@DienThoai", staff.DienThoai));
                    cmd.Parameters.Add(new SqlParameter("@CMND", staff.CMND));
                    cmd.Parameters.Add(new SqlParameter("@CCCD", staff.CCCD!=null ? staff.CCCD : ""));
                    cmd.Parameters.Add(new SqlParameter("@BHYT", ""));
                    cmd.Parameters.Add(new SqlParameter("@BoPhanId", staff.BoPhanId));
                    cmd.Parameters.Add(new SqlParameter("@Id", staff.Id));
                    cmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool UpdateStaffDiacChiNhanVien(Staff staff)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateStaffDiacChiNhanVien", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@TenTinh", staff.TenTinh));
                    cmd.Parameters.Add(new SqlParameter("@MaTinh", staff.MaTinh));
                    cmd.Parameters.Add(new SqlParameter("@TenQuan", staff.TenQuan));
                    cmd.Parameters.Add(new SqlParameter("@MaQuan", staff.MaQuan));
                    cmd.Parameters.Add(new SqlParameter("@TenPhuong", staff.TenPhuong));
                    cmd.Parameters.Add(new SqlParameter("@MaPhuong", staff.MaPhuong));
                    cmd.Parameters.Add(new SqlParameter("@DiaChi", staff.DiaChi));
                    cmd.Parameters.Add(new SqlParameter("@Ma_ToKhuPho", staff.Ma_ToKhuPho));
                    cmd.Parameters.Add(new SqlParameter("@Ten_ToKhuPho", staff.Ten_ToKhuPho));
                    cmd.Parameters.Add(new SqlParameter("@Id", staff.Id));
                    cmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool UpdateStaffDiacChiNhanVien_Temp(Staff staff)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateStaffDiacChiNhanVien_Temp", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@TenTinh", staff.TenTinh));
                    cmd.Parameters.Add(new SqlParameter("@MaTinh", staff.MaTinh));
                    cmd.Parameters.Add(new SqlParameter("@TenQuan", staff.TenQuan));
                    cmd.Parameters.Add(new SqlParameter("@MaQuan", staff.MaQuan));
                    cmd.Parameters.Add(new SqlParameter("@TenPhuong", staff.TenPhuong));
                    cmd.Parameters.Add(new SqlParameter("@MaPhuong", staff.MaPhuong));
                    cmd.Parameters.Add(new SqlParameter("@DiaChi", staff.DiaChi));
                    cmd.Parameters.Add(new SqlParameter("@Ma_ToKhuPho", staff.Ma_ToKhuPho));
                    cmd.Parameters.Add(new SqlParameter("@IdWeb", staff.IdWeb));
                    cmd.Parameters.Add(new SqlParameter("@Ten_ToKhuPho", staff.Ten_ToKhuPho));
                    cmd.Parameters.Add(new SqlParameter("@Id", staff.Id));
                    cmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool UpdateMui1Staff(Staff staff)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateMui1Staff", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Mui1_TenVacxin", staff.Mui1_TenVacxin));
                    cmd.Parameters.Add(new SqlParameter("@Mui1_NgayTiem", staff.Mui1_NgayTiem));
                    cmd.Parameters.Add(new SqlParameter("@Mui1_SoLo", staff.Mui1_SoLo));
                    cmd.Parameters.Add(new SqlParameter("@Mui1_DiaDiem", staff.Mui1_DiaDiem));
                    cmd.Parameters.Add(new SqlParameter("@Id", staff.Id));
                    cmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool UpdateMui1Staff_Temp(Staff staff)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateMui1Staff_Temp", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Mui1_TenVacxin", staff.Mui1_TenVacxin));
                    cmd.Parameters.Add(new SqlParameter("@Mui1_NgayTiem", staff.Mui1_NgayTiem));
                    cmd.Parameters.Add(new SqlParameter("@Mui1_SoLo", staff.Mui1_SoLo));
                    cmd.Parameters.Add(new SqlParameter("@Mui1_DiaDiem", staff.Mui1_DiaDiem));
                    cmd.Parameters.Add(new SqlParameter("@Id", staff.Id));
                    cmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool UpdateMui2Staff(Staff staff)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateMui2Staff", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Mui2_TenVacxin", staff.Mui2_TenVacxin));
                    cmd.Parameters.Add(new SqlParameter("@Mui2_NgayTiem", staff.Mui2_NgayTiem));
                    cmd.Parameters.Add(new SqlParameter("@Mui2_SoLo", staff.Mui2_SoLo));
                    cmd.Parameters.Add(new SqlParameter("@Mui2_DiaDiem", staff.Mui2_DiaDiem));
                    cmd.Parameters.Add(new SqlParameter("@Id", staff.Id));
                    cmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool UpdateMui2Staff_Temp(Staff staff)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateMui2Staff_Temp", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Mui2_TenVacxin", staff.Mui2_TenVacxin));
                    cmd.Parameters.Add(new SqlParameter("@Mui2_NgayTiem", staff.Mui2_NgayTiem));
                    cmd.Parameters.Add(new SqlParameter("@Mui2_SoLo", staff.Mui2_SoLo));
                    cmd.Parameters.Add(new SqlParameter("@Mui2_DiaDiem", staff.Mui2_DiaDiem));
                    cmd.Parameters.Add(new SqlParameter("@Id", staff.Id));
                    cmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        public bool UpdateThongTinKhacStaff(Staff staff)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateThongTinKhacStaff", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@GhiChu", staff.GhiChu));
                    cmd.Parameters.Add(new SqlParameter("@UrlHinhGiayXN", staff.UrlHinhGiayXN));
                    cmd.Parameters.Add(new SqlParameter("@Id", staff.Id));
                    cmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

        public void CreateStaff(Staff staff)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreateStaff_Temp", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@HoTen", staff.HoTen));
                    cmd.Parameters.Add(new SqlParameter("@NgaySinh", staff.NgayS));
                    cmd.Parameters.Add(new SqlParameter("@ThangSinh", staff.ThangS));
                    cmd.Parameters.Add(new SqlParameter("@NamSinh", staff.NamS));
                    cmd.Parameters.Add(new SqlParameter("@GioiTinh", staff.GioiTinh));
                    cmd.Parameters.Add(new SqlParameter("@MaNhom", staff.MaNhom));
                    cmd.Parameters.Add(new SqlParameter("@DonVi", staff.DonVi));
                    cmd.Parameters.Add(new SqlParameter("@DonViChiTiet", staff.DonViChiTiet));
                    cmd.Parameters.Add(new SqlParameter("@DienThoai", staff.DienThoai));
                    cmd.Parameters.Add(new SqlParameter("@CMND", staff.CMND));
                    cmd.Parameters.Add(new SqlParameter("@CCCD", staff.CCCD!=null ? staff.CCCD : ""));
                    cmd.Parameters.Add(new SqlParameter("@BoPhanId", staff.BoPhanId));
                    cmd.Parameters.Add(new SqlParameter("@BHYT", staff.BHYT!=null ? staff.BHYT : ""));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public Staff GetStaffByDienThoai(string DienThoai)
        {
            var staff = new Staff();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from Staff where DienThoai ='" + DienThoai + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    staff = new Staff
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        HoTen = reader["HoTen"].ToString(),
                        NgaySinh = reader["NgaySinh"] != null && reader["NgaySinh"] != DBNull.Value ? reader["NgaySinh"].ToString() : "",
                        NgayS = reader["NgayS"] != null && reader["NgayS"] != DBNull.Value ? Convert.ToInt32(reader["NgayS"].ToString()) : 1,
                        ThangS = reader["ThangS"] != null && reader["ThangS"] != DBNull.Value ? Convert.ToInt32(reader["ThangS"].ToString()) : 1,
                        NamS = reader["NamS"] != null && reader["NamS"] != DBNull.Value ? Convert.ToInt32(reader["NamS"].ToString()) : 1900,
                        GioiTinh = reader["GioiTinh"] != null && reader["GioiTinh"] != DBNull.Value ? reader["GioiTinh"].ToString() : "",
                        MaNhom = reader["MaNhom"] != null && reader["MaNhom"] != DBNull.Value ? reader["MaNhom"].ToString() : "",
                        DonVi = reader["DonVi"] != null && reader["DonVi"] != DBNull.Value ? reader["DonVi"].ToString() : "",
                        DienThoai = reader["DienThoai"] != null && reader["DienThoai"] != DBNull.Value ? reader["DienThoai"].ToString() : "",
                        CMND = reader["CMND"] != null && reader["CMND"] != DBNull.Value ? reader["CMND"].ToString() : "",
                        CCCD = reader["CCCD"] != null && reader["CCCD"] != DBNull.Value ? reader["CCCD"].ToString() : "",
                        BHYT = reader["BHYT"] != null && reader["BHYT"] != DBNull.Value ? reader["BHYT"].ToString() : "",
                        TenTinh = reader["TenTinh"] != null && reader["TenTinh"] != DBNull.Value ? reader["TenTinh"].ToString() : "",
                        MaTinh = reader["MaTinh"] != null && reader["MaTinh"] != DBNull.Value ? reader["MaTinh"].ToString() : "",
                        TenQuan = reader["TenQuan"] != null && reader["TenQuan"] != DBNull.Value ? reader["TenQuan"].ToString() : "",
                        MaQuan = reader["MaQuan"] != null && reader["MaQuan"] != DBNull.Value ? reader["MaQuan"].ToString() : "",
                        TenPhuong = reader["TenPhuong"] != null && reader["TenPhuong"] != DBNull.Value ? reader["TenPhuong"].ToString() : "",
                        MaPhuong = reader["MaPhuong"] != null && reader["MaPhuong"] != DBNull.Value ? reader["MaPhuong"].ToString() : "",
                        DiaChi = reader["DiaChi"] != null && reader["DiaChi"] != DBNull.Value ? reader["DiaChi"].ToString() : "",
                        Mui1_TenVacxin = reader["Mui1_TenVacxin"] != null && reader["Mui1_TenVacxin"] != DBNull.Value ? reader["Mui1_TenVacxin"].ToString() : "",
                        Mui1_NgayTiem = reader["Mui1_NgayTiem"] != null && reader["Mui1_NgayTiem"] != DBNull.Value ? reader["Mui1_NgayTiem"].ToString() : "",
                        Mui1_SoLo = reader["Mui1_SoLo"] != null && reader["Mui1_SoLo"] != DBNull.Value ? reader["Mui1_SoLo"].ToString() : "",
                        Mui1_DiaDiem = reader["Mui1_DiaDiem"] != null && reader["Mui1_DiaDiem"] != DBNull.Value ? reader["Mui1_DiaDiem"].ToString() : "",
                        Mui2_TenVacxin = reader["Mui2_TenVacxin"] != null && reader["Mui2_TenVacxin"] != DBNull.Value ? reader["Mui2_TenVacxin"].ToString() : "",
                        Mui2_NgayTiem = reader["Mui2_NgayTiem"] != null && reader["Mui2_NgayTiem"] != DBNull.Value ? reader["Mui2_NgayTiem"].ToString() : "",
                        Mui2_SoLo = reader["Mui2_SoLo"] != null && reader["Mui2_SoLo"] != DBNull.Value ? reader["Mui2_SoLo"].ToString() : "",
                        Mui2_DiaDiem = reader["Mui2_DiaDiem"] != null && reader["Mui2_DiaDiem"] != DBNull.Value ? reader["Mui2_DiaDiem"].ToString() : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? reader["GhiChu"].ToString() : "",
                        KetQuaImPort = reader["KetQuaImPort"] != null && reader["KetQuaImPort"] != DBNull.Value ? reader["KetQuaImPort"].ToString() : "",
                        DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : "",
                        NgayCapNhat = reader["NgayCapNhat"] != null && reader["NgayCapNhat"] != DBNull.Value ? reader["NgayCapNhat"].ToString() : "",
                        NguoiCapNhat = reader["NguoiCapNhat"] != null && reader["NguoiCapNhat"] != DBNull.Value ? reader["NguoiCapNhat"].ToString() : "",
                        UrlHinhGiayXN = reader["UrlHinhGiayXN"] != null && reader["UrlHinhGiayXN"] != DBNull.Value ? reader["UrlHinhGiayXN"].ToString() : "",
                        PhanNhom = reader["PhanNhom"] != null && reader["PhanNhom"] != DBNull.Value ? reader["PhanNhom"].ToString() : "",
                        TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? reader["TinhTrang"].ToString() : "",
                        BiF0 = reader["BiF0"] != null && reader["BiF0"] != DBNull.Value ? Convert.ToBoolean(reader["BiF0"]) : false,
                        NgayXuatVien = reader["NgayXuatVien"] != null && reader["NgayXuatVien"] != DBNull.Value ? reader["NgayXuatVien"].ToString() : "",
                        ISok = reader["ISok"] != null && reader["ISok"] != DBNull.Value ? Convert.ToBoolean(reader["ISok"]) : true,
                        Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                        Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                        VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString() : "",
                        BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]) : -1,
                        IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? reader["MaQR"].ToString() : "",
                    };
                }
            }
            return staff;
        }
        public DataTable GetNhanVienByDonViChiTiet(int BoPhanId)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetNhanVienByDonViChiTiet", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DonViChiTietId", BoPhanId));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {

            }
            return dt;
        }
        public DataTable GetThongTinTruocCapNhatBiF0(int IdStaff)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string text = "select MaQR,BiF0,NgayBiNhiem,NgayXuatVien from Staff where Id="+ IdStaff;
                    SqlCommand cmd = new SqlCommand(text, conn);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {

            }
            return dt;
        }
        public DataTable GetThongTinTruocCapNhatMuiTiem(string IdStaff, int LoaiMuiTiem)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string text = "";
                    if(LoaiMuiTiem ==1)
                        text = "select MaQR,Mui1_TenVacxin11,Mui1_DiaDiem11,Mui1_NgayTiem11,Mui1_SoLo11 from Staff where Id in (select items from SplitString('" + IdStaff+"',';')) ";
                    else if(LoaiMuiTiem ==2)
                        text = "select MaQR,Mui2_TenVacxin11,Mui2_DiaDiem11,Mui2_NgayTiem11,Mui2_SoLo11 from Staff where Id in (select items from SplitString('" + IdStaff+"',';')) ";
                    else if(LoaiMuiTiem ==3)
                        text = "select MaQR,Mui3_TenVacxin,Mui3_DiaDiem,Mui3_NgayTiem,Mui3_SoLo from Staff where Id in (select items from SplitString('" + IdStaff+"',';')) ";
                    SqlCommand cmd = new SqlCommand(text, conn);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {

            }
            return dt;
        }

        public void LuuThongTinMui_XoaThongTinTiem(string StaffId, int LoaiMuiTiem)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                if (LoaiMuiTiem == 1)
                    commandText = "update Staff set Mui1_TenVacxin11='',Mui1_DiaDiem11='',Mui1_NgayTiem11='',Mui1_SoLo11='' " +
                        " ,Mui1_TenVacxin1='',Mui1_DiaDiem1='',Mui1_NgayTiem1='',Mui1_SoLo1=''" +
                        " ,Mui1_TenVacxin='',Mui1_DiaDiem='',Mui1_NgayTiem='',Mui1_SoLo=''" +
                        " where Id in (select items from SplitString('" + StaffId + "',';')) ";
                else if (LoaiMuiTiem == 2)
                    commandText = "update Staff set Mui2_TenVacxin11='',Mui2_DiaDiem11='',Mui2_NgayTiem11='',Mui2_SoLo11='' " +
                        " Mui2_TenVacxin1='',Mui2_DiaDiem1='',Mui2_NgayTiem1='',Mui2_SoLo1=''" +
                        " Mui2_TenVacxin='',Mui2_DiaDiem='',Mui2_NgayTiem='',Mui2_SoLo=''" +
                        " where Id in (select items from SplitString('" + StaffId + "',';')) ";
                else
                    commandText = "update Staff set Mui3_TenVacxin='',Mui3_DiaDiem='',Mui3_NgayTiem='',Mui3_SoLo='' where Id in (select items from SplitString('" + StaffId + "',';')) ";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                cmd.ExecuteNonQuery();
            }
        }
        public DataTable GetAllNhanVienBaoCaoTiemChung(int IdDonVi, string TenDangNhap, int TinhTrang)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetNhanVienByDonViChiTiet_BaoCao", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DonViChiTietId", IdDonVi));
                    cmd.Parameters.Add(new SqlParameter("@TenDangNhap", TenDangNhap));
                    cmd.Parameters.Add(new SqlParameter("@TinhTrang", TinhTrang));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {

            }
            return dt;
        }
        public DataTable GetDanhSachNhanVienByDonViChiTiet_CapNhatLyDoKhongTiem(int IdDonVi, string TenDangNhap, int BiF0, int TiemTaiDiaPhuong, int DaNghiViec, int LyDoKhac, int PhanLoai)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDanhSachNhanVienByDonViChiTiet_CapNhatLyDoKhongTiem", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DonViChiTietId", IdDonVi));
                    cmd.Parameters.Add(new SqlParameter("@TenDangNhap", TenDangNhap));
                    cmd.Parameters.Add(new SqlParameter("@BiF0", BiF0));
                    cmd.Parameters.Add(new SqlParameter("@TiemTaiDiaPhuong", TiemTaiDiaPhuong));
                    cmd.Parameters.Add(new SqlParameter("@DaNghiViec", DaNghiViec));
                    cmd.Parameters.Add(new SqlParameter("@LyDoKhac", LyDoKhac));
                    cmd.Parameters.Add(new SqlParameter("@PhanLoai", PhanLoai));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {

            }
            return dt;
        }
        public void UpdateTiemMui3(int StaffId, int GiaTri) {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                if(GiaTri==1)
                    commandText = "update Staff set DangKyTiemMui3=" + GiaTri + " where (DangKyTiemMui3=0 or DangKyTiemMui3=3) and id =" + StaffId;
                else
                    commandText = "update Staff set DangKyTiemMui3=" + GiaTri + " where DangKyTiemMui3=1 and id =" + StaffId;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                cmd.ExecuteNonQuery();
            }
        }
        public void UpdateTiemMui3Import(int IdDonVi, string CMND, string DangKy)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";// "update Staff set DangKyTiemMui3="+ Convert.ToInt32(DangKy) + " where Mui3_TenVacxin!=1 and cmnd = '" + CMND + "' and DonViChiTiet = (select TenDonVi from DonViChiTiet where Id = " + IdDonVi + ")";
                if (Convert.ToInt32(DangKy) == 1)
                    commandText = "update Staff set DangKyTiemMui3="+ Convert.ToInt32(DangKy) + " where (DangKyTiemMui3=0 or DangKyTiemMui3=3) and cmnd = '" + CMND + "' and DonViChiTiet = (select TenDonVi from DonViChiTiet where Id = " + IdDonVi + ")";
                else    
                    commandText = "update Staff set DangKyTiemMui3="+ Convert.ToInt32(DangKy) + " where DangKyTiemMui3=1 and cmnd = '" + CMND + "' and DonViChiTiet = (select TenDonVi from DonViChiTiet where Id = " + IdDonVi + ")";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                cmd.ExecuteNonQuery();
            }
        }
        public bool UpdateMui1PCCovid(string IdStaff, int LoaiMuiTiem, string Mui1_TenVacxin1, string Mui1_SoLo1, string Mui1_NgayTiem1, string Mui1_DiaDiem1, string Mui1_LyDo, string Mui1_DiaDiem1_Old)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                string[] ngaytiems = Mui1_NgayTiem1.Split('-');
                string ngaytiem = ngaytiems[2] + "/" + ngaytiems[1] + "/" + ngaytiems[0];
                if (string.IsNullOrEmpty(Mui1_DiaDiem1))
                {
                    if (LoaiMuiTiem == 1)
                        commandText = "update Staff set Mui1_TenVacxin11=N'" + Mui1_TenVacxin1 + "',Mui1_SoLo11=N'" + Mui1_SoLo1 + "',Mui1_NgayTiem11=N'" + ngaytiem + "',Mui1_DiaDiem11=N'" + Mui1_DiaDiem1_Old + "',Mui1_LyDo=N'" + Mui1_LyDo + "' where Id in (select items from SplitString('"+IdStaff+"',';'))";
                    else if (LoaiMuiTiem == 2)
                        commandText = "update Staff set Mui2_TenVacxin11=N'" + Mui1_TenVacxin1 + "',Mui2_SoLo11=N'" + Mui1_SoLo1 + "',Mui2_NgayTiem11=N'" + ngaytiem + "',Mui2_DiaDiem11=N'" + Mui1_DiaDiem1_Old + "',Mui2_LyDo=N'" + Mui1_LyDo + "' where Id in (select items from SplitString('" + IdStaff + "',';'))";
                    else if (LoaiMuiTiem == 3)
                    {
                        commandText = "update Staff set Mui3_TenVacxin=N'" + Mui1_TenVacxin1 + "',Mui3_SoLo=N'" + Mui1_SoLo1 + "',Mui3_NgayTiem=N'" + ngaytiem + "',Mui3_DiaDiem=N'" + Mui1_DiaDiem1_Old + "',Mui3_LyDo=N'" + Mui1_LyDo + "' where Id in (select items from SplitString('" + IdStaff + "',';'))";
                        commandText += " update Staff set DangKyTiemMui3=2 where Id in (select items from SplitString('" + IdStaff + "',';'))";
                    }
                }
                else
                {
                    if (LoaiMuiTiem == 1)
                        commandText = "update Staff set Mui1_TenVacxin11=N'" + Mui1_TenVacxin1 + "',Mui1_SoLo11=N'" + Mui1_SoLo1 + "',Mui1_NgayTiem11=N'" + ngaytiem + "',Mui1_DiaDiem11=N'" + Mui1_DiaDiem1 + "',Mui1_LyDo=N'" + Mui1_LyDo + "' where Id in (select items from SplitString('" + IdStaff + "',';'))";
                    else if (LoaiMuiTiem == 2)
                        commandText = "update Staff set Mui2_TenVacxin11=N'" + Mui1_TenVacxin1 + "',Mui2_SoLo11=N'" + Mui1_SoLo1 + "',Mui2_NgayTiem11=N'" + ngaytiem + "',Mui2_DiaDiem11=N'" + Mui1_DiaDiem1 + "',Mui2_LyDo=N'" + Mui1_LyDo + "' where Id in (select items from SplitString('" + IdStaff + "',';'))";
                    else if (LoaiMuiTiem == 3)
                    {
                        commandText = "update Staff set Mui3_TenVacxin=N'" + Mui1_TenVacxin1 + "',Mui3_SoLo=N'" + Mui1_SoLo1 + "',Mui3_NgayTiem=N'" + ngaytiem + "',Mui3_DiaDiem=N'" + Mui1_DiaDiem1 + "',Mui3_LyDo=N'" + Mui1_LyDo + "' where Id in (select items from SplitString('" + IdStaff + "',';'))";
                        commandText += " update Staff set DangKyTiemMui3=2 where Id in (select items from SplitString('" + IdStaff + "',';'))";
                    }
                }
                
                SqlCommand cmd = new SqlCommand(commandText, conn);
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public bool UpdateMui1PCCovid_KhongTiem(string IdStaff, int LoaiMuiTiem, string Mui1_LyDo)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                if (LoaiMuiTiem == 1)
                {
                    commandText = "update Staff set Mui1_LyDo=N'" + Mui1_LyDo + "' where Id in (select items from SplitString('" + IdStaff + "',';'))"; 
                    commandText += " update Staff set Mui1_TenVacxin=N'',Mui1_SoLo=N'',Mui1_NgayTiem=N'',Mui1_DiaDiem=N'' where Id in (select items from SplitString('" + IdStaff + "',';'))";
                }
                else if (LoaiMuiTiem == 2)
                {
                    commandText = "update Staff set Mui2_LyDo=N'" + Mui1_LyDo + "' where Id in (select items from SplitString('" + IdStaff + "',';'))";
                    commandText += " update Staff set Mui2_TenVacxin=N'',Mui2_SoLo=N'',Mui2_NgayTiem=N'',Mui2_DiaDiem=N'' where Id in (select items from SplitString('" + IdStaff + "',';'))";
                }
                else if (LoaiMuiTiem == 3)
                {
                    commandText = "update Staff set Mui3_LyDo=N'" + Mui1_LyDo + "' where Id in (select items from SplitString('" + IdStaff + "',';'))";
                }
                commandText += " update Staff set DangKyTiemMui3=3 where Id in (select items from SplitString('" + IdStaff + "',';'))";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public void UpdateDaNghiViec(string MaQR, bool DaNghiViec)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("UpdateDaNghiViec", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@MaQR", MaQR));
                cmd.Parameters.Add(new SqlParameter("@DaNghiViec", DaNghiViec));
                cmd.ExecuteNonQuery();
            }
        }
        public void UpdateDaNghi(string IdStaff)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("UpdateDaNghi", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@IdStaff", IdStaff));
                cmd.ExecuteNonQuery();
            }
        }
        public void GhiLogCapNhat(long IdNguoiCapNhat, string TenNguoiCapNhat, string NoiDungTruocCapNhat, string NoiDungSauCapNhat)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("GhiLogCapNhat", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@IdNguoiCapNhat", IdNguoiCapNhat));
                cmd.Parameters.Add(new SqlParameter("@TenNguoiCapNhat", TenNguoiCapNhat));
                cmd.Parameters.Add(new SqlParameter("@NoiDungTruocCapNhat", NoiDungTruocCapNhat));
                cmd.Parameters.Add(new SqlParameter("@NoiDungSauCapNhat", NoiDungSauCapNhat));
                cmd.ExecuteNonQuery();
            }
        }
        public void UpdateBiF0(string MaQR, bool BiF0, string NgayNhiem, string NgayXuatVien)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("UpdateBiF0", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                string[] ngaynhiems = NgayNhiem.Split('-');
                string ngaynhiem = ngaynhiems[2] + "/" + ngaynhiems[1] + "/" + ngaynhiems[0];
                string[] ngayxuatviens = NgayXuatVien.Split('-');
                string ngayxuatvien = ngayxuatviens[2] + "/" + ngayxuatviens[1] + "/" + ngayxuatviens[0];

                cmd.Parameters.Add(new SqlParameter("@MaQR", MaQR));
                cmd.Parameters.Add(new SqlParameter("@BiF0", BiF0));
                cmd.Parameters.Add(new SqlParameter("@NgayNhiem", ngaynhiem));
                cmd.Parameters.Add(new SqlParameter("@NgayXuatVien", ngayxuatvien));
                cmd.ExecuteNonQuery();
            }
        }
        public void UpdateLyDoKhacKhongTiem(string MaQR, string LyDoKhac)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("UpdateLyDoKhacKhongTiem", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@MaQR", MaQR));
                cmd.Parameters.Add(new SqlParameter("@LyDoKhac", LyDoKhac));
                cmd.ExecuteNonQuery();
            }
        }
        public void UpdateTiemTaiDiaPhuong(string MaQR, string Mui_TenVacXin, string Mui_NgayTiem, string Mui_SoLo, string Mui_DiaDiem, string Mui_DiaDiem_Old)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string[] ngaytiems = Mui_NgayTiem.Split('-');
                string ngaytiem = ngaytiems[2] + "/" + ngaytiems[1] + "/" + ngaytiems[0];

                SqlCommand cmd = new SqlCommand("UpdateTiemTaiDiaPhuong", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@MaQR", MaQR));
                cmd.Parameters.Add(new SqlParameter("@Mui_TenVacXin", Mui_TenVacXin));
                cmd.Parameters.Add(new SqlParameter("@Mui_NgayTiem", ngaytiem));
                cmd.Parameters.Add(new SqlParameter("@Mui_SoLo", Mui_SoLo!=null ? Mui_SoLo : "")); 
                cmd.Parameters.Add(new SqlParameter("@Mui_DiaDiem", Mui_DiaDiem != null ? Mui_DiaDiem : ""));
                cmd.Parameters.Add(new SqlParameter("@Mui_DiaDiem_Old", Mui_DiaDiem_Old != null ? Mui_DiaDiem_Old : ""));
                cmd.ExecuteNonQuery();
            }
        }
        public void LuuThongTinPhanAnh(string DienThoai, string HoTen, string CMND, string NgaySinh, string ThangSinh, string NamSinh, string TenVacXin
            , DateTime NgayTiem, string TinhThanhPho, string QuanHuyen, string DiaDiem, string SoLo)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("LuuThongTinPhanAnh", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@DienThoai", DienThoai));
                cmd.Parameters.Add(new SqlParameter("@HoTen", HoTen));
                cmd.Parameters.Add(new SqlParameter("@CMND", CMND));
                cmd.Parameters.Add(new SqlParameter("@NgaySinh", NgaySinh));
                cmd.Parameters.Add(new SqlParameter("@ThangSinh", ThangSinh));
                cmd.Parameters.Add(new SqlParameter("@NamSinh", NamSinh));
                cmd.Parameters.Add(new SqlParameter("@TenVacXin", TenVacXin));
                cmd.Parameters.Add(new SqlParameter("@NgayTiem", NgayTiem.Date.ToString("dd/MM/yyyy")));
                cmd.Parameters.Add(new SqlParameter("@TinhThanhPho", TinhThanhPho));
                cmd.Parameters.Add(new SqlParameter("@QuanHuyen", QuanHuyen));
                cmd.Parameters.Add(new SqlParameter("@DiaDiem", DiaDiem));
                cmd.Parameters.Add(new SqlParameter("@SoLo", SoLo!=null ? SoLo : ""));
                cmd.ExecuteNonQuery();
            }
        }
        public bool UpdateThongTinF0(int IdStaff, string NgayBiNhiem, string NgayXuatVien)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                if(!string.IsNullOrEmpty(NgayBiNhiem) || !string.IsNullOrEmpty(NgayXuatVien))
                    commandText = "update staff set BiF0=1, NgayBiNhiem='" + NgayBiNhiem + "',NgayXuatVien='"+ NgayXuatVien + "' where Id="+IdStaff;
                else
                    commandText = "update staff set BiF0=0, NgayBiNhiem='" + NgayBiNhiem + "',NgayXuatVien='" + NgayXuatVien + "' where Id=" + IdStaff;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        public bool CheckNhanVien_DonVi(int IdDonVi, string CMND) {
           
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from staff where cmnd = '" + CMND + "' and DonViChiTiet = (select TenDonVi from DonViChiTiet where Id = " + IdDonVi + ")";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            if (dt.Rows.Count > 0) return true;
            return false;
        }
        public int CheckTrangThaiDangKy(int IdStaff) 
        { 
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select DangKyTiemMui3 from staff where Id=" + IdStaff;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["DangKyTiemMui3"] != null)
                    return Convert.ToInt32(dt.Rows[0]["DangKyTiemMui3"].ToString());
            }
            return 0;
        }
        public DataTable GetThongTinMui1(int IdStaff)
        {
           
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select CASE WHEN Mui1_TenVacxin is not null and Mui1_TenVacxin != '' THEN Mui1_TenVacxin " +
                    " ELSE ISNULL(ISNULL(Mui1_TenVacxin11, Mui1_TenVacxin1), Mui1_TenVacxin) END TenVacxin" +
                    " , CASE WHEN Mui1_NgayTiem is not null and Mui1_NgayTiem != '' THEN Mui1_NgayTiem " +
                    " ELSE ISNULL(ISNULL(Mui1_NgayTiem11, Mui1_NgayTiem1), Mui1_NgayTiem) END NgayTiem" +
                    " , CASE WHEN Mui1_SoLo is not null and Mui1_SoLo != '' THEN Mui1_SoLo " +
                    " ELSE ISNULL(ISNULL(Mui1_SoLo11, Mui1_SoLo1), Mui1_SoLo) END SoLo" +
                    " , CASE WHEN Mui1_DiaDiem is not null and Mui1_DiaDiem != '' THEN Mui1_DiaDiem " +
                    " ELSE ISNULL(ISNULL(Mui1_DiaDiem11, Mui1_DiaDiem1), Mui1_DiaDiem) END DiaDiem" +
                    " , isnull(Mui1_LyDo, '') LyDo from staff where Id=" + IdStaff;
                //string commandText = "select isnull(Mui1_TenVacxin,'') TenVacxin,isnull(Mui1_NgayTiem,'') NgayTiem,isnull(Mui1_SoLo,'') SoLo,isnull(Mui1_DiaDiem,'') DiaDiem,isnull(Mui1_LyDo,'') LyDo  from staff where Id=" + IdStaff;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetThongTinMui(string MaQR)
        {
           
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select BiF0,Mui_TenVacXin,Mui_NgayTiem,Mui_SoLo,Mui_DiaDiem,NgayNhiem,NgayXuatVien,DaNghiViec,LyDoKhac from danhsachtiem where MaQR='" + MaQR+"'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetThongTinMui2(int IdStaff)
        {
           
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select CASE WHEN Mui2_TenVacxin is not null and Mui2_TenVacxin != '' THEN Mui2_TenVacxin " +
                    " ELSE ISNULL(ISNULL(Mui2_TenVacxin11, Mui2_TenVacxin1), Mui2_TenVacxin) END TenVacxin" +
                    " , CASE WHEN Mui2_NgayTiem is not null and Mui2_NgayTiem != '' THEN Mui2_NgayTiem " +
                    " ELSE ISNULL(ISNULL(Mui2_NgayTiem11, Mui2_NgayTiem1), Mui2_NgayTiem) END NgayTiem" +
                    " , CASE WHEN Mui2_SoLo is not null and Mui2_SoLo != '' THEN Mui2_SoLo " +
                    " ELSE ISNULL(ISNULL(Mui2_SoLo11, Mui2_SoLo1), Mui2_SoLo) END SoLo" +
                    " , CASE WHEN Mui2_DiaDiem is not null and Mui2_DiaDiem != '' THEN Mui2_DiaDiem " +
                    " ELSE ISNULL(ISNULL(Mui2_DiaDiem11, Mui2_DiaDiem1), Mui2_DiaDiem) END DiaDiem" +
                    " , isnull(Mui2_LyDo, '') LyDo from staff where Id=" + IdStaff;
                //select isnull(Mui2_TenVacxin,'') TenVacxin,isnull(Mui2_NgayTiem,'') NgayTiem,isnull(Mui2_SoLo,'') SoLo,isnull(Mui2_DiaDiem,'') DiaDiem,isnull(Mui2_LyDo,'') LyDo  from staff where Id=" + IdStaff;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetThongTinMui3(int IdStaff)
        {
           
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select isnull(Mui3_TenVacxin,'') TenVacxin,isnull(Mui3_NgayTiem,'') NgayTiem,isnull(Mui3_SoLo,'') SoLo,isnull(Mui3_DiaDiem,'') DiaDiem,isnull(Mui3_LyDo,'') LyDo  from staff where Id=" + IdStaff;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetThongTinF0(int IdStaff)
        {
           
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select isnull(NgayBiNhiem,'') NgayBiNhiem,isnull(NgayXuatVien,'') NgayXuatVien from staff where Id=" + IdStaff;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetDanhSachDiaDiemTiem(string TinhThanhPho, string QuanHuyen)
        {
           
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                if (string.IsNullOrEmpty(TinhThanhPho))
                    commandText = "select distinct Ten_CoSo from PL4_DiaDiemTiem where Ten_CoSo='' order by Ten_CoSo";
                else
                {
                    if(string.IsNullOrEmpty(QuanHuyen))
                        commandText = "select distinct Ten_CoSo from PL4_DiaDiemTiem where TinhThanhPho=N'" + TinhThanhPho + "' order by Ten_CoSo";
                    else
                        commandText = "select distinct Ten_CoSo from PL4_DiaDiemTiem where TinhThanhPho=N'" + TinhThanhPho + "' and QuanHuyen=N'" + QuanHuyen + "' order by Ten_CoSo";
                }
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public List<PL3_DanhMucHanhChinh> GetAllProvice()
        {
            var lists = new List<PL3_DanhMucHanhChinh>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select distinct Ma_TinhThanhPho,Ten_TinhThanhPho from PL3_DanhMucHanhChinh order by Ma_TinhThanhPho";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new PL3_DanhMucHanhChinh
                    {
                        Ma_TinhThanhPho = reader["Ma_TinhThanhPho"] != null && reader["Ma_TinhThanhPho"] != DBNull.Value ? reader["Ma_TinhThanhPho"].ToString() : "",
                        Ten_TinhThanhPho = reader["Ten_TinhThanhPho"] != null && reader["Ten_TinhThanhPho"] != DBNull.Value ? reader["Ten_TinhThanhPho"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        public List<PL5_DanhMucVacxin> GetAllTenVacXin()
        {
            var lists = new List<PL5_DanhMucVacxin>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from PL5_DanhMucVacxin";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new PL5_DanhMucVacxin
                    {
                        TenVacxin = reader["TenVacxin"] != null && reader["TenVacxin"] != DBNull.Value ? reader["TenVacxin"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        public List<PL3_DanhMucHanhChinh> GetAllDistrictByProvice(string Ten_TinhThanhPho)
        {
            var lists = new List<PL3_DanhMucHanhChinh>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select distinct Ma_QuanHuyen,Ten_QuanHuyen from PL3_DanhMucHanhChinh where Ten_TinhThanhPho=N'" + Ten_TinhThanhPho + "' order by Ma_QuanHuyen";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var list = new PL3_DanhMucHanhChinh
                    {
                        Ma_QuanHuyen = reader["Ma_QuanHuyen"] != null && reader["Ma_QuanHuyen"] != DBNull.Value ? reader["Ma_QuanHuyen"].ToString() : "",
                        Ten_QuanHuyen = reader["Ten_QuanHuyen"] != null && reader["Ten_QuanHuyen"] != DBNull.Value ? reader["Ten_QuanHuyen"].ToString() : ""
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        public DataTable GetThongKeMuiTiemTheoDonVi(string DonViChiTiet)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("GetThongKeMuiTiemTheoDonVi", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@DonViChiTiet", DonViChiTiet));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
    }
}
