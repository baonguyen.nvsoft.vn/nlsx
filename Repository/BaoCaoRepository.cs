﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using dbChange.Models;
using dbChange.Models.BaoCao;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;

namespace dbChange.Repository{
    public class BaoCaoRepository : IBaoCaoRepository
    {
        private readonly IHubContext<SignalServer> _context;
        string connectionString = "";
        public BaoCaoRepository(IConfiguration configuration,
                                    IHubContext<SignalServer> context)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            _context = context;
        }
     
        public List<ThongKeTiemChungTheoTaiKhoan> GetAll_ThongKeTiemChungTheoTaiKhoan(string TenDangNhap)
        {
            var lists = new List<ThongKeTiemChungTheoTaiKhoan>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                SqlCommand cmd = new SqlCommand("ThongKeTiemChungTheoTaiKhoan", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@TenDangNhap", TenDangNhap));

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new ThongKeTiemChungTheoTaiKhoan
                    {                        
                        DonViChiTiet = reader["DonViChiTiet"].ToString(),
                        Mui1_DaTiem = Convert.ToInt32(reader["Mui1_DaTiem"]),
                        F0_Mui1_DaTiem = Convert.ToInt32(reader["F0_Mui1_DaTiem"]),
                        Mui1_ChuaTiem = Convert.ToInt32(reader["Mui1_ChuaTiem"]),
                        Mui2_DaTiem = Convert.ToInt32(reader["Mui2_DaTiem"]),
                        F0_Mui2_DaTiem = Convert.ToInt32(reader["F0_Mui2_DaTiem"]),
                        Mui2_ChuaTiem = Convert.ToInt32(reader["Mui2_ChuaTiem"]),
                        F0 = Convert.ToInt32(reader["F0"]),
                        Tong_NguoiTheoDonVi = Convert.ToInt32(reader["Tong_NguoiTheoDonVi"]),
                        TiLe_Mui1_DaTiem = Convert.ToDouble(reader["TiLe_Mui1_DaTiem"]),
                        TiLe_F0_Mui1_DaTiem = Convert.ToDouble(reader["TiLe_F0_Mui1_DaTiem"]),
                        TiLe_Mui1_ChuaTiem = Convert.ToDouble(reader["TiLe_Mui1_ChuaTiem"]),
                        TiLe_Mui2_DaTiem = Convert.ToDouble(reader["TiLe_Mui2_DaTiem"]),
                        TiLe_F0_Mui2_DaTiem = Convert.ToDouble(reader["TiLe_F0_Mui2_DaTiem"]),
                        TiLe_Mui2_ChuaTiem = Convert.ToDouble(reader["TiLe_Mui2_ChuaTiem"]),
                        TiLe_F0 = Convert.ToDouble(reader["TiLe_F0"]),
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public List<ThongKeTiemChungTheoTaiKhoan_Theo_DonViTong> GetAll_ThongKeTiemChungTheoTaiKhoan_Theo_DonViTong(string TenDangNhap)
        {
            var lists = new List<ThongKeTiemChungTheoTaiKhoan_Theo_DonViTong>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                SqlCommand cmd = new SqlCommand("ThongKeTiemChungTheoTaiKhoan_Theo_DonViTong", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@TenDangNhap", TenDangNhap));

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new ThongKeTiemChungTheoTaiKhoan_Theo_DonViTong
                    {
                        Nhom = reader["Nhom"].ToString(),
                        Mui1_DaTiem = Convert.ToInt32(reader["Mui1_DaTiem"]),
                        F0_Mui1_DaTiem = Convert.ToInt32(reader["F0_Mui1_DaTiem"]),
                        Mui1_ChuaTiem = Convert.ToInt32(reader["Mui1_ChuaTiem"]),
                        Mui2_DaTiem = Convert.ToInt32(reader["Mui2_DaTiem"]),
                        F0_Mui2_DaTiem = Convert.ToInt32(reader["F0_Mui2_DaTiem"]),
                        Mui2_ChuaTiem = Convert.ToInt32(reader["Mui2_ChuaTiem"]),
                        F0 = Convert.ToInt32(reader["F0"]),
                        Tong_NguoiTheoNhom = Convert.ToInt32(reader["Tong_NguoiTheoNhom"]),
                        TiLe_Mui1_DaTiem = Convert.ToDouble(reader["TiLe_Mui1_DaTiem"]),
                        TiLe_F0_Mui1_DaTiem = Convert.ToDouble(reader["TiLe_F0_Mui1_DaTiem"]),
                        TiLe_Mui1_ChuaTiem = Convert.ToDouble(reader["TiLe_Mui1_ChuaTiem"]),
                        TiLe_Mui2_DaTiem = Convert.ToDouble(reader["TiLe_Mui2_DaTiem"]),
                        TiLe_F0_Mui2_DaTiem = Convert.ToDouble(reader["TiLe_F0_Mui2_DaTiem"]),
                        TiLe_Mui2_ChuaTiem = Convert.ToDouble(reader["TiLe_Mui2_ChuaTiem"]),
                        TiLe_F0 = Convert.ToDouble(reader["TiLe_F0"]),
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }

        public List<ThongKeMuiTiem_Theo_DonViTong> GetAll_ThongKeMuiTiem_Theo_DonViTong(string TenDangNhap)
        {
            var lists = new List<ThongKeMuiTiem_Theo_DonViTong>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                SqlCommand cmd = new SqlCommand("ThongKeMuiTiem_Theo_DonViTong_TaiKhoan", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TenDangNhap", TenDangNhap));
             
                ////SqlDependency dependency = new SqlDependency(cmd);

                ////dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new ThongKeMuiTiem_Theo_DonViTong
                    {
                        DonViChiTiet = reader["DonViChiTiet"]!= DBNull.Value? reader["DonViChiTiet"].ToString():"",
                        DonViNhom = reader["DonViNhom"]!= DBNull.Value? reader["DonViNhom"].ToString():"",
                        NhomHienThi = reader["NhomHienThi"]!= DBNull.Value? reader["NhomHienThi"].ToString():"",
                        ChiTietHienThi = reader["ChiTietHienThi"] != DBNull.Value ? reader["ChiTietHienThi"].ToString():"",
                        Mui1_ChuaTiem_SL = reader["Mui1_ChuaTiem_SL"] != DBNull.Value ? Convert.ToInt32(reader["Mui1_ChuaTiem_SL"]):0,
                        Mui1_DaTiem_SL = reader["Mui1_DaTiem_SL"]!= DBNull.Value ? Convert.ToInt32(reader["Mui1_DaTiem_SL"]):0,
                        Mui2_ChuaTiem_SL = reader["Mui2_ChuaTiem_SL"] != DBNull.Value ? Convert.ToInt32(reader["Mui2_ChuaTiem_SL"]):0,
                        Mui2_DaTiem_SL = reader["Mui2_DaTiem_SL"] != DBNull.Value ? Convert.ToInt32(reader["Mui2_DaTiem_SL"]):0,
                        SoLuongF0 = reader["SoLuongF0"] != DBNull.Value ? Convert.ToInt32(reader["SoLuongF0"]):0,
                        TongSoLaoDong = reader["TongSoLaoDong"] != DBNull.Value? Convert.ToInt32(reader["TongSoLaoDong"]):0,
                        Mui1_KeHoachTiemTiepTheo = reader["Mui1_KeHoachTiemTiepTheo"] != DBNull.Value ? reader["Mui1_KeHoachTiemTiepTheo"].ToString(): "",
                        Mui2_KeHoachTiemTiepTheo = reader["Mui2_KeHoachTiemTiepTheo"] != DBNull.Value ? reader["Mui2_KeHoachTiemTiepTheo"].ToString() : "",
                        GhiChu = reader["GhiChu"] != DBNull.Value? reader["GhiChu"].ToString():"",                       
                        TiLe_Mui1_ChuaTiem_SL = reader["TiLe_Mui1_ChuaTiem_SL"]!= DBNull.Value ? Convert.ToDouble(reader["TiLe_Mui1_ChuaTiem_SL"]):0,
                        TiLe_Mui1_DaTiem_SL = reader["TiLe_Mui1_DaTiem_SL"] != DBNull.Value ? Convert.ToDouble(reader["TiLe_Mui1_DaTiem_SL"]) : 0,
                        TiLe_Mui2_ChuaTiem_SL = reader["TiLe_Mui2_ChuaTiem_SL"]!= DBNull.Value?Convert.ToDouble(reader["TiLe_Mui2_ChuaTiem_SL"]):0,
                        TiLe_Mui2_DaTiem_SL = reader["TiLe_Mui2_DaTiem_SL"] != DBNull.Value? Convert.ToDouble(reader["TiLe_Mui2_DaTiem_SL"]):0,
                        TiLe_SoLuongF0 = reader["TiLe_SoLuongF0"] != DBNull.Value ? Convert.ToDouble(reader["TiLe_SoLuongF0"]):0,
                        Id = reader["Id"] != DBNull.Value ? Convert.ToInt64(reader["Id"]):0
                        
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
        public List<ThongKeMuiTiem_Theo_DonViTong> GetAll_ThongKeMuiTiem_Tong_TheoTaiKhoan(string TenDangNhap)
        {
            var lists = new List<ThongKeMuiTiem_Theo_DonViTong>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                SqlCommand cmd = new SqlCommand("ThongKeMuiTiem_Theo_DonViTong", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TenDangNhap", TenDangNhap));

             

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var list = new ThongKeMuiTiem_Theo_DonViTong
                    {
                      
                        ChiTietHienThi = reader["ChiTietHienThi"] != DBNull.Value ? reader["ChiTietHienThi"].ToString() : "",
                        Mui1_ChuaTiem_SL = reader["Mui1_ChuaTiem_SL"] != DBNull.Value ? Convert.ToInt32(reader["Mui1_ChuaTiem_SL"]) : 0,
                        Mui1_DaTiem_SL = reader["Mui1_DaTiem_SL"] != DBNull.Value ? Convert.ToInt32(reader["Mui1_DaTiem_SL"]) : 0,
                        Mui2_ChuaTiem_SL = reader["Mui2_ChuaTiem_SL"] != DBNull.Value ? Convert.ToInt32(reader["Mui2_ChuaTiem_SL"]) : 0,
                        Mui2_DaTiem_SL = reader["Mui2_DaTiem_SL"] != DBNull.Value ? Convert.ToInt32(reader["Mui2_DaTiem_SL"]) : 0,
                        SoLuongF0 = reader["SoLuongF0"] != DBNull.Value ? Convert.ToInt32(reader["SoLuongF0"]) : 0,
                        TongSoLaoDong = reader["TongSoLaoDong"] != DBNull.Value ? Convert.ToInt32(reader["TongSoLaoDong"]) : 0,
                        Mui1_KeHoachTiemTiepTheo = reader["Mui1_KeHoachTiemTiepTheo"] != DBNull.Value ? reader["Mui1_KeHoachTiemTiepTheo"].ToString() : "",
                        Mui2_KeHoachTiemTiepTheo = reader["Mui2_KeHoachTiemTiepTheo"] != DBNull.Value ? reader["Mui2_KeHoachTiemTiepTheo"].ToString() : "",
                        GhiChu = reader["GhiChu"] != DBNull.Value ? reader["GhiChu"].ToString() : "",
                        TiLe_Mui1_ChuaTiem_SL = reader["TiLe_Mui1_ChuaTiem_SL"] != DBNull.Value ? Math.Round(Convert.ToDouble(reader["TiLe_Mui1_ChuaTiem_SL"]),2) : 0,
                        TiLe_Mui1_DaTiem_SL = reader["TiLe_Mui1_DaTiem_SL"] != DBNull.Value ? Math.Round(Convert.ToDouble(reader["TiLe_Mui1_DaTiem_SL"]), 2) : 0,
                        TiLe_Mui2_ChuaTiem_SL = reader["TiLe_Mui2_ChuaTiem_SL"] != DBNull.Value ? Math.Round(Convert.ToDouble(reader["TiLe_Mui2_ChuaTiem_SL"]), 2) : 0,
                        TiLe_Mui2_DaTiem_SL = reader["TiLe_Mui2_DaTiem_SL"] != DBNull.Value ? Math.Round(Convert.ToDouble(reader["TiLe_Mui2_DaTiem_SL"]), 2) : 0,
                        TiLe_SoLuongF0 = reader["TiLe_SoLuongF0"] != DBNull.Value ? Math.Round(Convert.ToDouble(reader["TiLe_SoLuongF0"]),2) : 0,
                        Id = reader["Id"] != DBNull.Value ? Convert.ToInt64(reader["Id"]) : 0,
                        ParentId = reader["ParentId"] != DBNull.Value ? Convert.ToInt64(reader["ParentId"]) : 0,
                        IDMau = reader["IDMau"] != DBNull.Value ? Convert.ToInt32(reader["IDMau"]) : 0,

                        //Cột F0
                        Mui1_ChuaTiem_SL_F0 = reader["Mui1_ChuaTiem_SL"] != DBNull.Value ? Convert.ToString(reader["Mui1_ChuaTiem_SL_F0"]) : "-",
                        Mui1_DaTiem_SL_F0 = reader["Mui1_DaTiem_SL"] != DBNull.Value ? Convert.ToString(reader["Mui1_DaTiem_SL_F0"]) : "-",
                        Mui2_ChuaTiem_SL_F0 = reader["Mui2_ChuaTiem_SL"] != DBNull.Value ? Convert.ToString(reader["Mui2_ChuaTiem_SL_F0"]) : "-",
                        Mui2_DaTiem_SL_F0 = reader["Mui2_DaTiem_SL"] != DBNull.Value ? Convert.ToString(reader["Mui2_DaTiem_SL_F0"]) : "-",
                        TongSoLaoDong_F0 = reader["TongSoLaoDong"] != DBNull.Value ? Convert.ToString(reader["TongSoLaoDong_F0"]) : "-",
                        TiLe_Mui1_ChuaTiem_SL_F0 = reader["TiLe_Mui1_ChuaTiem_SL"] != DBNull.Value ? Convert.ToString(reader["TiLe_Mui1_ChuaTiem_SL_F0"]) : "-",
                        TiLe_Mui1_DaTiem_SL_F0 = reader["TiLe_Mui1_DaTiem_SL"] != DBNull.Value ? Convert.ToString(reader["TiLe_Mui1_DaTiem_SL_F0"]) : "-",
                        TiLe_Mui2_ChuaTiem_SL_F0 = reader["TiLe_Mui2_ChuaTiem_SL"] != DBNull.Value ? Convert.ToString(reader["TiLe_Mui2_ChuaTiem_SL_F0"]) : "-",
                        TiLe_Mui2_DaTiem_SL_F0 = reader["TiLe_Mui2_DaTiem_SL"] != DBNull.Value ? Convert.ToString(reader["TiLe_Mui2_DaTiem_SL_F0"]) : "-"
                    };

                    lists.Add(list);
                }
            }

            return lists;
        }
       public  DataTable GetDanhSachNhanVienDiLamTheoThangNam(long DonViChiTiet, long BoPhan, int Thang, int Nam)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("DanhSachNhanVienDiLam_Theo_DonVi_BoPhan_Thang_Nam_New", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@DonVi", DonViChiTiet));
                cmd.Parameters.Add(new SqlParameter("@BoPhan", BoPhan));
                cmd.Parameters.Add(new SqlParameter("@Month", Thang));
                cmd.Parameters.Add(new SqlParameter("@Year", Nam));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
        public bool UpdateThongKe(int key, string values)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                SqlCommand cmd = new SqlCommand("Update_ThongKeMuiTiem_By_ID", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandType = CommandType.StoredProcedure;
             
                values = values.Replace('{', ' ');
                values = values.Replace('}', ' ');
                string[] pairs = values.Split(",");
                string update = "";
                for(int i =0; i < pairs.Length;i++)
                {
                    update += (pairs[i].Split(":")[0].Replace('"',' ')) + " = ";
                    if(pairs[i].Split(":")[0].Replace('"', ' ').Contains("Mui1_KeHoachTiemTiepTheo")|| pairs[i].Split(":")[0].Replace('"', ' ').Contains("Mui2_KeHoachTiemTiepTheo") || pairs[i].Split(":")[0].Replace('"', ' ').Contains("GhiChu"))
                        update += 'N'+pairs[i].Split(":")[1].Replace("\"","'")+",";
                    else
                        update += pairs[i].Split(":")[1].Replace("\"", "'") + ",";

                }
                update = update.Substring(0, update.Length - 1);
                cmd.Parameters.Add(new SqlParameter("@values", update));
                cmd.Parameters.Add(new SqlParameter("@Id", key));
                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                    return true;

            }
                return false;
        }

        private void dbChangeNotification(object sender, SqlNotificationEventArgs e)
        {
            Console.WriteLine(e.Info);
          //  _context.Clients.All.SendAsync("refreshEmployees");
        }

        public DataTable GetBaoCaoDanhSachDiLamTuan(DateTime tungay, DateTime denngay, string lsDonViChiTietId, string lsBoPhanId)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                //string lsNgay = "[T2],[T3],[T4],[T5],[T6],[T7]";

                SqlCommand cmd = new SqlCommand("GetBaoCaoDanhSachDiLamTuan", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@tungay", tungay));
                cmd.Parameters.Add(new SqlParameter("@denngay", denngay));
                cmd.Parameters.Add(new SqlParameter("@lsDonViChiTietId", lsDonViChiTietId));
                cmd.Parameters.Add(new SqlParameter("@lsBoPhanId", lsBoPhanId));
                //cmd.Parameters.Add(new SqlParameter("@lsNgay", lsNgay));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
        public DataTable GetBangTongHopNhanVienLamViec(DateTime tungay, DateTime denngay)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                DateTime tungayT = tungay.AddDays(-7);
                DateTime denngayT = denngay.AddDays(-7);
                SqlCommand cmd = new SqlCommand("GetBangTongHopNhanVienLamViec", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@tungayT", tungayT));
                cmd.Parameters.Add(new SqlParameter("@denngayT", denngayT));
                cmd.Parameters.Add(new SqlParameter("@tungay", tungay));
                cmd.Parameters.Add(new SqlParameter("@denngay", denngay));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
        public DataTable GetThongKePhieuDangKyRaVaoCong(DateTime tungay, DateTime denngay)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetThongKePhieuDangKyRaVaoCong", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@tungay", tungay));
                cmd.Parameters.Add(new SqlParameter("@denngay", denngay));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
        public DataTable GetBaoCaoTang(DateTime tungay, DateTime denngay)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                DateTime tungayT = tungay.AddDays(-7);
                DateTime denngayT = denngay.AddDays(-7);
                SqlCommand cmd = new SqlCommand("GetBaoCaoTang", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@tungayT", tungayT));
                cmd.Parameters.Add(new SqlParameter("@denngayT", denngayT));
                cmd.Parameters.Add(new SqlParameter("@tungay", tungay));
                cmd.Parameters.Add(new SqlParameter("@denngay", denngay));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
        public DataTable GetBaoCaoGiam(DateTime tungay, DateTime denngay)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                DateTime tungayT = tungay.AddDays(-7);
                DateTime denngayT = denngay.AddDays(-7);
                SqlCommand cmd = new SqlCommand("GetBaoCaoGiam", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@tungayT", tungayT));
                cmd.Parameters.Add(new SqlParameter("@denngayT", denngayT));
                cmd.Parameters.Add(new SqlParameter("@tungay", tungay));
                cmd.Parameters.Add(new SqlParameter("@denngay", denngay));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
        public DataTable GetBaoCaoThongTinYTe(DateTime tungay, DateTime denngay, string TenDonVi)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetBaoCaoThongTinYTe", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@tungay", tungay));
                cmd.Parameters.Add(new SqlParameter("@denngay", denngay));
                cmd.Parameters.Add(new SqlParameter("@TenDonViChiTiet", TenDonVi!=null?TenDonVi:""));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
        public DataTable GetBaoCaoThongTinYTeTang(DateTime tungay, DateTime denngay, string TenDonVi)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                DateTime tungayT = tungay.AddDays(-7);
                DateTime denngayT = denngay.AddDays(-7);
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetBaoCaoThongTinYTeTang", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@tungayT", tungayT));
                cmd.Parameters.Add(new SqlParameter("@denngayT", denngayT));
                cmd.Parameters.Add(new SqlParameter("@tungay", tungay));
                cmd.Parameters.Add(new SqlParameter("@denngay", denngay));
                cmd.Parameters.Add(new SqlParameter("@TenDonViChiTiet", TenDonVi!=null?TenDonVi:""));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
        public DataTable GetBaoCaoRaVaoCongTheoDonVi(DateTime tungay, string TenDonVi)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("BAO_CAO_RA_VAO_DON_VI", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@NgayBaoCao", tungay));
                cmd.Parameters.Add(new SqlParameter("@TenDonVi", TenDonVi));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
        public DataTable GetBaoCaoDanhSachChuaKhaiBaoYTe(DateTime tungay, string TenDonVi)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("BAO_CAO_RA_VAO_CHUAKHAIBAOYTE_REPORT", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@NgayBaoCao", tungay));
                cmd.Parameters.Add(new SqlParameter("@DonViChiTiet", TenDonVi!=null ? TenDonVi : ""));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;

        }
    }
}