﻿using dbChange.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public class StatisticVacxinRepository : IStatisticVacxinRepository
    {

        private readonly IHubContext<SignalServer> _context;
        string connectionString = "";
        public StatisticVacxinRepository(IConfiguration configuration,
                                    IHubContext<SignalServer> context)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            _context = context;
        }
        public List<StatisticVacxin> GetAllStatistic(string tendangnhap)
        {
            var StatisticVacxins = new List<StatisticVacxin>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select StatisticVacxin.* from StatisticVacxin  where DonViChiTiet in (select distinct DonViChiTiet from Account_DonViChiTiet where TenDangNhap = '" + tendangnhap + "')";
                 

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var statisticVacxin = new StatisticVacxin
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        DonVi = reader["DonVi"].ToString(),
                        DonViChiTiet = reader["DonViChiTiet"].ToString(),
                        HoTen = reader["HoTen"].ToString(),
                        GioiTinh = reader["GioiTinh"].ToString(),
                        NgaySinh = reader["NgaySinh"].ToString(),
                        Mui1_DaTiem = reader["Mui1_DaTiem"].ToString(),
                        Mui1_ChuaTiem = reader["Mui1_ChuaTiem"].ToString(),
                        Mui1_LyDo = reader["Mui1_LyDo"].ToString(),
                        Mui2_DaTiem = reader["Mui2_DaTiem"].ToString(),
                        Mui2_ChuaTiem = reader["Mui2_ChuaTiem"].ToString(),
                        Mui2_LyDo = reader["Mui2_LyDo"].ToString(),
                        F0 = reader["F0"].ToString(),
                        SoDienThoai = reader["SoDienThoai"].ToString(),
                        GhiChu = reader["GhiChu"].ToString(),
                        NgayBoSung = reader["NgayBoSung"].ToString(),
                    };

                    StatisticVacxins.Add(statisticVacxin);
                }
            }

            return StatisticVacxins;
        }
        public List<StatisticVacxin> GetAllStatistic()
        {
            var StatisticVacxins = new List<StatisticVacxin>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select * from StatisticVacxin";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var statisticVacxin = new StatisticVacxin
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        DonVi = reader["DonVi"].ToString(),
                        DonViChiTiet = reader["DonViChiTiet"].ToString(),
                        HoTen = reader["HoTen"].ToString(),
                        GioiTinh = reader["GioiTinh"].ToString(),
                        NgaySinh = reader["NgaySinh"].ToString(),
                        Mui1_DaTiem = reader["Mui1_DaTiem"].ToString(),
                        Mui1_ChuaTiem = reader["Mui1_ChuaTiem"].ToString(),
                        Mui1_LyDo = reader["Mui1_LyDo"].ToString(),
                        Mui2_DaTiem = reader["Mui2_DaTiem"].ToString(),
                        Mui2_ChuaTiem = reader["Mui2_ChuaTiem"].ToString(),
                        Mui2_LyDo = reader["Mui2_LyDo"].ToString(),
                        F0 = reader["F0"].ToString(),
                        SoDienThoai = reader["SoDienThoai"].ToString(),
                        GhiChu = reader["GhiChu"].ToString(),
                        NgayBoSung = reader["NgayBoSung"].ToString(),
                    };

                    StatisticVacxins.Add(statisticVacxin);
                }
            }

            return StatisticVacxins;
        }
        public bool UpdateStatisticVacxin(int id,string values)
        {

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                SqlCommand cmd = new SqlCommand("Update_StatisticVacxin_By_ID", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandType = CommandType.StoredProcedure;

                values = values.Replace('{', ' ');
                values = values.Replace('}', ' ');
                string[] pairs = values.Split(",");
                string update = "";
                for (int i = 0; i < pairs.Length; i++)
                {
                    update += (pairs[i].Split(":")[0].Replace('"', ' ')) + " = ";
                    if (pairs[i].Split(":")[0].Replace('"', ' ').Contains("Mui1_LyDo") || pairs[i].Split(":")[0].Replace('"', ' ').Contains("Mui2_LyDo") || pairs[i].Split(":")[0].Replace('"', ' ').Contains("GhiChu"))
                        update += 'N' + pairs[i].Split(":")[1].Replace("\"", "'") + ",";
                    else
                        update += pairs[i].Split(":")[1].Replace("\"", "'") + ",";

                }
                update = update.Substring(0, update.Length - 1);
                cmd.Parameters.Add(new SqlParameter("@values", update));
                cmd.Parameters.Add(new SqlParameter("@Id", id));
                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                int count = cmd.ExecuteNonQuery();
                return true;

            }
            return false;
        }
        private void dbChangeNotification(object sender, SqlNotificationEventArgs e)
        {
            Console.WriteLine(e.Info);
            //  _context.Clients.All.SendAsync("refreshEmployees");
        }
      
    }
}
