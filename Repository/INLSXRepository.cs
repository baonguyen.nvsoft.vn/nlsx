﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public interface INLSXRepository
    {
        public DataTable GetData_Tong(string lsThang, int Tuan);
        //public DataTable GetData_Tong_Tuan_Thang(int Tuan, int Thang);
        public DataTable GetData_Tong_Tuan_Thang(int Tuan, int Thang,int Nam);
        public DataTable GetData_DonVi(long IdDonVi, string lsThang);
        public DataTable GetData_ChenhLech_Tong(long IdRoot, string TenChungLoai, int TuanCapNhat, int Thang);
        public DataTable GetData_NangLuc_Tong(long IdRoot, string TenChungLoai, int TuanCapNhat, int Thang, int Nam);
        public DataTable GetData_NguonHang_Tong(long IdRoot, string TenChungLoai, int TuanCapNhat, int Thang, string TenDonVi);
        public DataTable GetData_NguonHang_SoSanh(long IdRoot, string TenChungLoai, int TuanCapNhat, int TuanSoSanh, int Thang);
        public DataTable GetData_NangLuc_SoSanh(long IdRoot, string TenChungLoai, int TuanCapNhat, int TuanSoSanh, int Thang, int ThangSoSanh, int Nam, int NamSoSanh);
        public DataTable GetData_ChenhLech_SoSanh(long IdRoot, string TenChungLoai, int TuanCapNhat, int TuanSoSanh, int Thang, int ThangSoSanh, int Nam, int NamSoSanh);
        public DataTable Get_List_Tuan_NangLuc();
        public DataTable Get_List_Tuan_ChenhLech();
        public int GET_TUANCAPNHAT_MOINHAT();
    }
}
