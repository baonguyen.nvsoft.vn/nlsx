﻿using dbChange.Models.VanPhongSo;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public class VanBanChiTietRepository : IVanBanChiTietRepository
    {
        string connectionString = "";
        string Domain = "";
        public VanBanChiTietRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            Domain = configuration.GetConnectionString("Domain");
        }

        public VanBan_ChiTiet CapNhap(VanBan_ChiTiet VanBanChiTiet, long IdNhanVien, string GhiChu)
        {
            VanBan_ChiTiet vanban = new VanBan_ChiTiet();
            vanban = GetById(VanBanChiTiet.Id);
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("VPS_UpdateVanBanChiTiet", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Id", VanBanChiTiet.Id));
                    cmd.Parameters.Add(new SqlParameter("@MaVanBan", VanBanChiTiet.MaVanBan));
                    cmd.Parameters.Add(new SqlParameter("@MaQuyenVanBan", VanBanChiTiet.MaQuyenVanBan));
                    cmd.Parameters.Add(new SqlParameter("@MaQR", VanBanChiTiet.MaQR));
                    cmd.Parameters.Add(new SqlParameter("@TieuDe", VanBanChiTiet.TieuDe));
                    cmd.Parameters.Add(new SqlParameter("@NoiDung", VanBanChiTiet.NoiDung));
                    cmd.Parameters.Add(new SqlParameter("@Link", VanBanChiTiet.Link));
                    cmd.Parameters.Add(new SqlParameter("@IdNguoiTao", VanBanChiTiet.IdNguoiTao));
                    cmd.ExecuteNonQuery();

                    cmd = new SqlCommand("GhiLogVanBanChiTiet", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdVanBanChiTiet", VanBanChiTiet.Id));
                    cmd.Parameters.Add(new SqlParameter("@IdNhanVien", IdNhanVien));
                    cmd.Parameters.Add(new SqlParameter("@NoiDungGoc", Newtonsoft.Json.JsonConvert.SerializeObject(vanban)));
                    cmd.Parameters.Add(new SqlParameter("@NoiDungThayDoi", Newtonsoft.Json.JsonConvert.SerializeObject(VanBanChiTiet)));
                    cmd.Parameters.Add(new SqlParameter("@GhiChu", GhiChu));

                    cmd.ExecuteNonQuery();
                    return vanban;
                }
            }
            catch (Exception ex)
            {
                return vanban;
            }
        }

        public VanBan_ChiTiet GetById(long IdVanBanChiTiet)
        {
            VanBan_ChiTiet lsResult = new VanBan_ChiTiet();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "SELECT * from VPS_VanBan_ChiTiet where Id='" + IdVanBanChiTiet + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lsResult = new VanBan_ChiTiet
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MaVanBan = reader["MaVanBan"] != null && reader["MaVanBan"] != DBNull.Value ? Convert.ToString(reader["MaVanBan"]) : "",
                        MaQuyenVanBan = reader["MaQuyenVanBan"] != null && reader["MaQuyenVanBan"] != DBNull.Value ? Convert.ToString(reader["MaQuyenVanBan"]) : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? Convert.ToString(reader["MaQR"]) : "",
                        TieuDe = reader["TieuDe"] != null && reader["TieuDe"] != DBNull.Value ? Convert.ToString(reader["TieuDe"]) : "",
                        NoiDung = reader["NoiDung"] != null && reader["NoiDung"] != DBNull.Value ? Convert.ToString(reader["NoiDung"]) : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? Convert.ToString(reader["GhiChu"]) : "",
                        Link = reader["Link"] != null && reader["Link"] != DBNull.Value ? Convert.ToString(reader["Link"]) : "",
                        IdNguoiTao = reader["IdNguoiTao"] != null && reader["IdNguoiTao"] != DBNull.Value ? Convert.ToInt64(reader["IdNguoiTao"]) : 0,
                        TenNguoiTao = reader["TenNguoiTao"] != null && reader["TenNguoiTao"] != DBNull.Value ? Convert.ToString(reader["TenNguoiTao"]) : "",
                        NgayTao = reader["NgayTao"] != null && reader["NgayTao"] != DBNull.Value ? Convert.ToDateTime(reader["NgayTao"]) : Utils.Utils.GetTimeServer(),


                    };
                }
            }
            return lsResult;
        }

        public List<VanBan_ChiTiet> GetDanhSach(string MaVanBan)
        {
            List<VanBan_ChiTiet> lsResult = new List<VanBan_ChiTiet>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "SELECT * from VPS_VanBan_ChiTiet where MaVanBan='"+ MaVanBan + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    VanBan_ChiTiet phieu = new VanBan_ChiTiet
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MaVanBan = reader["MaVanBan"] != null && reader["MaVanBan"] != DBNull.Value ? Convert.ToString(reader["MaVanBan"]) : "",
                        MaQuyenVanBan = reader["MaQuyenVanBan"] != null && reader["MaQuyenVanBan"] != DBNull.Value ? Convert.ToString(reader["MaQuyenVanBan"]) : "",
                        //MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? Convert.ToString(reader["MaQR"]) : "",
                        MaQR = reader["MaQR"] != null && reader["MaQR"] != DBNull.Value ? String.Format("data:image/png;base64,{0}", Convert.ToBase64String(Utils.Utils.BitmapToBytes(Utils.Utils.GetQRCode(Domain + "/VanPhongSo/ShareFile?code=" + Utils.Utils.Encrypt(reader["Link"].ToString()) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString()))))) : "",
                        TieuDe = reader["TieuDe"] != null && reader["TieuDe"] != DBNull.Value ? Convert.ToString(reader["TieuDe"]) : "",
                        NoiDung = reader["NoiDung"] != null && reader["NoiDung"] != DBNull.Value ? Convert.ToString(reader["NoiDung"]) : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? Convert.ToString(reader["GhiChu"]) : "",
                        Link = reader["Link"] != null && reader["Link"] != DBNull.Value ? Convert.ToString(reader["Link"]) : "",
                        LinkShare = reader["Link"]!=null && reader["Link"]!=DBNull.Value ? Domain + "/VanPhongSo/ShareFile?code=" + Utils.Utils.Encrypt(reader["Link"].ToString()) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString())  : "",
                        IdNguoiTao = reader["IdNguoiTao"] != null && reader["IdNguoiTao"] != DBNull.Value ? Convert.ToInt64(reader["IdNguoiTao"]) : 0,
                        TenNguoiTao = reader["TenNguoiTao"] != null && reader["TenNguoiTao"] != DBNull.Value ? Convert.ToString(reader["TenNguoiTao"]) : "",
                        NgayTao = reader["NgayTao"] != null && reader["NgayTao"] != DBNull.Value ? Convert.ToDateTime(reader["NgayTao"]) : Utils.Utils.GetTimeServer(),


                    };
                    lsResult.Add(phieu);
                }
            }
            return lsResult;
        }

        public VanBan_ChiTiet TaoMoi(VanBan_ChiTiet VanBanChiTiet)
        {
            VanBan_ChiTiet vanban = new VanBan_ChiTiet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("VPS_CreateVanBanChiTiet", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@MaVanBan", VanBanChiTiet.MaVanBan));
                    cmd.Parameters.Add(new SqlParameter("@MaQuyenVanBan", VanBanChiTiet.MaQuyenVanBan));
                    cmd.Parameters.Add(new SqlParameter("@MaQR", VanBanChiTiet.MaQR));
                    cmd.Parameters.Add(new SqlParameter("@TieuDe", VanBanChiTiet.TieuDe));
                    cmd.Parameters.Add(new SqlParameter("@NoiDung", VanBanChiTiet.NoiDung));
                    cmd.Parameters.Add(new SqlParameter("@IdNguoiTao", VanBanChiTiet.IdNguoiTao));
                    SqlParameter outputIdParam = new SqlParameter("@new_identity", SqlDbType.BigInt)
                    {
                        Direction = ParameterDirection.Output
                    };
                    cmd.Parameters.Add(outputIdParam);
                    cmd.ExecuteNonQuery();
                    return vanban;
                }
            }
            catch (Exception ex)
            {
                return vanban;
            }
        }

        public void Xoa(long IdVanBanChiTiet,long IdNhanVien, string GhiChu)
        {
            VanBan_ChiTiet vbct = new VanBan_ChiTiet();
            vbct = GetById(IdVanBanChiTiet);
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("VPS_DeleteVanBanChiTiet", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Id", IdVanBanChiTiet));
                    cmd.ExecuteNonQuery();

                    cmd = new SqlCommand("GhiLogVanBanChiTiet", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdVanBanChiTiet", IdVanBanChiTiet));
                    cmd.Parameters.Add(new SqlParameter("@IdNhanVien", IdNhanVien));
                    cmd.Parameters.Add(new SqlParameter("@NoiDungGoc", Newtonsoft.Json.JsonConvert.SerializeObject(vbct)));
                    cmd.Parameters.Add(new SqlParameter("@NoiDungThayDoi", ""));
                    cmd.Parameters.Add(new SqlParameter("@GhiChu", GhiChu));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void Huy(long IdVanBanChiTiet,long IdNhanVien, string GhiChu)
        {
            VanBan_ChiTiet vbct = new VanBan_ChiTiet();
            vbct = GetById(IdVanBanChiTiet);
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("VPS_HuyVanBanChiTiet", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Id", IdVanBanChiTiet));
                    cmd.ExecuteNonQuery();

                    cmd = new SqlCommand("GhiLogVanBanChiTiet", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdVanBanChiTiet", IdVanBanChiTiet));
                    cmd.Parameters.Add(new SqlParameter("@IdNhanVien", IdNhanVien));
                    cmd.Parameters.Add(new SqlParameter("@NoiDungGoc", Newtonsoft.Json.JsonConvert.SerializeObject(vbct)));
                    cmd.Parameters.Add(new SqlParameter("@NoiDungThayDoi", Newtonsoft.Json.JsonConvert.SerializeObject(GetById(IdVanBanChiTiet))));
                    cmd.Parameters.Add(new SqlParameter("@GhiChu", GhiChu));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public DataTable GetAllChungTuChiTiet(long IdNguoiTao, string TrangThai)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select ROW_NUMBER() OVER(ORDER BY STT ASC) AS STT,* " +
                    " from ( select a.Id as IdVanBan,b.MaVanBan + ' - ' + a.TieuDe + ' - ' + a.TrangThai as VanBan,b.*,'' STT from vps_vanban a " +
                    " inner join vps_vanban_chitiet b on a.MaVanBan = b.MaVanBan " +
                    " where a.TrangThai='" + TrangThai + "' and a.IdNguoiTao=" + IdNguoiTao + ") final order by MaVanBan";

                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetAllChungTuChiTiet_Search(string Search, int Option, long IdNhanVien, DateTime TuNgay, DateTime DenNgay, string IdDonVi, string IdLoaiVanBan)
        {
            DataTable dt = new DataTable();
            DataTable dtresult = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("VPS_SearchVanBan", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@TuKhoa", Search));
                    cmd.Parameters.Add(new SqlParameter("@Option", Option));
                    cmd.Parameters.Add(new SqlParameter("@IdNhanVien", IdNhanVien));
                    cmd.Parameters.Add(new SqlParameter("@TuNgay", TuNgay));
                    cmd.Parameters.Add(new SqlParameter("@DenNgay", DenNgay));
                    cmd.Parameters.Add(new SqlParameter("@IdDonVi", IdDonVi));
                    cmd.Parameters.Add(new SqlParameter("@IdLoaiVanBan", IdLoaiVanBan));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);

                    dtresult = dt.Clone();
                    foreach (DataRow reader in dt.Rows)
                    {
                        DataRow drNew = dtresult.NewRow();
                        drNew["IdVanBan"] = reader["IdVanBan"];
                        drNew["MaVanBan"] = reader["MaVanBan"];
                        drNew["TieuDeVanBan"] = reader["TieuDeVanBan"];
                        drNew["NoiDungVanBan"] = reader["NoiDungVanBan"];
                        drNew["TenDonVi"] = reader["TenDonVi"];
                        drNew["TenDangNhap"] = reader["TenDangNhap"];
                        drNew["NgayTao"] = reader["NgayTao"];
                        drNew["Link"] = reader["Link"] != null && reader["Link"] != DBNull.Value ? Domain + "/VanPhongSo/ShareFile?code=" + Utils.Utils.Encrypt(reader["Link"].ToString()) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString()) : "";
                        dtresult.Rows.Add(drNew);
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return dtresult;
        }
        public DataTable GetAllChungTuChiTiet_Search_TruongPhong(string Search, int Option, long IdNhanVien, DateTime TuNgay, DateTime DenNgay, string IdDonVi, string IdLoaiVanBan)
        {
            DataTable dt = new DataTable();
            DataTable dtresult = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("VPS_SearchVanBan_Admin", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@TuKhoa", Search));
                    cmd.Parameters.Add(new SqlParameter("@Option", Option));
                    cmd.Parameters.Add(new SqlParameter("@IdNhanVien", IdNhanVien));
                    cmd.Parameters.Add(new SqlParameter("@TuNgay", TuNgay));
                    cmd.Parameters.Add(new SqlParameter("@DenNgay", DenNgay));
                    cmd.Parameters.Add(new SqlParameter("@IdDonVi", IdDonVi));
                    cmd.Parameters.Add(new SqlParameter("@IdLoaiVanBan", IdLoaiVanBan));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                    dtresult = dt.Clone();
                    foreach (DataRow reader in dt.Rows)
                    {
                        DataRow drNew = dtresult.NewRow();
                        drNew["IdVanBan"] = reader["IdVanBan"];
                        drNew["MaVanBan"] = reader["MaVanBan"];
                        drNew["TieuDeVanBan"] = reader["TieuDeVanBan"];
                        drNew["NoiDungVanBan"] = reader["NoiDungVanBan"];
                        drNew["TenDonVi"] = reader["TenDonVi"];
                        drNew["TenDangNhap"] = reader["TenDangNhap"];
                        drNew["NgayTao"] = reader["NgayTao"];
                        drNew["Link"] = reader["Link"] != null && reader["Link"] != DBNull.Value ? Domain + "/VanPhongSo/ShareFile?code=" + Utils.Utils.Encrypt(reader["Link"].ToString()) + "&time=" + Utils.Utils.Encrypt(DateTime.Now.ToString()) : "";
                        dtresult.Rows.Add(drNew);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return dtresult;
        }
    }
}
