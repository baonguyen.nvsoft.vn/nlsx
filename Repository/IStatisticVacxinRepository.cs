﻿using dbChange.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public interface IStatisticVacxinRepository
    {
        public List<StatisticVacxin> GetAllStatistic();
        public List<StatisticVacxin> GetAllStatistic(string tendangnhap);
        public bool UpdateStatisticVacxin(int id, string values);


    }
}
