﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using dbChange.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;

namespace dbChange.Repository{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly IHubContext<SignalServer> _context;
        string connectionString = "";
        public EmployeeRepository(IConfiguration configuration,
                                    IHubContext<SignalServer> context)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            _context = context;
        }
        public void AddEmployee(Employee emp)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "INSERT INTO NHANVIEN (HoTen,DiaChi,NamSinh) values(N'"+emp.HoTen+"',N'"+emp.DiaChi+"',"+emp.NamSinh+")";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);
               
                var reader = cmd.ExecuteNonQuery();

                
            }
        }
        public List<Employee> GetAllEmployees()
        {
            var employees = new List<Employee>();

            using(SqlConnection conn = new SqlConnection(connectionString)){
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = "select Barcode AS Id, [HoTen] ,[NamSinh],[GioiTinh],[DiaChi],[DienThoai],[CMND],[NgayCapNhat],[KetQuaXN],[DonVi],[Barcode],Checkout from dbo.Employees where BoTiem is null and TamHoan is null order by cast(Barcode as int)";

                SqlCommand cmd = new SqlCommand(commandText,conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange+=new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while(reader.Read()){
                    var employee = new Employee{
                        Id = Convert.ToInt32(reader["Id"]),
                        HoTen = reader["HoTen"].ToString(),
                        NamSinh = reader["NamSinh"].ToString(),
                        GioiTinh = Convert.ToInt32(reader["GioiTinh"])==1 ?"Nữ":"Nam",
                        DiaChi = reader["DiaChi"].ToString(),
                        DienThoai = reader["DienThoai"].ToString(),
                        CMND = reader["CMND"].ToString(),
                       
                        NgayCapNhat = DateTime.Parse(reader["NgayCapNhat"].ToString()).ToString("dd/MM/yyyy HH:mm:ss"),
                        KetQuaXN = Convert.ToBoolean(reader["CheckOut"])==true?( Convert.ToInt32(reader["KetQuaXN"])==1? "Mũi 1" : (Convert.ToInt32(reader["KetQuaXN"]) == 2?"Mũi 2": "Chưa tiêm")):"Chưa tiêm",
                        DonVi = reader["DonVi"].ToString()
                    };

                    employees.Add(employee);
                }
            }

            return employees;
        }
        public List<ThongKe> GetThongKes()
        {
            var ThongKes = new List<ThongKe>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //SqlDependency.Start(connectionString);

                string commandText = 
                   " select DonVi.STT,DonVi.TenDonVi,Count(em.Barcode) DangKy, sum(case when KetQuaXN = 1 and Checkout=1 then 1 else 0 end) as Mui1"
+ " ,sum(case when KetQuaXN = 2 and Checkout=1 then 1 else 0 end) as Mui2"
+ "  ,sum(case when KetQuaXN = 0 then 1 else 0 end) as ChuaTiem"
+ "  ,sum(case when KetQuaXN != 0 then 1 else 0 end) as CheckIn"
+ " ,round(cast((sum(case when KetQuaXN = 1 and Checkout=1 then 1 else 0 end) +sum(case when KetQuaXN = 2 and Checkout=1 then 1 else 0 end) ) as real) *100 / Count(em.Barcode),2) TiLe"
 + " from DonVi"
+ " inner join Employees em on em.DonVi = DonVi.TenDonVi and  BoTiem is null and TamHoan is null"
+ " group by DonVi.STT,DonVi.TenDonVi"
+" order by DonVi.STT";

                SqlCommand cmd = new SqlCommand(commandText, conn);

                //SqlDependency dependency = new SqlDependency(cmd);

                //dependency.OnChange += new OnChangeEventHandler(dbChangeNotification);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var thongke = new ThongKe
                    {
                        stt = Convert.ToInt32(reader["STT"]),
                        donvi = reader["TenDonVi"].ToString(),
                        dangky = Convert.ToInt32(reader["DangKy"]),
                        mui1 = Convert.ToInt32(reader["Mui1"]),
                        mui2 = Convert.ToInt32(reader["Mui2"]),
                        chuatiem = Convert.ToInt32(reader["ChuaTiem"]),
                        checkin = Convert.ToInt32(reader["checkin"]),
                        tile = (reader["TiLe"]).ToString() + "%"

                        
                    };

                    ThongKes.Add(thongke);
                }
            }

            return ThongKes;
        }
        private void dbChangeNotification(object sender, SqlNotificationEventArgs e)
        {
            Console.WriteLine(e.Info);
          //  _context.Clients.All.SendAsync("refreshEmployees");
        }
    }
}