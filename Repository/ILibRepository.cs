using System.Collections.Generic;
using System.Data;
using dbChange.Models.LIB;
using dbChange.Models.VanPhongSo;

namespace dbChange.Repository
{
    public interface ILibRepository
    {
      
        List<PL2_NhomDoiTuong> GetAll_PL2_NhomDoiTuong();
        List<LoaiVanBan> GetAllLoaiVanBan();
        LoaiVanBan GetLoaiVanBan(long IdLoaiVanBan);

        List<PL3_DanhMucHanhChinh> GetAll_PL3_DanhMucHanhChinh();

        List<PL4_DiaDiemTiem> GetAll_PL4_DiaDiemTiem();

        List<PL5_DanhMucVacxin> GetAll_PL5_DanhMucVacxin();
        List<PL3_DanhMucHanhChinh> GetAllProvice();
        List<PL3_DanhMucHanhChinh> GetAllDistrictByProvice(string MaTinh);
        List<PL3_DanhMucHanhChinh> GetAllDistrict();
        List<PL3_DanhMucHanhChinh> GetAllWardByDistrict(string MaQuan);
        List<PL3_DanhMucHanhChinh> GetAllWard();
        List<DM_ToKhuPho> GetAllToKhuPhoByWard(string MaPhuong);
        public List<DM_BoPhanChiTiet> GetAllDonViChiTiet();
        public List<DM_BoPhanChiTiet> GetAllDonViChiTietByUser(string UserName);
        public DM_BoPhanChiTiet GetDonViChiTietByTen(string TenDonVi);
        public DM_BoPhanChiTiet GetDonViChiTietById(int IdDonVi);
        public List<DonViChiTiet_BoPhan> GetAllDonViChiTietBoPhanByDonViChiTietId(int DonViChiTietId);
        public List<ViTriNhaAn> GetAllViTriNhaAn();
        public List<DonViChiTiet_ViTriLamViec> GetAllViTriLamViec(int IdDonViChiTiet);
        public List<DonViChiTiet_PhanNhom> GetAllPhanNhom(int IdDonViChiTiet);
        public List<DonViChiTiet_ViTriVPLamViec> GetAllViTriVPLamViec(int IdDonViChiTiet);
        public List<DonViChiTiet_ViTriVeSinh> GetAllViTriVeSinh(int IdDonViChiTiet);
        public List<DonViChiTiet_ViTriDeXe> GetAllViTriDeXe(int IdDonViChiTiet);

        public DonViChiTiet_ViTriVeSinh GetViTriVeSinhByName(string name, int IdDonVi);
        public DonViChiTiet_ViTriVPLamViec GetViTriVPLamViecByName(string name, int IdDonVi);
        public DonViChiTiet_ViTriLamViec GetViTriLamViecByName(string name, int IdDonVi);
        public ViTriNhaAn GetViTriNhaAnByName(string name);
        public DonViChiTiet_PhanNhom GetPhanNhomByName(string name, int IdDonVi);
        public DonViChiTiet_ViTriDeXe GetViTriDeXeByName(string name, int IdDonVi);
        public List<DonViChiTiet_BoPhan> GetAllBoPhan();
        public List<VanBan_Quyen> GetAllQuyenVanBan();
        public DataTable GetDanhSachDonViPhanQuyen(string TenDangNhap);

    }
}