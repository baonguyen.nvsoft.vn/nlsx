﻿using dbChange.Models;
using dbChange.Models.LIB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public interface IStaffRepository
    {
        public List<Staff> GetAllStaffs(string account);
        public List<Staff> GetAllStaffByDonViBoPhan(string TenDangNhap,string DonVi, string BoPhan);
        public List<Staff> GetAllStaffByDonViBoPhan2(string TenDangNhap,int[] DonVi, string BoPhan);
        public Dictionary<string, int> GetDataVungCovidChart(string TenDangNhap, string DonVi, string BoPhan);
        public Dictionary<string, int> GetDataVungCovidChart2(string TenDangNhap, int[] DonVi, string BoPhan);
        public Staff GetStaff(string id);
        public void UpdateLinkGCN(string url, string id);
        public void UpdateStaff(int id, string hoten);
        public List<PL2_NhomDoiTuong> GetAll_PL2_NhomDoiTuong();
        public bool UpdateStaffThongTinNhanVien(Staff staff);
        public bool UpdateStaffThongTinNhanVien_Temp(Staff staff);
        public bool UpdateStaffDiacChiNhanVien(Staff staff);
        public bool UpdateStaffDiacChiNhanVien_Temp(Staff staff);
        public bool UpdateMui1Staff(Staff staff);
        public bool UpdateMui1PCCovid(string IdStaff, int LoaiMuiTiem, string Mui1_TenVacxin1, string Mui1_SoLo1, string Mui1_NgayTiem1, string Mui1_DiaDiem1, string Mui1_LyDo, string Mui1_DiaDiem1_Old);
        public bool UpdateMui1PCCovid_KhongTiem(string IdStaff, int LoaiMuiTiem, string Mui1_LyDo);
        public void UpdateDaNghiViec(string maQR, bool DaNghiViec);
        public void UpdateDaNghi(string IdStaff);
        public void GhiLogCapNhat(long IdNguoiCapNhat, string TenNguoiCapNhat, string NoiDungTruocCapNhat, string NoiDungSauCapNhat);
        public DataTable GetThongTinTruocCapNhatBiF0(int IdStaff);
        public DataTable GetThongTinTruocCapNhatMuiTiem(string IdStaff, int LoaiMuiTiem);
        public void LuuThongTinMui_XoaThongTinTiem(string StaffId, int LoaiMuiTiem);
        public void UpdateBiF0(string MaQR, bool BiF0, string NgayNhiem, string NgayXuatVien);
        public void UpdateLyDoKhacKhongTiem(string maQR, string LyDoKhac);
        public void UpdateTiemTaiDiaPhuong(string MaQR, string Mui_TenVacXin, string Mui_NgayTiem, string Mui_SoLo, string Mui_DiaDiem, string Mui_DiaDiem_Old);
        public bool UpdateThongTinF0(int IdStaff, string NgayBiNhiem, string NgayXuatVien);
        public DataTable GetDanhSachDiaDiemTiem(string TinhThanhPho, string QuanHuyen);
        public bool UpdateMui1Staff_Temp(Staff staff);
        public bool UpdateMui2Staff(Staff staff);
        public bool UpdateMui2Staff_Temp(Staff staff);
        public bool UpdateThongTinKhacStaff(Staff staff);
        public Staff GetStaffByCMND(string CMND);
        public Staff GetStaffByMaNS(string MaNS);
        public Staff GetStaffByDienThoai(string DienThoai);
        public void CreateStaff(Staff staff);
        public DataTable GetNhanVienByDonViChiTiet(int IdDonVi);
        public DataTable GetAllNhanVienBaoCaoTiemChung(int IdDonVi, string TenDangNhap, int TinhTrang);
        public DataTable GetDanhSachNhanVienByDonViChiTiet_CapNhatLyDoKhongTiem(int IdDonVi, string TenDangNhap, int BiF0, int TiemTaiDiaPhuong, int DaNghiViec, int LyDoKhac, int PhanLoai);
        public void UpdateTiemMui3(int IdStaff, int GiaTri);
        public bool CheckNhanVien_DonVi(int IdDonVi, string CMND);
        public void UpdateTiemMui3Import(int IdDonVi, string CMND, string DangKy);
        public DataTable GetThongTinMui(string MaQR);
        public DataTable GetThongTinMui1(int IdStaff);
        public DataTable GetThongTinMui2(int IdStaff);
        public DataTable GetThongTinMui3(int IdStaff);
        public DataTable GetThongTinF0(int IdStaff);
        public List<PL3_DanhMucHanhChinh> GetAllProvice();
        public List<PL5_DanhMucVacxin> GetAllTenVacXin();
        public List<PL3_DanhMucHanhChinh> GetAllDistrictByProvice(string Ten_TinhThanhPho);
        public int CheckTrangThaiDangKy(int IdStaff);
        public DataTable GetThongKeMuiTiemTheoDonVi(string DonViChiTiet);
        public void LuuThongTinPhanAnh(string DienThoai, string HoTen, string CMND, string NgaySinh, string ThangSinh, string NamSinh, string TenVacXin
            , DateTime NgayTiem, string TinhThanhPho, string QuanHuyen, string DiaDiem, string SoLo);
    }
}
