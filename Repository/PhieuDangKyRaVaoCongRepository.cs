﻿using dbChange.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Repository
{
    public class PhieuDangKyRaVaoCongRepository : IPhieuDangKyRaVaoCongRepository
    {
        string connectionString = "";
        public PhieuDangKyRaVaoCongRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        public bool IsBoSung()
        {
            bool result = false;
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("IsBoSung", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
                foreach(DataRow dr in dt.Rows)
                {
                    object a = dr[0];
                    if(a!=null && a != DBNull.Value)
                    {
                        if (Convert.ToInt32(a) > 0)
                            result = true;
                    }
                }
            }
            return result;
        }
        public DataTable GetAllPhieuDangKyRaVaoCong(string UserName)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "SELECT ROW_NUMBER() OVER(ORDER BY a.Id desc)  as STT, a.*,b.DonViChiTiet,c.HoTen FROM PhieuDangKyRaVaoCong a " +
                    " inner join Account_DonViChiTiet b on a.IdDonVi = b.DonViChiTietId " +
                    " left join Staff c on c.Id=a.NguoiTaoId " +
                    " where b.TenDangNhap='"+ UserName + "' " +
                    " and cast(a.NgayTao as date) between '"+ Utils.Utils.GetTimeServer().AddDays(-30).ToString("yyyy-MM-dd") +"' and '"+ Utils.Utils.GetTimeServer().ToString("yyyy-MM-dd")  + "'" +
                    "order by Id desc";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        
        public DataTable GetAllPhieuDangKyRaVaoCongDangChoDuyet(Account acc, DateTime TuNgay, DateTime DenNgay)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("GetAllPhieuDangKyRaVaoCongDangChoDuyet", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Type", acc.isYTeDuyet ? 0 : 1));
                cmd.Parameters.Add(new SqlParameter("@TenDangNhap", acc.TenDangNhap));
                cmd.Parameters.Add(new SqlParameter("@TuNgay", TuNgay));
                cmd.Parameters.Add(new SqlParameter("@DenNgay", DenNgay));
               
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetAllPhieuDangKyRaVaoCongDaDuyet(Account acc, DateTime TuNgay, DateTime DenNgay)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("GetAllPhieuDangKyRaVaoCongDaDuyet", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Type", acc.isYTeDuyet ? 0 : 1));
                cmd.Parameters.Add(new SqlParameter("@TenDangNhap", acc.TenDangNhap));
                cmd.Parameters.Add(new SqlParameter("@TuNgay", TuNgay));
                cmd.Parameters.Add(new SqlParameter("@DenNgay", DenNgay));

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public DataTable GetAllNhanVienPhieuDangKyRaVaoCong(int IdPhieu)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                PhieuDangKyRaVaoCong phieu = new PhieuDangKyRaVaoCong();
                phieu = GetPhieu(IdPhieu);
                if(phieu.TinhTrang == "ChoPhuTrachDuyet")
                commandText = "select ROW_NUMBER() OVER(ORDER BY a.Id desc)  as STT" +
                    " ,a.Id,a.MaQRStaff,a.ThoiGianLamViec,a.SoMuiTiem,a.NgayTestGanNhat,a.LyDo,a.Trong14Ngay_HoSot,a.Trong14Ngay_TiepXuc,a.Trong14Ngay_XungQuanhNha,a.NguoiOCung,IdPhieuDangKy,a.IdNhanVienYTeDuyet,a.TenNhanVienYTeDuyet,isnull(a.YTeDuyet,0) as YTeDuyet" +
                    " ,a.Duyet,a.IdNguoiDuyet,a.TenNguoiDuyet,a.YKienYTeDuyet,a.YKienNguoiDuyet " +
                    " ,b.HoTen,c.TenBoPhan,b.DienThoai,b.VungCovid" +
                    " ,a.ViTriVPLamViecId,a.PhanNhomId,a.ViTriLamViecId,a.ViTriDeXeId,a.ViTriVeSinhId,a.ViTriNhaAnId " +
                    " from phieudangkyravaocong_chitiet a " +
                    " inner join Staff b on a.MaQRStaff=b.MaQR " +
                    " left join DonViChiTiet_BoPhan c on c.Id=b.BoPhanId " +
                    " where IdPhieuDangKy=" + IdPhieu + " and a.YTeDuyet=1 " +
                    " order by a.Id desc";
                else
                commandText = "select ROW_NUMBER() OVER(ORDER BY a.Id desc)  as STT" +
                    " ,a.Id,a.MaQRStaff,a.ThoiGianLamViec,a.SoMuiTiem,a.NgayTestGanNhat,a.LyDo,a.Trong14Ngay_HoSot,a.Trong14Ngay_TiepXuc,a.Trong14Ngay_XungQuanhNha,a.NguoiOCung,IdPhieuDangKy,a.IdNhanVienYTeDuyet,a.TenNhanVienYTeDuyet,isnull(a.YTeDuyet,0) as YTeDuyet" +
                    " ,a.Duyet,a.IdNguoiDuyet,a.TenNguoiDuyet,a.YKienYTeDuyet,a.YKienNguoiDuyet " +
                    " ,b.HoTen,c.TenBoPhan,b.DienThoai,b.VungCovid " +
                    " ,a.ViTriVPLamViecId,a.PhanNhomId,a.ViTriLamViecId,a.ViTriDeXeId,a.ViTriVeSinhId,a.ViTriNhaAnId " +
                    " from phieudangkyravaocong_chitiet a " +
                    " inner join Staff b on a.MaQRStaff=b.MaQR " +
                    " left join DonViChiTiet_BoPhan c on c.Id=b.BoPhanId " +
                    " where IdPhieuDangKy=" + IdPhieu +
                    " order by a.Id desc";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        
        public DataTable GetAllNhanVienPhieuDangKyRaVaoCong2(int IdPhieu)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                PhieuDangKyRaVaoCong phieu = new PhieuDangKyRaVaoCong();
                phieu = GetPhieu(IdPhieu);
                if(phieu.TinhTrang == "ChoPhuTrachDuyet")
                commandText = "select ROW_NUMBER() OVER(ORDER BY a.Id desc)  as STT" +
                    " ,a.Id,a.MaQRStaff,a.ThoiGianLamViec,a.SoMuiTiem,a.NgayTestGanNhat,a.LyDo,a.Trong14Ngay_HoSot,a.Trong14Ngay_TiepXuc,a.Trong14Ngay_XungQuanhNha,a.NguoiOCung,IdPhieuDangKy,a.IdNhanVienYTeDuyet,a.TenNhanVienYTeDuyet,isnull(a.YTeDuyet,0) as YTeDuyet" +
                    " ,a.Duyet,a.IdNguoiDuyet,a.TenNguoiDuyet,a.YKienYTeDuyet,a.YKienNguoiDuyet " +
                    " ,b.HoTen,c.TenBoPhan,b.DienThoai,b.VungCovid " +
                    " ,a.ViTriVPLamViecId,a.PhanNhomId,a.ViTriLamViecId,a.ViTriDeXeId,a.ViTriVeSinhId,a.ViTriNhaAnId " +
                    " from phieudangkyravaocong_chitiet a " +
                    " inner join Staff b on a.MaQRStaff=b.MaQR " +
                    " left join DonViChiTiet_BoPhan c on c.Id=b.BoPhanId " +
                    " where IdPhieuDangKy=" + IdPhieu +
                    " order by a.Id desc";
                else
                commandText = "select ROW_NUMBER() OVER(ORDER BY a.Id desc)  as STT" +
                    " ,a.Id,a.MaQRStaff,a.ThoiGianLamViec,a.SoMuiTiem,a.NgayTestGanNhat,a.LyDo,a.Trong14Ngay_HoSot,a.Trong14Ngay_TiepXuc,a.Trong14Ngay_XungQuanhNha,a.NguoiOCung,IdPhieuDangKy,a.IdNhanVienYTeDuyet,a.TenNhanVienYTeDuyet,isnull(a.YTeDuyet,0) as YTeDuyet" +
                    " ,a.Duyet,a.IdNguoiDuyet,a.TenNguoiDuyet,a.YKienYTeDuyet,a.YKienNguoiDuyet " +
                    " ,b.HoTen,c.TenBoPhan,b.DienThoai,b.VungCovid " +
                    " ,a.ViTriVPLamViecId,a.PhanNhomId,a.ViTriLamViecId,a.ViTriDeXeId,a.ViTriVeSinhId,a.ViTriNhaAnId " +
                    " from phieudangkyravaocong_chitiet a " +
                    " inner join Staff b on a.MaQRStaff=b.MaQR " +
                    " left join DonViChiTiet_BoPhan c on c.Id=b.BoPhanId " +
                    " where IdPhieuDangKy=" + IdPhieu +
                    " order by a.Id desc";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        
        public List<Staff> GetDanhSachNhanVienYTeDuyet()
        {
            List<Staff> result = new List<Staff>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select b.* from Account a inner join staff b on a.manhanvien = b.mans where a.isYTeDuyet = 1";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var staff = new Staff
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        HoTen = reader["HoTen"].ToString(),
                        NgaySinh = reader["NgaySinh"] != null && reader["NgaySinh"] != DBNull.Value ? reader["NgaySinh"].ToString() : "",
                        NgayS = reader["NgayS"] != null && reader["NgayS"] != DBNull.Value ? Convert.ToInt32(reader["NgayS"].ToString()) : 1,
                        ThangS = reader["ThangS"] != null && reader["ThangS"] != DBNull.Value ? Convert.ToInt32(reader["ThangS"].ToString()) : 1,
                        NamS = reader["NamS"] != null && reader["NamS"] != DBNull.Value ? Convert.ToInt32(reader["NamS"].ToString()) : 1900,
                        GioiTinh = reader["GioiTinh"] != null && reader["GioiTinh"] != DBNull.Value ? reader["GioiTinh"].ToString() : "",
                        MaNhom = reader["MaNhom"] != null && reader["MaNhom"] != DBNull.Value ? reader["MaNhom"].ToString() : "",
                        DonVi = reader["DonVi"] != null && reader["DonVi"] != DBNull.Value ? reader["DonVi"].ToString() : "",
                        DienThoai = reader["DienThoai"] != null && reader["DienThoai"] != DBNull.Value ? reader["DienThoai"].ToString() : "",
                        CMND = reader["CMND"] != null && reader["CMND"] != DBNull.Value ? reader["CMND"].ToString() : "",
                        CCCD = reader["CCCD"] != null && reader["CCCD"] != DBNull.Value ? reader["CCCD"].ToString() : "",
                        BHYT = reader["BHYT"] != null && reader["BHYT"] != DBNull.Value ? reader["BHYT"].ToString() : "",
                        TenTinh = reader["TenTinh"] != null && reader["TenTinh"] != DBNull.Value ? reader["TenTinh"].ToString() : "",
                        MaTinh = reader["MaTinh"] != null && reader["MaTinh"] != DBNull.Value ? reader["MaTinh"].ToString() : "",
                        TenQuan = reader["TenQuan"] != null && reader["TenQuan"] != DBNull.Value ? reader["TenQuan"].ToString() : "",
                        MaQuan = reader["MaQuan"] != null && reader["MaQuan"] != DBNull.Value ? reader["MaQuan"].ToString() : "",
                        TenPhuong = reader["TenPhuong"] != null && reader["TenPhuong"] != DBNull.Value ? reader["TenPhuong"].ToString() : "",
                        MaPhuong = reader["MaPhuong"] != null && reader["MaPhuong"] != DBNull.Value ? reader["MaPhuong"].ToString() : "",
                        DiaChi = reader["DiaChi"] != null && reader["DiaChi"] != DBNull.Value ? reader["DiaChi"].ToString() : "",
                        Mui1_TenVacxin = reader["Mui1_TenVacxin"] != null && reader["Mui1_TenVacxin"] != DBNull.Value ? reader["Mui1_TenVacxin"].ToString() : "",
                        Mui1_NgayTiem = reader["Mui1_NgayTiem"] != null && reader["Mui1_NgayTiem"] != DBNull.Value ? reader["Mui1_NgayTiem"].ToString() : "",
                        Mui1_SoLo = reader["Mui1_SoLo"] != null && reader["Mui1_SoLo"] != DBNull.Value ? reader["Mui1_SoLo"].ToString() : "",
                        Mui1_DiaDiem = reader["Mui1_DiaDiem"] != null && reader["Mui1_DiaDiem"] != DBNull.Value ? reader["Mui1_DiaDiem"].ToString() : "",
                        Mui2_TenVacxin = reader["Mui2_TenVacxin"] != null && reader["Mui2_TenVacxin"] != DBNull.Value ? reader["Mui2_TenVacxin"].ToString() : "",
                        Mui2_NgayTiem = reader["Mui2_NgayTiem"] != null && reader["Mui2_NgayTiem"] != DBNull.Value ? reader["Mui2_NgayTiem"].ToString() : "",
                        Mui2_SoLo = reader["Mui2_SoLo"] != null && reader["Mui2_SoLo"] != DBNull.Value ? reader["Mui2_SoLo"].ToString() : "",
                        Mui2_DiaDiem = reader["Mui2_DiaDiem"] != null && reader["Mui2_DiaDiem"] != DBNull.Value ? reader["Mui2_DiaDiem"].ToString() : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? reader["GhiChu"].ToString() : "",
                        KetQuaImPort = reader["KetQuaImPort"] != null && reader["KetQuaImPort"] != DBNull.Value ? reader["KetQuaImPort"].ToString() : "",
                        DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : "",
                        NgayCapNhat = reader["NgayCapNhat"] != null && reader["NgayCapNhat"] != DBNull.Value ? reader["NgayCapNhat"].ToString() : "",
                        NguoiCapNhat = reader["NguoiCapNhat"] != null && reader["NguoiCapNhat"] != DBNull.Value ? reader["NguoiCapNhat"].ToString() : "",
                        UrlHinhGiayXN = reader["UrlHinhGiayXN"] != null && reader["UrlHinhGiayXN"] != DBNull.Value ? reader["UrlHinhGiayXN"].ToString() : "",
                        PhanNhom = reader["PhanNhom"] != null && reader["PhanNhom"] != DBNull.Value ? reader["PhanNhom"].ToString() : "",
                        TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? reader["TinhTrang"].ToString() : "",
                        BiF0 = reader["BiF0"] != null && reader["BiF0"] != DBNull.Value ? Convert.ToBoolean(reader["BiF0"]) : false,
                        NgayXuatVien = reader["NgayXuatVien"] != null && reader["NgayXuatVien"] != DBNull.Value ? reader["NgayXuatVien"].ToString() : "",
                        ISok = reader["ISok"] != null && reader["ISok"] != DBNull.Value ? Convert.ToBoolean(reader["ISok"]) : true,
                        Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                        Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                        VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString() : "chưa có tổ dân phố",
                        BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]) : -1,
                        IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",

                    };
                    result.Add(staff);
                }
            }
            return result;
        }

        public int CreatePhieu(PhieuDangKyRaVaoCong phieu)
        {
            int id = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreatePhieuDangKyRaVaoCong", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@NgayDangKyTuNgay", phieu.NgayDangKyTuNgay));
                    cmd.Parameters.Add(new SqlParameter("@NgayDangKyDenNgay", phieu.NgayDangKyDenNgay));
                    cmd.Parameters.Add(new SqlParameter("@GhiChu", phieu.GhiChu));
                    cmd.Parameters.Add(new SqlParameter("@IdDonVi", phieu.IdDonVi));
                    cmd.Parameters.Add(new SqlParameter("@NguoiTaoId", phieu.NguoiTaoId));
                    SqlParameter outputIdParam = new SqlParameter("@new_identity", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };
                    cmd.Parameters.Add(outputIdParam);
                    cmd.ExecuteNonQuery();
                    id = int.Parse(outputIdParam.Value.ToString());
                    return id;
                }
            }
            catch (Exception ex)
            {
                return id;
            }
        }

        public PhieuDangKyRaVaoCong GetPhieu(int IdPhieu)
        {
            PhieuDangKyRaVaoCong phieu = new PhieuDangKyRaVaoCong();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from PhieuDangKyRaVaoCong where Id= " + IdPhieu;
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var phieu1 = new PhieuDangKyRaVaoCong
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        NgayDangKyTuNgay = reader["NgayDangKyTuNgay"] !=null && reader["NgayDangKyTuNgay"] !=DBNull.Value ? Convert.ToDateTime(reader["NgayDangKyTuNgay"]) : Utils.Utils.GetTimeServer(),
                        NgayDangKyDenNgay = reader["NgayDangKyDenNgay"] !=null && reader["NgayDangKyDenNgay"] !=DBNull.Value ? Convert.ToDateTime(reader["NgayDangKyDenNgay"]) : Utils.Utils.GetTimeServer(),
                        NguoiTaoId = reader["NguoiTaoId"] !=null && reader["NguoiTaoId"] !=DBNull.Value ? Convert.ToInt32(reader["NguoiTaoId"]) : 0,
                        TenNguoiTao = reader["TenNguoiTao"] !=null && reader["TenNguoiTao"] !=DBNull.Value ? Convert.ToString(reader["TenNguoiTao"]) : "",
                        NgayTao = reader["NgayTao"] != null && reader["NgayTao"] != DBNull.Value ? Convert.ToDateTime(reader["NgayTao"]) : Utils.Utils.GetTimeServer(),
                        TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? Convert.ToString(reader["TinhTrang"]) : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? Convert.ToString(reader["GhiChu"]) : "",
                        IdDonVi = reader["IdDonVi"] != null && reader["IdDonVi"] != DBNull.Value ? Convert.ToInt32(reader["IdDonVi"]) : 0,
                        TenDonVi = reader["TenDonVi"] != null && reader["TenDonVi"] != DBNull.Value ? Convert.ToString(reader["TenDonVi"]) : "",

                    };
                    return phieu1;
                }
            }
            return phieu;
        }

        public DataTable GetDanhSachNhanVienByIdDonViIdPhieu(string TenDonVi, int IdPhieu) {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                if (IdPhieu ==0)
                {
                    commandText = "select *, null as isCheck, null as LyDo1 from Staff where DonViChiTiet=N'" + TenDonVi + "'";
                }
                else
                {
                    commandText = "select a.*,b.TenBoPhan from (select *,case when (select Id from PhieuDangKyRaVaoCong_ChiTiet where MaQRStaff=Staff.MaQR and IdPhieuDangKy=" + IdPhieu+") is NULL then 0 else 1 end as isCheck " +
                        " ,(select LyDo from PhieuDangKyRaVaoCong_ChiTiet where MaQRStaff=Staff.MaQR and IdPhieuDangKy="+IdPhieu+") as LyDo1 " +
                    " from Staff where DonViChiTiet = N'" + TenDonVi + "' ) a " +
                    " left join DonViChiTiet_BoPhan b on a.BoPhanId=b.Id " +
                    " where isCheck=0";
                }
                
                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
        }
        public void UpdateChiTiet(string ThoiGianLamViec, string LyDo, int IdChiTiet, int SoMuiTiem, DateTime NgayTestGanNhat, bool Trong14Ngay_TiepXuc, bool Trong14Ngay_XungQuanhNha, bool NguoiOCung,
            int ViTriVPLamViecId, int PhanNhomId, int ViTriLamViecId, int ViTriDeXeId, int ViTriVeSinhId, int ViTriNhaAnId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateChiTiet", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@ThoiGianLamViec", ThoiGianLamViec != null ? ThoiGianLamViec : ""));
                    cmd.Parameters.Add(new SqlParameter("@LyDo", LyDo!=null ? LyDo : ""));
                    cmd.Parameters.Add(new SqlParameter("@IdChiTiet", IdChiTiet));
                    cmd.Parameters.Add(new SqlParameter("@SoMuiTiem", SoMuiTiem));
                    if(NgayTestGanNhat.Year>1)
                        cmd.Parameters.Add(new SqlParameter("@NgayTestGanNhat", NgayTestGanNhat));
                    else
                        cmd.Parameters.Add(new SqlParameter("@NgayTestGanNhat", DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@Trong14Ngay_TiepXuc", Trong14Ngay_TiepXuc));
                    cmd.Parameters.Add(new SqlParameter("@Trong14Ngay_XungQuanhNha", Trong14Ngay_XungQuanhNha));
                    cmd.Parameters.Add(new SqlParameter("@NguoiOCung", NguoiOCung));
                    cmd.Parameters.Add(new SqlParameter("@ViTriVPLamViecId", ViTriVPLamViecId));
                    cmd.Parameters.Add(new SqlParameter("@PhanNhomId", PhanNhomId));
                    cmd.Parameters.Add(new SqlParameter("@ViTriLamViecId", ViTriLamViecId));
                    cmd.Parameters.Add(new SqlParameter("@ViTriDeXeId", ViTriDeXeId));
                    cmd.Parameters.Add(new SqlParameter("@ViTriVeSinhId", ViTriVeSinhId));
                    cmd.Parameters.Add(new SqlParameter("@ViTriNhaAnId", ViTriNhaAnId));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                int a = 0;
            }
        }
        public void UpdateChiTietImport(string LyDo,string ThoiGianLamViec, string MaNS, int SoMuiTiem, DateTime NgayTestGanNhat, bool Trong14Ngay_HoSot, bool Trong14Ngay_TiepXuc, bool Trong14Ngay_XungQuanhNha, bool NguoiOCung,
            long ViTriVPLamViecId, long PhanNhomId, long ViTriLamViecId, long ViTriDeXeId, long ViTriVeSinhId, long ViTriNhaAnId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateChiTietImport", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@LyDo", LyDo!=null ? LyDo : ""));
                    cmd.Parameters.Add(new SqlParameter("@ThoiGianLamViec", ThoiGianLamViec != null ? ThoiGianLamViec : ""));
                    cmd.Parameters.Add(new SqlParameter("@MaNS", MaNS));
                    cmd.Parameters.Add(new SqlParameter("@SoMuiTiem", SoMuiTiem));
                    if(NgayTestGanNhat.Year>1)
                        cmd.Parameters.Add(new SqlParameter("@NgayTestGanNhat", NgayTestGanNhat));
                    else
                        cmd.Parameters.Add(new SqlParameter("@NgayTestGanNhat", DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@Trong14Ngay_HoSot", Trong14Ngay_HoSot));
                    cmd.Parameters.Add(new SqlParameter("@Trong14Ngay_TiepXuc", Trong14Ngay_TiepXuc));
                    cmd.Parameters.Add(new SqlParameter("@Trong14Ngay_XungQuanhNha", Trong14Ngay_XungQuanhNha));
                    cmd.Parameters.Add(new SqlParameter("@NguoiOCung", NguoiOCung));
                    cmd.Parameters.Add(new SqlParameter("@ViTriVPLamViecId", ViTriVPLamViecId));
                    cmd.Parameters.Add(new SqlParameter("@PhanNhomId", PhanNhomId));
                    cmd.Parameters.Add(new SqlParameter("@ViTriLamViecId", ViTriLamViecId));
                    cmd.Parameters.Add(new SqlParameter("@ViTriDeXeId", ViTriDeXeId));
                    cmd.Parameters.Add(new SqlParameter("@ViTriVeSinhId", ViTriVeSinhId));
                    cmd.Parameters.Add(new SqlParameter("@ViTriNhaAnId", ViTriNhaAnId));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                int a = 0;
            }
        }
        public void UpdateNhanVienYTeDuyet(int IdNhanVienYTeDuyet, string TenNhanVienYTeDuyet, int IdPhieu)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string commandText = "update phieudangkyravaocong_chitiet set IdNhanVienYTeDuyet = "+ IdNhanVienYTeDuyet + ", TenNhanVienYTeDuyet = N'"+ TenNhanVienYTeDuyet + "'" +
                        " where IdPhieuDangKy=" + IdPhieu;
                    SqlCommand cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                    commandText = "update phieudangkyravaocong set TinhTrang = 'ChoYTeDuyet'" +
                        " where Id=" + IdPhieu;
                    cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                }
            }catch(Exception ex)
            {

            }
        }
        public void XoaNhanVienTrongPhieu(int IdChiTiet)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string commandText = "delete PhieuDangKyRaVaoCong_ChiTiet_Ngay where IdChiTiet =" + IdChiTiet;
                    SqlCommand cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                    commandText = "delete PhieuDangKyRaVaoCong_ChiTiet where Id=" + IdChiTiet;
                    cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                }
            }catch(Exception ex)
            {

            }
        }
        public void XoaChiTietNgay(int IdChiTietNgay)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string commandText = "delete PhieuDangKyRaVaoCong_ChiTiet_Ngay where Id=" + IdChiTietNgay;
                    SqlCommand cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                }
            }catch(Exception ex)
            {

            }
        }
        public void XoaPhieu(int IdPhieu)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string commandText = "delete PhieuDangKyRaVaoCong_ChiTiet_Ngay where IdChiTiet in (select Id from PhieuDangKyRaVaoCong_ChiTiet where IdPhieuDangKy="+ IdPhieu + ")";
                    SqlCommand cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                    commandText = "delete PhieuDangKyRaVaoCong_ChiTiet_Thu where IdChiTiet in (select Id from PhieuDangKyRaVaoCong_ChiTiet where IdPhieuDangKy=" + IdPhieu + ")";
                    cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                    commandText = "delete PhieuDangKyRaVaoCong_ChiTiet where IdPhieuDangKy=" + IdPhieu;
                    cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                    commandText = "delete PhieuDangKyRaVaoCong where Id=" + IdPhieu;
                    cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                }
            }catch(Exception ex)
            {

            }
        }
       
        public List<Staff> GetDanhSachNhanVienByIdDonVi(string TenDonVi)
        {
            List<Staff> result = new List<Staff>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "select * from Staff where DonViChiTiet=N'" + TenDonVi + "'";
                SqlCommand cmd = new SqlCommand(commandText, conn);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var staff = new Staff
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        HoTen = reader["HoTen"].ToString(),
                        NgaySinh = reader["NgaySinh"] != null && reader["NgaySinh"] != DBNull.Value ? reader["NgaySinh"].ToString() : "",
                        NgayS = reader["NgayS"] != null && reader["NgayS"] != DBNull.Value ? Convert.ToInt32(reader["NgayS"].ToString()) : 1,
                        ThangS = reader["ThangS"] != null && reader["ThangS"] != DBNull.Value ? Convert.ToInt32(reader["ThangS"].ToString()) : 1,
                        NamS = reader["NamS"] != null && reader["NamS"] != DBNull.Value ? Convert.ToInt32(reader["NamS"].ToString()) : 1900,
                        GioiTinh = reader["GioiTinh"] != null && reader["GioiTinh"] != DBNull.Value ? reader["GioiTinh"].ToString() : "",
                        MaNhom = reader["MaNhom"] != null && reader["MaNhom"] != DBNull.Value ? reader["MaNhom"].ToString() : "",
                        DonVi = reader["DonVi"] != null && reader["DonVi"] != DBNull.Value ? reader["DonVi"].ToString() : "",
                        DienThoai = reader["DienThoai"] != null && reader["DienThoai"] != DBNull.Value ? reader["DienThoai"].ToString() : "",
                        CMND = reader["CMND"] != null && reader["CMND"] != DBNull.Value ? reader["CMND"].ToString() : "",
                        CCCD = reader["CCCD"] != null && reader["CCCD"] != DBNull.Value ? reader["CCCD"].ToString() : "",
                        BHYT = reader["BHYT"] != null && reader["BHYT"] != DBNull.Value ? reader["BHYT"].ToString() : "",
                        TenTinh = reader["TenTinh"] != null && reader["TenTinh"] != DBNull.Value ? reader["TenTinh"].ToString() : "",
                        MaTinh = reader["MaTinh"] != null && reader["MaTinh"] != DBNull.Value ? reader["MaTinh"].ToString() : "",
                        TenQuan = reader["TenQuan"] != null && reader["TenQuan"] != DBNull.Value ? reader["TenQuan"].ToString() : "",
                        MaQuan = reader["MaQuan"] != null && reader["MaQuan"] != DBNull.Value ? reader["MaQuan"].ToString() : "",
                        TenPhuong = reader["TenPhuong"] != null && reader["TenPhuong"] != DBNull.Value ? reader["TenPhuong"].ToString() : "",
                        MaPhuong = reader["MaPhuong"] != null && reader["MaPhuong"] != DBNull.Value ? reader["MaPhuong"].ToString() : "",
                        DiaChi = reader["DiaChi"] != null && reader["DiaChi"] != DBNull.Value ? reader["DiaChi"].ToString() : "",
                        Mui1_TenVacxin = reader["Mui1_TenVacxin"] != null && reader["Mui1_TenVacxin"] != DBNull.Value ? reader["Mui1_TenVacxin"].ToString() : "",
                        Mui1_NgayTiem = reader["Mui1_NgayTiem"] != null && reader["Mui1_NgayTiem"] != DBNull.Value ? reader["Mui1_NgayTiem"].ToString() : "",
                        Mui1_SoLo = reader["Mui1_SoLo"] != null && reader["Mui1_SoLo"] != DBNull.Value ? reader["Mui1_SoLo"].ToString() : "",
                        Mui1_DiaDiem = reader["Mui1_DiaDiem"] != null && reader["Mui1_DiaDiem"] != DBNull.Value ? reader["Mui1_DiaDiem"].ToString() : "",
                        Mui2_TenVacxin = reader["Mui2_TenVacxin"] != null && reader["Mui2_TenVacxin"] != DBNull.Value ? reader["Mui2_TenVacxin"].ToString() : "",
                        Mui2_NgayTiem = reader["Mui2_NgayTiem"] != null && reader["Mui2_NgayTiem"] != DBNull.Value ? reader["Mui2_NgayTiem"].ToString() : "",
                        Mui2_SoLo = reader["Mui2_SoLo"] != null && reader["Mui2_SoLo"] != DBNull.Value ? reader["Mui2_SoLo"].ToString() : "",
                        Mui2_DiaDiem = reader["Mui2_DiaDiem"] != null && reader["Mui2_DiaDiem"] != DBNull.Value ? reader["Mui2_DiaDiem"].ToString() : "",
                        GhiChu = reader["GhiChu"] != null && reader["GhiChu"] != DBNull.Value ? reader["GhiChu"].ToString() : "",
                        KetQuaImPort = reader["KetQuaImPort"] != null && reader["KetQuaImPort"] != DBNull.Value ? reader["KetQuaImPort"].ToString() : "",
                        DonViChiTiet = reader["DonViChiTiet"] != null && reader["DonViChiTiet"] != DBNull.Value ? reader["DonViChiTiet"].ToString() : "",
                        NgayCapNhat = reader["NgayCapNhat"] != null && reader["NgayCapNhat"] != DBNull.Value ? reader["NgayCapNhat"].ToString() : "",
                        NguoiCapNhat = reader["NguoiCapNhat"] != null && reader["NguoiCapNhat"] != DBNull.Value ? reader["NguoiCapNhat"].ToString() : "",
                        UrlHinhGiayXN = reader["UrlHinhGiayXN"] != null && reader["UrlHinhGiayXN"] != DBNull.Value ? reader["UrlHinhGiayXN"].ToString() : "",
                        PhanNhom = reader["PhanNhom"] != null && reader["PhanNhom"] != DBNull.Value ? reader["PhanNhom"].ToString() : "",
                        TinhTrang = reader["TinhTrang"] != null && reader["TinhTrang"] != DBNull.Value ? reader["TinhTrang"].ToString() : "",
                        BiF0 = reader["BiF0"] != null && reader["BiF0"] != DBNull.Value ? Convert.ToBoolean(reader["BiF0"]) : false,
                        NgayXuatVien = reader["NgayXuatVien"] != null && reader["NgayXuatVien"] != DBNull.Value ? reader["NgayXuatVien"].ToString() : "",
                        ISok = reader["ISok"] != null && reader["ISok"] != DBNull.Value ? Convert.ToBoolean(reader["ISok"]) : true,
                        Ma_ToKhuPho = reader["Ma_ToKhuPho"] != null && reader["Ma_ToKhuPho"] != DBNull.Value ? reader["Ma_ToKhuPho"].ToString() : "",
                        Ten_ToKhuPho = reader["Ten_ToKhuPho"] != null && reader["Ten_ToKhuPho"] != DBNull.Value ? reader["Ten_ToKhuPho"].ToString() : "",
                        VungCovid = reader["VungCovid"] != null && reader["VungCovid"] != DBNull.Value ? reader["VungCovid"].ToString() : "chưa có tổ dân phố",
                        BoPhanId = reader["BoPhanId"] != null && reader["BoPhanId"] != DBNull.Value ? Convert.ToInt32(reader["BoPhanId"]) : -1,
                        IdWeb = reader["IdWeb"] != null && reader["IdWeb"] != DBNull.Value ? reader["IdWeb"].ToString() : "",

                    };
                    result.Add(staff);
                }
            }
            return result;
        }
        public void SaveNhanVienVaoPhieu(int IdNhanVien, int IdPhieu, string LyDo)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SaveNhanVienVaoPhieu", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdPhieu", IdPhieu));
                    cmd.Parameters.Add(new SqlParameter("@IdNhanVien", IdNhanVien));
                    cmd.Parameters.Add(new SqlParameter("@LyDo", LyDo));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        public void UpdatePhieu(PhieuDangKyRaVaoCong phieu)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdatePhieuDangKyRaVaoCong", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@NgayDangKyTuNgay", phieu.NgayDangKyTuNgay));
                    cmd.Parameters.Add(new SqlParameter("@NgayDangKyDenNgay", phieu.NgayDangKyDenNgay));
                    cmd.Parameters.Add(new SqlParameter("@GhiChu", phieu.GhiChu));
                    cmd.Parameters.Add(new SqlParameter("@IdDonVi", phieu.IdDonVi));
                    cmd.Parameters.Add(new SqlParameter("@IdPhieu", phieu.Id));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        public void XetDuyetPhieuMaster(int IdPhieu, int Type)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string commandText = "";
                    if(Type==0)
                        commandText = "update PhieuDangKyRaVaoCong set TinhTrang='ChoPhuTrachDuyet' where Id=" + IdPhieu;
                    else
                        commandText = "update PhieuDangKyRaVaoCong set TinhTrang='DaDuyet' where Id=" + IdPhieu;
                    SqlCommand cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void UpdateTrangThaiPhieu(int IdPhieu, string TrangThai)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string commandText = "update PhieuDangKyRaVaoCong set TinhTrang='"+ TrangThai +"' where Id=" + IdPhieu;
                    SqlCommand cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void CapNhatCacDongChiTiet(int IdChiTiet, string Loai, string GiaTri)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string commandText = "";
                    if(Loai=="ViTriVPLamViec") commandText = "update PhieuDangKyRaVaoCong_ChiTiet set ViTriVPLamViecId="+ Convert.ToInt32(GiaTri) + " where id=" + IdChiTiet;
                    if(Loai=="ViTriLamViec") commandText = "update PhieuDangKyRaVaoCong_ChiTiet set ViTriLamViecId="+ Convert.ToInt32(GiaTri) + " where id=" + IdChiTiet;
                    if(Loai=="PhanNhom") commandText = "update PhieuDangKyRaVaoCong_ChiTiet set PhanNhomId="+ Convert.ToInt32(GiaTri) + " where id=" + IdChiTiet;
                    if(Loai=="ViTriDeXe") commandText = "update PhieuDangKyRaVaoCong_ChiTiet set ViTriDeXeId=" + Convert.ToInt32(GiaTri) + " where id=" + IdChiTiet;
                    if(Loai== "ViTriVeSinh") commandText = "update PhieuDangKyRaVaoCong_ChiTiet set ViTriVeSinhId=" + Convert.ToInt32(GiaTri) + " where id=" + IdChiTiet;
                    if(Loai== "ViTriNhaAn") commandText = "update PhieuDangKyRaVaoCong_ChiTiet set ViTriNhaAnId=" + Convert.ToInt32(GiaTri) + " where id=" + IdChiTiet;
                    if(Loai== "LyDo") commandText = "update PhieuDangKyRaVaoCong_ChiTiet set LyDo=N'" + GiaTri+"' where id=" + IdChiTiet;
                    if(Loai== "ThoiGianLamViec") commandText = "update PhieuDangKyRaVaoCong_ChiTiet set ThoiGianLamViec=N'" + GiaTri+"' where id=" + IdChiTiet;
                    if(Loai== "SoMuiTiem") commandText = "update PhieuDangKyRaVaoCong_ChiTiet set SoMuiTiem=" + Convert.ToInt32(GiaTri) + " where id=" + IdChiTiet;
                    SqlCommand cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void XetDuyetPhieu(int IdPhieu, int IdChiTiet, bool IsDuyet, string LyDo, int IdNguoiDuyet, int Type)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("XetDuyetPhieuDangKyRaVaoCong", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdPhieu", IdPhieu));
                    cmd.Parameters.Add(new SqlParameter("@IdChiTiet", IdChiTiet));
                    cmd.Parameters.Add(new SqlParameter("@Duyet", IsDuyet));
                    cmd.Parameters.Add(new SqlParameter("@IdNguoiDuyet", IdNguoiDuyet));
                    cmd.Parameters.Add(new SqlParameter("@YKien", LyDo!=null ? LyDo : ""));
                    cmd.Parameters.Add(new SqlParameter("@Type", Type));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void ThemThuChoNhanVien(int IdPhieu, int IdChiTiet, string LsThu)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ThemThuChoNhanVien", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdPhieu", IdPhieu));
                    cmd.Parameters.Add(new SqlParameter("@IdChiTiet", IdChiTiet));
                    cmd.Parameters.Add(new SqlParameter("@LsThu", LsThu));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void ThemThuChoNhanVienImport(int IdPhieu, string MaQR, string LsThu)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ThemThuChoNhanVienImport", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdPhieu", IdPhieu));
                    cmd.Parameters.Add(new SqlParameter("@MaQR", MaQR));
                    cmd.Parameters.Add(new SqlParameter("@LsThu", LsThu));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void ThemNgayChoNhanVien(int IdPhieu, int IdChiTiet, DateTime Ngay)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ThemNgayChoNhanVien", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdPhieu", IdPhieu));
                    cmd.Parameters.Add(new SqlParameter("@IdChiTiet", IdChiTiet));
                    cmd.Parameters.Add(new SqlParameter("@Ngay", Ngay));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public DataTable DanhSachNgayDiLamGroupByNhanVien(string lsIdChiTiet)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("DanhSachNgayDiLamGroupByNhanVien", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@lsIdChiTiet", lsIdChiTiet!=null ? lsIdChiTiet: ""));
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
           
        }
        public DataTable DanhSachNgayDiLamTrenPhieu(int IdPhieu)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("LayDanhSachNgayDiLam_Theo_PhieuRaVaoCong", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@IdPhieu", IdPhieu));
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);

            }
            return dt;
           
        }
        public void CreateChiTietPhieuNgay(int IdChiTiet, DateTime Ngay)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("CreateChiTietPhieuNgay", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdChiTiet", IdChiTiet));
                    cmd.Parameters.Add(new SqlParameter("@Ngay", Ngay));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void XoaChiTietNgayTheoIdChiTiet(int IdChiTiet)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string commandText = "delete PhieuDangKyRaVaoCong_ChiTiet_Ngay where IdChiTiet=" + IdChiTiet + "";
                    SqlCommand cmd = new SqlCommand(commandText, conn);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void CopyChiTietPhieu(int IdPhieuCopy, int IdPhieu)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CopyChiTietPhieu", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdPhieuCopy", IdPhieuCopy));
                    cmd.Parameters.Add(new SqlParameter("@IdPhieu", IdPhieu));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public int SoLuongNhanVienTrongPhieu(int IdPhieu, string TinhTrang)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string commandText = "";
                if(TinhTrang == "MoiTao" || TinhTrang == "ChoYTeDuyet")
                    commandText = "select count(*) from (" +
                    " select distinct maqrstaff from phieudangkyravaocong a " +
                    " inner join PhieuDangKyRaVaoCong_ChiTiet b on a.Id = b.IdPhieuDangKy " +
                    " where a.id = "+ IdPhieu + " and b.MaQRStaff is not null ) x";
                else if(TinhTrang == "ChoPhuTrachDuyet")
                    commandText = "select count(*) from (" +
                    " select distinct maqrstaff from phieudangkyravaocong a " +
                    " inner join PhieuDangKyRaVaoCong_ChiTiet b on a.Id = b.IdPhieuDangKy " +
                    " where a.id = "+ IdPhieu + " and b.YTeDuyet=1 and b.MaQRStaff is not null) x";
                else if (TinhTrang == "DaDuyet")
                    commandText = "select count(*) from (" +
                    " select distinct maqrstaff from phieudangkyravaocong a " +
                    " inner join PhieuDangKyRaVaoCong_ChiTiet b on a.Id = b.IdPhieuDangKy " +
                    " where a.id = " + IdPhieu + " and b.YTeDuyet=1 and b.Duyet=1 and b.MaQRStaff is not null) x";

                SqlCommand cmd = new SqlCommand(commandText, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert.ToInt32(dt.Rows[0][0]);
                }
            }
            return 0;
        }
        public DataTable GetSoLuongNhanVienChiTietPhieuTheoThu(int IdPhieu, string TinhTrang)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetSoLuongNhanVienChiTietPhieuTheoThu", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdPhieu", IdPhieu));
                    cmd.Parameters.Add(new SqlParameter("@TinhTrang", TinhTrang));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
            }
            catch (Exception ex)
            {

            }
            return dt;
        }

    }
}
