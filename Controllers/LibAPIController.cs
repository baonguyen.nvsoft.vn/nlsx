﻿using dbChange.Models.LIB;
using dbChange.Repository;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;

namespace dbChange.Controllers
{
    [Route("api/[controller]")]
    public class LibAPIController : Controller
    {
        private readonly ILibRepository _repository;

        private readonly IWebHostEnvironment _env;
        public LibAPIController(ILibRepository repository, IWebHostEnvironment env)
        {
            _repository = repository;
            _env = env;
        }

        public IActionResult Index()
        {
            //var token = HttpContext.Session.GetString("token");
            //if (token == null)
            //{
            //    return View("Views/Account/Login.cshtml");
            //}
            return View();
        }
        [HttpGet]
        public IActionResult GetAll_PL2_NhomDoiTuong()
        {
            return Ok(_repository.GetAll_PL2_NhomDoiTuong());
        }
      
        [HttpGet]
        public IActionResult GetAll_PL4_DiaDiemTiem()
        {
            return Ok(_repository.GetAll_PL4_DiaDiemTiem());
        }
        [HttpGet]
        public IActionResult GetAll_PL5_DanhMucVacxin()
        {
            return Ok(_repository.GetAll_PL5_DanhMucVacxin());
        }

        [HttpGet]
        public object GetAll_PL3_DanhMucHanhChinh(DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(_repository.GetAll_PL3_DanhMucHanhChinh(), loadOptions);
        }
    }
}
