﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using dbChange.Models;
using dbChange.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dbChange.Controllers
{
    public class NLSXController : Controller
    {
        private bool isLoged = false;
        private readonly IAccountRepository _repository;
        private readonly ILibRepository _Librepository;
        private readonly INLSXRepository _NLSXrepository;
        private Account accMain;
        public NLSXController(IHttpContextAccessor httpContextAccessor,IAccountRepository repository, ILibRepository Librepository, INLSXRepository NLSXrepository)
        {
            _repository = repository;
            _Librepository = Librepository;
            _NLSXrepository = NLSXrepository;
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
                string a = Utils.Utils.Decrypt(httpContextAccessor.HttpContext.Request.Cookies["token"]);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                accMain = _repository.GetAccount(acc.TenDangNhap);
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
                string a = Utils.Utils.Decrypt(httpContextAccessor.HttpContext.Session.GetString("token"));
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                accMain = _repository.GetAccount(acc.TenDangNhap);
            }
        }
        public IActionResult Index()
        {
            if (isLoged)
            {
                return RedirectToAction(actionName: "FormTong", controllerName: "NLSX");
            }
            else
                return RedirectToAction(actionName: "Index", controllerName: "Account");
        }
        public IActionResult FormTong()
        {
            if (isLoged)
            {
                ViewData["acc"] = accMain;
                ViewData["css"] = "~/wwwroot/Includes/NLSX/index_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/NLSX/FormTong_js.cshtml";
                ViewData["TuanMoiNhat"] = _NLSXrepository.GET_TUANCAPNHAT_MOINHAT();
                return View();
            }
            else
                return RedirectToAction(actionName: "Index", controllerName: "Account");
        }
        public IActionResult NLSX_DonVi()
        {
            if (isLoged)
            {
                ViewData["acc"] = accMain;
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);
                ViewData["css"] = "~/wwwroot/Includes/NLSX/index_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/NLSX/index_js.cshtml";
                return View();
            }
            else
                return RedirectToAction(actionName: "Index", controllerName: "Account");
        }
        public IActionResult NLSX_FormTong_DonVi()
        {
            if (isLoged)
            {
                ViewData["acc"] = accMain;
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);
                ViewData["css"] = "~/wwwroot/Includes/NLSX/index_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/NLSX/FormTong_DonVi_js.cshtml";
                return View();
            }
            else
                return RedirectToAction(actionName: "Index", controllerName: "Account");
        }
        public IActionResult NLSX_FormTong_Tuan()
        {
            if (isLoged)
            {
                ViewData["acc"] = accMain;
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);
                ViewData["css"] = "~/wwwroot/Includes/NLSX/index_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/NLSX/FormTong_Tuan_js.cshtml";
                return View();
            }
            else
                return RedirectToAction(actionName: "Index", controllerName: "Account");
        }
        public string GetData_Tong(string lsThang, int Tuan)
        {
        DataTable dt = new DataTable();
            dt = _NLSXrepository.GetData_Tong(lsThang, Tuan);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetData_Tong_Tuan_Thang(int Tuan, int Thang)
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.GetData_Tong_Tuan_Thang(Tuan,Thang,0);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetData_NguonHang_Tong(long IdRoot, string TenChungLoai, int TuanCapNhat, int Thang, string DonVi)
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.GetData_NguonHang_Tong(IdRoot, TenChungLoai, TuanCapNhat, Thang, DonVi);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetData_NguonHang_SoSanh(long IdRoot, string TenChungLoai, int TuanCapNhat, int TuanSoSanh, int Thang)
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.GetData_NguonHang_SoSanh(IdRoot, TenChungLoai, TuanCapNhat, TuanSoSanh, Thang);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetData_NangLuc_Tong(long IdRoot, string TenChungLoai, int TuanCapNhat, int Thang, int Nam)
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.GetData_NangLuc_Tong(IdRoot, TenChungLoai, TuanCapNhat, Thang, Nam);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetData_NangLuc_SoSanh(long IdRoot, string TenChungLoai, int TuanCapNhat, int TuanSoSanh, int Thang, int ThangSoSanh, int NamSoSanh)
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.GetData_NangLuc_SoSanh(IdRoot, TenChungLoai, TuanCapNhat, TuanSoSanh, Thang, ThangSoSanh, Utils.Utils.GetTimeServer().Year, NamSoSanh);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetData_ChenhLech_Tong(long IdRoot, string TenChungLoai, int TuanCapNhat, int Thang)
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.GetData_ChenhLech_Tong(IdRoot, TenChungLoai, TuanCapNhat, Thang);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetData_ChenhLech_SoSanh(long IdRoot, string TenChungLoai, int TuanCapNhat, int TuanSoSanh, int Thang, int ThangSoSanh, int NamSoSanh)
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.GetData_ChenhLech_SoSanh(IdRoot, TenChungLoai, TuanCapNhat, TuanSoSanh, Thang, ThangSoSanh, Utils.Utils.GetTimeServer().Year, NamSoSanh);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetData_DonVi(long IdDonVi, string lsThang)
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.GetData_DonVi(IdDonVi,lsThang);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string Get_List_Tuan_NangLuc()
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.Get_List_Tuan_NangLuc();
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string Get_List_Tuan_ChenhLech()
        {
            DataTable dt = new DataTable();
            dt = _NLSXrepository.Get_List_Tuan_ChenhLech();
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        
    }
}