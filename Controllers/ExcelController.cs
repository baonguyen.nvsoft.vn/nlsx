﻿using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using dbChange.Repository;
using Microsoft.AspNetCore.Http;

namespace dbChange.Controllers
{
    public class ExcelController : Controller
    {
        private readonly IEmployeeRepository _repository;

        public ExcelController(IEmployeeRepository repository)
        {
            _repository = repository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ExportExcel()
        {
            var collection = _repository.GetAllEmployees();
            return View(collection);
        }
        public ActionResult List()
        {
           

            var collection = _repository.GetAllEmployees();

            return View(collection);
        }
        public void DownloadExcel()
        {
           
            var collection = _repository.GetAllEmployees();


            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
            Sheet.Cells["A1"].Value = "Họ Tên";
            Sheet.Cells["B1"].Value = "Năm Sinh";
            Sheet.Cells["C1"].Value = "Giới Tính";
            Sheet.Cells["D1"].Value = "Địa Chỉ";
            Sheet.Cells["E1"].Value = "Điện Thoại";
            Sheet.Cells["F1"].Value = "CMND";
            Sheet.Cells["G1"].Value = "Đơn Vị";
            Sheet.Cells["H1"].Value = "Kết Qủa";
            int row = 2;
            foreach (var item in collection)
            {

                Sheet.Cells[string.Format("A{0}", row)].Value = item.HoTen;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.NamSinh;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.GioiTinh;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.DiaChi;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.DienThoai;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.CMND;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.DonVi;
                Sheet.Cells[string.Format("H{0}", row)].Value = item.KetQuaXN;
                row++;
            }


            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Headers.Add("content-disposition", "attachment: filename=" + "Report.xlsx");
            Response.BodyWriter.WriteAsync(Ep.GetAsByteArray());
            Response.BodyWriter.Complete();
        }

    }
}
