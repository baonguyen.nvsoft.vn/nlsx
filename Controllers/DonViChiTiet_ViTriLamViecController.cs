﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using dbChange.Data;
using dbChange.Models.LIB;
using dbChange.Repository;

namespace dbChange.Controllers
{
    public class DonViChiTiet_ViTriLamViecController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ILibRepository _Librepository;

        public DonViChiTiet_ViTriLamViecController(ApplicationDbContext context, ILibRepository Librepository)
        {
            _context = context;
            _Librepository = Librepository;
        }

        // GET: DonViChiTiet_ViTriLamViec
        public async Task<IActionResult> Index()
        {
            return View(await _context.DonViChiTiet_ViTriLamViec.ToListAsync());
        }

        // GET: DonViChiTiet_ViTriLamViec/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_ViTriLamViec = await _context.DonViChiTiet_ViTriLamViec
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_ViTriLamViec == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_ViTriLamViec);
        }

        // GET: DonViChiTiet_ViTriLamViec/Create
        public IActionResult Create()
        {
            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            return View();
        }

        // POST: DonViChiTiet_ViTriLamViec/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create([Bind("id,TenViTriLamViec,IdDonViChiTiet")] DonViChiTiet_ViTriLamViec donViChiTiet_ViTriLamViec)
        {
            if (ModelState.IsValid)
            {
                donViChiTiet_ViTriLamViec.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_ViTriLamViec.IdDonViChiTiet).TenDonVi;
                _context.Add(donViChiTiet_ViTriLamViec);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_ViTriLamViec);
        }

        // GET: DonViChiTiet_ViTriLamViec/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            var donViChiTiet_ViTriLamViec = await _context.DonViChiTiet_ViTriLamViec.FindAsync(id);
            if (donViChiTiet_ViTriLamViec == null)
            {
                return NotFound();
            }
            return View(donViChiTiet_ViTriLamViec);
        }

        // POST: DonViChiTiet_ViTriLamViec/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Edit(long id, [Bind("id,TenViTriLamViec,IdDonViChiTiet")] DonViChiTiet_ViTriLamViec donViChiTiet_ViTriLamViec)
        {
            if (id != donViChiTiet_ViTriLamViec.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    donViChiTiet_ViTriLamViec.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_ViTriLamViec.IdDonViChiTiet).TenDonVi;
                    _context.Update(donViChiTiet_ViTriLamViec);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DonViChiTiet_ViTriLamViecExists(donViChiTiet_ViTriLamViec.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_ViTriLamViec);
        }

        // GET: DonViChiTiet_ViTriLamViec/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_ViTriLamViec = await _context.DonViChiTiet_ViTriLamViec
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_ViTriLamViec == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_ViTriLamViec);
        }

        // POST: DonViChiTiet_ViTriLamViec/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var donViChiTiet_ViTriLamViec = await _context.DonViChiTiet_ViTriLamViec.FindAsync(id);
            _context.DonViChiTiet_ViTriLamViec.Remove(donViChiTiet_ViTriLamViec);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DonViChiTiet_ViTriLamViecExists(long id)
        {
            return _context.DonViChiTiet_ViTriLamViec.Any(e => e.id == id);
        }
    }
}
