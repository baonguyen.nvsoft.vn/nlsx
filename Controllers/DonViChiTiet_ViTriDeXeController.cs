﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using dbChange.Data;
using dbChange.Models.LIB;
using dbChange.Repository;

namespace dbChange.Controllers
{
    public class DonViChiTiet_ViTriDeXeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ILibRepository _Librepository;

        public DonViChiTiet_ViTriDeXeController(ApplicationDbContext context, ILibRepository Librepository)
        {
            _context = context;
            _Librepository = Librepository;
        }

        // GET: DonViChiTiet_ViTriDeXe
        public async Task<IActionResult> Index()
        {
            return View(await _context.DonViChiTiet_ViTriDeXe.ToListAsync());
        }

        // GET: DonViChiTiet_ViTriDeXe/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_ViTriDeXe = await _context.DonViChiTiet_ViTriDeXe
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_ViTriDeXe == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_ViTriDeXe);
        }

        // GET: DonViChiTiet_ViTriDeXe/Create
        public IActionResult Create()
        {
            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            return View();
        }

        // POST: DonViChiTiet_ViTriDeXe/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create([Bind("id,TenViTriDeXe,IdDonViChiTiet")] DonViChiTiet_ViTriDeXe donViChiTiet_ViTriDeXe)
        {
            if (ModelState.IsValid)
            {
                donViChiTiet_ViTriDeXe.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_ViTriDeXe.IdDonViChiTiet).TenDonVi;
                _context.Add(donViChiTiet_ViTriDeXe);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_ViTriDeXe);
        }

        // GET: DonViChiTiet_ViTriDeXe/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            var donViChiTiet_ViTriDeXe = await _context.DonViChiTiet_ViTriDeXe.FindAsync(id);
            if (donViChiTiet_ViTriDeXe == null)
            {
                return NotFound();
            }
            return View(donViChiTiet_ViTriDeXe);
        }

        // POST: DonViChiTiet_ViTriDeXe/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Edit(long id, [Bind("id,TenViTriDeXe,IdDonViChiTiet")] DonViChiTiet_ViTriDeXe donViChiTiet_ViTriDeXe)
        {
            if (id != donViChiTiet_ViTriDeXe.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    donViChiTiet_ViTriDeXe.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_ViTriDeXe.IdDonViChiTiet).TenDonVi;
                    _context.Update(donViChiTiet_ViTriDeXe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DonViChiTiet_ViTriDeXeExists(donViChiTiet_ViTriDeXe.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_ViTriDeXe);
        }

        // GET: DonViChiTiet_ViTriDeXe/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_ViTriDeXe = await _context.DonViChiTiet_ViTriDeXe
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_ViTriDeXe == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_ViTriDeXe);
        }

        // POST: DonViChiTiet_ViTriDeXe/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var donViChiTiet_ViTriDeXe = await _context.DonViChiTiet_ViTriDeXe.FindAsync(id);
            _context.DonViChiTiet_ViTriDeXe.Remove(donViChiTiet_ViTriDeXe);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DonViChiTiet_ViTriDeXeExists(long id)
        {
            return _context.DonViChiTiet_ViTriDeXe.Any(e => e.id == id);
        }
    }
}
