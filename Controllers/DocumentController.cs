﻿using GroupDocs.Viewer.Options;
using GroupDocs.Viewer.Results;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Controllers
{
    public class DocumentController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private string projectRootPath;
        private string outputPath;
        private string storagePath;
        List<string> lstFiles;

        public DocumentController(IHostingEnvironment hostingEnvironment)
        {
            
            projectRootPath = _hostingEnvironment.ContentRootPath;
            outputPath = Path.Combine(projectRootPath, "wwwroot/Documents");
            storagePath = Path.Combine(projectRootPath, "wwwroot/Documents");
            lstFiles = new List<string>();
        }
        public IActionResult Index()
        {

            var token = HttpContext.Session.GetString("token");
            if (token == null)
            {
                return View("Views/Account/Login.cshtml");
            }

            var files = Directory.GetFiles(storagePath);
            foreach (var file in files)
            {
                lstFiles.Add(Path.GetFileName(file));
            }
            ViewBag.lstFiles = lstFiles;

            ViewData["css"] = "~/Views/Covid19/Css/Index.cshtml";
            ViewData["js"] = "~/Views/Covid19/Js/Index.cshtml";
            return View();
        }

        [HttpPost]
        public IActionResult OnPost(string FileName)
        {
            int pageCount = 0;
            string imageFilesFolder = Path.Combine(outputPath, Path.GetFileName(FileName).Replace(".", "_"));
            if (!Directory.Exists(imageFilesFolder))
            {
                Directory.CreateDirectory(imageFilesFolder);
            }
            string imageFilesPath = Path.Combine(imageFilesFolder, "page-{0}.png");
            using (GroupDocs.Viewer.Viewer viewer = new GroupDocs.Viewer.Viewer(Path.Combine(storagePath, FileName)))
            {
                //Get document info
                ViewInfo info = viewer.GetViewInfo(ViewInfoOptions.ForPngView(false));
                pageCount = info.Pages.Count;
                //Set options and render document
                PngViewOptions options = new PngViewOptions(imageFilesPath);
                viewer.View(options);
            }
            return new JsonResult(pageCount);
        }
    }
}
