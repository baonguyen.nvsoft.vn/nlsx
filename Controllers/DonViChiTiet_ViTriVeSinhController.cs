﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using dbChange.Data;
using dbChange.Models.LIB;
using dbChange.Repository;

namespace dbChange.Controllers
{
    public class DonViChiTiet_ViTriVeSinhController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ILibRepository _Librepository;

        public DonViChiTiet_ViTriVeSinhController(ApplicationDbContext context, ILibRepository Librepository)
        {
            _context = context;
            _Librepository = Librepository;
        }

        // GET: DonViChiTiet_ViTriVeSinh
        public async Task<IActionResult> Index()
        {
            return View(await _context.DonViChiTiet_ViTriVeSinh.ToListAsync());
        }

        // GET: DonViChiTiet_ViTriVeSinh/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_ViTriVeSinh = await _context.DonViChiTiet_ViTriVeSinh
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_ViTriVeSinh == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_ViTriVeSinh);
        }

        // GET: DonViChiTiet_ViTriVeSinh/Create
        public IActionResult Create()
        {
            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            return View();
        }

        // POST: DonViChiTiet_ViTriVeSinh/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create([Bind("id,TenViTriVeSinh,IdDonViChiTiet")] DonViChiTiet_ViTriVeSinh donViChiTiet_ViTriVeSinh)
        {
            if (ModelState.IsValid)
            {
                donViChiTiet_ViTriVeSinh.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_ViTriVeSinh.IdDonViChiTiet).TenDonVi;
                _context.Add(donViChiTiet_ViTriVeSinh);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_ViTriVeSinh);
        }

        // GET: DonViChiTiet_ViTriVeSinh/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            var donViChiTiet_ViTriVeSinh = await _context.DonViChiTiet_ViTriVeSinh.FindAsync(id);
            if (donViChiTiet_ViTriVeSinh == null)
            {
                return NotFound();
            }
            return View(donViChiTiet_ViTriVeSinh);
        }

        // POST: DonViChiTiet_ViTriVeSinh/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Edit(long id, [Bind("id,TenViTriVeSinh,IdDonViChiTiet")] DonViChiTiet_ViTriVeSinh donViChiTiet_ViTriVeSinh)
        {
            if (id != donViChiTiet_ViTriVeSinh.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    donViChiTiet_ViTriVeSinh.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_ViTriVeSinh.IdDonViChiTiet).TenDonVi;
                    _context.Update(donViChiTiet_ViTriVeSinh);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DonViChiTiet_ViTriVeSinhExists(donViChiTiet_ViTriVeSinh.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_ViTriVeSinh);
        }

        // GET: DonViChiTiet_ViTriVeSinh/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_ViTriVeSinh = await _context.DonViChiTiet_ViTriVeSinh
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_ViTriVeSinh == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_ViTriVeSinh);
        }

        // POST: DonViChiTiet_ViTriVeSinh/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var donViChiTiet_ViTriVeSinh = await _context.DonViChiTiet_ViTriVeSinh.FindAsync(id);
            _context.DonViChiTiet_ViTriVeSinh.Remove(donViChiTiet_ViTriVeSinh);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DonViChiTiet_ViTriVeSinhExists(long id)
        {
            return _context.DonViChiTiet_ViTriVeSinh.Any(e => e.id == id);
        }
    }
}
