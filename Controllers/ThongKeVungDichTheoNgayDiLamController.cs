﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using dbChange.Models;
using dbChange.Models.LIB;
using dbChange.Repository;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Http;
using System.Data;

namespace dbChange.Controllers
{
    public class ThongKeVungDichTheoNgayDiLamController : Controller
    {
        private bool isLoged = false;
        private readonly IAccountRepository _repository;
        private readonly ILibRepository _Librepository;
        private readonly IBaoCaoRepository _BaoCaorepository;
        public ThongKeVungDichTheoNgayDiLamController(IAccountRepository repository, ILibRepository Librepository, IHttpContextAccessor httpContextAccessor, IBaoCaoRepository BaoCaorepository)
        {
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
            }
            _Librepository = Librepository;
            _repository = repository;
            _BaoCaorepository = BaoCaorepository;
        }
        public IActionResult Index()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                ViewData["css"] = "~/wwwroot/Includes/ThongKeVungDichTheoNgayDiLam/Index_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/ThongKeVungDichTheoNgayDiLam/Index_js.cshtml";

                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                return View();
            }
        }

        public JsonResult GetNgayDiLamCuaNhanVien(long DonViChiTiet, long BoPhan, string Thang)
        {
            string result = "";
            string[] lsThangNam = Thang.Split('-');
            if (lsThangNam.Length == 2)
            {
                result = "{'DanhSachNhanVien':[";
                int _Thang = Convert.ToInt32(lsThangNam[1]);
                int _Nam = Convert.ToInt32(lsThangNam[0]);
                DataTable dtMaster = new DataTable();
                dtMaster = _BaoCaorepository.GetDanhSachNhanVienDiLamTheoThangNam(DonViChiTiet, BoPhan, _Thang, _Nam);
                int i = 0;
                List<long> lsIdNhanVien = new List<long>();
                foreach(DataRow dr in dtMaster.Rows)
                {
                    if (!lsIdNhanVien.Contains(Convert.ToInt64(dr["Id"])))
                    {
                        if (i == 0) result += "{'text': '" + dr["HoTen"] + "','id': " + dr["Id"] + ",'color': '#337ab7'}";
                        else result += ",{'text': '" + dr["HoTen"] + "','id': " + dr["Id"] + ",'color': '#337ab7'}";
                        i++;
                        lsIdNhanVien.Add(Convert.ToInt64(dr["Id"]));
                    }
                }
                result += "]";
                result += ",'DanhSachNgayDiLam':[";
                i = 0;
                foreach(DataRow dr in dtMaster.Rows)
                {
                    if(i==0) result += "{'NhanVienId': "+ dr["Id"] + ",'text': 'Đã đăng ký','startDate': '"+ Convert.ToDateTime(dr["Ngay"]).ToString("yyyy-MM-dd 00:00:00.000") + "','endDate': '" + Convert.ToDateTime(dr["Ngay"]).ToString("yyyy-MM-dd 00:00:00.000") + "'}";
                    else result += ",{'NhanVienId': "+ dr["Id"] + ",'text': 'Đã đăng ký','startDate': '" + Convert.ToDateTime(dr["Ngay"]).ToString("yyyy-MM-dd 00:00:00.000") + "','endDate': '" + Convert.ToDateTime(dr["Ngay"]).ToString("yyyy-MM-dd 00:00:00.000") + "'}";
                    i++;
                }
                result += "]}";
                return Json(result);
            }
            else
            {
                return Json("{'DanhSachNhanVien':[], 'DanhSachNgayDiLam':[]}");
            }
        }
    }
}