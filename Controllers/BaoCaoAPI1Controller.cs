﻿using dbChange.Models;
using dbChange.Models.BaoCao;
using dbChange.Repository;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;

namespace dbChange.Controllers
{
    [Route("api/[controller]")]
    public class BaoCaoAPI1Controller : Controller
    {
        private readonly IBaoCaoRepository _repository;

        private readonly IWebHostEnvironment _env;
        public BaoCaoAPI1Controller(IBaoCaoRepository repository, IWebHostEnvironment env)
        {
            _repository = repository;
            _env = env;
        }

        public IActionResult Index()
        {
            //var token = HttpContext.Session.GetString("token");
            //if (token == null)
            //{
            //    return View("Views/Account/Login.cshtml");
            //}
            return View();
        }

        public IActionResult GetAll_ThongKeTiemChungTheoTaiKhoan(string TenDangNhap)
        {
            return Ok(_repository.GetAll_ThongKeTiemChungTheoTaiKhoan(TenDangNhap));
        }


        public IActionResult GetAll_ThongKeTiemChungTheoTaiKhoan_Theo_DonViTong(string TenDangNhap)
        {
            return Ok(_repository.GetAll_ThongKeTiemChungTheoTaiKhoan_Theo_DonViTong(TenDangNhap));
        }

        [HttpGet]
        public IActionResult GetAll_ThongKeMuiTiem_Tong_TheoTaiKhoan()
        {
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");

            }
            if (token != "")
            {
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                bool xem = Convert.ToBoolean(ViewBag.XemTong);
               
                return Ok(_repository.GetAll_ThongKeMuiTiem_Tong_TheoTaiKhoan(acc.TenDangNhap));
              
            }
            else
                return Ok();
        }
    }
}
