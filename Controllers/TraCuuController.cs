﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using dbChange.Repository;
using dbChange.Models;
using QRCoder;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace dbChange.Controllers
{
    public class TraCuuController : Controller
    {
        public string DienThoaiTraCuu = "";
        IStaffRepository _staffRepository;
        public TraCuuController(IHttpContextAccessor httpContextAccessor, IStaffRepository staffRepository)
        {
            if (httpContextAccessor.HttpContext.Request.Cookies["DienThoaiTraCuu"] != null)
            {
                DienThoaiTraCuu = httpContextAccessor.HttpContext.Request.Cookies["DienThoaiTraCuu"];
            }
            _staffRepository = staffRepository;
        }
        public IActionResult Index()
        {
            if (DienThoaiTraCuu != "")
            {
                //return RedirectToAction(actionName: "Index", controllerName: "TraCuuQRCodeStaff");
                ViewData["css"] = "~/wwwroot/Includes/TraCuuQRCodeStaff/Index_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/TraCuuQRCodeStaff/Index_js.cshtml";
                return View();
            }
            else
            {
                ViewData["css"] = "~/wwwroot/Includes/TraCuuQRCodeStaff/Index_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/TraCuuQRCodeStaff/Index_js.cshtml";
                return View();

            }
        }
        public string GetQRCodeStaff(string DienThoaiTraCuu)
        {
            Staff staff = new Staff();
            staff = _staffRepository.GetStaffByDienThoai(DienThoaiTraCuu);
            if ( staff != null && staff.Id >0) {
                var bitmapBytes = BitmapToBytes(GetQRCode(staff.MaQR));
                Dictionary<string, string> result = new Dictionary<string, string>();
                result.Add("HoTen",staff.HoTen);
                result.Add("DonVi",staff.DonViChiTiet);
                result.Add("MaQR", String.Format("data:image/png;base64,{0}", Convert.ToBase64String(bitmapBytes)));
                return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            }
            else
            {
                return "";
            }
            /*
             List<Staff> staffs = new List<Staff>();
            staffs = _staffRepository.GetStaffByDienThoai(DienThoaiTraCuu);
            if ( staffs != null && staffs.Count ==1) {
                var bitmapBytes = BitmapToBytes(GetQRCode(staffs[0].MaQR));
                Dictionary<string, string> result = new Dictionary<string, string>();
                result.Add("HoTen",staffs[0].HoTen);
                result.Add("DonVi",staffs[0].DonViChiTiet);
                result.Add("MaQR", String.Format("data:image/png;base64,{0}", Convert.ToBase64String(bitmapBytes)));
                return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            }
            else
            {
                return "";
            }
            
             */
        }
        public Bitmap GetQRCode(string qrcode)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(qrcode, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            return qrCodeImage;
        }
        private static byte[] BitmapToBytes(Bitmap img)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
    }
}