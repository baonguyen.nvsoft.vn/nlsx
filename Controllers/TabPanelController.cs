﻿using dbChange.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Controllers
{
    public class TabPanelController : Controller
    {
        public ActionResult Overview()
        {
            return View();
        }

        [HttpGet]
        public object GetMenu(DataSourceLoadOptions loadOptions)
        {
            List<Staff_Index_Menu> list = new List<Staff_Index_Menu>();
            list.Add(new Staff_Index_Menu { Id = 1, Text = "01. Danh Sách Nhân Viên", Expanded = true });
            list.Add(new Staff_Index_Menu { Id = 2, Text = "02. Cập Nhật Thông Tin", Expanded = true });
            list.Add(new Staff_Index_Menu { Id = 3, Text = "03. Thống Kê Mũi Tiêm", Expanded = true });
            list.Add(new Staff_Index_Menu { Id = 4, Text = "04. Khai Báo Ca Nhiễm", Expanded = true });
            return DataSourceLoader.Load(list, loadOptions);
        }
    }
}
