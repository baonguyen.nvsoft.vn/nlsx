﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using dbChange.Models;
using dbChange.Models.VanPhongSo;
using dbChange.Repository;
using dbChange.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Net.Http;
using System.Net;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using dbChange.Models.LIB;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.XtraRichEdit;
using System.Drawing;
using System.Drawing.Imaging;
using asprise_imaging_api;

namespace dbChange.Controllers
{
    public class VanPhongSoController : Controller
    {
        #region "define global"
        private bool isLoged = false;
        private readonly IAccountRepository _repository;
        private readonly ILibRepository _Librepository;
        private readonly IVanBanRepository _vanBanRepository;
        private readonly IVanBanChiTietRepository _vanBanChiTietRepository;
        private readonly string IPCloud = "";
        private readonly string LinkCloud ="";
        private readonly string UserCloud ="";
        private readonly string PassCloud ="";
        private Account accMain;
        private IHttpContextAccessor _httpContextAccessor;
        private IHostingEnvironment _environment;
        private string SoNgayHetHanChiaSe = "0";
        public VanPhongSoController(IHttpContextAccessor httpContextAccessor, ILibRepository Librepository, IAccountRepository repository
            , IVanBanRepository vanBanRepository, IVanBanChiTietRepository vanBanChiTietRepository, IConfiguration configuration, IHostingEnvironment environment)
        {
            _httpContextAccessor = httpContextAccessor;
            _repository = repository;
            _Librepository = Librepository;
            _vanBanRepository = vanBanRepository;
            _vanBanChiTietRepository = vanBanChiTietRepository;
            _environment = environment;
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
                string a = Utils.Utils.Decrypt(httpContextAccessor.HttpContext.Request.Cookies["token"]);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                accMain = _repository.GetAccount(acc.TenDangNhap);
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
                string a = Utils.Utils.Decrypt(httpContextAccessor.HttpContext.Session.GetString("token"));
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                accMain = _repository.GetAccount(acc.TenDangNhap);
            }
            IPCloud = configuration.GetConnectionString("IPCloud");
            LinkCloud = configuration.GetConnectionString("LinkCloud");
            UserCloud = configuration.GetConnectionString("UserCloud");
            PassCloud = configuration.GetConnectionString("PassCloud");
            SoNgayHetHanChiaSe = configuration.GetConnectionString("SoNgayHetHanChiaSe");
        }

        #endregion

        #region "Cac chuc nang chinh"
        public IActionResult ScanBarcode()
        {
            if (isLoged && accMain.isVPS)
            {
                ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                ViewData["acc"] = accMain;
                if (_repository.isScan(accMain))
                {
                    ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                    ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/VPS_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/ScanBarcode_js.cshtml";
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
        }
        public IActionResult Index()
        {
            if (isLoged && accMain.isVPS)
            {
                
                ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                ViewData["acc"] = accMain;
                if (accMain.isTaoPhieu) // dc phep truy cap khong
                {
                    if (_repository.isScan(accMain)) // co dc scan khong
                    {
                        ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);
                        ViewData["LoaiVanBan"] = _Librepository.GetAllLoaiVanBan();
                        ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();
                        ViewData["isScan"] = 1;
                        ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/VPS_css.cshtml";
                        ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/VPS_js.cshtml";
                        return View();

                    }
                    else
                    {
                        ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);
                        ViewData["LoaiVanBan"] = _Librepository.GetAllLoaiVanBan();
                        ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();
                        ViewData["isScan"] = 0;
                        ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/VPS_css.cshtml";
                        ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/VPS_js.cshtml";
                        return View();
                    }
                }
                else
                {
                    if (_repository.isScan(accMain))
                    {
                        return RedirectToAction(actionName: "ScanBarcode", controllerName: "VanPhongSo");
                    }
                    else
                    {
                        return RedirectToAction(actionName: "Index", controllerName: "Home");
                    }
                }
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
        }
        public IActionResult ChungTuChiTiet()
        {
            if (isLoged && accMain.isVPS)
            {
                ViewData["acc"] = accMain;
                ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                ViewData["isAdmin"] = _repository.isAdmin(accMain) == true ? 1 : 0;
                ViewData["IdAccount"] = accMain.Id;
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);
                ViewData["LoaiVanBan"] = _Librepository.GetAllLoaiVanBan();

                ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();
                ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/ChungTuChiTiet_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/ChungTuChiTiet_js.cshtml";

                return View();
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
        }
        [HttpGet]
        public string GetAllVanBan(string TrangThai)
        {
            if (isLoged && accMain.isVPS)
            {
                List<VanBan> dt = new List<VanBan>();
                if (accMain.isTaoPhieu)
                {
                    if(accMain.isTruongPhong)
                     dt = _vanBanRepository.GetDanhSach_TruongPhong(accMain.Id,TrangThai!=null ? TrangThai : "MoiTao");
                    else
                     dt = _vanBanRepository.GetDanhSach(accMain.Id,TrangThai!=null ? TrangThai : "MoiTao");
                    return dt.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        [HttpGet]
        public string GetVanBanByMaVanBan(string MaVanBan)
        {
            if (isLoged && accMain.isVPS)
            {
                VanBan vb = new VanBan();
                vb = _vanBanRepository.GetByMaVanBan(MaVanBan);
                return Newtonsoft.Json.JsonConvert.SerializeObject(vb);
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        public string GetVanBanByMaVanBanOrLinkShare(string MaVanBan)
        {
            if (isLoged && accMain.isVPS)
            {
                try
                {
                    VanBan vb = new VanBan();
                    vb = _vanBanRepository.GetByMaVanBan(MaVanBan);
                    if (vb.Id > 0)
                        return Newtonsoft.Json.JsonConvert.SerializeObject(vb);
                    else
                    {
                        // tim theo link chia se
                        string[] links = MaVanBan.Split('?');
                        string query = links[1];
                        string[] codes = query.Split('&');
                        string mavanban_de = Utils.Utils.Decrypt(codes[0].Substring(5, codes[0].Length - 5));
                        vb = _vanBanRepository.GetByMaVanBan(mavanban_de);
                        return Newtonsoft.Json.JsonConvert.SerializeObject(vb);
                    }
                }catch(Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        [HttpGet]
        public string GetAllChungTuChiTiet(string TrangThai)
        {
            if (isLoged && accMain.isVPS)
            {
                
                DataTable dt = new DataTable();
                if (accMain.isTaoPhieu)
                {
                    dt = _vanBanChiTietRepository.GetAllChungTuChiTiet(accMain.Id,TrangThai!=null ? TrangThai : "MoiTao");
                    return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        public IActionResult TaoMoi()
        {
            if (isLoged && accMain.isVPS)
            {
                ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                ViewData["acc"] = accMain;
                ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/VPS_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/VPS_js.cshtml";

                return View();
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
        }
        public string TaoPhieuDangKy(string TieuDe, string NoiDung, long IdDonVi, string MaQuyenVanBan, string GhiChu, long IdLoaiVanBan)
        {
            VanBan vb = new VanBan();
            if(GhiChu!=null)
             vb.GhiChu = GhiChu;
            else
             vb.GhiChu = "";
            vb.IdDonVi = IdDonVi;
            vb.IdNguoiTao = accMain.Id;
            vb.MaQR = "";
            vb.MaVanBan = "";
            vb.MaQuyenVanBan = MaQuyenVanBan;
            vb.NoiDung = NoiDung;
            vb.TieuDe = TieuDe;
            vb.TrangThai = "MoiTao";
            vb.IdLoaiVanban = IdLoaiVanBan;
            vb = _vanBanRepository.TaoMoi(vb);

            return vb.Id.ToString();
        }
        public string CapNhatPhieuDangKy(long IdVanBan, string TieuDe, string NoiDung, string MaQuyenVanBan, string GhiChu, long IdLoaiVanBan)
        {
            
            VanBan vb = new VanBan();
            vb = _vanBanRepository.GetById(IdVanBan);
            vb.MaQuyenVanBan = MaQuyenVanBan;
            vb.NoiDung = NoiDung;
            vb.TieuDe = TieuDe;
            vb.IdLoaiVanban = IdLoaiVanBan;
            if (GhiChu != null)
                vb.GhiChu = GhiChu;
            vb = _vanBanRepository.CapNhap(vb,accMain.Id, "Cập nhật thông tin phiếu đăng ký");
            return vb.Id.ToString();
        }
        public string KhoaPhieuDangKy(long IdVanBan)
        {
            if (IdVanBan > 0)
            {
                VanBan vb = new VanBan();
                vb = _vanBanRepository.GetById(IdVanBan);
                List<VanBan_ChiTiet> lsChiTiet = new List<VanBan_ChiTiet>();
                lsChiTiet = _vanBanChiTietRepository.GetDanhSach(vb.MaVanBan);
                if (lsChiTiet.Count > 0)
                {
                    vb.TrangThai = "DaKhoa";
                    _vanBanRepository.CapNhap(vb,accMain.Id, "Khóa phiếu đăng ký");
                    //_vanBanRepository.KhoaPhieu(IdVanBan);
                    return "SUCCESS";
                }
                else
                {
                    return "ERROR_DETAIL";
                }
            }
            return "ERROR_MASTER";
        }
        public string HoanTatPhieuDangKy(long IdVanBan)
        {
            if (IdVanBan > 0)
            {
                VanBan vb = new VanBan();
                vb = _vanBanRepository.GetById(IdVanBan);
                List<VanBan_ChiTiet> lsChiTiet = new List<VanBan_ChiTiet>();
                lsChiTiet = _vanBanChiTietRepository.GetDanhSach(vb.MaVanBan);
                if (lsChiTiet.Count > 0)
                {
                    vb.TrangThai = "HoanTat";
                    _vanBanRepository.CapNhap(vb,accMain.Id, "Cập nhật hoàn tất phiếu đăng ký");
                    //_vanBanRepository.KhoaPhieu(IdVanBan);
                    // send email cho nguoi tao
                    _repository.SendEmailHoanTatVanBan(vb.IdNguoiTao, vb);
                    return "SUCCESS";
                }
                else
                {
                    return "ERROR_DETAIL";
                }
            }
            return "ERROR_MASTER";
        }
        public string MoKhoaPhieuDangKy(long IdVanBan)
        {
            if (IdVanBan > 0)
            {
                VanBan vb = new VanBan();
                vb = _vanBanRepository.GetById(IdVanBan);
                vb.TrangThai = "MoiTao";
                _vanBanRepository.CapNhap(vb, accMain.Id,"Mở khóa phiếu đăng ký");
                //_vanBanRepository.MoKhoaPhieu(IdVanBan);
                return "SUCCESS";
            }
            return "ERROR_MASTER";
        }
        public IActionResult BoChungTu_ChiTiet(long id)
        {
            if (isLoged && accMain.isVPS)
            {

                ViewData["acc"] = accMain; 
                ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                ViewData["isScan"] = _repository.isScan(accMain) ==true ? 1 : 0;
                ViewData["isAdmin"] = _repository.isAdmin(accMain) ==true ? 1 : 0;
                ViewData["IdAccount"] = accMain.Id;
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);
                ViewData["LoaiVanBan"] = _Librepository.GetAllLoaiVanBan();

                ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_js.cshtml";
                VanBan vanban = new VanBan();
                vanban = _vanBanRepository.GetById(id);
                List<VanBan_ChiTiet> vanBan_ChiTiet = new List<VanBan_ChiTiet>();
                vanBan_ChiTiet = _vanBanChiTietRepository.GetDanhSach(vanban.MaVanBan);
                ViewData["vanban"] = vanban;
                ViewData["vanBan_ChiTiet"] = vanBan_ChiTiet;
                ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();
                return View();
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            
        }
        [HttpGet]
        public string GetAllChiTietVanBan(long IdVanBan)
        {
            if (isLoged && accMain.isVPS)
            {
                
                VanBan vb = new VanBan();
                vb = _vanBanRepository.GetById(IdVanBan);
                List<VanBan_ChiTiet> dt = new List<VanBan_ChiTiet>();
                dt = _vanBanChiTietRepository.GetDanhSach(vb.MaVanBan);
                return dt.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        public string HuyPhieu(long Id)
        {
            VanBan vb = new VanBan();
            vb = _vanBanRepository.GetById(Id);
            vb.TrangThai = "DaHuy";
            _vanBanRepository.CapNhap(vb, accMain.Id, "Hủy phiếu đăng ký");
            //_vanBanRepository.HuyPhieu(Id);
            return "";
        }
        public string XoaChiTiet(long Id)
        {
            if (Id > 0)
            {
                VanBan_ChiTiet vbct = new VanBan_ChiTiet();
                vbct = _vanBanChiTietRepository.GetById(Id);
                if (vbct.Link != "")
                {
                    _vanBanChiTietRepository.Huy(Id,accMain.Id,"Hủy chi tiết ["+ vbct.Id + "-" + vbct.TieuDe + "]");
                    return "SUCCESS";
                }
                else
                {
                    _vanBanChiTietRepository.Xoa(Id, accMain.Id, "Xóa chi tiết [" + vbct.Id + "-" + vbct.TieuDe + "]");
                    return "SUCCESS";
                }
            }
            return "ERROR_MASTER";
        }
        public string ThemVanBanChiTiet(string TieuDe, string NoiDung, long IdVanBan, string MaQuyenVanBan)
        {
            if (TieuDe != null)
            {
                
                VanBan vb = new VanBan();
                vb = _vanBanRepository.GetById(IdVanBan);
                VanBan_ChiTiet vbct = new VanBan_ChiTiet();
                vbct.IdNguoiTao = accMain.Id;
                vbct.MaQR = "";
                vbct.MaVanBan = vb.MaVanBan;
                vbct.MaQuyenVanBan = MaQuyenVanBan;
                vbct.NoiDung = NoiDung != null ? NoiDung : "";
                vbct.TieuDe = TieuDe;
                vbct = _vanBanChiTietRepository.TaoMoi(vbct);

                return vbct.Id.ToString();
            }
            else
            {
                return "";
            }
        }
        public IActionResult TimKiem()
        {
            if (isLoged && accMain.isVPS)
            {
                ViewData["acc"] = accMain; 
                ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                ViewData["IdAccount"] = accMain.Id;
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);
                ViewData["LoaiVanBan"] = _Librepository.GetAllLoaiVanBan();

                ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_timkiem_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_timkiem_js.cshtml";
                
                return View();
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }

        }

        public string GetSearch(string Search, int Option, DateTime TuNgay, DateTime DenNgay, string IdDonVi, string IdLoaiVanBan)
        {
            DataTable dt = new DataTable();
            if(accMain.isTruongPhong)
                dt = _vanBanChiTietRepository.GetAllChungTuChiTiet_Search_TruongPhong(Search != null ? Search : "", Option, accMain.Id, TuNgay, DenNgay, IdDonVi != null ? IdDonVi : "", IdLoaiVanBan != null ? IdLoaiVanBan : "");
            else
                dt = _vanBanChiTietRepository.GetAllChungTuChiTiet_Search(Search != null ? Search : "", Option, accMain.Id, TuNgay, DenNgay, IdDonVi!=null ? IdDonVi : "", IdLoaiVanBan!=null ? IdLoaiVanBan : "");
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        #endregion

        #region "Upload files"
        
        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo ici in encoders)
                if (ici.MimeType == mimeType) return ici;

            return null;
        }
        public byte[] ConvertListImageToPdf_Image(List<byte[]> lsImage)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document();
            doc.SetPageSize(iTextSharp.text.PageSize.A4);
            var ms = new System.IO.MemoryStream();
            iTextSharp.text.pdf.PdfCopy pdf = new iTextSharp.text.pdf.PdfCopy(doc, ms);
            doc.Open();
            foreach (var row in lsImage)
            {
                byte[] data = row;
                doc.NewPage();
                iTextSharp.text.Document imageDocument = null;
                iTextSharp.text.pdf.PdfWriter imageDocumentWriter = null;
                switch ("jpg")
                {
                    case "bmp":
                    case "gif":
                    case "jpg":
                    case "png":
                        imageDocument = new iTextSharp.text.Document();
                        using (var imageMS = new MemoryStream())
                        {
                            imageDocumentWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(imageDocument, imageMS);
                            imageDocument.Open();
                            if (imageDocument.NewPage())
                            {
                                var image = iTextSharp.text.Image.GetInstance(data);
                                image.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
                                image.ScaleToFit(doc.PageSize.Width - 10, doc.PageSize.Height - 10);
                                if (!imageDocument.Add(image))
                                {
                                    throw new Exception("Unable to add image to page!");
                                }
                                imageDocument.Close();
                                imageDocumentWriter.Close();
                                iTextSharp.text.pdf.PdfReader imageDocumentReader = new iTextSharp.text.pdf.PdfReader(imageMS.ToArray());
                                var page = pdf.GetImportedPage(imageDocumentReader, 1);
                                pdf.AddPage(page);
                                imageDocumentReader.Close();
                            }
                        }
                        break;
                    
                    default:
                        break;
                }
            }
            if (doc.IsOpen() && doc!=null)
                doc.Close();
            return ms.ToArray();
        }
        public string TestUploadAPI(string imagesScanned, long id, string MaVanBan)
        {
            try {
                List<byte[]> lsImage = new List<byte[]>();
                foreach (var s in imagesScanned.Split('-'))
                {
                    byte[] imageBytes = Convert.FromBase64String(s.Split(',')[1]);
                    lsImage.Add(imageBytes);
                }
                byte[] document = ConvertListImageToPdf_Image(lsImage);
                string path_pdf = Path.GetTempPath() + "1.pdf";
                System.IO.File.WriteAllBytes(path_pdf, document);
                FileStream file = new FileStream(path_pdf, FileMode.Open);
                //System.IO.File.Delete(path_pdf);
                return UploadAPI(file, id, MaVanBan);
            }catch(Exception ex)
            {
                return "ERROR";
            }
        }

        public ActionResult ShareVanBan([FromQuery(Name = "code")] string code, [FromQuery(Name = "time")] string time)
        {
            try
            {
                string giaima = Utils.Utils.Decrypt(code);
                string ngayshare = Utils.Utils.Decrypt(time);
                DateTime dt = Convert.ToDateTime(ngayshare);
                if (Convert.ToInt32(SoNgayHetHanChiaSe) > 0)
                {
                    if (dt.AddDays(Convert.ToInt32(SoNgayHetHanChiaSe)) >= DateTime.Now && accMain != null)
                    {

                        ViewData["acc"] = accMain;
                        ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                        ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                        ViewData["isAdmin"] = _repository.isAdmin(accMain) == true ? 1 : 0;
                        ViewData["IdAccount"] = accMain.Id;
                        ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);

                        ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_css.cshtml";
                        ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_share_js.cshtml";

                        VanBan vanban = new VanBan();
                        vanban = _vanBanRepository.GetByMaVanBan(giaima);
                        List<VanBan_ChiTiet> vanBan_ChiTiet = new List<VanBan_ChiTiet>();
                        vanBan_ChiTiet = _vanBanChiTietRepository.GetDanhSach(vanban.MaVanBan);
                        ViewData["vanban"] = vanban;
                        ViewData["vanBan_ChiTiet"] = vanBan_ChiTiet;
                        ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();

                        return View();
                    }
                    else
                    {
                        var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}{Request.QueryString}");
                        var url = location.AbsoluteUri;
                        var cookieOptions = new CookieOptions()
                        {
                            Path = "/",
                            HttpOnly = false,
                            IsEssential = true,
                            Expires = DateTime.Now.AddMinutes(5),
                        };

                        HttpContext.Session.SetString("callbackURL", url);
                        return RedirectToAction(actionName: "LoiChiSe", controllerName: "VanPhongSo");
                    }
                }
                else
                {
                    ViewData["acc"] = accMain;
                    ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                    ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                    ViewData["isAdmin"] = _repository.isAdmin(accMain) == true ? 1 : 0;
                    ViewData["IdAccount"] = accMain.Id;
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);

                    ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_share_js.cshtml";

                    VanBan vanban = new VanBan();
                    vanban = _vanBanRepository.GetByMaVanBan(giaima);
                    List<VanBan_ChiTiet> vanBan_ChiTiet = new List<VanBan_ChiTiet>();
                    vanBan_ChiTiet = _vanBanChiTietRepository.GetDanhSach(vanban.MaVanBan);
                    ViewData["vanban"] = vanban;
                    ViewData["vanBan_ChiTiet"] = vanBan_ChiTiet;
                    ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();

                    return View();
                }
            }catch(Exception ex)
            {
                var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}{Request.QueryString}");
                var url = location.AbsoluteUri;
                var cookieOptions = new CookieOptions()
                {
                    Path = "/",
                    HttpOnly = false,
                    IsEssential = true,
                    Expires = DateTime.Now.AddMinutes(5),
                };

                HttpContext.Session.SetString("callbackURL", url);
                return RedirectToAction(actionName: "LoiChiSe", controllerName: "VanPhongSo");
            }
            
        }
        public ActionResult LoiChiSe()
        {
            ViewData["isScan"] = 0;
            ViewData["isTaoPhieu"] = 0;
            ViewData["acc"] = accMain;
            return View();
        }
        public async Task<IActionResult> ShareFile([FromQuery(Name = "code")] string code, [FromQuery(Name = "time")] string time)
        {
            string giaima = Utils.Utils.Decrypt(code);
            string ngayshare = Utils.Utils.Decrypt(time);
            DateTime dt = Convert.ToDateTime(ngayshare);
            if (Convert.ToInt32(SoNgayHetHanChiaSe) > 0)
            {
                if (isLoged)
                {
                    ViewData["acc"] = accMain;
                    ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                    ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                    ViewData["isAdmin"] = _repository.isAdmin(accMain) == true ? 1 : 0;
                    ViewData["IdAccount"] = accMain.Id;
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);

                    ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_share_js.cshtml";

                    VanBan vanban = new VanBan();
                    vanban = _vanBanRepository.GetByMaVanBan(giaima);
                    List<VanBan_ChiTiet> vanBan_ChiTiet = new List<VanBan_ChiTiet>();
                    vanBan_ChiTiet = _vanBanChiTietRepository.GetDanhSach(vanban.MaVanBan);
                    ViewData["vanban"] = vanban;
                    ViewData["vanBan_ChiTiet"] = vanBan_ChiTiet;
                    ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();
                    string[] files_path = giaima.Split('/');


                    ViewData["path"] = "";
                    try
                    {
                        using (var httpClient = new HttpClient())
                        {
                            Uri uploadUri = new Uri(IPCloud + giaima);
                            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uploadUri);
                            string method = WebRequestMethods.Http.Get.ToString();
                            NetworkCredential networkCredential = new NetworkCredential(UserCloud, PassCloud);
                            httpWebRequest.Credentials = networkCredential;
                            // Send authentication along with first request.
                            httpWebRequest.PreAuthenticate = true;
                            httpWebRequest.Method = method;
                            httpWebRequest.ContentType = "multipart/form-data";
                            HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();

                            string path = Path.GetTempPath();// Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

                            if (!path.EndsWith(@"\")) path += @"\";
                            path += files_path[files_path.Length - 1];
                            int contentLength = int.Parse(response.GetResponseHeader("Content-Length"));
                            using (Stream s = response.GetResponseStream())
                            {
                                using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                                {
                                    byte[] content = new byte[4096];
                                    int bytesRead = 0;
                                    do
                                    {
                                        bytesRead = s.Read(content, 0, content.Length);
                                        fs.Write(content, 0, bytesRead);
                                    } while (bytesRead > 0);
                                }
                                var memory = new MemoryStream();
                                using (var stream = new FileStream(path, FileMode.Open))
                                {
                                    await stream.CopyToAsync(memory);
                                }
                                memory.Position = 0;
                                System.IO.File.Delete(path);
                                return File(memory, GetContentType(path), Path.GetFileName(path));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewData["ex"] = ex;
                    }
                }
                else
                {
                    ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_share_js.cshtml";
                    VanBan vanban = new VanBan();
                    vanban = _vanBanRepository.GetByMaVanBan(giaima);
                    List<VanBan_ChiTiet> vanBan_ChiTiet = new List<VanBan_ChiTiet>();
                    vanBan_ChiTiet = _vanBanChiTietRepository.GetDanhSach(vanban.MaVanBan);
                    ViewData["vanban"] = vanban;
                    ViewData["vanBan_ChiTiet"] = vanBan_ChiTiet;
                    ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();
                    string[] files_path = giaima.Split('/');


                    ViewData["path"] = "";
                    try
                    {
                        using (var httpClient = new HttpClient())
                        {
                            Uri uploadUri = new Uri(IPCloud + giaima);
                            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uploadUri);
                            string method = WebRequestMethods.Http.Get.ToString();
                            NetworkCredential networkCredential = new NetworkCredential(UserCloud, PassCloud);
                            httpWebRequest.Credentials = networkCredential;
                            // Send authentication along with first request.
                            httpWebRequest.PreAuthenticate = true;
                            httpWebRequest.Method = method;
                            httpWebRequest.ContentType = "multipart/form-data";
                            HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();

                            string path = Path.GetTempPath();// Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

                            if (!path.EndsWith(@"\")) path += @"\";
                            path += files_path[files_path.Length - 1];
                            int contentLength = int.Parse(response.GetResponseHeader("Content-Length"));
                            using (Stream s = response.GetResponseStream())
                            {
                                using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                                {
                                    byte[] content = new byte[4096];
                                    int bytesRead = 0;
                                    do
                                    {
                                        bytesRead = s.Read(content, 0, content.Length);
                                        fs.Write(content, 0, bytesRead);
                                    } while (bytesRead > 0);
                                }
                                var memory = new MemoryStream();
                                using (var stream = new FileStream(path, FileMode.Open))
                                {
                                    await stream.CopyToAsync(memory);
                                }
                                memory.Position = 0;
                                System.IO.File.Delete(path);
                                return File(memory, GetContentType(path), Path.GetFileName(path));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewData["ex"] = ex;
                    }
                }
                return View();
            }
            else
            {
                if (dt.AddDays(5) >= DateTime.Now)
                {
                    if (isLoged)
                    {
                        ViewData["acc"] = accMain;
                        ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                        ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                        ViewData["isAdmin"] = _repository.isAdmin(accMain) == true ? 1 : 0;
                        ViewData["IdAccount"] = accMain.Id;
                        ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);

                        ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_css.cshtml";
                        ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_share_js.cshtml";

                        VanBan vanban = new VanBan();
                        vanban = _vanBanRepository.GetByMaVanBan(giaima);
                        List<VanBan_ChiTiet> vanBan_ChiTiet = new List<VanBan_ChiTiet>();
                        vanBan_ChiTiet = _vanBanChiTietRepository.GetDanhSach(vanban.MaVanBan);
                        ViewData["vanban"] = vanban;
                        ViewData["vanBan_ChiTiet"] = vanBan_ChiTiet;
                        ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();
                        string[] files_path = giaima.Split('/');


                        ViewData["path"] = "";
                        try
                        {
                            using (var httpClient = new HttpClient())
                            {
                                Uri uploadUri = new Uri(IPCloud + giaima);
                                HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uploadUri);
                                string method = WebRequestMethods.Http.Get.ToString();
                                NetworkCredential networkCredential = new NetworkCredential(UserCloud, PassCloud);
                                httpWebRequest.Credentials = networkCredential;
                                // Send authentication along with first request.
                                httpWebRequest.PreAuthenticate = true;
                                httpWebRequest.Method = method;
                                httpWebRequest.ContentType = "multipart/form-data";
                                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();

                                string path = Path.GetTempPath();// Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

                                if (!path.EndsWith(@"\")) path += @"\";
                                path += files_path[files_path.Length - 1];
                                int contentLength = int.Parse(response.GetResponseHeader("Content-Length"));
                                using (Stream s = response.GetResponseStream())
                                {
                                    using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                                    {
                                        byte[] content = new byte[4096];
                                        int bytesRead = 0;
                                        do
                                        {
                                            bytesRead = s.Read(content, 0, content.Length);
                                            fs.Write(content, 0, bytesRead);
                                        } while (bytesRead > 0);
                                    }
                                    var memory = new MemoryStream();
                                    using (var stream = new FileStream(path, FileMode.Open))
                                    {
                                        await stream.CopyToAsync(memory);
                                    }
                                    memory.Position = 0;
                                    System.IO.File.Delete(path);
                                    return File(memory, GetContentType(path), Path.GetFileName(path));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ViewData["ex"] = ex;
                        }
                    }
                    else
                    {
                        ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_css.cshtml";
                        ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/Bochungtu_chitiet_share_js.cshtml";
                        VanBan vanban = new VanBan();
                        vanban = _vanBanRepository.GetByMaVanBan(giaima);
                        List<VanBan_ChiTiet> vanBan_ChiTiet = new List<VanBan_ChiTiet>();
                        vanBan_ChiTiet = _vanBanChiTietRepository.GetDanhSach(vanban.MaVanBan);
                        ViewData["vanban"] = vanban;
                        ViewData["vanBan_ChiTiet"] = vanBan_ChiTiet;
                        ViewData["Quyen"] = _Librepository.GetAllQuyenVanBan();
                        string[] files_path = giaima.Split('/');


                        ViewData["path"] = "";
                        try
                        {
                            using (var httpClient = new HttpClient())
                            {
                                Uri uploadUri = new Uri(IPCloud + giaima);
                                HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uploadUri);
                                string method = WebRequestMethods.Http.Get.ToString();
                                NetworkCredential networkCredential = new NetworkCredential(UserCloud, PassCloud);
                                httpWebRequest.Credentials = networkCredential;
                                // Send authentication along with first request.
                                httpWebRequest.PreAuthenticate = true;
                                httpWebRequest.Method = method;
                                httpWebRequest.ContentType = "multipart/form-data";
                                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();

                                string path = Path.GetTempPath();// Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

                                if (!path.EndsWith(@"\")) path += @"\";
                                path += files_path[files_path.Length - 1];
                                int contentLength = int.Parse(response.GetResponseHeader("Content-Length"));
                                using (Stream s = response.GetResponseStream())
                                {
                                    using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                                    {
                                        byte[] content = new byte[4096];
                                        int bytesRead = 0;
                                        do
                                        {
                                            bytesRead = s.Read(content, 0, content.Length);
                                            fs.Write(content, 0, bytesRead);
                                        } while (bytesRead > 0);
                                    }
                                    var memory = new MemoryStream();
                                    using (var stream = new FileStream(path, FileMode.Open))
                                    {
                                        await stream.CopyToAsync(memory);
                                    }
                                    memory.Position = 0;
                                    System.IO.File.Delete(path);
                                    return File(memory, GetContentType(path), Path.GetFileName(path));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ViewData["ex"] = ex;
                        }
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "VanPhongSo");
                }
            }
        }
        
        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats  officedocument.spreadsheetml.sheet"},  
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
        public void CreateDir(string DirFolder)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    Uri uploadUri = new Uri(LinkCloud + DirFolder);
                    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uploadUri);
                    string method = WebRequestMethods.Http.MkCol.ToString();
                    NetworkCredential networkCredential = new NetworkCredential(UserCloud, PassCloud);
                    httpWebRequest.Credentials = networkCredential;
                    // Send authentication along with first request.
                    httpWebRequest.PreAuthenticate = true;
                    httpWebRequest.Method = method;
                    httpWebRequest.ContentType = "multipart/form-data";
                    HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();

                }
            }catch (Exception ex)
            {

            }
        }
        
        [HttpPost]
        public string Upload1(IList<IFormFile> files, long id, string MaVanBan)
        {
            if (files.Count > 0)
            {
                using (var httpClient = new HttpClient())
                {
                    VanBan vb = _vanBanRepository.GetByMaVanBan(MaVanBan);
                    LoaiVanBan lvb = _Librepository.GetLoaiVanBan(vb.IdLoaiVanban);
                    //YYYY - MM - DD - LOAI_VB - PHONG_BAN - NGUOI_TAO - VB.PDF
                    string phongban = "";
                    phongban = Utils.Utils.GetTimeServer().Year.ToString();
                    CreateDir(phongban);
                    phongban = phongban + "/" + Utils.Utils.GetTimeServer().Month.ToString("00");
                    CreateDir(phongban);
                    phongban = phongban + "/" + Utils.Utils.GetTimeServer().Day.ToString("00");
                    CreateDir(phongban);
                    if (lvb != null && lvb.TenLoaiVanBan != "")
                    {
                        phongban = phongban + "/" + lvb.TenLoaiVanBan;
                        CreateDir(phongban);
                    }
                    phongban = phongban + "/" + vb.TenDonVi;
                    CreateDir(phongban);
                    phongban = phongban + "/" + vb.TenNguoiTao;
                    CreateDir(phongban);
                    Uri uploadUri = new Uri(LinkCloud + phongban + "/" + MaVanBan + "-" + id + "-" + DateTime.Now.Ticks + ".pdf");
                    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uploadUri);
                    string method = WebRequestMethods.Http.Put.ToString();
                    NetworkCredential networkCredential = new NetworkCredential(UserCloud, PassCloud);
                    httpWebRequest.Credentials = networkCredential;
                    // Send authentication along with first request.
                    httpWebRequest.PreAuthenticate = true;
                    httpWebRequest.Method = method;
                    httpWebRequest.ContentType = "multipart/form-data";
                    Stream rs = httpWebRequest.GetRequestStream();
                    files[0].CopyTo(rs);

                    HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                    if (response.ResponseUri.ToString() != "")
                    {
                        VanBan_ChiTiet vbct = _vanBanChiTietRepository.GetById(id);
                        vbct.Link = response.ResponseUri.LocalPath.ToString();
                        _vanBanChiTietRepository.CapNhap(vbct,accMain.Id,"Cập nhật đường dẫn file scan");
                    }
                    return response.ResponseUri.ToString();
                }
            }
            else
            {
                // khong co file de upload
                return "";
            }
            
        }
        public string UploadAPI(Stream s,long id, string MaVanBan)
        {
            if (s!=null)
            {
                using (var httpClient = new HttpClient())
                {
                    VanBan vb = _vanBanRepository.GetByMaVanBan(MaVanBan);
                    LoaiVanBan lvb = _Librepository.GetLoaiVanBan(vb.IdLoaiVanban);
                    //YYYY - MM - DD - LOAI_VB - PHONG_BAN - NGUOI_TAO - VB.PDF
                    string phongban = "";
                    phongban = Utils.Utils.GetTimeServer().Year.ToString();
                    CreateDir(phongban);
                    phongban = phongban + "/" + Utils.Utils.GetTimeServer().Month.ToString("00");
                    CreateDir(phongban);
                    phongban = phongban + "/" + Utils.Utils.GetTimeServer().Day.ToString("00");
                    CreateDir(phongban);
                    if (lvb != null && lvb.TenLoaiVanBan != "")
                    {
                        phongban = phongban + "/" + lvb.TenLoaiVanBan;
                        CreateDir(phongban);
                    }
                    phongban = phongban + "/" + vb.TenDonVi;
                    CreateDir(phongban);
                    phongban = phongban + "/" + vb.TenNguoiTao;
                    CreateDir(phongban);
                    Uri uploadUri = new Uri(LinkCloud + phongban + "/" + MaVanBan + "-" + id + "-" + DateTime.Now.Ticks + ".pdf");
                    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uploadUri);
                    string method = WebRequestMethods.Http.Put.ToString();
                    NetworkCredential networkCredential = new NetworkCredential(UserCloud, PassCloud);
                    httpWebRequest.Credentials = networkCredential;
                    // Send authentication along with first request.
                    httpWebRequest.PreAuthenticate = true;
                    httpWebRequest.Method = method;
                    httpWebRequest.ContentType = "application/pdf";
                    Stream rs = httpWebRequest.GetRequestStream();
                    s.CopyTo(rs);

                    HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                    if (response.ResponseUri.ToString() != "")
                    {
                        VanBan_ChiTiet vbct = _vanBanChiTietRepository.GetById(id);
                        vbct.Link = response.ResponseUri.LocalPath.ToString();
                        _vanBanChiTietRepository.CapNhap(vbct, accMain.Id, "Cập nhật đường dẫn file scan");
                    }
                    return response.ResponseUri.ToString();
                }
            }
            else
            {
                // khong co file de upload
                return "";
            }

        }
        #endregion

        #region "Quan ly tai khoan"
        public ActionResult TaiKhoan()
        {
            if (isLoged)
            {
                ViewData["acc"] = accMain;
                ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                ViewData["IdAccount"] = accMain.Id;
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);

                ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/Taikhoan_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/Taikhoan_js.cshtml";

                return View();
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
        }
        public ActionResult PhanQuyen()
        {
            if (isLoged)
            {
                ViewData["acc"] = accMain;
                ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                ViewData["IdAccount"] = accMain.Id;
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);

                ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/PhanQuyen_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/PhanQuyen_js.cshtml";

                return View();
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
        }
        [HttpGet]
        public string GetDanhSachTaiKhoan()
        {
            List<Account> dt = new List<Account>();
            dt = _repository.GetAllAccountsPhanQuyen(accMain);
            return dt.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetTaiKhoan(long Id)
        {
            Account dt = new Account();
            dt = _repository.GetAccount(Id);
            return dt!=null ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public bool CheckTenDangNhap(string TenDangNhap)
        {
            Account acc = _repository.GetAccount(TenDangNhap);
            if(acc!=null && acc.Id!=null && acc.Id > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool UpdateTaiKhoan(long Id, string MaNhanVien, string TenDangNhap, string Email, string HoTen)
        {
            Account acc = _repository.GetAccount(Id);
            acc.MaNhanVien = MaNhanVien != null ? MaNhanVien : "";
            acc.TenDangNhap = TenDangNhap != null ? TenDangNhap : "";
            acc.Email = Email != null ? Email : "";
            acc.HoTen = HoTen != null ? HoTen : "";
            _repository.UpdateTaiKhoan(acc);
            return true;
        }
        public bool CreateTaiKhoan( string MaNhanVien, string TenDangNhap, string Email, string HoTen, string MatKhau)
        {
            Account acc = new Account();
            acc.MaNhanVien = MaNhanVien != null ? MaNhanVien : "";
            acc.TenDangNhap = TenDangNhap != null ? TenDangNhap : "";
            acc.Email = Email != null ? Email : "";
            acc.HoTen = HoTen != null ? HoTen : "";
            acc.MatKhau = MatKhau != null ? MatKhau : "";
            _repository.CreateTaiKhoan(acc);
            return true;
        }

        [HttpGet]
        public string GetDanhSachDonVi(string TenDangNhap)
        {
            DataTable dt = new DataTable();
            dt = _Librepository.GetDanhSachDonViPhanQuyen(TenDangNhap);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public bool SetIsScanTaiKhoan(string TenDangNhap)
        {
            Account acc = _repository.GetAccount(TenDangNhap);
            acc.isScan = !acc.isScan;
            _repository.UpdateTaiKhoan(acc);
            return true;
        }
        public bool SetIsTruongPhongTaiKhoan(string TenDangNhap)
        {
            Account acc = _repository.GetAccount(TenDangNhap);
            acc.isTruongPhong = !acc.isTruongPhong;
            _repository.UpdateTaiKhoan(acc);
            return true;
        }
        public bool SetIsVPSTaiKhoan(string TenDangNhap)
        {
            Account acc = _repository.GetAccount(TenDangNhap);
            acc.isVPS = !acc.isVPS;
            _repository.UpdateTaiKhoan(acc);
            return true;
        }
        public bool SetPhanQuyenDonViTaiKhoan(string TenDangNhap, int Id, bool isCheck)
        {
            Account acc = _repository.GetAccount(TenDangNhap);
            _repository.SetPhanQuyenDonViTaiKhoan(acc, Id,isCheck?1:0);
            return true;
        }
        #endregion

        #region "Bao cao thong ke"
        public ActionResult BaoCaoTheoThoiGian()
        {
            if (isLoged && accMain.isVPS)
            {
                ViewData["acc"] = accMain;
                ViewData["isTaoPhieu"] = accMain.isTaoPhieu == true ? 1 : 0;
                ViewData["isScan"] = _repository.isScan(accMain) == true ? 1 : 0;
                ViewData["IdAccount"] = accMain.Id;
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(accMain.TenDangNhap);

                ViewData["css"] = "~/wwwroot/Includes/VanPhongSo/BaoCaoTheoThoiGian_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/VanPhongSo/BaoCaoTheoThoiGian_js.cshtml";

                return View();
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
        }
        [HttpGet]
        public string GetBaoCaoVanBanTheoThoiGian(long IdDonVi, DateTime TuNgay, DateTime DenNgay)
        {
            DataTable dt = new DataTable();
            dt = _vanBanRepository.GetBaoCaoVanBanTheoThoiGian(accMain.TenDangNhap, IdDonVi, TuNgay, DenNgay);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        [HttpGet]
        public string GetBaoCaoDanhSachVanBanTheoThoiGian(long IdDonVi, DateTime TuNgay, DateTime DenNgay)
        {
            DataTable dt = new DataTable();
            dt = _vanBanRepository.GetBaoCaoDanhSachVanBanTheoThoiGian(accMain.TenDangNhap, IdDonVi, TuNgay, DenNgay);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        #endregion
    }
}