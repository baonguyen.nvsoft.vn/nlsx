﻿using GroupDocs.Viewer.Options;
using GroupDocs.Viewer.Results;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Controllers
{
    public class ThongTinYTeController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private string projectRootPath;
        private string outputPath;
        private string storagePath;
        List<string> lstFiles;
        Dictionary<string, string> id_paths = new Dictionary<string, string>();

        public ThongTinYTeController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            projectRootPath = _hostingEnvironment.ContentRootPath;
            outputPath = Path.Combine(projectRootPath, "wwwroot/document");
            storagePath = Path.Combine(projectRootPath, "wwwroot/document");
            lstFiles = new List<string>();
            //id_paths.Add("000", "000 - Phát đồ điều trị ca nhiễm.pdf");
            //id_paths.Add("001", "001- Quy định đối với CBCNV.pdf");
            //id_paths.Add("002", "002- Thông tin y tế.pdf");
            //id_paths.Add("003", "003- Quy định an toàn khử khuẩn tại TCT.pdf");
            //id_paths.Add("004", "004- Quy định an toàn khử khuẩn xe tải hàng hóa, giấy tờ tại TCT.pdf");
            //id_paths.Add("005", "005- Quy định phòng họp.pdf");
            //id_paths.Add("006", "006- Quy định F0 được chăm sóc sức khỏe.pdf");
            //id_paths.Add("007", "007- Quy định người bị F1.pdf");
            //id_paths.Add("008", "008- Thông tin khai báo của người nghi nhiễm hoặc F0.pdf");
            //id_paths.Add("009", "009- Thông tin người tiếp xúc với ca nhiễm hoặc F0.pdf");
            //id_paths.Add("010", "010- Phiếu đồng ý chữa trị tại TCT.pdf");
            //id_paths.Add("011", "011- Quy trình xử lý ca nghi nhiễm.pdf");
            var files = Directory.GetFiles(storagePath);
            foreach (var file in files)
            {
                id_paths.Add(Path.GetFileName(file).Substring(0, 3), Path.GetFileName(file));
            }
        }
        public IActionResult Index(string idfile="")
        {
            var files = Directory.GetFiles(storagePath);
            foreach (var file in files)
            {
                lstFiles.Add(Path.GetFileName(file));
            }
            ViewBag.lstFiles = lstFiles;

            ViewData["css"] = "~/Views/Shared/css/covid19.cshtml";
            ViewData["js"] = "~/Views/Shared/js/covid19.cshtml";
            return View();
        }

        [HttpPost]
        public IActionResult OnPost(string FileName)
        {
            int pageCount = 0;
            string imageFilesFolder = Path.Combine(outputPath, Path.GetFileName(FileName).Replace(".", "_"));
            if (!Directory.Exists(imageFilesFolder))
            {
                Directory.CreateDirectory(imageFilesFolder);
            }
            string imageFilesPath = Path.Combine(imageFilesFolder, "page-{0}.png");
            using (GroupDocs.Viewer.Viewer viewer = new GroupDocs.Viewer.Viewer(Path.Combine(storagePath, FileName)))
            {
                //Get document info
                ViewInfo info = viewer.GetViewInfo(ViewInfoOptions.ForPngView(false));
                pageCount = info.Pages.Count;
                //Set options and render document
                PngViewOptions options = new PngViewOptions(imageFilesPath);
                viewer.View(options);
            }
            return new JsonResult(pageCount);
        }
        public IActionResult PhysicalLocation()
        {
            string a = Request.Path.ToString();
            string fileid = a.Substring(a.Length - 3, 3);
            string fileName = "";
            if (fileid != null)
            {
                id_paths.TryGetValue(fileid, out fileName);
                string physicalPath = "wwwroot/document/" + fileName;// "000-Qui trinh_ver3.pdf";
                byte[] pdfBytes = System.IO.File.ReadAllBytes(physicalPath);
                MemoryStream ms = new MemoryStream(pdfBytes);
                return new FileStreamResult(ms, "application/pdf");
            }
            else
            {
                string physicalPath = "wwwroot/document/" + "000 - Phát đồ điều trị ca nhiễm.pdf";
                byte[] pdfBytes = System.IO.File.ReadAllBytes(physicalPath);
                MemoryStream ms = new MemoryStream(pdfBytes);
                return new FileStreamResult(ms, "application/pdf");
            }
        }
    }
}
