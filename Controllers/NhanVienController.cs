﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dbChange.Models;
using dbChange.Models.LIB;
using dbChange.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dbChange.Controllers
{
    public class NhanVienController : Controller
    {
        private readonly IAccountRepository _repository;
        private readonly ILibRepository _Librepository;
        public string CMND = "";
        public NhanVienController(IAccountRepository repository, ILibRepository Librepository, IHttpContextAccessor httpContextAccessor)
        {
            _repository = repository;
            _Librepository = Librepository;
            if (httpContextAccessor.HttpContext.Request.Cookies["CMND"] != null)
            {
                CMND = httpContextAccessor.HttpContext.Request.Cookies["CMND"];
            }
        }

        // CMND/CCCD
        public IActionResult Index()
        {
            if (CMND != "")
            {
                return RedirectToAction(actionName: "Infomation", controllerName: "NhanVien");
            }
            else
            {
                ViewData["css"] = "~/wwwroot/Includes/NhanVien/Profile_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/NhanVien/Index_js.cshtml";
                return View();

            }
        }
        // Infomation
        public IActionResult Infomation()
        {
            if (CMND != "")
            {
                Staff staff = new Staff();
                staff = _repository.GetStaffByCMND(CMND);
                ViewData["css"] = "~/wwwroot/Includes/NhanVien/Profile_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/NhanVien/Infomation_js.cshtml";
                ViewData["MaNhom"] = _Librepository.GetAll_PL2_NhomDoiTuong();
                ViewData["Provice"] = _Librepository.GetAllProvice();
                ViewData["District"] = _Librepository.GetAllDistrictByProvice(staff.MaTinh);
                ViewData["Ward"] = _Librepository.GetAllWardByDistrict(staff.MaQuan);
                ViewData["ToKhuPho"] = _Librepository.GetAllToKhuPhoByWard(staff.MaPhuong);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTiet();
                DM_BoPhanChiTiet dv = _Librepository.GetDonViChiTietByTen(staff.DonViChiTiet);
                ViewData["BoPhan"] = _Librepository.GetAllDonViChiTietBoPhanByDonViChiTietId(Convert.ToInt32(dv.Id));
                ViewData["staff"] = staff;
                ViewBag.CMND = CMND;
                return View();
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "NhanVien");
            }
            
        }
        public string GetInfoStaff(string CMND)
        {
            Staff staff = new Staff();
            staff = _repository.GetStaffByCMND(CMND);
            if (staff != null && staff.Id > 0) return "true";
            return "false";
        }
        public string SaveInfoGeneral(string HoTen, string NgaySinh, string ThangSinh, string NamSinh, string GioiTinh, string MaNhom, string DonVi, string DienThoai, string BHYT, string DonViChiTiet, string BoPhan, string CCCD)
        {
            Staff staff = new Staff();
            staff = _repository.GetStaffByCMND(CMND);
            staff.HoTen = HoTen;
            staff.NgayS = NgaySinh!=null ? Convert.ToInt32(NgaySinh) : 1;
            staff.ThangS = ThangSinh!=null ? Convert.ToInt32(ThangSinh) : 1;
            staff.NamS = NamSinh!=null ? Convert.ToInt32(NamSinh) : 1900;
            staff.GioiTinh = GioiTinh;
            staff.MaNhom = MaNhom;
            staff.DonVi = DonVi;
            staff.DienThoai = DienThoai;
            staff.BHYT = BHYT;
            staff.CMND = CMND;
            staff.CCCD = CCCD;
            staff.DonViChiTiet = DonViChiTiet;
            staff.BoPhanId = Convert.ToInt32(BoPhan);
            if (staff != null && staff.Id > 0)
            {
                _repository.UpdateStaffThongTinNhanVien_Temp(staff);
            }
            else
            {
                _repository.CreateStaff(staff);
            }
            return "true";
        }
        public string UpdateInfoAddress(string MaTinh, string MaQuan, string MaPhuong, string MaToKhuPho, string DiaChi)
        {
            Staff staff = new Staff();
            staff = _repository.GetStaffByCMND(CMND);
            staff.MaTinh = MaTinh;
            staff.MaQuan = MaQuan;
            staff.MaPhuong = MaPhuong;
            if(MaToKhuPho!="-1")
                staff.IdWeb = MaToKhuPho;
            staff.DiaChi = DiaChi;
            _repository.UpdateStaffDiacChiNhanVien_Temp(staff);
            return "true";
        }
        public string UpdateMui1Staff(string Mui1_TenVacxin, string Mui1_NgayTiem, string Mui1_SoLo, string Mui1_DiaDiem)
        {
            Staff staff = new Staff();
            staff = _repository.GetStaffByCMND(CMND);
            staff.Mui1_TenVacxin = Mui1_TenVacxin;
            staff.Mui1_NgayTiem = Mui1_NgayTiem;
            staff.Mui1_SoLo = Mui1_SoLo;
            staff.Mui1_DiaDiem = Mui1_DiaDiem;
            _repository.UpdateMui1Staff_Temp(staff);
            return "true";
        }
        public string UpdateMui2Staff(string Mui2_TenVacxin, string Mui2_NgayTiem, string Mui2_SoLo, string Mui2_DiaDiem)
        {
            Staff staff = new Staff();
            staff = _repository.GetStaffByCMND(CMND);
            staff.Mui2_TenVacxin = Mui2_TenVacxin;
            staff.Mui2_NgayTiem = Mui2_NgayTiem;
            staff.Mui2_SoLo = Mui2_SoLo;
            staff.Mui2_DiaDiem = Mui2_DiaDiem;
            _repository.UpdateMui2Staff_Temp(staff);
            return "true";
        }
        public string GetAllDistrictByProvice(string MaTinh)
        {
            string result = "";
            List<PL3_DanhMucHanhChinh> ls = _Librepository.GetAllDistrictByProvice(MaTinh);
            result += "<option value = '-1' >Chọn Quận/Huyện</option> ";
            foreach (PL3_DanhMucHanhChinh row in ls)
            {
                result += "<option value='" + row.Ma_QuanHuyen + "'>" + row.Ten_QuanHuyen + "</option>";
            }
            return result;
        }
        public string GetAllWardByDistrict(string MaQuan)
        {
            string result = "";
            List<PL3_DanhMucHanhChinh> ls = _Librepository.GetAllWardByDistrict(MaQuan);
            result += "<option value = '-1' >Chọn Phường/Xã</option> ";
            foreach (PL3_DanhMucHanhChinh row in ls)
            {
                result += "<option value='" + row.Ma_PhuongXa + "'>" + row.Ten_PhuongXa + "</option>";
            }
            return result;
        }
        public string GetAllToKhuPhoByWard(string MaPhuong)
        {
            string result = "";
            List<DM_ToKhuPho> ls = _Librepository.GetAllToKhuPhoByWard(MaPhuong);
            result += "<option value = '-1' >Chọn Tổ/Khu Phố</option> ";
            foreach (DM_ToKhuPho row in ls)
            {
                result += "<option value='" + row.IdWeb + "'>" + row.Ten_ToKhuPho + "-" + row.TenKhuPho + "</option>";
            }
            return result;
        }
        public string GetAllBoPhanByDonViChiTietId(string DonViChiTiet)
        {
            string result = "";
            DM_BoPhanChiTiet dv = _Librepository.GetDonViChiTietByTen(DonViChiTiet);
            List<DonViChiTiet_BoPhan> ls = _Librepository.GetAllDonViChiTietBoPhanByDonViChiTietId(Convert.ToInt32(dv.Id));
            result += "<option value = '-1' >Chọn bộ phận</option> ";
            foreach (DonViChiTiet_BoPhan row in ls)
            {
                result += "<option value='" + row.Id + "'>" + row.TenBoPhan + "</option>";
            }
            return result;
        }
    }
}