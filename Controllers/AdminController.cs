﻿using dbChange.Models;
using dbChange.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace dbChange.Controllers
{
    public class AdminController : Controller
    {
        private readonly IAccountRepository _repository;
        private readonly IBaoCaoRepository _baoCaoRepository;
        private readonly ILibRepository _Librepository;
        private bool isLoged = false;

        public AdminController(IAccountRepository repository, IHttpContextAccessor httpContextAccessor, IBaoCaoRepository baoCaoRepository, ILibRepository Librepository)
        {
            _repository = repository;
            _baoCaoRepository = baoCaoRepository; 
            _Librepository = Librepository;
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
            }

        }
        public IActionResult Index()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                if (_repository.isAdmin(acc))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
            }
        }
        public IActionResult BaoCaoDanhSachDiLam()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                if (_repository.isAdmin(acc))
                {
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                    ViewData["css"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_js.cshtml";
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
                
            }
        }
        public IActionResult ThongKePhieuRaVaoCong()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                if (_repository.isAdmin(acc))
                {
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                    ViewData["css"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_js.cshtml";
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
                
            }
        }
        public IActionResult BaoCaoDanhSachDiLamTuan()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                ViewData["css"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_js.cshtml";
                return View();
            }
        }
        public string GetData_BaoCaoDanhSachDiLam(DateTime NgayDangKyTuNgay, DateTime NgayDangKyDenNgay, int DonVi)
        {
            DataTable dt = new DataTable();
            dt = _baoCaoRepository.GetBaoCaoDanhSachDiLamTuan(NgayDangKyTuNgay, NgayDangKyDenNgay, DonVi.ToString(), "");
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetThongKePhieuDangKyRaVaoCong(DateTime NgayDangKyTuNgay, DateTime NgayDangKyDenNgay)
        {
            DataTable dt = new DataTable();
            dt = _baoCaoRepository.GetThongKePhieuDangKyRaVaoCong(NgayDangKyTuNgay, NgayDangKyDenNgay);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetBangTongHopNhanVienLamViec(DateTime NgayDangKyTuNgay, DateTime NgayDangKyDenNgay)
        {
            DataTable dt = new DataTable();
            dt = _baoCaoRepository.GetBangTongHopNhanVienLamViec(NgayDangKyTuNgay, NgayDangKyDenNgay);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }

        public IActionResult BangTongHopNhanVienLamViec()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                if (_repository.isAdmin(acc))
                {
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                    ViewData["css"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BangTongHopNhanVienLamViec_js.cshtml";
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
            }
        }
        public IActionResult BaoCaoRaVaoCong()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                if (_repository.isAdmin(acc))
                {
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                    ViewData["css"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoRaVaoCong_js.cshtml";
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
            }
        }
        public IActionResult BaoCaoThongTinYTe()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                if (_repository.isAdmin(acc))
                {
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                    ViewData["css"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoThongTinYTe_js.cshtml";
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
            }
        }
        public IActionResult BaoCaoThongTinYTeTang()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                if (_repository.isAdmin(acc))
                {
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                    ViewData["css"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoThongTinYTeTang_js.cshtml";
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
            }
        }
        public IActionResult BaocaoTang()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                if (_repository.isAdmin(acc))
                {
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                    ViewData["css"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoTang_js.cshtml";
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
            }
        }
        public IActionResult BaocaoGiam()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                if (_repository.isAdmin(acc))
                {
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                    ViewData["css"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoDanhSachDiLam_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes_Admin/BaoCaoDanhSachDiLam/BaoCaoGiam_js.cshtml";
                    return View();
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
            }
        }
        public string GetData_BaoCaoTang(DateTime NgayDangKyTuNgay, DateTime NgayDangKyDenNgay)
        {
            DataTable dt = new DataTable();
            dt = _baoCaoRepository.GetBaoCaoTang(NgayDangKyTuNgay, NgayDangKyDenNgay);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetData_BaoCaoGiam(DateTime NgayDangKyTuNgay, DateTime NgayDangKyDenNgay)
        {
            DataTable dt = new DataTable();
            dt = _baoCaoRepository.GetBaoCaoGiam(NgayDangKyTuNgay, NgayDangKyDenNgay);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetBaoCaoThongTinYTe(DateTime NgayDangKyTuNgay, DateTime NgayDangKyDenNgay, string DonVi)
        {
            DataTable dt = new DataTable();
            dt = _baoCaoRepository.GetBaoCaoThongTinYTe(NgayDangKyTuNgay, NgayDangKyDenNgay, DonVi);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetBaoCaoThongTinYTeTang(DateTime NgayDangKyTuNgay, DateTime NgayDangKyDenNgay, string DonVi)
        {
            DataTable dt = new DataTable();
            dt = _baoCaoRepository.GetBaoCaoThongTinYTeTang(NgayDangKyTuNgay, NgayDangKyDenNgay, DonVi);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetBaoCaoRaVaoCongTheoDonVi(DateTime NgayDangKyTuNgay, string TenDonVi)
        {
            DataTable dt = new DataTable();
            dt = _baoCaoRepository.GetBaoCaoRaVaoCongTheoDonVi(NgayDangKyTuNgay, TenDonVi);
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach(DataRow dr in dt.Rows)
            {
                int i = 0;
                for (i = 0;i < dr.ItemArray.Length; i++){
                    result.Add(dt.Columns[i].ColumnName,dr.ItemArray.GetValue(i).ToString());
                }
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            //return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt.Rows[0]) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetBaoCaoDanhSachChuaKhaiBaoYTe(DateTime NgayDangKyTuNgay, string TenDonVi)
        {
            DataTable dt = new DataTable();
            dt = _baoCaoRepository.GetBaoCaoDanhSachChuaKhaiBaoYTe(NgayDangKyTuNgay, TenDonVi);
            
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
    }
}