﻿using dbChange.Models;
using dbChange.Repository;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;

namespace dbChange.Controllers
{
    [Route("api/[controller]/[action]")]
    public class StaffAPIController : Controller
    {
        private readonly IStaffRepository _repository;
        private readonly IThongTinQuetQRRepository _repositoryQR;
        private readonly IWebHostEnvironment _env;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public StaffAPIController(IStaffRepository repository, IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor,IThongTinQuetQRRepository repositoryQR)
        {
            _repository = repository;
            _env = env;
            _httpContextAccessor = httpContextAccessor;
            _repositoryQR = repositoryQR;
        }


        [HttpGet]
        public IActionResult GetAllStaffs()
        {
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");

            }
            if(token!="")
            {
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
               
                return Ok(_repository.GetAllStaffs(acc.TenDangNhap));
            }
            else
            {
                return Ok();
            }
                
        }
        [HttpPost]
        public IActionResult UpdateStaff(int key, string values)
        {

            _repository.UpdateStaff(key, values);
            return Ok();
        }

        public JsonResult KiemSoatRaVao_CheckIn(string key)
        {
            bool isOK = false;
            KetQuaCheckIn kq = new KetQuaCheckIn();

            //Check danh sách đăng ký

            //Kiểm tra với YTEHCM

            //Nếu OK thì cho phép checkin
            if (key != "111")
            {


                isOK = true;
            }
            else {
                isOK = false;
            }
            if (isOK)
            {
                ThongTinQuetQR newQR = new ThongTinQuetQR();
                newQR.MaQR = key;
                newQR.GioQuet = DateTime.Now;
                newQR.HoTen = "NGUYỄN KIM TRỌNG";
                newQR.NamSinh = 1988;
                newQR.DonViChiTiet = "Trung Tâm Hệ Thống CNTT (ITSC)";
                newQR.DonViChiTietId = 10;
                newQR.BoPhan = "Trung Tâm Hệ Thống CNTT (ITSC)";
                newQR.BoPhanId = 44;
                newQR.LinkTKYT = "https://kbytcq.tphcm.gov.vn/static/export_pdf/ToKhaiYTe_1633938164.9454467.pdf";
                newQR.TrangThai = "Có nguy cơ mức I";
                newQR.Color = ".bg-warning";
                newQR.NguoiQuet = "NGUYỄN KIM TRỌNG";
                newQR = _repositoryQR.CreteThongTinQuetQRs(newQR);
                kq.Data = new List<ThongTinQuetQR>();
                kq.Data.Add(newQR);
                kq.Status = "success";
                kq.Message = "Yêu cầu theo dõi sức khỏe, triệu chứng";
                kq.Link = "https://kbytcq.tphcm.gov.vn/static/export_pdf/ToKhaiYTe_1633938164.9454467.pdf";
                kq.Color = "bg-warning";
            }
            else
            {
                kq.Status = "error";
                kq.Message = "Yêu cầu theo dõi sức khỏe, triệu chứng";
                kq.Link = "https://kbytcq.tphcm.gov.vn/static/export_pdf/ToKhaiYTe_1633938164.9454467.pdf";
                kq.Color = "bg-warning";
            }
            return Json(kq);
        }
        public string KiemSoatRaVao_LayDSCheckin(string ngaycheckin)
        {
            List<ThongTinQuetQR> ls = new List<ThongTinQuetQR>();
            ls = _repositoryQR.GetThongTinQuetQRsByNgay(Convert.ToDateTime(ngaycheckin));
            return Newtonsoft.Json.JsonConvert.SerializeObject(ls);
        }

        public ActionResult NavigationOverview()
        {
            Staff_Index_Menu menu = new Staff_Index_Menu { Id = 1, Text = "01. Danh Sách Nhân Viên", Expanded = true };
            return View(menu);
        }
         
       
     
        public object GetMenu(DataSourceLoadOptions loadOptions)
        {
            List<Staff_Index_Menu> list = new List<Staff_Index_Menu>();
            list.Add(new Staff_Index_Menu { Id = 1, Text= "01. Danh Sách Nhân Viên", Expanded=true });
            list.Add(new Staff_Index_Menu { Id = 2, Text= "02. Cập Nhật Thông Tin", Expanded=true });
            list.Add(new Staff_Index_Menu { Id = 3, Text= "03. Thống Kê Mũi Tiêm", Expanded=true });
            list.Add(new Staff_Index_Menu { Id = 4, Text= "04. Khai Báo Ca Nhiễm", Expanded=true });
            return DataSourceLoader.Load(list, loadOptions);
        }

        //[HttpGet]
        //public IActionResult GetAll_PL2_NhomDoiTuong(string a)
        //{
        //    return Ok(_repository.GetAll_PL2_NhomDoiTuong());
        //}


        [HttpPost]
        public ActionResult FileSelection(string id,IFormFile photo)
        {
            // Learn to use the entire functionality of the dxFileUploader widget.
            // http://js.devexpress.com/Documentation/Guide/UI_Widgets/UI_Widgets_-_Deep_Dive/dxFileUploader/
            if (photo != null)
            {
               
                SaveFile(photo, id);
            }

            return View("Index");
        }

        void SaveFile(IFormFile file,string id)
        {
            try
            {
                var path = Path.Combine(_env.WebRootPath, "uploads");
                ////  Uncomment to save the file
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                using (var fileStream = System.IO.File.Create(Path.Combine(path, id+"-"+ file.FileName)))
                {
                    file.CopyToAsync(fileStream).Wait();
                }

                _repository.UpdateLinkGCN(Path.Combine(@"..\..\uploads", id + "-" + file.FileName), id);
                //GoogleDriveAPIHelper helper = new GoogleDriveAPIHelper(_env);
               
                //helper.UploadFIle(Path.Combine(path, file.FileName));
            }
            catch(Exception ex)
            {
                Response.WriteAsync(ex.Message);
                Response.StatusCode = 400;
            }
        }
    }
}
