﻿using dbChange.Models;
using dbChange.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace dbChange.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountRepository _repository;
        private bool isLoged = false;
        private int height_screen = 0;

        public AccountController(IAccountRepository repository, IHttpContextAccessor httpContextAccessor)
        {
            _repository = repository;
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
            }
            if (httpContextAccessor.HttpContext.Request.Cookies["height_screen"] != null)
            {
                height_screen = Convert.ToInt32(httpContextAccessor.HttpContext.Request.Cookies["height_screen"]);
            }

        }
        public IActionResult Index()
        {
            if (isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                return View();
            }
        }
        [HttpGet]
        public IActionResult CapNhatDiaChi()
        {
            return View("CapNhatDiaChi");
        }
        [HttpGet]
        public IActionResult Infomation()
        {
            if (isLoged)
            {
                ViewData["css"] = "~/wwwroot/Includes/Profile/Profile_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/Profile/Profile_js.cshtml";
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if(HttpContext.Session.GetString("token")!=null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                Account acc1 = _repository.GetAccount(acc.TenDangNhap);
                Staff staff  = _repository.GetStaffByUserName(acc.TenDangNhap);
                ViewData["Verify"] = token;
                ViewData["staff"] = staff;
                ViewData["acc"] = acc1;
                return View(); 
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }

        [HttpPost]
        public ActionResult Login(Account account, string rememberMe)
        {
            ViewData["css"] = "~/wwwroot/Includes/Home/Home_css.cshtml";
            ViewData["js"] = "~/wwwroot/Includes/Home/Home_js.cshtml";
            Account account1 = _repository.CheckLogin(account);
            if (account1 != null && account1.Id > 0)
            {
                account.isNBC = account1.isNBC;
                if (rememberMe != null)
                {
                    // có ghi nhớ --> lưu cookie
                    var cookieOptions = new CookieOptions()
                    {
                        Path = "/",
                        HttpOnly = false,
                        IsEssential = true,
                        Expires = DateTime.Now.AddDays(365),
                    };
                    
                    Response.Cookies.Append("token", Utils.Utils.Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(account1)), cookieOptions);
                }
                else
                {
                    // ko ghi nhớ --> lưu session
                    var cookieOptions = new CookieOptions()
                    {
                        Path = "/",
                        HttpOnly = false,
                        IsEssential = true,
                        Expires = DateTime.Now.AddDays(365),
                    };

                    Response.Cookies.Append("token", Utils.Utils.Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(account1)), cookieOptions);
                    //HttpContext.Session.SetString("token", Utils.Utils.Encrypt(Newtonsoft.Json.JsonConvert.SerializeObject(account1)));
                }
                if (!string.IsNullOrEmpty(HttpContext.Session.GetString("callbackURL")))
                {
                    return Redirect(HttpContext.Session.GetString("callbackURL"));
                }
                return RedirectToAction(actionName: "Index", controllerName: "NLSX");
            }
            else
            {
                ViewBag.Message = "Tài khoản và mật khẩu không đúng";
                return View();
            }
        }
        [HttpGet]
        public ActionResult Logout()
        {
            ViewData["css"] = "~/wwwroot/Includes/Home/Home_css.cshtml";
            ViewData["js"] = "~/wwwroot/Includes/Home/Home_js.cshtml";

            Response.Cookies.Delete("token");
            Response.Cookies.Delete("DonViChiTiet");
            Response.Cookies.Delete("BoPhan");
            HttpContext.Session.Remove("token");
            isLoged = false;

            return RedirectToAction(actionName: "Index", controllerName: "Account");
        }
        [HttpGet]
        public void SendEmailTest()
        {
            _repository.SendEmailTest();
        }

        [HttpPost]
        public string Forgot(string TenDangNhap, string Email)
        {
            Account account = _repository.GetAccount(TenDangNhap);
            account.Email = Email;
            if (_repository.Forgot(account))
            {
                if (_repository.SendEmailForgot(account)){
                    return "SUCCESS";
                }
                else
                {
                   return "Hệ thống đang bận, vui lòng thử lại sau!";
                }
               
            }
            else
            {
                return "Tài khoản hoặc email không tồn tại!";
            }
        }
        [HttpPost]
        public ActionResult ForgotOld(Account account)
        {
            if (_repository.Forgot(account))
            {
                if (_repository.SendEmailForgot(account))
                {
                    ViewBag.Message = "Đã gửi mật khẩu qua email. Vui lòng kiểm tra email để thực hiện đổi mật khẩu mới!";
                    return View("~/Views/Account/Index.cshtml");
                }
                else
                {
                    ViewBag.Message = "Hệ thống đang bận, vui lòng thử lại sau!";
                    return View("~/Views/Account/Index.cshtml");
                }

            }
            else
            {
                ViewBag.Message = "Tài khoản hoặc email không tồn tại!";
                return View("~/Views/Account/Index.cshtml");
            }
        }
        public ActionResult ChangePassword(string Verify, string Exp)
        {
            ViewData["verify"] = Verify;
            ViewData["exp"] = Exp;
            return View();
        }
        [HttpPost]
        public ActionResult UpdatePassword(string MatKhauMoi, string MatKhauMoiRe, string Verify, string Exp)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(Utils.Utils.Decrypt(Exp));
                DateTime cDt = Utils.Utils.GetTimeServer();

                if(DateTime.Compare(cDt, dt.AddDays(1)) <=0)
                {
                    if (!string.IsNullOrEmpty(MatKhauMoi) && !string.IsNullOrWhiteSpace(MatKhauMoi) && MatKhauMoi == MatKhauMoiRe)
                    {
                        if (MatKhauMoi.Length <= 5)
                        {
                            ViewData["verify"] = Verify;
                            ViewData["exp"] = Exp;
                            ViewBag.Message = "Mật khẩu mới quá ngắn, vui lòng đặt từ 6 ký tự trở lên!";
                            return View();
                        }
                        else
                        {
                            string a = Utils.Utils.Decrypt(Verify);
                            Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                            _repository.UpdatePassword(acc, MatKhauMoi);
                            ViewData["verify"] = Verify;
                            ViewData["exp"] = Exp;
                            ViewBag.Message = "Đã đổi thành công!";
                            return View();
                        }
                    }
                    else
                    {
                        ViewData["verify"] = Verify;
                        ViewData["exp"] = Exp;
                        ViewBag.Message = "Mật khẩu mới chưa trùng khớp hoặc chưa điền mật khẩu mới!";
                        return View();
                    }
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "Account");
                }
                
            }
            catch(Exception ex)
            {
                ViewBag.Message = "Hệ thống đang bận, vui lòng thử lại sau!";
                ViewData["verify"] = Verify;
                ViewData["exp"] = Exp;
                return View();
            }
           
        }

        [HttpPost]
        public string UpdatePasswordInfo(string MatKhauCu, string MatKhauMoi, string Verify)
        {
            try
            {
                if (string.IsNullOrEmpty(MatKhauCu) || string.IsNullOrWhiteSpace(MatKhauCu) || string.IsNullOrEmpty(MatKhauMoi) || string.IsNullOrWhiteSpace(MatKhauMoi))
                {
                    return "Vui lòng nhập đủ thông tin!";
                }
                else
                {
                    if (MatKhauMoi.Length <= 5)
                    {
                        return "Mật khẩu mới quá ngắn, vui lòng đặt từ 6 ký tự trở lên!";
                    }
                    else
                    {
                        string a = Utils.Utils.Decrypt(Verify);
                        Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                        Account acc1 = _repository.GetAccount(acc.TenDangNhap);
                        acc1.MatKhau = Utils.Utils.Encrypt(MatKhauCu);
                        if (_repository.CheckLogin(acc1) != null)
                        {
                            _repository.UpdatePassword(acc1, MatKhauMoi);
                            return "";
                        }
                        else
                        {
                            return "Mật khẩu cũ không đúng!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "Hệ thống đang bận, vui lòng thử lại sau!";
            }
        }
        [HttpPost]
        public string UpdateInfoStaff(string Verify,string Id, string HoTen, string NgaySinh, string GioiTinh
            , string MaNhom, string DonVi, string DienThoai, string CMND, string BHYT)
        {
            try
            {
                if (string.IsNullOrEmpty(HoTen) || string.IsNullOrWhiteSpace(HoTen) 
                    || string.IsNullOrEmpty(NgaySinh) || string.IsNullOrWhiteSpace(NgaySinh)
                    || string.IsNullOrEmpty(GioiTinh) || string.IsNullOrWhiteSpace(GioiTinh)
                    || string.IsNullOrEmpty(MaNhom) || string.IsNullOrWhiteSpace(MaNhom)
                    || string.IsNullOrEmpty(DonVi) || string.IsNullOrWhiteSpace(DonVi)
                    )
                {
                    return "Vui lòng nhập đủ thông tin!";
                }
                else
                {
                    string a = Utils.Utils.Decrypt(Verify);
                    Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                    Staff staff = _repository.GetStaffByUserName(acc.TenDangNhap);
                    staff.HoTen = string.IsNullOrEmpty(HoTen) ? "" : HoTen;
                    staff.NgayS = string.IsNullOrEmpty(NgaySinh) ? 1 : Convert.ToInt32(NgaySinh);
                    staff.GioiTinh = GioiTinh=="Nam" ? "0" : "1";
                    staff.MaNhom = string.IsNullOrEmpty(MaNhom) ? "" : MaNhom;
                    staff.DonVi = string.IsNullOrEmpty(DonVi) ? "" : DonVi;
                    staff.DienThoai = string.IsNullOrEmpty(DienThoai) ? "" : DienThoai;
                    staff.CMND = string.IsNullOrEmpty(CMND) ? "" : CMND;
                    staff.BHYT = string.IsNullOrEmpty(BHYT) ? "" : BHYT;

                    _repository.UpdateStaffThongTinNhanVien(staff);
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "Hệ thống đang bận, vui lòng thử lại sau!";
            }
        }

        [HttpPost]
        public string UpdateAddressStaff(string Verify, string TenTinh, string MaTinh, string TenQuan, string MaQuan, string TenPhuong, string MaPhuong, string Ten_ToKhuPho, string Ma_ToKhuPho, string DiaChi)
        {
            try
            {
                if (string.IsNullOrEmpty(TenTinh) || string.IsNullOrWhiteSpace(TenTinh)
                    || string.IsNullOrEmpty(MaTinh) || string.IsNullOrWhiteSpace(MaTinh)
                    || string.IsNullOrEmpty(TenQuan) || string.IsNullOrWhiteSpace(TenQuan)
                    || string.IsNullOrEmpty(MaQuan) || string.IsNullOrWhiteSpace(MaQuan)
                    || string.IsNullOrEmpty(TenPhuong) || string.IsNullOrWhiteSpace(TenPhuong)
                    || string.IsNullOrEmpty(MaPhuong) || string.IsNullOrWhiteSpace(MaPhuong)
                    || string.IsNullOrEmpty(Ten_ToKhuPho) || string.IsNullOrWhiteSpace(Ten_ToKhuPho)
                    || string.IsNullOrEmpty(Ma_ToKhuPho) || string.IsNullOrWhiteSpace(Ma_ToKhuPho)
                    || string.IsNullOrEmpty(DiaChi) || string.IsNullOrWhiteSpace(DiaChi)
                    )
                {
                    return "Vui lòng nhập đủ thông tin!";
                }
                else
                {
                    string a = Utils.Utils.Decrypt(Verify);
                    Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                    Staff staff = _repository.GetStaffByUserName(acc.TenDangNhap);
                    staff.TenTinh = string.IsNullOrEmpty(TenTinh) ? "" : TenTinh;
                    staff.MaTinh = string.IsNullOrEmpty(MaTinh) ? "" : MaTinh ;
                    staff.TenQuan = string.IsNullOrEmpty(TenQuan) ? "" : TenQuan ;
                    staff.MaQuan = string.IsNullOrEmpty(MaQuan) ? "" : MaQuan;
                    staff.TenPhuong = string.IsNullOrEmpty(TenPhuong) ? "" : TenPhuong;
                    staff.MaPhuong = string.IsNullOrEmpty(MaPhuong) ? "" : MaPhuong;
                    staff.DiaChi = string.IsNullOrEmpty(DiaChi) ? "" : DiaChi;
                    staff.Ten_ToKhuPho = string.IsNullOrEmpty(Ten_ToKhuPho) ? "" : Ten_ToKhuPho;
                    staff.Ma_ToKhuPho = string.IsNullOrEmpty(Ma_ToKhuPho) ? "" : Ma_ToKhuPho;

                    _repository.UpdateStaffDiacChiNhanVien(staff);
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "Hệ thống đang bận, vui lòng thử lại sau!";
            }
        }
        [HttpPost]
        public string UpdateMui2Staff(string Verify, string Mui2_TenVacxin, string Mui2_NgayTiem, string Mui2_SoLo, string Mui2_DiaDiem)
        {
            try
            {
                if (string.IsNullOrEmpty(Mui2_TenVacxin) || string.IsNullOrWhiteSpace(Mui2_TenVacxin)
                    || string.IsNullOrEmpty(Mui2_NgayTiem) || string.IsNullOrWhiteSpace(Mui2_NgayTiem)
                    || string.IsNullOrEmpty(Mui2_DiaDiem) || string.IsNullOrWhiteSpace(Mui2_DiaDiem)
                    )
                {
                    return "Vui lòng nhập đủ thông tin!";
                }
                else
                {
                    string a = Utils.Utils.Decrypt(Verify);
                    Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                    Staff staff = _repository.GetStaffByUserName(acc.TenDangNhap);
                    staff.Mui2_TenVacxin = string.IsNullOrEmpty(Mui2_TenVacxin) ? "" : Mui2_TenVacxin;
                    staff.Mui2_NgayTiem = string.IsNullOrEmpty(Mui2_NgayTiem) ? "" : Mui2_NgayTiem;
                    staff.Mui2_SoLo = string.IsNullOrEmpty(Mui2_SoLo) ? "" : Mui2_SoLo;
                    staff.Mui2_DiaDiem = string.IsNullOrEmpty(Mui2_DiaDiem) ? "" : Mui2_DiaDiem;

                    _repository.UpdateMui2Staff(staff);
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "Hệ thống đang bận, vui lòng thử lại sau!";
            }
        }
        [HttpPost]
        public string UpdateMui1Staff(string Verify, string Mui1_TenVacxin, string Mui1_NgayTiem, string Mui1_SoLo, string Mui1_DiaDiem)
        {
            try
            {
                if (string.IsNullOrEmpty(Mui1_TenVacxin) || string.IsNullOrWhiteSpace(Mui1_TenVacxin)
                    || string.IsNullOrEmpty(Mui1_NgayTiem) || string.IsNullOrWhiteSpace(Mui1_NgayTiem)
                    || string.IsNullOrEmpty(Mui1_DiaDiem) || string.IsNullOrWhiteSpace(Mui1_DiaDiem)
                    )
                {
                    return "Vui lòng nhập đủ thông tin!";
                }
                else
                {
                    string a = Utils.Utils.Decrypt(Verify);
                    Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                    Staff staff = _repository.GetStaffByUserName(acc.TenDangNhap);
                    staff.Mui1_TenVacxin = string.IsNullOrEmpty(Mui1_TenVacxin) ? "" : Mui1_TenVacxin;
                    staff.Mui1_NgayTiem = string.IsNullOrEmpty(Mui1_NgayTiem) ? "" : Mui1_NgayTiem;
                    staff.Mui1_SoLo = string.IsNullOrEmpty(Mui1_SoLo) ? "" : Mui1_SoLo;
                    staff.Mui1_DiaDiem = string.IsNullOrEmpty(Mui1_DiaDiem) ? "" : Mui1_DiaDiem;

                    _repository.UpdateMui1Staff(staff);
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "Hệ thống đang bận, vui lòng thử lại sau!";
            }
        }
        [HttpPost]
        public async Task<string> UpdateThongTinKhacStaff(string Verify, string GhiChu, IFormFile file)
        {
            try
            {
                string a = Utils.Utils.Decrypt(Verify);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                Staff staff = _repository.GetStaffByUserName(acc.TenDangNhap);
                staff.GhiChu = string.IsNullOrEmpty(GhiChu) ? "" : GhiChu;

                if (file != null)
                {
                    string filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    filename = Utils.Utils.EnsureCorrectFilename(filename);
                    FileInfo fi = new FileInfo(filename);
                    using (FileStream output = System.IO.File.Create("wwwroot/uploads/" + staff.Id + fi.Extension))
                        await file.CopyToAsync(output);
                    staff.UrlHinhGiayXN = "uploads/" + staff.Id +  fi.Extension;
                }

                _repository.UpdateThongTinKhacStaff(staff);
                return "";
            }
            catch (Exception ex)
            {
                return "Hệ thống đang bận, vui lòng thử lại sau!";
            }
        }
    }
}

