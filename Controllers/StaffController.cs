﻿using dbChange.Models;
using dbChange.Repository;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace dbChange.Controllers
{
  
    public class StaffController : Controller
    {
        private readonly IStaffRepository _repository;
        private  ILibRepository _Librepository;
        private bool isLoged = false;
        private int height_screen = 0;
        private string acc = "";

        private readonly IWebHostEnvironment _env;
        public StaffController(IStaffRepository repository, IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor, ILibRepository Librepository)
        {
            _repository = repository;
            _env = env;
            _Librepository = Librepository;
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
                acc = httpContextAccessor.HttpContext.Request.Cookies["token"].ToString();
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
                acc = httpContextAccessor.HttpContext.Session.GetString("token");
            }
            if (httpContextAccessor.HttpContext.Request.Cookies["height_screen"] != null)
            {
                height_screen = Convert.ToInt32(httpContextAccessor.HttpContext.Request.Cookies["height_screen"]);
            }
        }
        public IActionResult Index()
        {

            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
            else
            {
                ViewData["height_screen"] = height_screen;
              
                Account acc1 = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(Utils.Utils.Decrypt(acc));
             
                ViewData["isNBC"] = acc1.isNBC;

                return View();
            }

        }
        public IActionResult TongHop()
        {

            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
            else
            {
                ViewData["css"] = "~/wwwroot/Includes/Staff/TongHop_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/Staff/TongHop_js.cshtml";
                ViewData["height_screen"] = height_screen;
              
                Account acc1 = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(Utils.Utils.Decrypt(acc));
             
                ViewData["isNBC"] = acc1.isNBC;

                return View();
            }

        }
        public IActionResult BaoCaoTiemChung()
        {

            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
            else
            {
                ViewData["css"] = "~/wwwroot/Includes/Staff/TongHop_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/Staff/BaoCaoTiemChung_js.cshtml";
                ViewData["height_screen"] = height_screen;
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);

                ViewData["isNBC"] = acc.isNBC;

                return View();
            }

        }
        public IActionResult CapNhatLyDoKhongTiem()
        {

            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
            else
            {
                ViewData["css"] = "~/wwwroot/Includes/Staff/TongHop_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/Staff/CapNhatLyDoKhongTiem_js.cshtml";
                ViewData["height_screen"] = height_screen;
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);

                ViewData["isNBC"] = acc.isNBC;

                return View();
            }

        }
        
        public IActionResult KiemSoatRaVao()
        {

            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
            else
            {
                ViewData["css"] = "~/wwwroot/Includes/Staff/KiemSoatRaVao_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/Staff/KiemSoatRaVao_js.cshtml";
                return View("KiemSoatRaVao");
            }

        }

        public IActionResult TheoDoiTiemVacxin()
        {

            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                ViewData["css"] = "~/wwwroot/Includes/Staff/TongHop_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/Staff/TheoDoiTiemVacxin_js.cshtml";

                ViewData["height_screen"] = height_screen;
                ViewData["isNBC"] = acc.isNBC;
                return View();
            }

        }
        public IActionResult BaoCaoTheoDoiTiemVacxin()
        {

            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
            else
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                ViewData["css"] = "~/wwwroot/Includes/Staff/TongHop_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/Staff/TheoDoiTiemVacxin_js.cshtml";

                ViewData["height_screen"] = height_screen;
                ViewData["isNBC"] = acc.isNBC;
                return View();
            }

        }
        public string GetAllNhanVienByDonViId(string IdDonVi)
        {
            DataTable dt = new DataTable();
            dt = _repository.GetNhanVienByDonViChiTiet(Convert.ToInt32(IdDonVi));
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetAllNhanVienBaoCaoTiemChung(string IdDonVi, int TinhTrang=-1)
        {
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");
            }
            string a = Utils.Utils.Decrypt(token);
            Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
            DataTable dt = new DataTable();
            dt = _repository.GetAllNhanVienBaoCaoTiemChung(Convert.ToInt32(IdDonVi),acc.TenDangNhap, TinhTrang);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetDanhSachNhanVienByDonViChiTiet_CapNhatLyDoKhongTiem(string IdDonVi, int BiF0=0, int TiemTaiDiaPhuong=0, int DaNghiViec=0, int LyDoKhac=0)
        {
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");
            }
            string a = Utils.Utils.Decrypt(token);
            Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
            DataTable dt = new DataTable();
            if(BiF0==0 && TiemTaiDiaPhuong==0 && DaNghiViec==0 && LyDoKhac==0)
                dt = _repository.GetDanhSachNhanVienByDonViChiTiet_CapNhatLyDoKhongTiem(Convert.ToInt32(IdDonVi),acc.TenDangNhap, BiF0, TiemTaiDiaPhuong, DaNghiViec, LyDoKhac,-1);
            else
                dt = _repository.GetDanhSachNhanVienByDonViChiTiet_CapNhatLyDoKhongTiem(Convert.ToInt32(IdDonVi),acc.TenDangNhap, BiF0, TiemTaiDiaPhuong, DaNghiViec, LyDoKhac,0);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetThongKeMuiTiemTheoDonVi(string IdDonVi)
        {
            DataTable dt = new DataTable();
            Models.LIB.DM_BoPhanChiTiet bp = _Librepository.GetDonViChiTietById(Convert.ToInt32(IdDonVi));
            dt = _repository.GetThongKeMuiTiemTheoDonVi(bp.TenDonVi);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public void UpdateTiemMui3(int IdStaff, int GiaTri)
        {
            _repository.UpdateTiemMui3(IdStaff, GiaTri);
        }
        public string UpdateTiemMui3_1Nguoi(int IdStaff, int GiaTri)
        {
            int a = _repository.CheckTrangThaiDangKy(IdStaff);
            if (GiaTri == 1)
            {
                if(a==0 || a==3)
                    _repository.UpdateTiemMui3(IdStaff, GiaTri);
                else
                    return "Chỉ đăng ký cho người chưa đăng ký hoặc chưa tiêm mũi 3!";
            }
            else
            {
                if (a == 1)
                    _repository.UpdateTiemMui3(IdStaff, GiaTri);
                else
                    return "Chỉ hủy đăng ký cho người đã đăng ký!";
            }
            return "";
        }
        public string KiemTraNhanVienImport(int IdDonVi,string CMND, string DangKy)
        {
            try
            {
                if (DangKy != null && DangKy != "")
                {
                    if (Convert.ToInt32(DangKy) >= 0)
                    {
                        if (!_repository.CheckNhanVien_DonVi(IdDonVi, CMND))
                            return "Lỗi không tìm thấy nhân viên [" + CMND + "] của đơn vị này";
                        return "";
                    }
                    else
                    {
                        return "Lỗi cột đăng ký nhân viên [" + CMND + "], điền giá trị 1 hoặc 0";
                    }
                }
                else
                {
                    return "Lỗi cột đăng ký nhân viên [" + CMND + "], điền giá trị 1 hoặc 0";
                }
            }
            catch (Exception ex)
            {
                return "Lỗi cột đăng ký nhân viên [" + CMND + "], điền giá trị 1 hoặc 0";
            }
        }
        public string LuuNhanVienImport(int IdDonVi,string CMND, string DangKy)
        {
            _repository.UpdateTiemMui3Import(IdDonVi, CMND, DangKy);
            return "";
        }
        public string GetThongTinVacXinTinhThanh()
        {
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("LsTinhThanh");
            dtResult.Columns.Add("LsTenVacXin");
            string tinhthanh = "<option value=''>Chọn Tỉnh/Thành Phố</option>";
            string tenvacxin = "<option value=''>Chọn</option>";

            List<Models.LIB.PL3_DanhMucHanhChinh> lsTTP = _repository.GetAllProvice();
            foreach (Models.LIB.PL3_DanhMucHanhChinh item in lsTTP)
            {
                tinhthanh += "<option value='" + item.Ten_TinhThanhPho + "'>" + item.Ten_TinhThanhPho + "</option>";
            }
            List<Models.LIB.PL5_DanhMucVacxin> lsTenVacXin = _repository.GetAllTenVacXin();
            foreach (Models.LIB.PL5_DanhMucVacxin item in lsTenVacXin)
            {
                tenvacxin += "<option value='" + item.TenVacxin + "'>" + item.TenVacxin + "</option>";
            }

            DataRow drNew = dtResult.NewRow();
            drNew["LsTinhThanh"] = tinhthanh;
            drNew["LsTenVacXin"] = tenvacxin;
            dtResult.Rows.Add(drNew);
            return dtResult.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dtResult) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetThongTinMui(string MaQR)
        {
            DataTable dt = new DataTable();
            DataTable dtResult = new DataTable();
            dt = _repository.GetThongTinMui(MaQR);
            dtResult = dt.Clone();
            dtResult.Columns.Add("LsDiaDiem");
            dtResult.Columns.Add("LsTinhThanh");
            dtResult.Columns.Add("LsTenVacXin");
            string diadiem = "";
            string tinhthanh = "<option value=''>Chọn Tỉnh/Thành Phố</option>";
            string tenvacxin = "<option value=''>Chọn</option>";

            if (dt.Rows.Count > 0)
            {
                List<Models.LIB.PL3_DanhMucHanhChinh> lsTTP = _repository.GetAllProvice();
                foreach (Models.LIB.PL3_DanhMucHanhChinh item in lsTTP)
                {
                    tinhthanh += "<option value='"+item.Ten_TinhThanhPho+"'>"+item.Ten_TinhThanhPho+"</option>";
                }
                List<Models.LIB.PL5_DanhMucVacxin> lsTenVacXin = _repository.GetAllTenVacXin();
                foreach (Models.LIB.PL5_DanhMucVacxin item in lsTenVacXin)
                {
                    if(item.TenVacxin.Trim() == dt.Rows[0]["Mui_TenVacXin"].ToString().Trim())
                        tenvacxin += "<option selected value='"+item.TenVacxin+"'>"+item.TenVacxin + "</option>";
                    else
                        tenvacxin += "<option value='"+item.TenVacxin+"'>"+item.TenVacxin + "</option>";
                }
            }
            foreach(DataRow dr in dt.Rows)
            {
                DataRow drNew = dtResult.NewRow();
                drNew["Mui_TenVacXin"] = dr["Mui_TenVacXin"];
                drNew["Mui_NgayTiem"] = dr["Mui_NgayTiem"].ToString();
                if (dr["Mui_NgayTiem"] != null && dr["Mui_NgayTiem"] != DBNull.Value && !string.IsNullOrEmpty(dr["Mui_NgayTiem"].ToString()))
                {
                    string[] n = drNew["Mui_NgayTiem"].ToString().Split('/');
                    drNew["Mui_NgayTiem"] = n[2] + "-" + n[1] + "-" + n[0];
                }
                drNew["Mui_SoLo"] = dr["Mui_SoLo"];
                drNew["LyDoKhac"] = dr["LyDoKhac"];
                drNew["Mui_DiaDiem"] = dr["Mui_DiaDiem"];
                drNew["NgayNhiem"] = dr["NgayNhiem"].ToString();
                if (dr["NgayNhiem"] != null && dr["NgayNhiem"] != DBNull.Value && !string.IsNullOrEmpty(dr["NgayNhiem"].ToString()))
                {
                    string[] n = drNew["NgayNhiem"].ToString().Split('/');
                    drNew["NgayNhiem"] = n[2] + "-" + n[1] + "-" + n[0];
                }
                drNew["NgayXuatVien"] = dr["NgayXuatVien"].ToString();
                if (dr["NgayXuatVien"] != null && dr["NgayXuatVien"] != DBNull.Value && !string.IsNullOrEmpty(dr["NgayXuatVien"].ToString()))
                {
                    string[] n = drNew["NgayXuatVien"].ToString().Split('/');
                    drNew["NgayXuatVien"] = n[2] + "-" + n[1] + "-" + n[0];
                }
                drNew["LsDiaDiem"] = diadiem;
                drNew["LsTinhThanh"] = tinhthanh;
                drNew["LsTenVacXin"] = tenvacxin;
                dtResult.Rows.Add(drNew);
            }
            return dtResult.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dtResult) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetThongTinMui1(int IdStaff)
        {
            DataTable dt = new DataTable();
            DataTable dtResult = new DataTable();
            dt = _repository.GetThongTinMui1(IdStaff);
            dtResult = dt.Clone();
            dtResult.Columns.Add("LsDiaDiem");
            dtResult.Columns.Add("LsTinhThanh");
            dtResult.Columns.Add("LsTenVacXin");
            string diadiem = "";
            string tinhthanh = "<option value=''>Chọn Tỉnh/Thành Phố</option>";
            string tenvacxin = "<option value=''>Chọn</option>";

            if (dt.Rows.Count > 0)
            {
                List<Models.LIB.PL3_DanhMucHanhChinh> lsTTP = _repository.GetAllProvice();
                foreach (Models.LIB.PL3_DanhMucHanhChinh item in lsTTP)
                {
                    tinhthanh += "<option value='"+item.Ten_TinhThanhPho+"'>"+item.Ten_TinhThanhPho+"</option>";
                }
                List<Models.LIB.PL5_DanhMucVacxin> lsTenVacXin = _repository.GetAllTenVacXin();
                foreach (Models.LIB.PL5_DanhMucVacxin item in lsTenVacXin)
                {
                    if(item.TenVacxin.Trim() == dt.Rows[0]["TenVacxin"].ToString().Trim())
                        tenvacxin += "<option selected value='"+item.TenVacxin+"'>"+item.TenVacxin + "</option>";
                    else
                        tenvacxin += "<option value='"+item.TenVacxin+"'>"+item.TenVacxin + "</option>";
                }
            }
            foreach(DataRow dr in dt.Rows)
            {
                DataRow drNew = dtResult.NewRow();
                drNew["TenVacxin"] = dr["TenVacxin"];
                drNew["NgayTiem"] = dr["NgayTiem"].ToString();
                if (dr["NgayTiem"] != null && dr["NgayTiem"] != DBNull.Value && !string.IsNullOrEmpty(dr["NgayTiem"].ToString()))
                {
                    string[] n = drNew["NgayTiem"].ToString().Split('/');
                    drNew["NgayTiem"] = n[2] + "-" + n[1] + "-" + n[0];
                }
                drNew["SoLo"] = dr["SoLo"];
                drNew["LyDo"] = dr["LyDo"];
                drNew["DiaDiem"] = dr["DiaDiem"];
                drNew["LsDiaDiem"] = diadiem;
                drNew["LsTinhThanh"] = tinhthanh;
                drNew["LsTenVacXin"] = tenvacxin;
                dtResult.Rows.Add(drNew);
            }
            return dtResult.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dtResult) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetThongTinMui2(int IdStaff)
        {
            DataTable dt = new DataTable();
            DataTable dtResult = new DataTable();
            dt = _repository.GetThongTinMui2(IdStaff);
            dtResult = dt.Clone();
            dtResult.Columns.Add("LsDiaDiem");
            dtResult.Columns.Add("LsTinhThanh");
            dtResult.Columns.Add("LsTenVacXin");
            string diadiem = "";
            string tinhthanh = "<option value=''>Chọn Tỉnh/Thành Phố</option>";
            string tenvacxin = "<option value=''>Chọn</option>";

            if (dt.Rows.Count > 0)
            {
                List<Models.LIB.PL3_DanhMucHanhChinh> lsTTP = _repository.GetAllProvice();
                foreach (Models.LIB.PL3_DanhMucHanhChinh item in lsTTP)
                {
                    tinhthanh += "<option value='"+item.Ten_TinhThanhPho+"'>"+item.Ten_TinhThanhPho+"</option>";
                }
                List<Models.LIB.PL5_DanhMucVacxin> lsTenVacXin = _repository.GetAllTenVacXin();
                foreach (Models.LIB.PL5_DanhMucVacxin item in lsTenVacXin)
                {
                    if (item.TenVacxin.Trim() == dt.Rows[0]["TenVacxin"].ToString().Trim())
                        tenvacxin += "<option selected value='" + item.TenVacxin + "'>" + item.TenVacxin + "</option>";
                    else
                        tenvacxin += "<option value='" + item.TenVacxin + "'>" + item.TenVacxin + "</option>";
                }
            }
            foreach(DataRow dr in dt.Rows)
            {
                DataRow drNew = dtResult.NewRow();
                drNew["TenVacxin"] = dr["TenVacxin"];
                drNew["NgayTiem"] = dr["NgayTiem"];
                if (dr["NgayTiem"] != null && dr["NgayTiem"] != DBNull.Value && !string.IsNullOrEmpty(dr["NgayTiem"].ToString()))
                {
                    string[] n = drNew["NgayTiem"].ToString().Split('/');
                    drNew["NgayTiem"] = n[2] + "-" + n[1] + "-" + n[0];
                }
                drNew["SoLo"] = dr["SoLo"];
                drNew["LyDo"] = dr["LyDo"];
                drNew["DiaDiem"] = dr["DiaDiem"];
                drNew["LsDiaDiem"] = diadiem;
                drNew["LsTinhThanh"] = tinhthanh;
                drNew["LsTenVacXin"] = tenvacxin;
                dtResult.Rows.Add(drNew);
            }
            return dtResult.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dtResult) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetThongTinMui3(int IdStaff)
        {
            DataTable dt = new DataTable();
            DataTable dtResult = new DataTable();
            dt = _repository.GetThongTinMui3(IdStaff);
            dtResult = dt.Clone();
            dtResult.Columns.Add("LsDiaDiem");
            dtResult.Columns.Add("LsTinhThanh");
            dtResult.Columns.Add("LsTenVacXin");
            string diadiem = "";
            string tinhthanh = "<option value=''>Chọn Tỉnh/Thành Phố</option>";
            string tenvacxin = "<option value=''>Chọn</option>";

            if (dt.Rows.Count > 0)
            {
                List<Models.LIB.PL3_DanhMucHanhChinh> lsTTP = _repository.GetAllProvice();
                foreach (Models.LIB.PL3_DanhMucHanhChinh item in lsTTP)
                {
                    tinhthanh += "<option value='"+item.Ten_TinhThanhPho+"'>"+item.Ten_TinhThanhPho+"</option>";
                }
                List<Models.LIB.PL5_DanhMucVacxin> lsTenVacXin = _repository.GetAllTenVacXin();
                foreach (Models.LIB.PL5_DanhMucVacxin item in lsTenVacXin)
                {
                    if (item.TenVacxin.Trim() == dt.Rows[0]["TenVacxin"].ToString().Trim())
                        tenvacxin += "<option selected value='" + item.TenVacxin + "'>" + item.TenVacxin + "</option>";
                    else
                        tenvacxin += "<option value='" + item.TenVacxin + "'>" + item.TenVacxin + "</option>";
                }
            }
            foreach(DataRow dr in dt.Rows)
            {
                DataRow drNew = dtResult.NewRow();
                drNew["TenVacxin"] = dr["TenVacxin"];
                drNew["NgayTiem"] = dr["NgayTiem"];
                if (dr["NgayTiem"] != null && dr["NgayTiem"] != DBNull.Value && !string.IsNullOrEmpty(dr["NgayTiem"].ToString()))
                {
                    string[] n = drNew["NgayTiem"].ToString().Split('/');
                    drNew["NgayTiem"] = n[2] + "-" + n[1] + "-" + n[0];
                }
                drNew["SoLo"] = dr["SoLo"];
                drNew["LyDo"] = dr["LyDo"];
                drNew["DiaDiem"] = dr["DiaDiem"];
                drNew["LsDiaDiem"] = diadiem;
                drNew["LsTinhThanh"] = tinhthanh;
                drNew["LsTenVacXin"] = tenvacxin;
                dtResult.Rows.Add(drNew);
            }
            return dtResult.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dtResult) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string GetQuanHuyenByTinhThanh(string TinhThanhPho)
        {
            string quanhuyen = "<option value=''>Chọn</option>";
            List<Models.LIB.PL3_DanhMucHanhChinh> lsQH = _repository.GetAllDistrictByProvice(TinhThanhPho);
            foreach (Models.LIB.PL3_DanhMucHanhChinh item in lsQH)
            {
                quanhuyen += "<option value='" + item.Ten_QuanHuyen + "'>" + item.Ten_QuanHuyen + "</option>";
            }
            return quanhuyen;
        }
        public string GetDiaDiemTiemByQuanHuyen(int IdStaff,string TinhThanhPho, string QuanHuyen)
        {
            string diadiem = "<option value=''>Chọn</option>";
            DataTable dt = new DataTable();
            dt = _repository.GetThongTinMui1(IdStaff);
            DataTable dtTTP = _repository.GetDanhSachDiaDiemTiem(TinhThanhPho, QuanHuyen);
            foreach (DataRow dr in dtTTP.Rows)
            {
                if (dt.Rows.Count>0 && dt.Rows[0]["DiaDiem"].ToString() == dr["Ten_CoSo"].ToString())
                {
                    diadiem += "<option seleted value='" + dr["Ten_CoSo"].ToString() + "'>" + dr["Ten_CoSo"].ToString() + "</option>";
                }
                else
                {
                    diadiem += "<option value='" + dr["Ten_CoSo"].ToString() + "'>" + dr["Ten_CoSo"].ToString() + "</option>";
                }
            }
            return diadiem;
        }
        public string GetThongTinF0(int IdStaff)
        {
            DataTable dt = new DataTable();
            dt = _repository.GetThongTinF0(IdStaff);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public void LuuThongTinMui(string IdStaff, int LoaiMuiTiem, string Mui1_TenVacxin1, string Mui1_SoLo1, string Mui1_NgayTiem1, string Mui1_DiaDiem1, string Mui1_LyDo, string Mui1_DiaDiem1_Old)
        {
            string NoiDungTruocCapNhat = Newtonsoft.Json.JsonConvert.SerializeObject(_repository.GetThongTinTruocCapNhatMuiTiem(IdStaff,LoaiMuiTiem));
            _repository.UpdateMui1PCCovid(IdStaff,LoaiMuiTiem, Mui1_TenVacxin1, Mui1_SoLo1, Mui1_NgayTiem1, Mui1_DiaDiem1, Mui1_LyDo, Mui1_DiaDiem1_Old);
            string NoiDungSauCapNhat = Newtonsoft.Json.JsonConvert.SerializeObject(_repository.GetThongTinTruocCapNhatMuiTiem(IdStaff, LoaiMuiTiem));

            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");
            }
            string a = Utils.Utils.Decrypt(token);
            Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
            _repository.GhiLogCapNhat(acc.Id, acc.TenDangNhap, "Cập nhật thông tin mũi "+LoaiMuiTiem+" cho IdStaff [" + IdStaff + "]["+ NoiDungTruocCapNhat + "]", "Cập nhật thông tin mũi " + LoaiMuiTiem + " cho IdStaff [" + IdStaff + "]["+NoiDungSauCapNhat+"]");
        }
        public void LuuThongTinMui_KhongTiem(string IdStaff, int LoaiMuiTiem, string Mui1_LyDo)
        {
            _repository.UpdateMui1PCCovid_KhongTiem(IdStaff,LoaiMuiTiem, Mui1_LyDo);
        }
        public void LuuDaNghi(string IdStaff)
        {
            _repository.UpdateDaNghi(IdStaff);
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");
            }
            string a = Utils.Utils.Decrypt(token);
            Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
            _repository.GhiLogCapNhat(acc.Id, acc.TenDangNhap,"Cập nhật đã nghỉ cho IdStaff ["+IdStaff+"]", "Cập nhật đã nghỉ cho IdStaff [" + IdStaff + "]");
        }
        public void LuuThongTinLyDoKhongTiem(string MaQR, string Mui_TenVacxin, string Mui_SoLo, string Mui_NgayTiem, string Mui_DiaDiem, string Mui_LyDoKhac, string Mui_DiaDiem_Old, int opt
            , string NgayNhiem, string NgayXuatVien)
        {
           if(opt == 1)
            {
                _repository.UpdateTiemTaiDiaPhuong(MaQR, Mui_TenVacxin, Mui_NgayTiem, Mui_SoLo, Mui_DiaDiem, Mui_DiaDiem_Old);
            }else if(opt == 4) // ly do khac
            {
                _repository.UpdateLyDoKhacKhongTiem(MaQR, Mui_LyDoKhac);
            }
            else if (opt == 2)// f0
            {
                _repository.UpdateBiF0(MaQR, true,NgayNhiem,NgayXuatVien);
            }
            else // nghi viec
            {
                _repository.UpdateDaNghiViec(MaQR, true);
            }
        }
        public void LuuThongTinF0(int IdStaff, string NgayBiNhiem, string NgayXuatVien)
        {
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");
            }
            string a = Utils.Utils.Decrypt(token);
            Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
            string NoiDungTruocCapNhat = Newtonsoft.Json.JsonConvert.SerializeObject(_repository.GetThongTinTruocCapNhatBiF0(IdStaff));
            _repository.UpdateThongTinF0(IdStaff, NgayBiNhiem, NgayXuatVien);
            string NoiDungSauCapNhat = Newtonsoft.Json.JsonConvert.SerializeObject(_repository.GetThongTinTruocCapNhatBiF0(IdStaff));
            _repository.GhiLogCapNhat(acc.Id, acc.TenDangNhap, "Cập nhật bị F0 cho IdStaff [" + IdStaff + "][" + NoiDungTruocCapNhat + "]", "Cập nhật bị F0 cho IdStaff [" + IdStaff + "][" + NoiDungSauCapNhat + "]");
        }
        public void LuuThongTinMui_XoaThongTinTiem(string IdStaff, int LoaiMuiTiem)
        {
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");
            }
            string a = Utils.Utils.Decrypt(token);
            Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
            string NoiDungTruocCapNhat = Newtonsoft.Json.JsonConvert.SerializeObject(_repository.GetThongTinTruocCapNhatMuiTiem(IdStaff, LoaiMuiTiem));
            _repository.LuuThongTinMui_XoaThongTinTiem(IdStaff,LoaiMuiTiem);
            string NoiDungSauCapNhat = Newtonsoft.Json.JsonConvert.SerializeObject(_repository.GetThongTinTruocCapNhatMuiTiem(IdStaff, LoaiMuiTiem));
            _repository.GhiLogCapNhat(acc.Id, acc.TenDangNhap, "Cập nhật xóa thông tin mũi tiêm ["+LoaiMuiTiem+"] cho IdStaff [" + IdStaff + "][" + NoiDungTruocCapNhat + "]", "Cập nhật xóa thông tin mũi tiêm [" + LoaiMuiTiem + "] cho IdStaff [" + IdStaff + "][" + NoiDungSauCapNhat + "]");
        }
    }
}
