﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using dbChange.Data;
using dbChange.Models.LIB;
using dbChange.Repository;

namespace dbChange.Controllers
{
    public class DonViChiTiet_PhanNhomController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ILibRepository _Librepository;

        public DonViChiTiet_PhanNhomController(ApplicationDbContext context, ILibRepository Librepository)
        {
            _context = context;
            _Librepository = Librepository;
        }

        // GET: DonViChiTiet_PhanNhom
        public async Task<IActionResult> Index()
        {
            return View(await _context.DonViChiTiet_PhanNhom.ToListAsync());
        }

        // GET: DonViChiTiet_PhanNhom/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_PhanNhom = await _context.DonViChiTiet_PhanNhom
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_PhanNhom == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_PhanNhom);
        }

        // GET: DonViChiTiet_PhanNhom/Create
        public IActionResult Create()
        {
            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            return View();
        }

        // POST: DonViChiTiet_PhanNhom/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create([Bind("id,TenPhanNhom,IdDonViChiTiet")] DonViChiTiet_PhanNhom donViChiTiet_PhanNhom)
        {
            if (ModelState.IsValid)
            {
                donViChiTiet_PhanNhom.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_PhanNhom.IdDonViChiTiet).TenDonVi;
                _context.Add(donViChiTiet_PhanNhom);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_PhanNhom);
        }

        // GET: DonViChiTiet_PhanNhom/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            var donViChiTiet_PhanNhom = await _context.DonViChiTiet_PhanNhom.FindAsync(id);
            if (donViChiTiet_PhanNhom == null)
            {
                return NotFound();
            }
            return View(donViChiTiet_PhanNhom);
        }

        // POST: DonViChiTiet_PhanNhom/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Edit(long id, [Bind("id,TenPhanNhom,IdDonViChiTiet")] DonViChiTiet_PhanNhom donViChiTiet_PhanNhom)
        {
            if (id != donViChiTiet_PhanNhom.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    donViChiTiet_PhanNhom.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_PhanNhom.IdDonViChiTiet).TenDonVi;
                    _context.Update(donViChiTiet_PhanNhom);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DonViChiTiet_PhanNhomExists(donViChiTiet_PhanNhom.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_PhanNhom);
        }

        // GET: DonViChiTiet_PhanNhom/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_PhanNhom = await _context.DonViChiTiet_PhanNhom
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_PhanNhom == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_PhanNhom);
        }

        // POST: DonViChiTiet_PhanNhom/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var donViChiTiet_PhanNhom = await _context.DonViChiTiet_PhanNhom.FindAsync(id);
            _context.DonViChiTiet_PhanNhom.Remove(donViChiTiet_PhanNhom);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DonViChiTiet_PhanNhomExists(long id)
        {
            return _context.DonViChiTiet_PhanNhom.Any(e => e.id == id);
        }
    }
}
