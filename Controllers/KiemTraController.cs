﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using dbChange.Models;
using dbChange.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dbChange.Controllers
{
    public class KiemTraController : Controller
    {
        private readonly IStaffRepository _repository;
        private ILibRepository _Librepository;

        public KiemTraController(IStaffRepository repository, ILibRepository Librepository)
        {
            _repository = repository;
            _Librepository = Librepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Mui3()
        {
            ViewData["css"] = "~/wwwroot/Includes/KiemTra/Mui3_css.cshtml";
            ViewData["js"] = "~/wwwroot/Includes/KiemTra/Mui3_js.cshtml";
            ViewData["lsTenVacXin"] = _Librepository.GetAll_PL5_DanhMucVacxin();
            ViewData["lsDiaDiem"] = _Librepository.GetAll_PL4_DiaDiemTiem();
            ViewData["lsTinhThanh"] = _Librepository.GetAllProvice();
            return View();

        }
        public string GetThongTinTraCuu(string DienThoai)
        {
            Staff st = _repository.GetStaffByDienThoai(DienThoai);
            if(st!=null && st.HoTen!=null && !string.IsNullOrEmpty(st.HoTen))
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("HoTen");
                dt.Columns.Add("DienThoai");
                dt.Columns.Add("CMND");
                dt.Columns.Add("NgayS");
                dt.Columns.Add("ThangS");
                dt.Columns.Add("NamS");
                DataRow dr = dt.NewRow();
                dr["HoTen"] = st.HoTen;
                dr["DienThoai"] = st.DienThoai;
                dr["CMND"] = st.CMND;
                dr["NgayS"] = st.NgayS;
                dr["ThangS"] = st.ThangS;
                dr["NamS"] = st.NamS;
                dt.Rows.Add(dr);
                return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
            else
            {
                return "NOT_FOUND";
            }
            
        }
        public string LuuThongTinPhanAnh(string DienThoai, string HoTen, string CMND, string NgaySinh, string ThangSinh, string NamSinh, string TenVacXin
            , DateTime NgayTiem,string TinhThanhPho, string QuanHuyen, string DiaDiem, string SoLo)
        {
            if (NgayTiem.Year < 1900) 
                return "NgayTiem";
            else
                _repository.LuuThongTinPhanAnh(DienThoai, HoTen, CMND, NgaySinh, ThangSinh, NamSinh, TenVacXin, NgayTiem, TinhThanhPho, QuanHuyen, DiaDiem, SoLo);
            return "";
        }
    }
}