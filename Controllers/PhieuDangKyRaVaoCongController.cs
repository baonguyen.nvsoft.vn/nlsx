﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using dbChange.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using dbChange.Repository;
using dbChange.Models.LIB;
using Microsoft.Extensions.Caching.Memory;
using dbChange.Utils;

namespace dbChange.Controllers
{
    public class PhieuDangKyRaVaoCongController : Controller
    {
        private bool isLoged = false;
        private int height_screen = 0;
        IPhieuDangKyRaVaoCongRepository phieuDangKyRaVaoCongRepository;
        private readonly ILibRepository _Librepository;
        private readonly IAccountRepository _repository;
        private readonly IStaffRepository _Staffrepository;
        int idPhieu = -1;
        public PhieuDangKyRaVaoCongController(IHttpContextAccessor httpContextAccessor, IPhieuDangKyRaVaoCongRepository _phieuDangKyRaVaoCongRepository
            , ILibRepository Librepository, IAccountRepository repository, IStaffRepository Staffrepository)
        {
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
            }
            if (httpContextAccessor.HttpContext.Request.Cookies["height_screen"] != null)
            {
                height_screen = Convert.ToInt32(httpContextAccessor.HttpContext.Request.Cookies["height_screen"]);
            }
            phieuDangKyRaVaoCongRepository = _phieuDangKyRaVaoCongRepository;
            _Librepository = Librepository;
            _repository = repository;
            _Staffrepository = Staffrepository;
            if (httpContextAccessor.HttpContext.Request.Query.Count > 0 && httpContextAccessor.HttpContext.Request.Query.ContainsKey("idPhieu")) { 
                idPhieu = Convert.ToInt32(httpContextAccessor.HttpContext.Request.Query["idPhieu"].ToString());
            }
        }
        public IActionResult Index()
        {
            if (isLoged)
            {
                //ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                ViewData["css"] = "~/wwwroot/Includes/DangKyRaVaoCong/PhieuDangKyRaVaoCong_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/DangKyRaVaoCong/PhieuDangKyRaVaoCong_js.cshtml";
                ViewData["SoLuongPhieu"] = 0;
                ViewData["SoLuongDangChoDuyet"] = 0;
                ViewData["SoLuongDaDuyet"] = 0;
                ViewData["TuNgay"] = Utils.Utils.GetTimeServer().AddDays(-30);
                ViewData["DenNgay"] = Utils.Utils.GetTimeServer();
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["isViewPhieuDangChoDuyet"] = acc.isYTeDuyet ? 1 : acc.isDuyet ? 1 : 0;
                ViewData["isTaoPhieu"] = acc.isTaoPhieu ? 1 : 0;
                // is bo sung
                bool isBoSung = phieuDangKyRaVaoCongRepository.IsBoSung();
                if (Convert.ToInt32(ViewData["isViewPhieuDangChoDuyet"]) > 0)
                {
                    DataTable dt = new DataTable();
                    dt = phieuDangKyRaVaoCongRepository.GetAllPhieuDangKyRaVaoCongDangChoDuyet(acc, Utils.Utils.GetTimeServer().AddDays(-30), Utils.Utils.GetTimeServer());
                    ViewData["SoLuongDangChoDuyet"] = dt.Rows.Count;
                    dt = phieuDangKyRaVaoCongRepository.GetAllPhieuDangKyRaVaoCongDaDuyet(acc, Utils.Utils.GetTimeServer().AddDays(-30), Utils.Utils.GetTimeServer());
                    ViewData["SoLuongDaDuyet"] = dt.Rows.Count;
                    dt = phieuDangKyRaVaoCongRepository.GetAllPhieuDangKyRaVaoCong(acc.TenDangNhap);
                    ViewData["SoLuongPhieu"] = dt.Rows.Count;
                }
                ViewData["isBoSung"] = isBoSung;
                if (acc.isTaoPhieu) 
                    return View();
                else
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }
        public IActionResult PhieuDangKyRaVaoCongDangChoDuyet()
        {
            if (isLoged)
            {
                ViewData["css"] = "~/wwwroot/Includes/DangKyRaVaoCong/PhieuDangKyRaVaoCongDangChoDuyet_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/DangKyRaVaoCong/PhieuDangKyRaVaoCongDangChoDuyet_js.cshtml";
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["isViewPhieuDangChoDuyet"] = acc.isYTeDuyet ? 1 : acc.isDuyet ? 1 : 0;
                if (acc.isTaoPhieu)
                    return View();
                else
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }
        public IActionResult PhieuDangKyRaVaoCongDaDuyet()
        {
            if (isLoged)
            {
                ViewData["css"] = "~/wwwroot/Includes/DangKyRaVaoCong/PhieuDangKyRaVaoCongDangChoDuyet_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/DangKyRaVaoCong/PhieuDangKyRaVaoCongDaDuyet_js.cshtml";
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["isViewPhieuDangChoDuyet"] = acc.isYTeDuyet ? 1 : acc.isDuyet ? 1 : 0;
                if (acc.isTaoPhieu)
                    return View();
                else
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }
        public IActionResult ThemPhieuDangKyRaVaoCong()
        {
            if (isLoged)
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                ViewData["css"] = "~/wwwroot/Includes/DangKyRaVaoCong/ThemPhieuDangKyRaVaoCong_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/DangKyRaVaoCong/ThemPhieuDangKyRaVaoCong_js.cshtml";
                if (acc.isTaoPhieu)
                    return View();
                else
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }
        public IActionResult ThemPhieuDangKyRaVaoCongBoSung()
        {
            if (isLoged)
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                ViewData["css"] = "~/wwwroot/Includes/DangKyRaVaoCong/ThemPhieuDangKyRaVaoCong_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/DangKyRaVaoCong/ThemPhieuDangKyRaVaoCongBoSung_js.cshtml";
                if (acc.isTaoPhieu)
                    return View();
                else
                    return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }
        [HttpGet]
        public string GetAllPhieuDangKyRaVaoCong()
        {
            if (isLoged)
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                DataTable dt = new DataTable();
                if (acc.isTaoPhieu)
                {
                    dt = phieuDangKyRaVaoCongRepository.GetAllPhieuDangKyRaVaoCong(acc.TenDangNhap);
                    return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        public string GetAllPhieuDangKyRaVaoCongByTrangThai()
        {
            if (isLoged)
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                DataTable dt = new DataTable();
                if (acc.isTaoPhieu)
                {
                    dt = phieuDangKyRaVaoCongRepository.GetAllPhieuDangKyRaVaoCongDangChoDuyet(acc, Utils.Utils.GetTimeServer().AddDays(-7), Utils.Utils.GetTimeServer());
                    return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
                
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        public string GetAllPhieuDangKyRaVaoCongByTrangThaiDaDuyet()
        {
            if (isLoged)
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                DataTable dt = new DataTable();
                if (acc.isTaoPhieu)
                {
                    dt = phieuDangKyRaVaoCongRepository.GetAllPhieuDangKyRaVaoCongDaDuyet(acc, Utils.Utils.GetTimeServer().AddDays(-2), Utils.Utils.GetTimeServer());
                    return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject("");
                }
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        public string GetAllNhanVienPhieuDangKyRaVaoCong(int IdPhieu)
        {
            if (isLoged)
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);

                DataTable dt = new DataTable();

                if(acc.isDuyet) 
                    dt = phieuDangKyRaVaoCongRepository.GetAllNhanVienPhieuDangKyRaVaoCong(IdPhieu);
                else
                    dt = phieuDangKyRaVaoCongRepository.GetAllNhanVienPhieuDangKyRaVaoCong2(IdPhieu);

                return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        public string SavePhieuDangKyRaVaoCong(string NgayDangKyTuNgay, string NgayDangKyDenNgay, string DonVi,string GhiChu, int IdPhieu)
        {
            try
            {
                if (isLoged)
                {

                    if (Convert.ToDateTime(NgayDangKyTuNgay) >= Convert.ToDateTime(Utils.Utils.GetTimeServer().ToString("yyy-MM-dd")) && Convert.ToDateTime(NgayDangKyTuNgay) <= Convert.ToDateTime(NgayDangKyDenNgay))
                    {
                        string token = "";
                        if (HttpContext.Request.Cookies["token"] != null)
                        {
                            token = HttpContext.Request.Cookies["token"];
                        }
                        else if (HttpContext.Session.GetString("token") != null)
                        {
                            token = HttpContext.Session.GetString("token");
                        }
                        string a = Utils.Utils.Decrypt(token);
                        Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                        Staff staff = _repository.GetStaffByUserName(acc.TenDangNhap);
                        if (acc.isTaoPhieu)
                        {
                            PhieuDangKyRaVaoCong phieu = new PhieuDangKyRaVaoCong();
                            if (IdPhieu != null && IdPhieu > 0)
                            {
                                phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
                                LuuThuVienVaoCache(phieu.IdDonVi);
                                // is change date
                                if (phieu.NgayDangKyTuNgay.CompareTo(Convert.ToDateTime(NgayDangKyTuNgay)) == 0 && phieu.NgayDangKyDenNgay.CompareTo(Convert.ToDateTime(NgayDangKyDenNgay)) == 0)
                                {
                                    //no
                                    // update GhiChu in master
                                   
                                    phieu.GhiChu = GhiChu != null ? GhiChu : "";
                                    phieuDangKyRaVaoCongRepository.UpdatePhieu(phieu);
                                }
                                else
                                {
                                    //yes
                                    // update master
                                    phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
                                    phieu.NgayDangKyTuNgay = Convert.ToDateTime(NgayDangKyTuNgay);
                                    phieu.NgayDangKyDenNgay = Convert.ToDateTime(NgayDangKyDenNgay);
                                    phieu.GhiChu = GhiChu != null ? GhiChu : "";
                                    phieuDangKyRaVaoCongRepository.UpdatePhieu(phieu);
                                    // update detail
                                    DataTable dtChiTietPhieu = new DataTable();
                                    dtChiTietPhieu = phieuDangKyRaVaoCongRepository.GetAllNhanVienPhieuDangKyRaVaoCong(IdPhieu);
                                    foreach (DataRow dr in dtChiTietPhieu.Rows)
                                    {
                                        phieuDangKyRaVaoCongRepository.XoaChiTietNgayTheoIdChiTiet(Convert.ToInt32(dr["Id"]));
                                        DateTime dtTemp = Convert.ToDateTime(NgayDangKyTuNgay);
                                        while (dtTemp <= Convert.ToDateTime(NgayDangKyDenNgay))
                                        {
                                            phieuDangKyRaVaoCongRepository.CreateChiTietPhieuNgay(Convert.ToInt32(dr["Id"]), dtTemp);
                                            dtTemp = dtTemp.AddDays(1);
                                        }
                                    }
                                }
                                
                                return "UPDATED";
                            }
                            else
                            {
                                phieu.NgayDangKyTuNgay = Convert.ToDateTime(NgayDangKyTuNgay);
                                phieu.NgayDangKyDenNgay = Convert.ToDateTime(NgayDangKyDenNgay);
                                phieu.IdDonVi = Convert.ToInt32(DonVi);
                                phieu.GhiChu = GhiChu != null ? GhiChu : "";
                                phieu.NguoiTaoId = staff.Id;
                                phieu.TenNguoiTao = staff.HoTen;
                                int idPhieu = 0;
                                idPhieu = phieuDangKyRaVaoCongRepository.CreatePhieu(phieu);
                                LuuThuVienVaoCache(phieu.IdDonVi);
                                if (idPhieu > 0)
                                {
                                    return idPhieu.ToString();
                                }
                                else
                                {
                                    return "";
                                }
                                
                            }
                        }
                        else
                        {
                            return "";
                        }
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }catch(Exception ex)
            {
                int a = 0;
                return "";
            }
        }
        public string SavePhieuDangKyRaVaoCongBoSung(string NgayDangKyTuNgay, string NgayDangKyDenNgay, string DonVi,string GhiChu, int IdPhieu)
        {
            try
            {
                if (isLoged)
                {

                    if (Convert.ToDateTime(NgayDangKyTuNgay) >= Convert.ToDateTime(Utils.Utils.GetTimeServer().ToString("yyy-MM-dd")) && Convert.ToDateTime(NgayDangKyTuNgay) <= Convert.ToDateTime(NgayDangKyDenNgay))
                    {
                        string token = "";
                        if (HttpContext.Request.Cookies["token"] != null)
                        {
                            token = HttpContext.Request.Cookies["token"];
                        }
                        else if (HttpContext.Session.GetString("token") != null)
                        {
                            token = HttpContext.Session.GetString("token");
                        }
                        string a = Utils.Utils.Decrypt(token);
                        Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                        Staff staff = _repository.GetStaffByUserName(acc.TenDangNhap);
                        if (acc.isTaoPhieu)
                        {
                            PhieuDangKyRaVaoCong phieu = new PhieuDangKyRaVaoCong();
                            if (IdPhieu != null && IdPhieu > 0)
                            {
                                phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
                                // is change date
                                if (phieu.NgayDangKyTuNgay.CompareTo(Convert.ToDateTime(NgayDangKyTuNgay)) == 0 && phieu.NgayDangKyDenNgay.CompareTo(Convert.ToDateTime(NgayDangKyDenNgay)) == 0)
                                {
                                    //no
                                    // update GhiChu in master
                                   
                                    phieu.GhiChu = GhiChu != null ? GhiChu : "";
                                    phieuDangKyRaVaoCongRepository.UpdatePhieu(phieu);
                                }
                                else
                                {
                                    //yes
                                    // update master
                                    phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
                                    phieu.NgayDangKyTuNgay = Convert.ToDateTime(NgayDangKyTuNgay);
                                    phieu.NgayDangKyDenNgay = Convert.ToDateTime(NgayDangKyDenNgay);
                                    phieu.GhiChu = GhiChu != null ? GhiChu : "";
                                    phieu.IsBoSung = true;
                                    phieuDangKyRaVaoCongRepository.UpdatePhieu(phieu);
                                    // update detail
                                    DataTable dtChiTietPhieu = new DataTable();
                                    dtChiTietPhieu = phieuDangKyRaVaoCongRepository.GetAllNhanVienPhieuDangKyRaVaoCong(IdPhieu);
                                    foreach (DataRow dr in dtChiTietPhieu.Rows)
                                    {
                                        phieuDangKyRaVaoCongRepository.XoaChiTietNgayTheoIdChiTiet(Convert.ToInt32(dr["Id"]));
                                        DateTime dtTemp = Convert.ToDateTime(NgayDangKyTuNgay);
                                        while (dtTemp <= Convert.ToDateTime(NgayDangKyDenNgay))
                                        {
                                            phieuDangKyRaVaoCongRepository.CreateChiTietPhieuNgay(Convert.ToInt32(dr["Id"]), dtTemp);
                                            dtTemp = dtTemp.AddDays(1);
                                        }
                                    }
                                }
                                
                                return "UPDATED";
                            }
                            else
                            {
                                phieu.NgayDangKyTuNgay = Convert.ToDateTime(NgayDangKyTuNgay);
                                phieu.NgayDangKyDenNgay = Convert.ToDateTime(NgayDangKyDenNgay);
                                phieu.IdDonVi = Convert.ToInt32(DonVi);
                                phieu.GhiChu = GhiChu != null ? GhiChu : "";
                                phieu.NguoiTaoId = staff.Id;
                                phieu.TenNguoiTao = staff.HoTen;
                                phieu.IsBoSung = true;
                                int idPhieu = 0;
                                idPhieu = phieuDangKyRaVaoCongRepository.CreatePhieu(phieu);
                                if (idPhieu > 0)
                                {
                                    return idPhieu.ToString();
                                }
                                else
                                {
                                    return "";
                                }
                            }
                        }
                        else
                        {
                            return "";
                        }
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }catch(Exception ex)
            {
                int a = 0;
                return "";
            }
        }
        public ActionResult ChiTietPhieu()
        {
            if (isLoged)
            {
                PhieuDangKyRaVaoCong phieu = phieuDangKyRaVaoCongRepository.GetPhieu(idPhieu);
                if (phieu.TinhTrang == "ChoYTeDuyet" || phieu.TinhTrang == "ChoPhuTrachDuyet")
                {
                    string url = Url.Action("ChiTietPhieuChoDuyet", "PhieuDangKyRaVaoCong") + "?IdPhieu=" + phieu.Id;
                    return Redirect(url);
                }
                else
                {
                    if (phieu != null && phieu.Id > 0)
                    {
                        string token = "";
                        if (HttpContext.Request.Cookies["token"] != null)
                        {
                            token = HttpContext.Request.Cookies["token"];
                        }
                        else if (HttpContext.Session.GetString("token") != null)
                        {
                            token = HttpContext.Session.GetString("token");
                        }
                        string a = Utils.Utils.Decrypt(token);
                        Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                        ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                        ViewData["css"] = "~/wwwroot/Includes/DangKyRaVaoCong/ThemPhieuDangKyRaVaoCong_css.cshtml";
                        ViewData["js"] = "~/wwwroot/Includes/DangKyRaVaoCong/ThemPhieuDangKyRaVaoCong_js.cshtml";
                        ViewData["Phieu"] = phieuDangKyRaVaoCongRepository.GetPhieu(idPhieu);
                        ViewData["isViewPhieuDangChoDuyet"] = acc.isYTeDuyet ? 1 : acc.isDuyet ? 1 : 0;

                        ViewData["SoLuongNhanVienDangKy"] = phieuDangKyRaVaoCongRepository.SoLuongNhanVienTrongPhieu(phieu.Id,"MoiTao");
                        ViewData["SoLuongNhanVienYTeDuyet"] = phieuDangKyRaVaoCongRepository.SoLuongNhanVienTrongPhieu(phieu.Id,"ChoPhuTrachDuyet");
                        ViewData["SoLuongNhanVienDuocDuyet"] = phieuDangKyRaVaoCongRepository.SoLuongNhanVienTrongPhieu(phieu.Id,"DaDuyet");
                        ViewData["GetSoLuongNhanVienChiTietPhieuTheoThu"] = phieuDangKyRaVaoCongRepository.GetSoLuongNhanVienChiTietPhieuTheoThu(phieu.Id, phieu.TinhTrang);

                        LuuThuVienVaoCache(phieu.IdDonVi);

                        if (acc.isTaoPhieu)
                            return View();
                        else
                            return RedirectToAction(actionName: "Index", controllerName: "Home");
                    }
                    else
                    {
                        return RedirectToAction(actionName: "Index", controllerName: "PhieuDangKyRaVaoCong");
                    }
                }
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }
        public ActionResult ChiTietPhieuChoDuyet()
        {
            if (isLoged)
            {
                PhieuDangKyRaVaoCong phieu = phieuDangKyRaVaoCongRepository.GetPhieu(idPhieu);
                if (phieu!=null && phieu.Id > 0)
                {
                    string token = "";
                    if (HttpContext.Request.Cookies["token"] != null)
                    {
                        token = HttpContext.Request.Cookies["token"];
                    }
                    else if (HttpContext.Session.GetString("token") != null)
                    {
                        token = HttpContext.Session.GetString("token");
                    }
                    string a = Utils.Utils.Decrypt(token);
                    Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                    ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                    ViewData["css"] = "~/wwwroot/Includes/DangKyRaVaoCong/ChiTietPhieuChoDuyet_css.cshtml";
                    ViewData["js"] = "~/wwwroot/Includes/DangKyRaVaoCong/ChiTietPhieuChoDuyet_js.cshtml";
                    ViewData["Phieu"] = phieuDangKyRaVaoCongRepository.GetPhieu(idPhieu);
                    ViewData["isViewPhieuDangChoDuyet"] = acc.isYTeDuyet ? 1 : acc.isDuyet ? 2 : 0;

                    ViewData["SoLuongNhanVienDangKy"] = phieuDangKyRaVaoCongRepository.SoLuongNhanVienTrongPhieu(phieu.Id, "MoiTao");
                    ViewData["SoLuongNhanVienYTeDuyet"] = phieuDangKyRaVaoCongRepository.SoLuongNhanVienTrongPhieu(phieu.Id, "ChoPhuTrachDuyet");
                    ViewData["SoLuongNhanVienDuocDuyet"] = phieuDangKyRaVaoCongRepository.SoLuongNhanVienTrongPhieu(phieu.Id, "DaDuyet");
                    ViewData["GetSoLuongNhanVienChiTietPhieuTheoThu"] = phieuDangKyRaVaoCongRepository.GetSoLuongNhanVienChiTietPhieuTheoThu(phieu.Id, phieu.TinhTrang);

                    LuuThuVienVaoCache(phieu.IdDonVi);

                    if (acc.isTaoPhieu)
                        return View();
                    else
                        return RedirectToAction(actionName: "Index", controllerName: "Home");
                }
                else
                {
                    return RedirectToAction(actionName: "Index", controllerName: "PhieuDangKyRaVaoCong");
                }
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }

        public string GetDanhSachNhanVienYTeDuyet()
        {
            List<Staff> staffs = new List<Staff>();
            staffs = phieuDangKyRaVaoCongRepository.GetDanhSachNhanVienYTeDuyet();
            return Newtonsoft.Json.JsonConvert.SerializeObject(staffs);
        }
        public string GetDanhSachNhanVienByIdDonVi(string TenDonVi)
        {
            List<Staff> staffs = new List<Staff>();
            staffs = phieuDangKyRaVaoCongRepository.GetDanhSachNhanVienByIdDonVi(TenDonVi);
            return Newtonsoft.Json.JsonConvert.SerializeObject(staffs);
        }
        public string GetDanhSachNhanVienByIdDonViIdPhieu(string TenDonVi, int IdPhieu)
        {
            DataTable dt = new DataTable();
            dt = phieuDangKyRaVaoCongRepository.GetDanhSachNhanVienByIdDonViIdPhieu(TenDonVi,IdPhieu);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string SaveNhanVienVaoPhieu(int IdNhanVien, int IdPhieu, string LyDo)
        {

            phieuDangKyRaVaoCongRepository.SaveNhanVienVaoPhieu(IdNhanVien, IdPhieu,LyDo!=null ? LyDo : "");
            return "";
        }
        public string UpdateLyDo(string ThoiGianLamViec,string LyDo, int IdChiTiet, int SoMuiTiem, DateTime NgayTestGanNhat,bool Trong14Ngay_TiepXuc,bool Trong14Ngay_XungQuanhNha, bool NguoiOCung
            ,int ViTriVPLamViecId, int PhanNhomId, int ViTriLamViecId, int ViTriDeXeId, int ViTriVeSinhId, int ViTriNhaAnId)
        {
            try
            {
                phieuDangKyRaVaoCongRepository.UpdateChiTiet(ThoiGianLamViec!=null? ThoiGianLamViec: "",LyDo != null ? LyDo : "", IdChiTiet, SoMuiTiem, NgayTestGanNhat, Trong14Ngay_TiepXuc, Trong14Ngay_XungQuanhNha, NguoiOCung,
                    ViTriVPLamViecId, PhanNhomId, ViTriLamViecId, ViTriDeXeId, ViTriVeSinhId, ViTriNhaAnId);
                return "";
            }catch(Exception ex)
            {
                return "";
            }
        }
        public string UpdateNhanVienYTeDuyet(int IdNhanVienYTeDuyet, string TenNhanVienYTeDuyet, int IdPhieu)
        {
            phieuDangKyRaVaoCongRepository.UpdateNhanVienYTeDuyet(IdNhanVienYTeDuyet, TenNhanVienYTeDuyet, IdPhieu);
            return "";
        }
        public ActionResult XoaNhanVienTrongPhieu(int IdChiTiet, int IdPhieu)
        {
            phieuDangKyRaVaoCongRepository.XoaNhanVienTrongPhieu(IdChiTiet);
            string url = Url.Action("ChiTietPhieu", "PhieuDangKyRaVaoCong") + "?IdPhieu=" + IdPhieu;
            return Redirect(url);
        }
        public string XoaNhanVienTrongPhieuNoRedirect(int IdChiTiet, int IdPhieu)
        {
            phieuDangKyRaVaoCongRepository.XoaNhanVienTrongPhieu(IdChiTiet);
            return "";
        }
        public string XoaPhieu(int IdPhieu)
        {
            PhieuDangKyRaVaoCong phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
            if (phieu.TinhTrang == "MoiTao")
            {
                phieuDangKyRaVaoCongRepository.XoaPhieu(IdPhieu);
                return "DELETED";
            }
            else
            {
                return "";
            }
        }
        public string XetDuyetPhieu(int IdPhieu, int IdChiTiet, bool IsDuyet, string LyDo, int Type)
        {
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");
            }
            string a = Utils.Utils.Decrypt(token);
            Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
            Staff staff = _repository.GetStaffByUserName(acc.TenDangNhap);
            phieuDangKyRaVaoCongRepository.XetDuyetPhieuMaster(IdPhieu,Type);
            phieuDangKyRaVaoCongRepository.XetDuyetPhieu(IdPhieu, IdChiTiet, IsDuyet, LyDo, staff.Id, Type);
            return IdPhieu.ToString() + "-" + IdChiTiet.ToString() + "-" + IsDuyet.ToString() + "-" + LyDo;
        }
        public string ThemThuChoNhanVien(int IdPhieu,int IdChiTiet, string LsThu)
        {
            phieuDangKyRaVaoCongRepository.ThemThuChoNhanVien(IdPhieu,IdChiTiet, LsThu);
            return IdChiTiet.ToString() + "-" + LsThu;
        }
        public string ThemNgayChoNhanVien(int IdPhieu,int IdChiTiet, DateTime Ngay)
        {
            phieuDangKyRaVaoCongRepository.ThemNgayChoNhanVien(IdPhieu,IdChiTiet, Ngay);
            return Ngay.ToString("dd/MM/yyyy"); ;
        }
        public string DanhSachNgayDiLamGroupByNhanVien(string lsIdChiTiet)
        {
            DataTable dt = new DataTable();
            dt = phieuDangKyRaVaoCongRepository.DanhSachNgayDiLamGroupByNhanVien(lsIdChiTiet);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string DanhSachNgayDiLamTrenPhieu(int IdPhieu)
        {
            DataTable dt = new DataTable();
            dt = phieuDangKyRaVaoCongRepository.DanhSachNgayDiLamTrenPhieu(IdPhieu);
            return dt.Rows.Count > 0 ? Newtonsoft.Json.JsonConvert.SerializeObject(dt) : Newtonsoft.Json.JsonConvert.SerializeObject("");
        }
        public string CapNhatCacDongChiTiet(int IdChiTiet, string Loai, string GiaTri)
        {
            string result = "";
            phieuDangKyRaVaoCongRepository.CapNhatCacDongChiTiet(IdChiTiet, Loai, GiaTri);
            return result;
        }
        public string XoaChiTietNgay(int IdChiTietNgay)
        {
            phieuDangKyRaVaoCongRepository.XoaChiTietNgay(IdChiTietNgay);
            //string url = Url.Action("ChiTietPhieu", "PhieuDangKyRaVaoCong") + "?IdPhieu=" + IdPhieu;
            //return Redirect(url);
            return "";
        }
        public string TraPhieu(int IdPhieu)
        {
            phieuDangKyRaVaoCongRepository.UpdateTrangThaiPhieu(IdPhieu, "MoiTao");
            return "";
        }
        public string GetViTriLamViec(int IdPhieu)
        {
            PhieuDangKyRaVaoCong phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
            return Newtonsoft.Json.JsonConvert.SerializeObject(_Librepository.GetAllViTriLamViec(phieu.IdDonVi));
        }
        public string GetViTriVPLamViec(int IdPhieu)
        {
            PhieuDangKyRaVaoCong phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
            return Newtonsoft.Json.JsonConvert.SerializeObject(_Librepository.GetAllViTriVPLamViec(phieu.IdDonVi));
        }
        public string GetViTriVeSinh(int IdPhieu)
        {
            PhieuDangKyRaVaoCong phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
            return Newtonsoft.Json.JsonConvert.SerializeObject(_Librepository.GetAllViTriVeSinh(phieu.IdDonVi));
        }
        public string GetViTriDeXe(int IdPhieu)
        {
            PhieuDangKyRaVaoCong phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
            return Newtonsoft.Json.JsonConvert.SerializeObject(_Librepository.GetAllViTriDeXe(phieu.IdDonVi));
        }
        public string GetPhanNhom(int IdPhieu)
        {
            PhieuDangKyRaVaoCong phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
            return Newtonsoft.Json.JsonConvert.SerializeObject(_Librepository.GetAllPhanNhom(phieu.IdDonVi));
        }
        public string GetViTriNhaAn()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(_Librepository.GetAllViTriNhaAn());
        }
        public string CopyPhieu(int IdPhieuTu, DateTime NgayDangKyTuNgay, DateTime NgayDangKyDenNgay)
        {
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");
            }
            string a = Utils.Utils.Decrypt(token);
            Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
            Staff staff = _repository.GetStaffByUserName(acc.TenDangNhap);

            PhieuDangKyRaVaoCong phieu = new PhieuDangKyRaVaoCong();
            phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieuTu);
            int idPhieuDen = 0;
            // copy phieu master"
            PhieuDangKyRaVaoCong phieuCopy = new PhieuDangKyRaVaoCong();
            phieuCopy.GhiChu = phieu.GhiChu;
            phieuCopy.IdDonVi = phieu.IdDonVi;
            phieuCopy.NgayDangKyDenNgay = NgayDangKyDenNgay;
            phieuCopy.NgayDangKyTuNgay = NgayDangKyTuNgay;
            phieuCopy.NgayTao = Utils.Utils.GetTimeServer() ;
            phieuCopy.NguoiTaoId = staff.Id;
            phieuCopy.TenDonVi = phieu.TenDonVi;
            phieuCopy.TenNguoiTao = staff.HoTen;
            phieuCopy.TinhTrang = "MoiTao";
            idPhieuDen = phieuDangKyRaVaoCongRepository.CreatePhieu(phieuCopy);
            // copy chi tiet
            phieuDangKyRaVaoCongRepository.CopyChiTietPhieu(IdPhieuTu, idPhieuDen);
            // copy chi tiet ngay
            DataTable dtChiTietPhieu = new DataTable();
            dtChiTietPhieu = phieuDangKyRaVaoCongRepository.GetAllNhanVienPhieuDangKyRaVaoCong(idPhieuDen);
            foreach (DataRow dr in dtChiTietPhieu.Rows)
            {
                phieuDangKyRaVaoCongRepository.XoaChiTietNgayTheoIdChiTiet(Convert.ToInt32(dr["Id"]));
                DateTime dtTemp = Convert.ToDateTime(NgayDangKyTuNgay);
                while (dtTemp <= Convert.ToDateTime(NgayDangKyDenNgay))
                {
                    phieuDangKyRaVaoCongRepository.CreateChiTietPhieuNgay(Convert.ToInt32(dr["Id"]), dtTemp);
                    dtTemp = dtTemp.AddDays(1);
                }
            }
            return idPhieuDen.ToString();
        }
        public string LuuNhanVienImport(int IdPhieu, string BoPhan, string MaNhanVien, string Hoten, string LyDo, string ThoiGianLamViec
            , int SoMuiTiem, string NgayTestGanNhat, string Trong14Ngay_HoSot, string Trong14Ngay_TiepXuc, string Trong14Ngay_XungQuanhNha, string NguoiOCung
            , string ViTriVPLamViec, string PhanNhom, string ViTriLamViec, string ViTriDeXe, string ViTriVeSinh, string ViTriNhaAn
            , string T2, string T3, string T4, string T5, string T6, string T7 )
        {
            try
            {
                PhieuDangKyRaVaoCong phieu = new PhieuDangKyRaVaoCong();
                phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
                Staff staff = new Staff();
                if (MaNhanVien != null && MaNhanVien != "")
                {
                    staff = _Staffrepository.GetStaffByMaNS(MaNhanVien.Trim());
                    if (staff != null && staff.Id > 0 && ViTriVPLamViec != null && PhanNhom != null && ViTriLamViec != null && ViTriDeXe != null && ViTriVeSinh != null && ViTriNhaAn != null
                        && Trong14Ngay_HoSot != null && Trong14Ngay_TiepXuc != null && Trong14Ngay_XungQuanhNha != null && NguoiOCung != null)
                    {
                        LyDo = LyDo != null ? LyDo : "";
                        NgayTestGanNhat = NgayTestGanNhat != null ? NgayTestGanNhat : "";// Utils.Utils.GetTimeServer();
                        T2 = T2 != null ? T2 : "";
                        T3 = T3 != null ? T3 : "";
                        T4 = T4 != null ? T4 : "";
                        T5 = T5 != null ? T5 : "";
                        T6 = T6 != null ? T6 : "";
                        T7 = T7 != null ? T7 : "";
                        // save nhan vien vao phieu
                        phieuDangKyRaVaoCongRepository.SaveNhanVienVaoPhieu(staff.Id, IdPhieu, LyDo);
                        // update dong chi tiet 
                        DonViChiTiet_ViTriVPLamViec ViTriVPLamViecId = _Librepository.GetViTriVPLamViecByName(ViTriVPLamViec.Trim(), phieu.IdDonVi);
                        DonViChiTiet_PhanNhom PhanNhomId = _Librepository.GetPhanNhomByName(PhanNhom.Trim(), phieu.IdDonVi);
                        DonViChiTiet_ViTriDeXe ViTriDeXeId = _Librepository.GetViTriDeXeByName(ViTriDeXe.Trim(), phieu.IdDonVi);
                        DonViChiTiet_ViTriLamViec ViTriLamViecId = _Librepository.GetViTriLamViecByName(ViTriLamViec.Trim(), phieu.IdDonVi);
                        DonViChiTiet_ViTriVeSinh ViTriVeSinhId = _Librepository.GetViTriVeSinhByName(ViTriVeSinh.Trim(), phieu.IdDonVi);
                        ViTriNhaAn ViTriNhaAnId = _Librepository.GetViTriNhaAnByName(ViTriNhaAn.Trim());
                        if (ViTriVPLamViecId != null && PhanNhomId != null && ViTriDeXeId != null && ViTriLamViecId != null && ViTriVeSinhId != null && ViTriNhaAnId != null)
                        {
                            string[] ngaythangnam;
                            if (NgayTestGanNhat.Contains("/"))
                            {
                                ngaythangnam = NgayTestGanNhat.Split('/');
                            }
                            else
                            {
                                ngaythangnam = Utils.Utils.GetTimeServer().ToString("dd/MM/yyyy").Split('/');
                            }
                            phieuDangKyRaVaoCongRepository.UpdateChiTietImport(LyDo != null ? LyDo : "", ThoiGianLamViec != null ? ThoiGianLamViec : "", MaNhanVien.Trim(), SoMuiTiem, new DateTime(Convert.ToInt32(ngaythangnam[2]), Convert.ToInt32(ngaythangnam[1]), Convert.ToInt32(ngaythangnam[0]))
                                , Trong14Ngay_HoSot.Trim() == "Có" ? true : false, Trong14Ngay_TiepXuc.Trim() == "Có" ? true : false, Trong14Ngay_XungQuanhNha.Trim() == "Có" ? true : false, NguoiOCung.Trim() == "Có" ? true : false,
                                    ViTriVPLamViecId.id, PhanNhomId.id, ViTriLamViecId.id, ViTriDeXeId.id, ViTriVeSinhId.id, ViTriNhaAnId.id);
                            // update ngay lam viec
                            string LsThu = "";
                            if (T2.ToUpper() == "X")
                            {
                                LsThu = "2";
                            }
                            if (T3.ToUpper() == "X")
                            {
                                if (LsThu == "") LsThu = "3"; else LsThu += ",3";
                            }
                            if (T4.ToUpper() == "X")
                            {
                                if (LsThu == "") LsThu = "4"; else LsThu += ",4";
                            }
                            if (T5.ToUpper() == "X")
                            {
                                if (LsThu == "") LsThu = "5"; else LsThu += ",5";
                            }
                            if (T6.ToUpper() == "X")
                            {
                                if (LsThu == "") LsThu = "6"; else LsThu += ",6";
                            }
                            if (T7.ToUpper() == "X")
                            {
                                if (LsThu == "") LsThu = "7"; else LsThu += ",7";
                            }
                            phieuDangKyRaVaoCongRepository.ThemThuChoNhanVienImport(IdPhieu, staff.MaQR, LsThu);
                        }
                        else
                        {
                            return "404";
                        }
                        return "201";
                    }
                    else
                    {
                        return "404";
                    }
                }
                else
                {
                    return "201";
                }
            }catch(Exception ex)
            {
                return "404";
            }
        }
        public void LuuThuVienVaoCache(int IdDonVi)
        {
            List<ViTriNhaAn> lsViTriNhaAn = new List<ViTriNhaAn>();
            List<DonViChiTiet_ViTriVPLamViec> lsViTriVPLamViec = new List<DonViChiTiet_ViTriVPLamViec>();
            List<DonViChiTiet_PhanNhom> lsPhanNhom = new List<DonViChiTiet_PhanNhom>();
            List<DonViChiTiet_ViTriLamViec> lsViTriLamViec = new List<DonViChiTiet_ViTriLamViec>();
            List<DonViChiTiet_ViTriDeXe> lsViTriDeXe = new List<DonViChiTiet_ViTriDeXe>();
            List<DonViChiTiet_ViTriVeSinh> lsViTriVeSinh = new List<DonViChiTiet_ViTriVeSinh>();

            lsViTriNhaAn = _Librepository.GetAllViTriNhaAn();
            lsViTriVPLamViec = _Librepository.GetAllViTriVPLamViec(IdDonVi);
            lsPhanNhom = _Librepository.GetAllPhanNhom(IdDonVi);
            lsViTriLamViec = _Librepository.GetAllViTriLamViec(IdDonVi);
            lsViTriDeXe = _Librepository.GetAllViTriDeXe(IdDonVi);
            lsViTriVeSinh = _Librepository.GetAllViTriVeSinh(IdDonVi);

            MemoryCacheHelper.Add("lsViTriNhaAn", lsViTriNhaAn, DateTimeOffset.UtcNow.AddHours(12));
            MemoryCacheHelper.Add("lsViTriVPLamViec", lsViTriVPLamViec, DateTimeOffset.UtcNow.AddHours(12));
            MemoryCacheHelper.Add("lsPhanNhom", lsPhanNhom, DateTimeOffset.UtcNow.AddHours(12));
            MemoryCacheHelper.Add("lsViTriLamViec", lsViTriLamViec, DateTimeOffset.UtcNow.AddHours(12));
            MemoryCacheHelper.Add("lsViTriDeXe", lsViTriDeXe, DateTimeOffset.UtcNow.AddHours(12));
            MemoryCacheHelper.Add("lsViTriVeSinh", lsViTriVeSinh, DateTimeOffset.UtcNow.AddHours(12));

        }
        public bool CheckViTriNhaAn(string TenViTri)
        {
            List<ViTriNhaAn> lsViTriNhaAn = MemoryCacheHelper.GetValue("lsViTriNhaAn") as List<ViTriNhaAn>;
            if (lsViTriNhaAn == null)
            {
                lsViTriNhaAn = _Librepository.GetAllViTriNhaAn();
                MemoryCacheHelper.Add("lsViTriNhaAn", lsViTriNhaAn, DateTimeOffset.UtcNow.AddHours(12));
            }
            foreach (ViTriNhaAn item in lsViTriNhaAn)
            {
                if (item.TenViTriNhaAn == TenViTri)
                    return true;
            }
            return false;
        }
        public bool CheckViTriVPLamViec(string TenViTri, int IdDonVi)
        {
            List<DonViChiTiet_ViTriVPLamViec> lsViTriVPLamViec = MemoryCacheHelper.GetValue("lsViTriVPLamViec") as List<DonViChiTiet_ViTriVPLamViec>;
            if (lsViTriVPLamViec == null)
            {
                lsViTriVPLamViec = _Librepository.GetAllViTriVPLamViec(IdDonVi);
                MemoryCacheHelper.Add("lsViTriVPLamViec", lsViTriVPLamViec, DateTimeOffset.UtcNow.AddHours(12));
            }
            foreach (DonViChiTiet_ViTriVPLamViec item in lsViTriVPLamViec)
            {
                if (item.IdDonViChiTiet == IdDonVi && item.TenViTriVPLamViec == TenViTri)
                    return true;
            }
            return false;
        }
        public bool CheckViTriLamViec(string TenViTri, int IdDonVi)
        {
            List<DonViChiTiet_ViTriLamViec> lsViTriLamViec = MemoryCacheHelper.GetValue("lsViTriLamViec") as List<DonViChiTiet_ViTriLamViec>;
            if (lsViTriLamViec == null)
            {
                lsViTriLamViec = _Librepository.GetAllViTriLamViec(IdDonVi);
                MemoryCacheHelper.Add("lsViTriLamViec", lsViTriLamViec, DateTimeOffset.UtcNow.AddHours(12));
            }
            foreach (DonViChiTiet_ViTriLamViec item in lsViTriLamViec)
            {
                if (item.IdDonViChiTiet == IdDonVi && item.TenViTriLamViec == TenViTri)
                    return true;
            }
            return false;
        }
        public bool CheckViTriPhanNhom(string TenViTri, int IdDonVi)
        {
            List<DonViChiTiet_PhanNhom> lsPhanNhom = MemoryCacheHelper.GetValue("lsPhanNhom") as List<DonViChiTiet_PhanNhom>;
            if (lsPhanNhom == null)
            {
                lsPhanNhom = _Librepository.GetAllPhanNhom(IdDonVi);
                MemoryCacheHelper.Add("lsPhanNhom", lsPhanNhom, DateTimeOffset.UtcNow.AddHours(12));
            }
            foreach (DonViChiTiet_PhanNhom item in lsPhanNhom)
            {
                if (item.IdDonViChiTiet == IdDonVi && item.TenPhanNhom == TenViTri)
                    return true;
            }
            return false;
        }
        public bool CheckViTriDeXe(string TenViTri, int IdDonVi)
        {
            List<DonViChiTiet_ViTriDeXe> lsViTriDeXe = MemoryCacheHelper.GetValue("lsViTriDeXe") as List<DonViChiTiet_ViTriDeXe>;
            if (lsViTriDeXe == null)
            {
                lsViTriDeXe = _Librepository.GetAllViTriDeXe(IdDonVi);
                MemoryCacheHelper.Add("lsViTriDeXe", lsViTriDeXe, DateTimeOffset.UtcNow.AddHours(12));
            }
            foreach (DonViChiTiet_ViTriDeXe item in lsViTriDeXe)
            {
                if (item.IdDonViChiTiet == IdDonVi && item.TenViTriDeXe == TenViTri)
                    return true;
            }
            return false;
        }
        public bool CheckViTriVeSinh(string TenViTri, int IdDonVi)
        {
            List<DonViChiTiet_ViTriVeSinh> lsViTriVeSinh = MemoryCacheHelper.GetValue("lsViTriVeSinh") as List<DonViChiTiet_ViTriVeSinh>;
            if (lsViTriVeSinh == null)
            {
                lsViTriVeSinh = _Librepository.GetAllViTriVeSinh(IdDonVi);
                MemoryCacheHelper.Add("lsViTriVeSinh", lsViTriVeSinh, DateTimeOffset.UtcNow.AddHours(12));
            }
            foreach (DonViChiTiet_ViTriVeSinh item in lsViTriVeSinh)
            {
                if (item.IdDonViChiTiet == IdDonVi && item.TenViTriVeSinh == TenViTri)
                    return true;
            }
            return false;
        }
        public string KiemTraNhanVienImport(int IdPhieu, string BoPhan, string DienThoai, string NgaySinh, string MaNhanVien, string Hoten, string LyDo, string ThoiGianLamViec
            , int SoMuiTiem, string NgayTestGanNhat, string Trong14Ngay_HoSot, string Trong14Ngay_TiepXuc, string Trong14Ngay_XungQuanhNha, string NguoiOCung
            , string ViTriVPLamViec, string PhanNhom, string ViTriLamViec, string ViTriDeXe, string ViTriVeSinh, string ViTriNhaAn
            , string T2, string T3, string T4, string T5, string T6, string T7)
        {
            try
            {
                if (MaNhanVien != null && MaNhanVien != "")
                {
                    ViTriLamViec = ViTriLamViec != null ? ViTriLamViec : "";
                    ViTriDeXe = ViTriDeXe != null ? ViTriDeXe : "";
                    ViTriVeSinh = ViTriVeSinh != null ? ViTriVeSinh : "";
                    ViTriNhaAn = ViTriNhaAn != null ? ViTriNhaAn : "";
                    ViTriVPLamViec = ViTriVPLamViec != null ? ViTriVPLamViec : "";
                    PhanNhom = PhanNhom != null ? PhanNhom : "";

                    LyDo = LyDo != null ? LyDo : "";
                    NgayTestGanNhat = NgayTestGanNhat != null ? NgayTestGanNhat : "";// Utils.Utils.GetTimeServer();
                    if (NgayTestGanNhat == "") return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột ngày test nhanh gần nhất. Chưa điền hoặc định dạng sai (trường text và theo định dạng dd/MM/yyyy)";
                    else
                    {
                        string[] ngaythangnam = NgayTestGanNhat.Split('/');
                        if (ngaythangnam.Length == 3)
                        {
                            if (Convert.ToInt32(ngaythangnam[2]) < 1000 || Convert.ToInt32(ngaythangnam[1]) > 12 || Convert.ToInt32(ngaythangnam[0]) > 31)
                                return "Mã nhân viên [" + MaNhanVien + "] bị lỗi tại cột ngày test nhanh gần nhất. Chưa điền hoặc định dạng sai (trường text và theo định dạng dd/MM/yyyy)";
                        }
                        else
                        {
                            return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột ngày test nhanh gần nhất. Chưa điền hoặc định dạng sai (trường text và theo định dạng dd/MM/yyyy)";
                        }
                    }
                    Trong14Ngay_HoSot = Trong14Ngay_HoSot != null ? Trong14Ngay_HoSot : "Không";
                    Trong14Ngay_TiepXuc = Trong14Ngay_TiepXuc != null ? Trong14Ngay_TiepXuc : "Không";
                    Trong14Ngay_XungQuanhNha = Trong14Ngay_XungQuanhNha != null ? Trong14Ngay_XungQuanhNha : "Không";
                    NguoiOCung = NguoiOCung != null ? NguoiOCung : "Không";

                    T2 = T2 != null ? T2 : "";
                    T3 = T3 != null ? T3 : "";
                    T4 = T4 != null ? T4 : "";
                    T5 = T5 != null ? T5 : "";
                    T6 = T6 != null ? T6 : "";
                    T7 = T7 != null ? T7 : "";
                    if (T2 == "" && T3 == "" && T4 == "" && T5 == "" && T6 == "" && T7 == "")
                        return "Mã nhân viên[" + MaNhanVien + "-" + Hoten + "] không ghi nhận được ngày đi làm, vui lòng tải lại form mẫu.";
                    Staff staff = new Staff();
                    staff = _Staffrepository.GetStaffByMaNS(MaNhanVien.Trim());
                    PhieuDangKyRaVaoCong phieu = new PhieuDangKyRaVaoCong();
                    phieu = phieuDangKyRaVaoCongRepository.GetPhieu(IdPhieu);
                    if (staff != null && staff.Id > 0)
                    {
                        if (staff.HoTen == Hoten.Trim() && staff.DienThoai == DienThoai.Trim())
                        {
                            int id_don_vi = phieu.IdDonVi;
                            DonViChiTiet_ViTriVPLamViec ViTriVPLamViecId = _Librepository.GetViTriVPLamViecByName(ViTriVPLamViec.Trim(), phieu.IdDonVi);
                            DonViChiTiet_PhanNhom PhanNhomId = _Librepository.GetPhanNhomByName(PhanNhom.Trim(), phieu.IdDonVi);
                            DonViChiTiet_ViTriDeXe ViTriDeXeId = _Librepository.GetViTriDeXeByName(ViTriDeXe.Trim(), phieu.IdDonVi);
                            DonViChiTiet_ViTriLamViec ViTriLamViecId = _Librepository.GetViTriLamViecByName(ViTriLamViec.Trim(), phieu.IdDonVi);
                            DonViChiTiet_ViTriVeSinh ViTriVeSinhId = _Librepository.GetViTriVeSinhByName(ViTriVeSinh.Trim(), phieu.IdDonVi);
                            ViTriNhaAn ViTriNhaAnId = _Librepository.GetViTriNhaAnByName(ViTriNhaAn.Trim());
                            if (ViTriVPLamViecId != null && ViTriVPLamViecId.id > 0 && PhanNhomId != null && PhanNhomId.id > 0 && ViTriDeXeId != null && ViTriDeXeId.id > 0
                                && ViTriLamViecId != null && ViTriLamViecId.id > 0 && ViTriVeSinhId != null && ViTriVeSinhId.id > 0 && ViTriNhaAnId != null && ViTriNhaAnId.id > 0)
                            //if(CheckViTriLamViec(ViTriLamViec.Trim(), id_don_vi) && CheckViTriVPLamViec(ViTriVPLamViec.Trim(), id_don_vi)
                            //    && CheckViTriPhanNhom(PhanNhom.Trim(), id_don_vi) && CheckViTriDeXe(ViTriDeXe.Trim(), id_don_vi)
                            //    && CheckViTriVeSinh(ViTriVeSinh.Trim(), id_don_vi) && CheckViTriNhaAn(ViTriNhaAn.Trim())
                            //    )
                            {
                                if (phieu.TenDonVi == staff.DonViChiTiet)
                                {
                                    if ((T2.Trim().ToUpper() == "" || T2.Trim().ToUpper() == "X") &&
                                        (T3.Trim().ToUpper() == "" || T3.Trim().ToUpper() == "X") &&
                                        (T4.Trim().ToUpper() == "" || T4.Trim().ToUpper() == "X") &&
                                        (T5.Trim().ToUpper() == "" || T5.Trim().ToUpper() == "X") &&
                                        (T6.Trim().ToUpper() == "" || T6.Trim().ToUpper() == "X") &&
                                        (T7.Trim().ToUpper() == "" || T7.Trim().ToUpper() == "X")
                                        )
                                    {
                                        if ((Trong14Ngay_HoSot.Trim().ToLower() == "có" || Trong14Ngay_HoSot.Trim().ToLower() == "không") &&
                                            (Trong14Ngay_TiepXuc.Trim().ToLower() == "có" || Trong14Ngay_TiepXuc.Trim().ToLower() == "không") &&
                                            (Trong14Ngay_XungQuanhNha.Trim().ToLower() == "có" || Trong14Ngay_XungQuanhNha.Trim().ToLower() == "không") &&
                                            (NguoiOCung.Trim().ToLower() == "có" || NguoiOCung.Trim().ToLower() == "không")
                                            )
                                        {
                                            return "";
                                        }
                                        else
                                        {
                                            if(Trong14Ngay_HoSot.Trim().ToLower() != "có" && Trong14Ngay_HoSot.Trim().ToLower() != "không")
                                                return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Trong vòng 14 ngày qua có sốt, ho, khó thở, viêm phổi, Đau họng, mệt mỏi]. Chỉ được đánh giá trị Có hoặc Không";
                                            else if (Trong14Ngay_TiepXuc.Trim().ToLower() != "có" && Trong14Ngay_TiepXuc.Trim().ToLower() != "không")
                                                return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Trong vòng 14 ngày qua có tiếp xúc với ca nghi nhiễm hoặc F0]. Chỉ được đánh giá trị Có hoặc Không";
                                            else if (Trong14Ngay_XungQuanhNha.Trim().ToLower() != "có" && Trong14Ngay_XungQuanhNha.Trim().ToLower() != "không")
                                                return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Trong vòng 14 ngày qua xung quanh nhà có ca nghi nhiễm hoặc F0]. Chỉ được đánh giá trị Có hoặc Không";
                                            else //(NguoiOCung.Trim().ToLower() != "có" && NguoiOCung.Trim().ToLower() != "không")
                                                return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Người nhà ở cùng có làm nơi nguy cao nhiễm Covid-19 cao]. Chỉ được đánh giá trị Có hoặc Không";
                                        }
                                    }
                                    else
                                    {
                                        return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại các cột thứ T2->T7. Chỉ được đánh giá trị X hoặc rỗng";
                                    }
                                }
                                else
                                {
                                    return "Đơn vị của nhân viên [" + MaNhanVien + "-" + Hoten + "] không trùng khớp. Đơn vị trên phiếu là [" + phieu.TenDonVi + "]";
                                }
                            }
                            else
                            {
                                if (ViTriVPLamViecId == null || ViTriVPLamViecId.id <=0)
                                    return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Vị trí VP làm việc]. Cần copy chính xác từ thư viện.";
                                else if (PhanNhomId == null || PhanNhomId.id <= 0)
                                    return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Phân nhóm]. Cần copy chính xác từ thư viện.";
                                else if (ViTriDeXeId == null || ViTriDeXeId.id <= 0)
                                    return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Vị trí để xe]. Cần copy chính xác từ thư viện.";
                                else if (ViTriLamViecId == null || ViTriLamViecId.id <= 0)
                                    return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Vị trí làm việc]. Cần copy chính xác từ thư viện.";
                                else if (ViTriVeSinhId == null || ViTriVeSinhId.id <= 0)
                                    return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Vị trí vệ sinh]. Cần copy chính xác từ thư viện.";
                                else //(ViTriNhaAnId == null || ViTriNhaAnId.id <= 0)
                                    return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] bị lỗi tại cột [Vị trí nhà ăn]. Cần copy chính xác từ thư viện.";
                            }
                        }
                        else
                        {
                            if(staff.HoTen != Hoten.Trim())
                                return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] không trùng tên, nên copy từ danh sách đi làm tuần qua excel cho chính xác!";
                            else //(staff.DienThoai != DienThoai.Trim())
                                return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] không trùng điện thoại, nếu có cập nhật điện thoại vui lòng báo LĐTL!";
                        }
                    }
                    else
                    {
                        return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] không tồn tại trên hệ thống.";
                    }
                }
                else
                {
                    if(Hoten!=null || Hoten!="")
                        return "Lỗi tên các cột, vui lòng tải lại form mẫu trên hệ thống.";
                    else return "";
                }
            }catch(Exception ex)
            {
                return "Mã nhân viên [" + MaNhanVien + "-" + Hoten + "] Lỗi dữ liệu "+ ex.ToString() +".";
            }
        }
    }
}