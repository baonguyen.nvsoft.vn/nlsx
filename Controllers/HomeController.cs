﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using dbChange.Models;
using dbChange.Repository;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using dbChange.Data;

namespace dbChange.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository _repository;
        private bool isLoged = false;



        public HomeController(IEmployeeRepository repository, IHttpContextAccessor httpContextAccessor)
        {
            _repository = repository; 
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
            }
        }
        public IActionResult Index()
        {
            if (isLoged)
            {
                ViewData["css"] = "~/wwwroot/Includes/Home/Home_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/Home/Home_js.cshtml";
                return View();
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }
        public IActionResult ThongKe()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetEmployees(){
            return Ok(_repository.GetAllEmployees());
        }
        [HttpGet]
        public IActionResult GetThongKes()
        {
            return Ok(_repository.GetThongKes());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public void DownloadExcel()
        {

            var collection = _repository.GetAllEmployees();

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
           
            ExcelRange range = Sheet.Cells[1, 1, 1, 7];
            range.Merge = true;
            Sheet.Cells["A1"].Style.Font.Bold = true;
            Sheet.Cells["A1"].Value = "DANH SÁCH NHÂN VIÊN XÉT NGHIỆM";
            Sheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            Sheet.Cells["A2"].Value = "Họ Tên";
            Sheet.Cells["B2"].Value = "Năm Sinh";
            Sheet.Cells["C2"].Value = "Giới Tính";
            Sheet.Cells["D2"].Value = "Địa Chỉ";
            Sheet.Cells["E2"].Value = "Điện Thoại";
            Sheet.Cells["F2"].Value = "CMND";
            Sheet.Cells["G2"].Value = "Đơn Vị";
            Sheet.Cells["H2"].Value = "Kết Quả";
            Sheet.Cells["A2"].Style.Fill.SetBackground(OfficeOpenXml.Drawing.eThemeSchemeColor.Accent1);
            Sheet.Cells["A2"].Style.Font.Bold = true;
            Sheet.Cells["B2"].Style.Fill.SetBackground(OfficeOpenXml.Drawing.eThemeSchemeColor.Accent1);
            Sheet.Cells["B2"].Style.Font.Bold = true;
            Sheet.Cells["C2"].Style.Fill.SetBackground(OfficeOpenXml.Drawing.eThemeSchemeColor.Accent1);
            Sheet.Cells["C2"].Style.Font.Bold = true;
            Sheet.Cells["D2"].Style.Fill.SetBackground(OfficeOpenXml.Drawing.eThemeSchemeColor.Accent1);
            Sheet.Cells["D2"].Style.Font.Bold = true;
            Sheet.Cells["E2"].Style.Fill.SetBackground(OfficeOpenXml.Drawing.eThemeSchemeColor.Accent1);
            Sheet.Cells["E2"].Style.Font.Bold = true;
            Sheet.Cells["F2"].Style.Fill.SetBackground(OfficeOpenXml.Drawing.eThemeSchemeColor.Accent1);
            Sheet.Cells["F2"].Style.Font.Bold = true;
            Sheet.Cells["G2"].Style.Fill.SetBackground(OfficeOpenXml.Drawing.eThemeSchemeColor.Accent1);
            Sheet.Cells["G2"].Style.Font.Bold = true;
            Sheet.Cells["H2"].Style.Fill.SetBackground(OfficeOpenXml.Drawing.eThemeSchemeColor.Accent1);
            Sheet.Cells["H2"].Style.Font.Bold = true;
            int row = 3;
            foreach (var item in collection)
            {

                Sheet.Cells[string.Format("A{0}", row)].Value = item.HoTen;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.NamSinh;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.GioiTinh;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.DiaChi;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.DienThoai;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.CMND;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.DonVi;
                Sheet.Cells[string.Format("H{0}", row)].Value = item.KetQuaXN;
                row++;
            }


            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Headers.Add("content-disposition", "attachment: filename=" + "Report.xlsx");
            Response.BodyWriter.WriteAsync(Ep.GetAsByteArray());
            Response.BodyWriter.Complete();
        }

      
        

        [HttpGet]
        public IActionResult Create()
        {
            Employee emp = new Employee();
            
            return PartialView("_addEmployeePartial", emp);
        }
        [HttpPost]
        public IActionResult Create(Employee emp)
        {
            _repository.AddEmployee(emp);
            return PartialView("_addEmployeePartial", emp);
        }

      

     
    }
}
