﻿using dbChange.Models;
using dbChange.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Controllers
{
    [Route("api/[controller]")]
    public class StatisticController : Controller
    {
        private readonly IStatisticVacxinRepository _repository;


        private readonly IWebHostEnvironment _env;

       
        public StatisticController(IStatisticVacxinRepository repository, IWebHostEnvironment env)
        {
            _repository = repository;
            _env = env;
        }
     

        [HttpGet]
        public IActionResult GetAllStatistics()
        {
            string token = "";
            if (HttpContext.Request.Cookies["token"] != null)
            {
                token = HttpContext.Request.Cookies["token"];
            }
            else if (HttpContext.Session.GetString("token") != null)
            {
                token = HttpContext.Session.GetString("token");

            }
            if (token != "")
            {
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);


                return Ok(_repository.GetAllStatistic(acc.TenDangNhap));
            }
            else
                return Ok();

        }
        [HttpPost]
        public IActionResult Put(int key, string values)
        {
            _repository.UpdateStatisticVacxin(key, values);
            return   Ok(); 
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
