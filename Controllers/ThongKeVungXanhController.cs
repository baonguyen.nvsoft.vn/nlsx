﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dbChange.Models;
using dbChange.Models.LIB;
using dbChange.Repository;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dbChange.Controllers
{
    public class ThongKeVungXanhController : Controller
    {
        private bool isLoged = false;
        private readonly IAccountRepository _repository;
        private readonly ILibRepository _Librepository;
        public ThongKeVungXanhController(IAccountRepository repository, ILibRepository Librepository, IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
            }
            _Librepository = Librepository;
            _repository = repository;
        }
        public IActionResult Index()
        {
            if (!isLoged)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            else
            {
                ViewData["css"] = "~/wwwroot/Includes/ThongKeVungXanh/Profile_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/ThongKeVungXanh/Profile_js.cshtml";

                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                ViewData["DonVi"] = _Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                return View();
            }
        }
        public JsonResult GetDonVi()
        {
            if (!isLoged)
            {
                return Json("");
            }
            else
            {
                ViewData["css"] = "~/wwwroot/Includes/ThongKeVungXanh/Profile_css.cshtml";
                ViewData["js"] = "~/wwwroot/Includes/ThongKeVungXanh/Profile_js.cshtml";

                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                return Json(_Librepository.GetAllDonViChiTietByUser(acc.TenDangNhap));
            }

        }

        public string GetAllBoPhanByDonViChiTietId(string DonViChiTiet)
        {
            string result = "";
            DM_BoPhanChiTiet dv = _Librepository.GetDonViChiTietByTen(DonViChiTiet);
            List<DonViChiTiet_BoPhan> ls = _Librepository.GetAllDonViChiTietBoPhanByDonViChiTietId(Convert.ToInt32(dv.Id));
            result += "<option value = 'All' >Tất cả</option> ";
            foreach (DonViChiTiet_BoPhan row in ls)
            {
                result += "<option value='" + row.Id + "'>" + row.TenBoPhan + "</option>";
            }
            return result;
        }
        public string GetAllBoPhanByDonViChiTietId2(int DonViChiTiet,int BoPhan)
        {
            string result = "";
            //DM_BoPhanChiTiet dv = _Librepository.GetDonViChiTietById(DonViChiTiet);
            List<DonViChiTiet_BoPhan> ls = _Librepository.GetAllDonViChiTietBoPhanByDonViChiTietId(Convert.ToInt32(DonViChiTiet));
            result += "<option value = 'All' >Tất cả</option> ";
            foreach (DonViChiTiet_BoPhan row in ls)
            {
                if(row.Id == BoPhan)
                    result += "<option selected value='" + row.Id + "'>" + row.TenBoPhan + "</option>";
                else
                    result += "<option value='" + row.Id + "'>" + row.TenBoPhan + "</option>";
            }
            return result;
        }
        public JsonResult GetDataVungCovidChart(int[] DonViChiTiet, string BoPhan)
        {
            if (isLoged)
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                Dictionary<string, int> lsStaff = new Dictionary<string, int>();
                lsStaff = _repository.GetDataVungCovidChart2(acc.TenDangNhap, DonViChiTiet, BoPhan);
                return Json(lsStaff);
            }
            else
            {
                return Json("");
            }
        }

        public JsonResult GetStaffsByDonViBoPhan(string DonViChiTiet, string BoPhan)
        {
            if (isLoged)
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                List<Staff> lsStaff = new List<Staff>();
                lsStaff = _repository.GetAllStaffByDonViBoPhan(acc.TenDangNhap, DonViChiTiet, BoPhan);
                return Json(lsStaff);
            }
            else
            {
                return Json("");
            }
        }
        public JsonResult GetStaffsByDonViBoPhan2(int[] DonViChiTiet, string BoPhan)
        {
            if (isLoged)
            {
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                List<Staff> lsStaff = new List<Staff>();
                lsStaff = _repository.GetAllStaffByDonViBoPhan2(acc.TenDangNhap, DonViChiTiet, BoPhan);
                return Json(lsStaff);
            }
            else
            {
                return Json("");
            }
        }
    }
}