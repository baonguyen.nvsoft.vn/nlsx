﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using dbChange.Data;
using dbChange.Models.LIB;
using dbChange.Repository;

namespace dbChange.Controllers
{
    public class DonViChiTiet_ViTriVPLamViecController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ILibRepository _Librepository;

        public DonViChiTiet_ViTriVPLamViecController(ApplicationDbContext context, ILibRepository Librepository)
        {
            _context = context;
            _Librepository = Librepository;
        }

        // GET: DonViChiTiet_ViTriVPLamViec
        public async Task<IActionResult> Index()
        {
            return View(await _context.DonViChiTiet_ViTriVPLamViec.ToListAsync());
        }

        // GET: DonViChiTiet_ViTriVPLamViec/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_ViTriVPLamViec = await _context.DonViChiTiet_ViTriVPLamViec
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_ViTriVPLamViec == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_ViTriVPLamViec);
        }

        // GET: DonViChiTiet_ViTriVPLamViec/Create
        public IActionResult Create()
        {
            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            return View();
        }

        // POST: DonViChiTiet_ViTriVPLamViec/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create([Bind("id,TenViTriVPLamViec,IdDonViChiTiet")] DonViChiTiet_ViTriVPLamViec donViChiTiet_ViTriVPLamViec)
        {
            if (ModelState.IsValid)
            {
                donViChiTiet_ViTriVPLamViec.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_ViTriVPLamViec.IdDonViChiTiet).TenDonVi;
                _context.Add(donViChiTiet_ViTriVPLamViec);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_ViTriVPLamViec);
        }

        // GET: DonViChiTiet_ViTriVPLamViec/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["lsDonVi"] = _Librepository.GetAllDonViChiTiet();
            var donViChiTiet_ViTriVPLamViec = await _context.DonViChiTiet_ViTriVPLamViec.FindAsync(id);
            if (donViChiTiet_ViTriVPLamViec == null)
            {
                return NotFound();
            }
            return View(donViChiTiet_ViTriVPLamViec);
        }

        // POST: DonViChiTiet_ViTriVPLamViec/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Edit(long id, [Bind("id,TenViTriVPLamViec,IdDonViChiTiet")] DonViChiTiet_ViTriVPLamViec donViChiTiet_ViTriVPLamViec)
        {
            if (id != donViChiTiet_ViTriVPLamViec.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    donViChiTiet_ViTriVPLamViec.TenDonViChiTiet = _Librepository.GetDonViChiTietById(donViChiTiet_ViTriVPLamViec.IdDonViChiTiet).TenDonVi;
                    _context.Update(donViChiTiet_ViTriVPLamViec);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DonViChiTiet_ViTriVPLamViecExists(donViChiTiet_ViTriVPLamViec.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(donViChiTiet_ViTriVPLamViec);
        }

        // GET: DonViChiTiet_ViTriVPLamViec/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donViChiTiet_ViTriVPLamViec = await _context.DonViChiTiet_ViTriVPLamViec
                .FirstOrDefaultAsync(m => m.id == id);
            if (donViChiTiet_ViTriVPLamViec == null)
            {
                return NotFound();
            }

            return View(donViChiTiet_ViTriVPLamViec);
        }

        // POST: DonViChiTiet_ViTriVPLamViec/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var donViChiTiet_ViTriVPLamViec = await _context.DonViChiTiet_ViTriVPLamViec.FindAsync(id);
            _context.DonViChiTiet_ViTriVPLamViec.Remove(donViChiTiet_ViTriVPLamViec);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DonViChiTiet_ViTriVPLamViecExists(long id)
        {
            return _context.DonViChiTiet_ViTriVPLamViec.Any(e => e.id == id);
        }
    }
}
