﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using dbChange.Data;
using dbChange.Models;
using dbChange.Repository;
using dbChange.Models.LIB;
using Microsoft.AspNetCore.Http;

namespace dbChange.Controllers
{
    public class StaffsAdminController : Controller
    {
        private bool isLoged = false;
        private readonly ApplicationDbContext _context;
        public ILibRepository _libRepository;
        long IdDonVi = -1;
        public StaffsAdminController(IHttpContextAccessor httpContextAccessor, ApplicationDbContext context, ILibRepository libRepository)
        {
            _context = context;
            _libRepository = libRepository;
            if (httpContextAccessor.HttpContext.Request.Query.Count > 0 && httpContextAccessor.HttpContext.Request.Query.ContainsKey("DonVi"))
            {
                IdDonVi = Convert.ToInt64(httpContextAccessor.HttpContext.Request.Query["DonVi"].ToString());
            }
            if (httpContextAccessor.HttpContext.Request.Cookies["token"] != null)
            {
                isLoged = true;
            }
            if (!string.IsNullOrEmpty(httpContextAccessor.HttpContext.Session.GetString("token")))
            {
                isLoged = true;
            }
        }

        // GET: StaffsAdmin
        public async Task<IActionResult> Index()
        {
            if (isLoged)
            {
                ViewData["lsBoPhan"] = _libRepository.GetAllBoPhan();
                string token = "";
                if (HttpContext.Request.Cookies["token"] != null)
                {
                    token = HttpContext.Request.Cookies["token"];
                }
                else if (HttpContext.Session.GetString("token") != null)
                {
                    token = HttpContext.Session.GetString("token");
                }
                string a = Utils.Utils.Decrypt(token);
                Account acc = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(a);
                List<DM_BoPhanChiTiet> lsBoPhan = _libRepository.GetAllDonViChiTietByUser(acc.TenDangNhap);
                ViewData["lsDonVi"] = lsBoPhan;
                List<string> lsSDV = new List<string>();
                if (IdDonVi == 0)
                {
                    foreach (DM_BoPhanChiTiet row in lsBoPhan)
                    {
                        lsSDV.Add(row.TenDonVi);
                    }
                }
                else if (IdDonVi > 0)
                {
                    foreach (DM_BoPhanChiTiet row in lsBoPhan)
                    {
                        if (row.Id == IdDonVi)
                        {
                            lsSDV.Add(row.TenDonVi);
                            break;
                        }
                    }
                }

                object x = await _context.Staff.Where(obj => lsSDV.Contains(obj.DonViChiTiet)).ToListAsync();
                return View(x);
            }
            else
            {
                return RedirectToAction(actionName: "Index", controllerName: "Account");
            }
        }

        // GET: StaffsAdmin/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff
                .FirstOrDefaultAsync(m => m.Id == id);
            if (staff == null)
            {
                return NotFound();
            }

            return View(staff);
        }

        // GET: StaffsAdmin/Create
        public IActionResult Create()
        {
            //ViewData["lsBoPhan"] = _libRepository.GetAllBoPhan();
            ViewData["lsDonVi"] = _libRepository.GetAllDonViChiTiet();
            ViewData["lsTinh"] = _libRepository.GetAllProvice();
            //ViewData["lsQuan"] = _libRepository.GetAllDistrict();
            //ViewData["lsPhuong"] = _libRepository.GetAllWard();
            ViewData["lsDiaDiemTiem"] = _libRepository.GetAll_PL4_DiaDiemTiem();
           
            ViewData["js"] = "~/wwwroot/Includes/StaffsAdmin/Create_js.cshtml";
            return View();
        }

        // POST: StaffsAdmin/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create([Bind("Id,HoTen,GioiTinh,MaNhom,DonVi,DienThoai,CMND,BHYT,TenTinh,MaTinh,TenQuan,MaQuan,TenPhuong,MaPhuong,DiaChi,Mui1_TenVacxin,Mui1_NgayTiem,Mui1_SoLo,Mui1_DiaDiem,Mui2_TenVacxin,Mui2_NgayTiem,Mui2_SoLo,Mui2_DiaDiem,GhiChu,KetQuaImPort,DonViChiTiet,NgayCapNhat,NguoiCapNhat,UrlHinhGiayXN,PhanNhom,TinhTrang,BiF0,NgayXuatVien,NgayS,ThangS,NamS,ISok,Ma_ToKhuPho,Ten_ToKhuPho,VungCovid,BoPhanId,TenBoPhan,IdWeb,LinkWeb,CCCD,MaQR,MaNS,SSBH,LoaiLD,LinkImage,TrangThaiNhanSu")] Staff staff)
        {
            
            _context.Add(staff);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: StaffsAdmin/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff.FindAsync(id);
            if (staff == null)
            {
                return NotFound();
            }
            ViewData["lsBoPhan"] = _libRepository.GetAllBoPhan();
            ViewData["lsDonVi"] = _libRepository.GetAllDonViChiTiet();
            ViewData["lsTinh"] = _libRepository.GetAllProvice();
            ViewData["lsQuan"] = _libRepository.GetAllDistrict();
            ViewData["lsPhuong"] = _libRepository.GetAllWard();
            ViewData["lsDiaDiemTiem"] = _libRepository.GetAll_PL4_DiaDiemTiem();
            ViewData["ToKhuPho"] = _libRepository.GetAllToKhuPhoByWard(staff.MaPhuong);

            ViewData["js"] = "~/wwwroot/Includes/StaffsAdmin/Edit_js.cshtml";
            return View(staff);
        }

        // POST: StaffsAdmin/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Edit(int id, [Bind("Id,HoTen,GioiTinh,MaNhom,DonVi,DienThoai,CMND,BHYT,TenTinh,MaTinh,TenQuan,MaQuan,TenPhuong,MaPhuong,DiaChi,Mui1_TenVacxin,Mui1_NgayTiem,Mui1_SoLo,Mui1_DiaDiem,Mui2_TenVacxin,Mui2_NgayTiem,Mui2_SoLo,Mui2_DiaDiem,GhiChu,KetQuaImPort,DonViChiTiet,NgayCapNhat,NguoiCapNhat,UrlHinhGiayXN,PhanNhom,TinhTrang,BiF0,NgayXuatVien,NgayS,ThangS,NamS,ISok,Ma_ToKhuPho,Ten_ToKhuPho,VungCovid,BoPhanId,TenBoPhan,IdWeb,LinkWeb,CCCD,MaQR,MaNS,SSBH,LoaiLD,LinkImage,TrangThaiNhanSu")] Staff staff)
        {
            if (id != staff.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(staff);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StaffExists(staff.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(staff);
        }

        // GET: StaffsAdmin/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff
                .FirstOrDefaultAsync(m => m.Id == id);
            if (staff == null)
            {
                return NotFound();
            }

            return View(staff);
        }

        // POST: StaffsAdmin/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var staff = await _context.Staff.FindAsync(id);
            _context.Staff.Remove(staff);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StaffExists(int id)
        {
            return _context.Staff.Any(e => e.Id == id);
        }
    }
}
