﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace dbChange.Controllers
{
    public class GoogleDriveAPIHelper : Controller
    {
        private readonly IWebHostEnvironment _env;
        // granted full control of google drive api 
        private string[] Scopes = { DriveService.Scope.Drive };
        public GoogleDriveAPIHelper( IWebHostEnvironment env)
        {
           
            _env = env;
        }
        public IActionResult Index()
        {  
            return View();
        }
        private DriveService GetDriveServiceInstance()
        {
            UserCredential credential;
           
            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
               
               string credPath = Path.Combine(Path.GetTempPath(), "token");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "IntranetNBC",
            });

            return service;
        }
        public void UploadFIle(string path)
        {

            var fileMetaData = new Google.Apis.Drive.v3.Data.File
            {
                Name = Path.GetFileName(path),
                MimeType = MimeTypes.GetMimeType(path)
            };

            try
            {
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    //var request = GetDriveServiceInstance().Files.Create(fileMetaData, stream, fileMetaData.MimeType);
                    //request.Fields = "id";
                    //request.Upload();
                    Response.WriteAsync(GetDriveServiceInstance().Files.Get("1dYB1byvzNUBmxoK1LHqg86gsdKjbDUup").OauthToken);
                }
                Response.WriteAsync("Upload success");
            }
            catch(Exception ex)
            {
                Response.WriteAsync(ex.Message);
            }
          
            //File.Delete(path);
        }
        public void DownloadFile(string blobId, string savePath,string webroot)
        {
            var service = GetDriveServiceInstance();
            var request = service.Files.Get(blobId);
            var stream = new MemoryStream();
            // Add a handler which will be notified on progress changes.
            // It will notify on each chunk download and when the
            // download is completed or failed.
            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                switch (progress.Status)
                {
                    case Google.Apis.Download.DownloadStatus.Downloading:
                        {
                            Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case Google.Apis.Download.DownloadStatus.Completed:
                        {
                            Console.WriteLine("Download complete.");
                            SaveStream(stream, savePath);
                            break;
                        }
                    case Google.Apis.Download.DownloadStatus.Failed:
                        {
                            Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.Download(stream);
        }

        private static void SaveStream(MemoryStream stream, string saveTo)
        {
            using (FileStream file = new FileStream(saveTo, FileMode.Create, FileAccess.Write))
            {
                stream.WriteTo(file);
            }
        }
    }
}
