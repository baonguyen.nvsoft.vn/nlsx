﻿using System;
using System.ComponentModel.DataAnnotations;

namespace dbChange.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string TenDangNhap { get; set; }
        public string MatKhau { get; set; }
        public string HoTen { get; set; }
        public string DonViChiTiet { get; set; }
        public string MaNhanVien { get; set; }
        [Required(ErrorMessage = "Tên đăng nhập và mật khẩu không đúng")]
        public string Email { get; set; }
        public bool isNBC { get; set; }
        public bool isYTeDuyet { get; set; }
        public bool isDuyet { get; set; }
        public bool isTaoPhieu { get; set; }
        public bool isTruongPhong { get; set; }
        public bool isScan { get; set; }
        public bool? isAdmin { get; set; }
        public bool isVPS{ get; set; }
        

    }
}