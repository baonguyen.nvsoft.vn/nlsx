﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models
{
    public class KetQuaCheckIn
    {
        public string Status;
        public List<ThongTinQuetQR> Data;
        public string Color;
        public string Message;
        public string Link;
    }
}
