﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models
{
    public class PhieuDangKyRaVaoCong_ChiTiet_Ngay
    {
        public int Id { set; get; }
        public int IdChiTiet { set; get; }
        public DateTime Ngay{ set; get; }
    }
}
