using System;

namespace dbChange.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string HoTen { get; set; }
        public string NamSinh { get; set; }
        public string GioiTinh { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public string CMND { get; set; }
        public string NgayCapNhat { get; set; }
        public string KetQuaXN { get; set; }
        public string DonVi { get; set; }
        public string PhongBan { get; set; }
        public string Barcode { get; set; }
        public bool Checkout { get; set; }
    }
}