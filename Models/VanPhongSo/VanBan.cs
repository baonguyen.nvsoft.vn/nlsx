﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models.VanPhongSo
{
    public class VanBan
    {
        public long Id { get; set; }
        public string MaVanBan { get; set; }
        public string MaQuyenVanBan { get; set; }
        public string TrangThai { get; set; }
        public string MaQR { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public string GhiChu { get; set; }
        public long IdDonVi { get; set; }
        public string TenDonVi { get; set; }
        public long IdNguoiTao { get; set; }
        public string TenNguoiTao { get; set; }
        public string Link { get; set; }
        public DateTime NgayTao { get; set; }
        public long IdLoaiVanban { get; set; }
        public string TenLoaiVanban { get; set; }
    }
}
