﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models.VanPhongSo
{
    public class VanBan_Quyen
    {
        public long Id { get; set; }
        public string MaQuyenVanBan { get; set; }
        public string TenQuyenVanBan { get; set; }
        public bool IsActive { get; set; }
    }
}
