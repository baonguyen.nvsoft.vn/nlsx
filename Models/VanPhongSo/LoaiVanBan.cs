﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models.VanPhongSo
{
    public class LoaiVanBan
    {
        public long Id { get; set; }
        public string MaLoaiVanBan { get; set; }
        public string TenLoaiVanBan { get; set; }
        public long IdPhongBan { get; set; }
    }
}
