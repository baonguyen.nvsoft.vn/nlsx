﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models.VanPhongSo
{
    public class VanBan_ChiTiet
    {
        public long Id { get; set; }
        public string MaVanBan { get; set; }
        public string MaQuyenVanBan { get; set; }
        public string MaQR { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public string GhiChu { get; set; }
        public string Link { get; set; }
        public long IdNguoiTao { get; set; }
        public string TenNguoiTao { get; set; }
        public string LinkShare { get; set; }
        public DateTime NgayTao { get; set; }
    }
}
