﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models
{
    public class StatisticVacxin
    {
        public int Id { get; set; }
        public string DonVi { get; set; }
        public string DonViChiTiet { get; set; }
        public string HoTen { get; set; }
        public string GioiTinh { get; set; }
        public string NgaySinh { get; set; }
        public string Mui1_DaTiem { get; set; }
        public string Mui1_ChuaTiem { get; set; }
        public string Mui1_LyDo { get; set; }
        public string Mui2_DaTiem { get; set; }
        public string Mui2_ChuaTiem { get; set; }
        public string Mui2_LyDo { get; set; }
        public string F0 { get; set; }
        public string SoDienThoai { get; set; }
        public string GhiChu { get; set; }
        public string NgayBoSung { get; set; }
    }
}
