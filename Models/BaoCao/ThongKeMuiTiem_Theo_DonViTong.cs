﻿using System;
using System.ComponentModel.DataAnnotations;

namespace dbChange.Models.BaoCao
{
    public class ThongKeMuiTiem_Theo_DonViTong
    {
        public long Id { get; set; }
        public long ParentId { get; set; }
        public int STT { get; set; }
        public string DonViChiTiet { get; set; }

        public string DonViNhom { get; set; }

        public string NhomHienThi { get; set; }

        public string ChiTietHienThi { get; set; }

        public int Mui1_DaTiem_SL { get; set; }

        public int Mui1_ChuaTiem_SL { get; set; }

        public int Mui2_DaTiem_SL { get; set; }

        public int Mui2_ChuaTiem_SL { get; set; }

        public int SoLuongF0 { get; set; }

        public int TongSoLaoDong { get; set; }

        public string Mui1_KeHoachTiemTiepTheo { get; set; }

        public string Mui2_KeHoachTiemTiepTheo { get; set; }

        public string GhiChu { get; set; }

        public double TiLe_Mui1_DaTiem_SL { get; set; }

        public double TiLe_Mui1_ChuaTiem_SL { get; set; }

        public double TiLe_Mui2_DaTiem_SL { get; set; }

        public double TiLe_Mui2_ChuaTiem_SL { get; set; }

        public double TiLe_SoLuongF0 { get; set; }

        public int IDMau { get; set; }

        //FO
        public string Mui1_DaTiem_SL_F0 { get; set; }

        public string Mui1_ChuaTiem_SL_F0 { get; set; }

        public string Mui2_DaTiem_SL_F0 { get; set; }

        public string Mui2_ChuaTiem_SL_F0 { get; set; }

        public string TongSoLaoDong_F0 { get; set; }

        public string TiLe_Mui1_DaTiem_SL_F0 { get; set; }

        public string TiLe_Mui1_ChuaTiem_SL_F0 { get; set; }

        public string TiLe_Mui2_DaTiem_SL_F0 { get; set; }

        public string TiLe_Mui2_ChuaTiem_SL_F0 { get; set; }
    }
}