﻿using System;
using System.ComponentModel.DataAnnotations;

namespace dbChange.Models.BaoCao
{
    public class ThongKeTiemChungTheoTaiKhoan
    {        
        public string DonViChiTiet { get; set; }

        public int Mui1_DaTiem { get; set; }

        public int F0_Mui1_DaTiem { get; set; }

        public int Mui1_ChuaTiem { get; set; }

        public int Mui2_DaTiem { get; set; }

        public int F0_Mui2_DaTiem { get; set; }

        public int Mui2_ChuaTiem { get; set; }

        public int F0 { get; set; }

        public int Tong_NguoiTheoDonVi { get; set; }

        public double TiLe_Mui1_DaTiem { get; set; }

        public double TiLe_F0_Mui1_DaTiem { get; set; }

        public double TiLe_Mui1_ChuaTiem { get; set; }

        public double TiLe_Mui2_DaTiem { get; set; }

        public double TiLe_F0_Mui2_DaTiem { get; set; }

        public double TiLe_Mui2_ChuaTiem { get; set; }

        public double TiLe_F0 { get; set; }
    }
}