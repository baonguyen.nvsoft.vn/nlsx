﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models
{
    public class ThongKe
    {
        public int stt { get; set; }
        public string donvi { get; set; }
        public int dangky { get; set; }
        public int checkin { get; set; }
        public int mui1 { get; set; }
        public int mui2 { get; set; }
        public int chuatiem { get; set; }
        public string tile { get; set; }
    }
}
