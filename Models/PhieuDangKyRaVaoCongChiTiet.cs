﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models
{
    public class PhieuDangKyRaVaoCongChiTiet
    {
        public int Id { set; get; }
        public string MaQRStaff { set; get; }
        public string LyDo { set; get; }

        public int IdNhanVienYTeDuyet { set; get; }
        public string TenNhanVienYTeDuyet { set; get; }
        public bool YTeDuyet { set; get; }
        public DateTime NgayNhanVienYTeDuyet { set; get; }

        public int IdNguoiDuyet { set; get; }
        public string TenNguoiDuyet { set; get; }
        public bool Duyet { set; get; }
        public DateTime NgayDuyet { set; get; }

        public int IdPhieuDangKy { set; get; }

    }
}
