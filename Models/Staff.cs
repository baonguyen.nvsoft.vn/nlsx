﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models
{
    public class Staff
    {
        public int Id { get; set; }
        public string HoTen { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string NgaySinh { get; set; }
        public string GioiTinh { get; set; }
        public string MaNhom { get; set; }
        public string DonVi { get; set; }       
        public string DienThoai { get; set; }
        public string CMND { get; set; }
        public string BHYT { get; set; }
        public string TenTinh { get; set; }
        public string MaTinh { get; set; }
        public string TenQuan { get; set; }
        public string MaQuan { get; set; }
        public string TenPhuong { get; set; }
        public string MaPhuong { get; set; }
        public string DiaChi { get; set; }
        public string Mui1_TenVacxin { get; set; }
        public string Mui1_NgayTiem { get; set; }
        public string Mui1_SoLo { get; set; }
        public string Mui1_DiaDiem { get; set; }
        public string Mui2_TenVacxin { get; set; }
        public string Mui2_NgayTiem { get; set; }
        public string Mui2_SoLo { get; set; }
        public string Mui2_DiaDiem { get; set; }
        public string GhiChu { get; set; }
        public string KetQuaImPort { get; set; }
        public string DonViChiTiet { get; set; }
        public string NgayCapNhat { get; set; }
        public string NguoiCapNhat { get; set; }
        public string UrlHinhGiayXN { get; set; }
        public string PhanNhom { get; set; }
        public string TinhTrang { get; set; }
        public bool? BiF0 { get; set; }
        public string NgayXuatVien { get; set; }
        public int NgayS { get; set; }
        public int ThangS { get; set; }
        public int NamS { get; set; }
        public bool? ISok { get; set; }
        public string Ma_ToKhuPho { get; set; }
        public string Ten_ToKhuPho { get; set; }
        public string VungCovid { get; set; }
        public int? BoPhanId { get; set; }
        public string TenBoPhan { get; set; }
        public string IdWeb { get; set; }
        public string LinkWeb { get; set; }
        public string CCCD { get; set; }
        public string MaQR { get; set; }
        public string MaNS { get; set; }
        public string SSBH { get; set; }
        public string LoaiLD { get; set; }
        public string LinkImage { get; set; }
        public string TrangThaiNhanSu { get; set; }

       
    }
}
