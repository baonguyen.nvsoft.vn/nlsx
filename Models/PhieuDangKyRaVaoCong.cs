﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models
{
    public class PhieuDangKyRaVaoCong
    {
        public int Id { set; get; }
        public DateTime NgayDangKyTuNgay { set; get; }
        public DateTime NgayDangKyDenNgay { set; get; }
        public int NguoiTaoId { set; get; }
        public string TenNguoiTao { set; get; }
        public DateTime NgayTao { set; get; }
        public string TinhTrang { set; get; }
        public string GhiChu { set; get; }
        public int IdDonVi { set; get; }
        public string TenDonVi { set; get; }
        public bool IsBoSung { set; get; }
    }
}
