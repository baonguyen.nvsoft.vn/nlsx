﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models
{
    public class ThongTinQuetQR
    {
        public long Id;
        public string MaQR;
        public DateTime GioQuet;
        public string HoTen;
        public int NamSinh;
        public string DonViChiTiet;
        public int DonViChiTietId;
        public string BoPhan;
        public int BoPhanId;
        public string LinkTKYT;
        public string TrangThai;
        public string Color;
        public string NguoiQuet;

    }
}
