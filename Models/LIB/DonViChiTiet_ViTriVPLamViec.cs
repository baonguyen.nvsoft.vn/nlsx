﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models.LIB
{
    public class DonViChiTiet_ViTriVPLamViec
    {
        public long id { get; set; }

        public string TenViTriVPLamViec { get; set; }

        public int IdDonViChiTiet { get; set; }
        public string TenDonViChiTiet { get; set; }
    }
}
