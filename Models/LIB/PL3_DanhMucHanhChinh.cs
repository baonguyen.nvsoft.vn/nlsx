﻿using System;
using System.ComponentModel.DataAnnotations;

namespace dbChange.Models.LIB
{
    public class PL3_DanhMucHanhChinh
    {        
        public long ID { get; set; }

        public string Ma_TinhThanhPho { get; set; }

        public string Ten_TinhThanhPho { get; set; }

        public string Ma_QuanHuyen { get; set; }

        public string Ten_QuanHuyen { get; set; }

        public string Ma_PhuongXa { get; set; }

        public string Ten_PhuongXa { get; set; }
    }
}