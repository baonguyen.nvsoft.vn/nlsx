﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models.LIB
{
    public class DM_BoPhanChiTiet
    {
        public long Id { get; set; }

        public string MaDonVi { get; set; }

        public string TenDonVi { get; set; }

        public int DonViTongId { get; set; }

        public string DonViNhom { get; set; }
        public int STT_BangThongKeTiemChung { get; set; }
    }
}
