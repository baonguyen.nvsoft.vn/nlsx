﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models.LIB
{
    public class DonViChiTiet_BoPhan
    {
        public long Id { get; set; }

        public string MaBoPhan { get; set; }

        public string TenBoPhan { get; set; }

        public int DonViChiTietId { get; set; }
    }
}
