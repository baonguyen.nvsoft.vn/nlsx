﻿using System;
using System.ComponentModel.DataAnnotations;

namespace dbChange.Models.LIB
{
    public class PL4_DiaDiemTiem
    {        
        public long ID { get; set; }

        public string Ma_CoSo { get; set; }

        public string Ten_CoSo { get; set; }

        public string XaPhuong { get; set; }

        public string QuanHuyen { get; set; }

        public string TinhThanhPho { get; set; }
    }
}