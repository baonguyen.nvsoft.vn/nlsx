﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models.LIB
{
    public class DonViChiTiet_ViTriVeSinh
    {
        public long id { get; set; }

        public string TenViTriVeSinh { get; set; }

        public int IdDonViChiTiet { get; set; }
        public string TenDonViChiTiet { get; set; }
    }
}
