﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dbChange.Models.LIB
{
    public class DM_ToKhuPho
    {
        public long Id { get; set; }

        public string Ma_ToKhuPho { get; set; }

        public string Ten_ToKhuPho { get; set; }

        public string path_ToKhuPho { get; set; }

        public string Ma_Phuong { get; set; }
        public string MaKhuPho { get; set; }
        public string TenKhuPho { get; set; }
        public string IdWeb { get; set; }
        public string MoTa { get; set; }
    }
}
