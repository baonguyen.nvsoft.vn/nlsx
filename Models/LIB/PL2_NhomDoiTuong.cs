﻿using System;
using System.ComponentModel.DataAnnotations;

namespace dbChange.Models.LIB
{
    public class PL2_NhomDoiTuong
    {        
        public long ID { get; set; }

        public int Nhom { get; set; }

        public string NgheNghiep { get; set; }
    }
}