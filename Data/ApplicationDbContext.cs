﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using dbChange.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using dbChange.Models.LIB;

namespace dbChange.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<dbChange.Models.Staff> Staff { get; set; }

        public DbSet<dbChange.Models.PhieuDangKyRaVaoCong> PhieuDangKyRaVaoCong { get; set; }

        public DbSet<dbChange.Models.LIB.ViTriNhaAn> ViTriNhaAn { get; set; }

        public DbSet<dbChange.Models.LIB.DonViChiTiet_PhanNhom> DonViChiTiet_PhanNhom { get; set; }

        public DbSet<dbChange.Models.LIB.DonViChiTiet_ViTriDeXe> DonViChiTiet_ViTriDeXe { get; set; }

        public DbSet<dbChange.Models.LIB.DonViChiTiet_ViTriLamViec> DonViChiTiet_ViTriLamViec { get; set; }

        public DbSet<dbChange.Models.LIB.DonViChiTiet_ViTriVeSinh> DonViChiTiet_ViTriVeSinh { get; set; }

        public DbSet<dbChange.Models.LIB.DonViChiTiet_ViTriVPLamViec> DonViChiTiet_ViTriVPLamViec { get; set; }

        public DbSet<dbChange.Models.Account> Account { get; set; }
        public DbSet<dbChange.Models.VanPhongSo.VanBan> VanBan { get; set; }
    }
}
