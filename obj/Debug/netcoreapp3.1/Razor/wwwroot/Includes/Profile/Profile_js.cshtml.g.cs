#pragma checksum "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\Profile\Profile_js.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7b47dadd0b0b0ab2f0e473105c366045d2dbec9b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.wwwroot_Includes_Profile_Profile_js), @"mvc.1.0.view", @"/wwwroot/Includes/Profile/Profile_js.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7b47dadd0b0b0ab2f0e473105c366045d2dbec9b", @"/wwwroot/Includes/Profile/Profile_js.cshtml")]
    public class wwwroot_Includes_Profile_Profile_js : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("text/javascript"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/plugins/jquery-inputmask/jquery.input-ip-address-control-1.0.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/sweetalert.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<!-- <script src=\"~/assets/plugins/jquery-validation/jquery.validate.min.js\"></script>\r\n<script src=\"~/assets/plugins/jquery-validation/additional-methods.min.js\"></script> -->\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7b47dadd0b0b0ab2f0e473105c366045d2dbec9b4332", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7b47dadd0b0b0ab2f0e473105c366045d2dbec9b5454", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7b47dadd0b0b0ab2f0e473105c366045d2dbec9b6576", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
<script type=""text/javascript"">
    $(""#NgaySinh"").inputmask(""d/m/y"", {
        placeholder: ""__/__/____""
    });
    $(""#Mui1_NgayTiem"").inputmask(""d/m/y"", {
        placeholder: ""__/__/____""
    });
    $(""#Mui2_NgayTiem"").inputmask(""d/m/y"", {
        placeholder: ""__/__/____""
    });
</script>

<script type=""text/javascript"">
    /*------------------Form Validation-----------------*/
    /*Example starter JavaScript for disabling form submissions if there are invalid fields*/
    window.addEventListener('load', function () {
        /* Thông tin tài khoản */
        var form_account = document.getElementsByClassName('form_account_validation');
        var validation = Array.prototype.filter.call(form_account, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add");
            WriteLiteral(@"('was-validated');
            }, false);
        });

        /* Thông tin nhân viên */
        var form_employee = document.getElementsByClassName('form_employee_validation');
        var validation = Array.prototype.filter.call(form_employee, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });

        /* Thông tin địa chỉ */
        var form_address = document.getElementsByClassName('form_address_validation');
        var validation = Array.prototype.filter.call(form_address, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
              ");
            WriteLiteral(@"  }
                form.classList.add('was-validated');
            }, false);
        });

        /* Mũi 1 */
        var form_mui_1 = document.getElementsByClassName('form_mui_1_validation');
        var validation = Array.prototype.filter.call(form_mui_1, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });

        /* Mũi 2*/
        var form_mui_2 = document.getElementsByClassName('form_mui_2_validation');
        var validation = Array.prototype.filter.call(form_mui_2, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }");
            WriteLiteral(@"
                form.classList.add('was-validated');
            }, false);
        });

    }, false);
</script>
<script>
    $('.btnCapNhatThongTinTaiKhoan').click(function () {
        var Verify = $('#Verify').val();
        var TenDangNhap = $('#TenDangNhap').val();
        var MatKhauCu = $('#MatKhauCu').val();
        var MatKhauMoi = $('#MatKhauMoi').val();
        console.log(TenDangNhap);
        $.ajax({
            type: ""POST"",
            url: '");
#nullable restore
#line 93 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\Profile\Profile_js.cshtml"
             Write(Url.Action("UpdatePasswordInfo", "Account"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
            data: { Verify: Verify, MatKhauCu: MatKhauCu, MatKhauMoi: MatKhauMoi},
            dataType: 'text',
            success: function (data) {
                console.log(data);
                if (data == """") {
                    swal({
                        title: ""Thành công"",
                        text: ""Đã cập nhật thông tin thành công! Bạn có muốn thoát ra đăng nhập lại?"",
                        type: 'success',
                        confirmButtonColor: ""#DD6B55"",
                        confirmButtonText: ""Đồng ý"",
                        showCancelButton: true,
                        cancelButtonText: ""Không"",
                    },
                       function (isConfirm) {
                            if (isConfirm) {
                                window.location.href = '");
#nullable restore
#line 110 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\Profile\Profile_js.cshtml"
                                                   Write(Url.Action("Logout","Account"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"';
                            }
                        }
                    );
                } else {
                    swal({
                        title: ""Lỗi cập nhật"",
                        text: data,
                        type: 'error',
                        showCancelButtonClass: 'OK'
                    });
                }
            },
            error: function (e) {
                swal({
                    title: ""Lỗi cập nhật"",
                    text: data,
                    type: 'error',
                    showCancelButtonClass: 'OK'
                });
            }
        });
    });

    function ChangeFile(evt) {
        console.log($('#file')[0].files[0]);
        var tgt = evt.target || window.event.srcElement,
            files = tgt.files;

        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {
                document.getElementById('ImageHinhGiayXN').src =");
            WriteLiteral(@" fr.result;
                console.log(fr.result);
            }
            fr.readAsDataURL(files[0]);
        }
        else {
            console.log(""error"");
        }
    }

    $(document).ready(function () {
        $(""#v-pills-tabContent"").delegate('.CapNhatThongTinNhanVien', 'click', function () {
            var Verify = $('#Verify').val();
            console.log(Verify);
            var Id = $('#Id').val();
            var HoTen = $('#HoTen').val();
            var NgaySinh = $('#NgaySinh').val();
            var GioiTinh = $('#GioiTinh').val();
            var MaNhom = $('#MaNhom').val();
            var DonVi = $('#DonVi').val();
            var DienThoai = $('#DienThoai').val();
            var CMND = $('#CMND').val();
            var BHYT = $('#BHYT').val();
            console.log(TenDangNhap);
            $.ajax({
                type: ""POST"",
                url: '");
#nullable restore
#line 168 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\Profile\Profile_js.cshtml"
                 Write(Url.Action("UpdateInfoStaff", "Account"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
                data: { Verify: Verify,Id: Id, HoTen: HoTen, NgaySinh: NgaySinh, GioiTinh: GioiTinh, MaNhom: MaNhom, DonVi: DonVi, DienThoai: DienThoai, CMND: CMND, BHYT: BHYT},
                dataType: 'text',
                success: function (data) {
                    console.log(data);
                    if (data == """") {
                        swal({
                            title: ""Thành công"",
                            text: ""Đã cập nhật thông tin thành công!"",
                            type: 'success',
                            showCancelButtonClass: 'OK'
                        });
                    } else {
                        swal({
                            title: ""Lỗi cập nhật"",
                            text: data,
                            type: 'error',
                            showCancelButtonClass: 'OK'
                        });
                    }
                },
                error: function (e) {
                    swal({
  ");
            WriteLiteral(@"                      title: ""Lỗi cập nhật"",
                        text: data,
                        type: 'error',
                        showCancelButtonClass: 'OK'
                    });
                }
            });
        });
        $(""#v-pills-tabContent"").delegate('.btnCapNhatDiaChiNhanVien', 'click', function () {
            var Verify = $('#Verify').val();
            var TenTinh = $('#TenTinh').val();
            var MaTinh = $('#MaTinh').val();
            var TenQuan = $('#TenQuan').val();
            var MaQuan = $('#MaQuan').val();
            var TenPhuong = $('#TenPhuong').val();
            var MaPhuong = $('#MaPhuong').val();
            var DiaChi = $('#DiaChi').val();
            $.ajax({
                type: ""POST"",
                url: '");
#nullable restore
#line 210 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\Profile\Profile_js.cshtml"
                 Write(Url.Action("UpdateAddressStaff", "Account"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
                data: { Verify: Verify, TenTinh: TenTinh, MaTinh: MaTinh, TenQuan: TenQuan, MaQuan: MaQuan, TenPhuong: TenPhuong, MaPhuong: MaPhuong, DiaChi: DiaChi},
                dataType: 'text',
                success: function (data) {
                    console.log(data);
                    if (data == """") {
                        swal({
                            title: ""Thành công"",
                            text: ""Đã cập nhật thông tin thành công!"",
                            type: 'success',
                            showCancelButtonClass: 'OK'
                        });
                    } else {
                        swal({
                            title: ""Lỗi cập nhật"",
                            text: data,
                            type: 'error',
                            showCancelButtonClass: 'OK'
                        });
                    }
                },
                error: function (e) {
                    swal({
             ");
            WriteLiteral(@"           title: ""Lỗi cập nhật"",
                        text: data,
                        type: 'error',
                        showCancelButtonClass: 'OK'
                    });
                }
            });
        });
        $(""#v-pills-tabContent"").delegate('.btnCapNhatThongTinMuiMot', 'click', function () {
            var Verify = $('#Verify').val();
            var Mui1_TenVacxin = $('#Mui1_TenVacxin').val();
            var Mui1_NgayTiem = $('#Mui1_NgayTiem').val();
            var Mui1_SoLo = $('#Mui1_SoLo').val();
            var Mui1_DiaDiem = $('#Mui1_DiaDiem').val();
            $.ajax({
                type: ""POST"",
                url: '");
#nullable restore
#line 249 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\Profile\Profile_js.cshtml"
                 Write(Url.Action("UpdateMui1Staff", "Account"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
                data: { Verify: Verify, Mui1_TenVacxin: Mui1_TenVacxin, Mui1_NgayTiem: Mui1_NgayTiem, Mui1_SoLo: Mui1_SoLo, Mui1_DiaDiem: Mui1_DiaDiem},
                dataType: 'text',
                success: function (data) {
                    console.log(data);
                    if (data == """") {
                        swal({
                            title: ""Thành công"",
                            text: ""Đã cập nhật thông tin thành công!"",
                            type: 'success',
                            showCancelButtonClass: 'OK'
                        });
                    } else {
                        swal({
                            title: ""Lỗi cập nhật"",
                            text: data,
                            type: 'error',
                            showCancelButtonClass: 'OK'
                        });
                    }
                },
                error: function (e) {
                    swal({
                        tit");
            WriteLiteral(@"le: ""Lỗi cập nhật"",
                        text: data,
                        type: 'error',
                        showCancelButtonClass: 'OK'
                    });
                }
            });
        });
        $(""#v-pills-tabContent"").delegate('.btnCapNhatThongTinMuiHai', 'click', function () {
            var Verify = $('#Verify').val();
            var Mui2_TenVacxin = $('#Mui2_TenVacxin').val();
            var Mui2_NgayTiem = $('#Mui2_NgayTiem').val();
            var Mui2_SoLo = $('#Mui2_SoLo').val();
            var Mui2_DiaDiem = $('#Mui2_DiaDiem').val();
            $.ajax({
                type: ""POST"",
                url: '");
#nullable restore
#line 288 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\Profile\Profile_js.cshtml"
                 Write(Url.Action("UpdateMui2Staff", "Account"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
                data: { Verify: Verify, Mui2_TenVacxin: Mui2_TenVacxin, Mui2_NgayTiem: Mui2_NgayTiem, Mui2_SoLo: Mui2_SoLo, Mui2_DiaDiem: Mui2_DiaDiem},
                dataType: 'text',
                success: function (data) {
                    console.log(data);
                    if (data == """") {
                        swal({
                            title: ""Thành công"",
                            text: ""Đã cập nhật thông tin thành công!"",
                            type: 'success',
                            showCancelButtonClass: 'OK'
                        });
                    } else {
                        swal({
                            title: ""Lỗi cập nhật"",
                            text: data,
                            type: 'error',
                            showCancelButtonClass: 'OK'
                        });
                    }
                },
                error: function (e) {
                    swal({
                        tit");
            WriteLiteral(@"le: ""Lỗi cập nhật"",
                        text: data,
                        type: 'error',
                        showCancelButtonClass: 'OK'
                    });
                }
            });
        });
        $(""#v-pills-tabContent"").delegate('.btnCapNhatThongTinKhac', 'click', function () {
            var Verify = $('#Verify').val();
            var GhiChu = $('#GhiChu').val();
            console.log($('#file')[0].files[0]);
            var formData = new FormData();
            formData.append('file', $('#file')[0].files[0]);
            formData.append('Verify', Verify);
            formData.append('GhiChu', GhiChu);

            console.log(formData);
            $.ajax({
                type: ""POST"",
                url: '");
#nullable restore
#line 331 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\Profile\Profile_js.cshtml"
                 Write(Url.Action("UpdateThongTinKhacStaff", "Account"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data) {
                    console.log(data);
                    if (data == """") {
                        swal({
                            title: ""Thành công"",
                            text: ""Đã cập nhật thông tin thành công!"",
                            type: 'success',
                            showCancelButtonClass: 'OK'
                        });
                    } else {
                        swal({
                            title: ""Lỗi cập nhật"",
                            text: data,
                            type: 'error',
                            showCancelButtonClass: 'OK'
                        });
                    }
                },
                error: function (e) {
                    swal({
                        title:");
            WriteLiteral(" \"Lỗi cập nhật\",\r\n                        text: data,\r\n                        type: \'error\',\r\n                        showCancelButtonClass: \'OK\'\r\n                    });\r\n                }\r\n            });\r\n        });\r\n        \r\n    });\r\n</script>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
