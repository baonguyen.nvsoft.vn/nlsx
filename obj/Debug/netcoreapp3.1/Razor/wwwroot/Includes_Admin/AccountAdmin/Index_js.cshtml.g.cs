#pragma checksum "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes_Admin\AccountAdmin\Index_js.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8915f3b3221ab66ea563905abb1f2e2c889c6f7d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.wwwroot_Includes_Admin_AccountAdmin_Index_js), @"mvc.1.0.view", @"/wwwroot/Includes_Admin/AccountAdmin/Index_js.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8915f3b3221ab66ea563905abb1f2e2c889c6f7d", @"/wwwroot/Includes_Admin/AccountAdmin/Index_js.cshtml")]
    public class wwwroot_Includes_Admin_AccountAdmin_Index_js : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"

<script type=""text/javascript"" src=""https://cdn3.devexpress.com/jslib/21.1.6/js/dx.all.js""></script>

<script type=""text/javascript"">



    $(document).ready(function () {
        LoadDonVi();
    });
    function LoadDonVi() {
        $(function(){
            var dataGrid;
            $.ajax({
                    type: ""Post"",
                    url: '");
#nullable restore
#line 17 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes_Admin\AccountAdmin\Index_js.cshtml"
                     Write(Url.Action("GetDonVi", "ThongKeVungXanh"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
                    dataType: 'text',
                    success: function (data) {
                        var makeAsyncDataSource = function(dataDemo){
                            return new DevExpress.data.CustomStore({
                                loadMode: ""raw"",
                                key: ""Id"",
                                load: function() {
                                    return dataDemo;
                                }
                            });
                        };

                        $(""#gridBox"").dxDropDownBox({
                            valueExpr: ""Id"",
                            placeholder: ""Tất cả"",
                            displayExpr: ""TenDonVi"",
                            showClearButton: true,
                            dataSource: makeAsyncDataSource(JSON.parse(data)),
                            contentTemplate: function (e) {
                                var value = e.component.option(""value""),
                    ");
            WriteLiteral(@"                $dataGrid = $(""<div>"").dxDataGrid({
                                        dataSource: e.component.getDataSource(),
                                        columns: [{
                                            dataField: ""TenDonVi"",
                                            caption: ""Tất cả đơn vị""
                                        }],
                                        hoverStateEnabled: true,
                                        paging: { enabled: true, pageSize: 10 },
                                        filterRow: { visible: true },
                                        scrolling: { mode: ""virtual"" },
                                        height: 345,
                                        selection: { mode: ""multiple"" },
                                        selectedRowKeys: value,
                                        onSelectionChanged: function (selectedItems) {
                                            var keys = selectedItems.selectedRow");
            WriteLiteral(@"Keys;
                                            e.component.option(""value"", keys);
                                        }
                                    });

                                dataGrid = $dataGrid.dxDataGrid(""instance"");

                                e.component.on(""valueChanged"", function (args) {
                                    var value = args.value;
                                    dataGrid.selectRows(value, false);
                                });

                                return $dataGrid;
                            },
                            onOptionChanged: function (e) {
                                
                            }
                        });
                    },
                    error: function (e) {
                        swal({
                            title: ""Lỗi"",
                            text: ""Không lấy được dữ liệu"",
                            type: 'error',
                            show");
            WriteLiteral("CancelButtonClass: \'OK\'\r\n                        });\r\n                    }\r\n                });\r\n\r\n\r\n        });\r\n    }\r\n\r\n\r\n</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
