#pragma checksum "E:\WEBSITE\NLSX\nlsx\Views\Staff\Thongketiemchung.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ddce1452d2b74cba211792a1e0a7d8356f91aa45"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Staff_Thongketiemchung), @"mvc.1.0.view", @"/Views/Staff/Thongketiemchung.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ddce1452d2b74cba211792a1e0a7d8356f91aa45", @"/Views/Staff/Thongketiemchung.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1141da4c9ba0da46695c5565cf3ad28e0e2b115b", @"/Views/_ViewImports.cshtml")]
    public class Views_Staff_Thongketiemchung : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "E:\WEBSITE\NLSX\nlsx\Views\Staff\Thongketiemchung.cshtml"
Write(Html.DevExtreme().DataGrid<dbChange.Models.BaoCao.ThongKeMuiTiem_Theo_DonViTong>
    ()
    .DataSource(ds => ds.Mvc()
    .Controller("BaoCaoAPI")
    .LoadAction("GetAll_ThongKeMuiTiem_Theo_DonViTong")
    .UpdateAction("Put")
      .UpdateMethod("POST")
    .Key("Id")
    )
    .RemoteOperations(false)
    .AllowColumnReordering(true)
    .RowAlternationEnabled(true)
    .ShowBorders(true)

    .SearchPanel(s => s
    .Visible(true)
    .HighlightCaseSensitive(true)
    )
    .GroupPanel(g => g.Visible(true))
    .Grouping(g => g.AutoExpandAll(true))
    .WordWrapEnabled(false)
    .ShowColumnLines(true)
    .ShowRowLines(true)
    .RowAlternationEnabled(true)
    .AllowColumnResizing(true)

    .ColumnAutoWidth(true)
    .FilterRow(f => f.Visible(true))
    .HeaderFilter(h => h.Visible(true))
    .RemoteOperations(false)
    // .Scrolling(scrolling => scrolling.ColumnRenderingMode(GridColumnRenderingMode.Virtual))
    .Paging(p => p.PageSize(25))
    .Pager(p => p
    .ShowPageSizeSelector(true)
    .AllowedPageSizes(new[] { 10, 25, 50, 100 })

    )

    .ColumnHidingEnabled(true)
    .ColumnResizingMode(ColumnResizingMode.Widget)
    .FocusedRowEnabled(true)
    .Columns(columns => {

        columns.AddFor(m => m.Id).Caption("#").HeaderCellTemplate(new JS("headerCellTemplate")).AllowEditing(false).Visible(false);
        columns.AddFor(m => m.DonViChiTiet).Caption("Đơn vị").HeaderCellTemplate(new JS("headerCellTemplate")).GroupIndex(0).AllowEditing(false);

        columns.AddFor(m => m.ChiTietHienThi).Caption("Khối").HeaderCellTemplate(new JS("headerCellTemplate")).AllowEditing(false);


        columns.Add().Caption("Số lượng").Columns(c =>
        {

            c.AddFor(m => m.Mui1_DaTiem_SL).Caption("ĐÃ\r\nTIÊM\r\nMŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
            .Alignment(HorizontalAlignment.Center);

            c.AddFor(m => m.Mui2_DaTiem_SL).Caption("ĐÃ\r\nTIÊM\r\nMŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
          .Alignment(HorizontalAlignment.Center);
            c.AddFor(m => m.Mui1_ChuaTiem_SL).Caption("CHƯA\r\nTIÊM\r\nMŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90).AllowEditing(false)
        .Alignment(HorizontalAlignment.Center);
            c.AddFor(m => m.Mui2_ChuaTiem_SL).Caption("CHƯA\r\nTIÊM\r\nMŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90).AllowEditing(false)
        .Alignment(HorizontalAlignment.Center);
            c.AddFor(m => m.TongSoLaoDong).Caption("TỔNG\r\nSỐ\r\nLAO ĐỘNG").HeaderCellTemplate(new JS("headerCellTemplate")).Width(70)
     .Alignment(HorizontalAlignment.Center);


        }).HeaderCellTemplate(new JS("headerCellTemplate"));
        columns.Add().Caption("Tỉ lệ").Columns(c =>
        {
            c.AddFor(m => m.TiLe_Mui1_DaTiem_SL).Caption("ĐÃ\r\nTIÊM\r\nMŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
           .Alignment(HorizontalAlignment.Center);

            c.AddFor(m => m.TiLe_Mui2_DaTiem_SL).Caption("ĐÃ\r\nTIÊM\r\nMŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
          .Alignment(HorizontalAlignment.Center);
            c.AddFor(m => m.TiLe_Mui1_ChuaTiem_SL).Caption("CHƯA\r\nTIÊM\r\nMŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
        .Alignment(HorizontalAlignment.Center);
            c.AddFor(m => m.TiLe_Mui2_ChuaTiem_SL).Caption("CHƯA\r\nTIÊM\r\nMŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
        .Alignment(HorizontalAlignment.Center);
        }).HeaderCellTemplate(new JS("headerCellTemplate"));
        columns.Add().Caption("Số lượng").Columns(c =>
        {
            c.AddFor(m => m.SoLuongF0).Caption("F0").HeaderCellTemplate(new JS("headerCellTemplate")).Width(70)
           .Alignment(HorizontalAlignment.Center);


        }).HeaderCellTemplate(new JS("headerCellTemplate"));
        columns.Add().Caption("Kế hoạch tiêm chủng tiếp theo").Columns(c =>
        {
            c.AddFor(m => m.Mui1_KeHoachTiemTiepTheo).Caption("MŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(120)
          .Alignment(HorizontalAlignment.Center);
            c.AddFor(m => m.Mui2_KeHoachTiemTiepTheo).Caption("MŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(120)
        .Alignment(HorizontalAlignment.Center);


        }).HeaderCellTemplate(new JS("headerCellTemplate"));


        columns.AddFor(m => m.GhiChu).Caption("Ghi chú").HeaderCellTemplate(new JS("headerCellTemplate")).Width(120);

    })
    .Editing(

    e => e.Mode(GridEditMode.Popup)

                            .AllowUpdating(true)
                            .AllowAdding(false)
                            .UseIcons(true)

                            .Form(
                            f => f.Items(items =>
                            {
                                items.AddGroup().Caption("Số lượng (Tip: Bấm phía Tab để xuống dòng)")
                                .ColCount(1)
                                .ColSpan(1)

                                .Items(groupItems =>
                                {



                                    groupItems.AddSimpleFor(m => m.Mui1_DaTiem_SL).HelpText("Tổng số người đã tiêm 1 mũi");
                                    groupItems.AddSimpleFor(m => m.Mui2_DaTiem_SL).HelpText("Tổng số người đã tiêm 2 mũi");
                                    groupItems.AddSimpleFor(m => m.TongSoLaoDong).HelpText("Tổng số lao động");


                                });

                                items.AddGroup().Caption("Kế hoạch tiêm chủng tiếp theo")
                               .ColCount(1)
                               .ColSpan(1)

                               .Items(groupItems =>
                               {
                                   groupItems.AddSimpleFor(m => m.Mui1_KeHoachTiemTiepTheo).HelpText("Số lượng và ngày tiêm mũi 1 dự kiến hoặc ghi chú liên quan");
                                   groupItems.AddSimpleFor(m => m.Mui2_KeHoachTiemTiepTheo).HelpText("Số lượng và ngày tiêm mũi 2 dự kiến hoặc ghi chú liên quan");


                               });

                                items.AddSimpleFor(m => m.GhiChu).ColSpan(1)
                                               .Editor(editor => editor.TextArea().Height(70));




                            }).ScrollingEnabled(true).ShowRequiredMark(true).LabelLocation(FormLabelLocation.Left).ColCount(1)
                            )

    )
    );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
