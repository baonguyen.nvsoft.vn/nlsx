#pragma checksum "E:\WEBSITE\NLSX\nlsx\Views\Staff\Statistic.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d9cd82ff52c109a6ca1abeb03a3ca823fa8c2c07"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Staff_Statistic), @"mvc.1.0.view", @"/Views/Staff/Statistic.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d9cd82ff52c109a6ca1abeb03a3ca823fa8c2c07", @"/Views/Staff/Statistic.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1141da4c9ba0da46695c5565cf3ad28e0e2b115b", @"/Views/_ViewImports.cshtml")]
    public class Views_Staff_Statistic : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "E:\WEBSITE\NLSX\nlsx\Views\Staff\Statistic.cshtml"
Write(Html.DevExtreme().DataGrid<dbChange.Models.StatisticVacxin>
    ()
    .DataSource(ds => ds.Mvc()
    .Controller("Statistic")
    .LoadAction("GetAllStatistics")
    .UpdateAction("Put")
    .UpdateMethod("POST")
    
    .Key("Id")
    )
    .RemoteOperations(false)
    .AllowColumnReordering(true)
    .RowAlternationEnabled(true)
    .ShowBorders(true)

    .SearchPanel(s => s
    .Visible(true)
    .HighlightCaseSensitive(true)
    )
    .GroupPanel(g => g.Visible(true))
    .Grouping(g => g.AutoExpandAll(true))
    .WordWrapEnabled(false)
    .ShowColumnLines(true)
    .ShowRowLines(true)
    .RowAlternationEnabled(true)
    .AllowColumnResizing(true)

    .ColumnAutoWidth(true)
    .FilterRow(f => f.Visible(true))
    .HeaderFilter(h => h.Visible(true))
    .RemoteOperations(false)
    // .Scrolling(scrolling => scrolling.ColumnRenderingMode(GridColumnRenderingMode.Virtual))
    .Paging(p => p.PageSize(25))
    .Pager(p => p
    .ShowPageSizeSelector(true)
    .AllowedPageSizes(new[] { 10, 25, 50, 100 })

    )

    .ColumnHidingEnabled(true)
    .ColumnResizingMode(ColumnResizingMode.Widget)
    .FocusedRowEnabled(true)
    .Columns(columns => {

        columns.AddFor(m => m.Id).Caption("#").HeaderCellTemplate(new JS("headerCellTemplate")).AllowEditing(false);

        columns.AddFor(m => m.DonVi).Caption("Đơn vị").HeaderCellTemplate(new JS("headerCellTemplate")).Visible(false).AllowEditing(false);

        columns.AddFor(m => m.DonViChiTiet).Caption("Đơn vị chi tiết").HeaderCellTemplate(new JS("headerCellTemplate")).GroupIndex(0).AllowEditing(false);

        columns.AddFor(m => m.HoTen).Caption("Họ tên").HeaderCellTemplate(new JS("headerCellTemplate")).AllowEditing(false);

        columns.AddFor(m => m.GioiTinh).Caption("Giới\r\ntính").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90);

        columns.AddFor(m => m.NgaySinh).Caption("Ngày\r\nSinh").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90);
        columns.Add().Caption("Mũi 1").Columns(c =>
        {

            c.AddFor(m => m.Mui1_DaTiem).Caption("Đã\r\ntiêm").HeaderCellTemplate(new JS("headerCellTemplate")).Width(70)
            .Alignment(HorizontalAlignment.Center);

            c.AddFor(m => m.Mui1_ChuaTiem).Caption("Chưa\r\ntiêm").HeaderCellTemplate(new JS("headerCellTemplate")).Width(70)
            .Alignment(HorizontalAlignment.Center);

            c.AddFor(m => m.Mui1_LyDo).Caption("Lý do").HeaderCellTemplate(new JS("headerCellTemplate"));
        }).HeaderCellTemplate(new JS("headerCellTemplate"));
        columns.Add().Caption("Mũi 2").Columns(c =>
        {
            c.AddFor(m => m.Mui2_DaTiem).Caption("Đã\r\ntiêm").HeaderCellTemplate(new JS("headerCellTemplate")).Width(70)
            .Alignment(HorizontalAlignment.Center);

            c.AddFor(m => m.Mui2_ChuaTiem).Caption("Chưa\r\ntiêm").HeaderCellTemplate(new JS("headerCellTemplate")).Width(70)
            .Alignment(HorizontalAlignment.Center);

            c.AddFor(m => m.Mui2_LyDo).Caption("Lý do").HeaderCellTemplate(new JS("headerCellTemplate"));
        }).HeaderCellTemplate(new JS("headerCellTemplate"));

        columns.AddFor(m => m.F0).Caption("F0").HeaderCellTemplate(new JS("headerCellTemplate")).Alignment(HorizontalAlignment.Center);

        columns.AddFor(m => m.SoDienThoai).Caption("Số \r\nđiện thoại").HeaderCellTemplate(new JS("headerCellTemplate")).Width(100)
        .Alignment(HorizontalAlignment.Center);

        columns.AddFor(m => m.GhiChu).Caption("Ghi chú").HeaderCellTemplate(new JS("headerCellTemplate"));

        columns.AddFor(m => m.NgayBoSung).Caption("Ngày\r\ncập nhật").HeaderCellTemplate(new JS("headerCellTemplate")).Width(100).AllowEditing(false);
    })
    .Editing(

    e => e.Mode(GridEditMode.Popup)

                            .AllowUpdating(true)
                            .AllowAdding(true)
                            .UseIcons(true)

                            .Form(
                            f => f.Items(items =>
                            {
                                items.AddGroup().Caption("Thông tin nhân viên")
                                .ColCount(1)
                                .ColSpan(1)

                                .Items(groupItems =>
                                {



                                    groupItems.AddSimpleFor(m => m.Id);
                                    groupItems.AddSimpleFor(m => m.HoTen);
                                    groupItems.AddSimpleFor(m => m.NgaySinh);
                                    groupItems.AddSimpleFor(m => m.GioiTinh);
                                    groupItems.AddSimpleFor(m => m.SoDienThoai);
                                    groupItems.AddSimpleFor(m => m.DonViChiTiet);



                                });
                                items.AddGroup()
                                .Items(groupItems =>
                                {
                                    groupItems.AddGroup().Caption("Mũi 1")
                                    .ColCount(3)
                                    .ColSpan(3)

                                    .Items(groupItems1 =>
                                    {
                                        groupItems1.AddSimpleFor(m => m.Mui1_DaTiem);
                                        groupItems1.AddSimpleFor(m => m.Mui1_ChuaTiem);
                                        groupItems1.AddSimpleFor(m => m.Mui1_LyDo);

                                    });
                                    groupItems.AddGroup().Caption("Mũi 2")
                                    .ColCount(3)
                                    .ColSpan(3)

                                    .Items(groupItems1 =>
                                    {
                                        groupItems1.AddSimpleFor(m => m.Mui2_DaTiem);
                                        groupItems1.AddSimpleFor(m => m.Mui2_ChuaTiem);
                                        groupItems1.AddSimpleFor(m => m.Mui2_LyDo);

                                    });
                                    groupItems.AddSimpleFor(m => m.F0);
                                    groupItems.AddSimpleFor(m => m.GhiChu).ColSpan(1)
                                .Editor(editor => editor.TextArea().Height(70));

                                });





                            }).ScrollingEnabled(true).ShowRequiredMark(true).LabelLocation(FormLabelLocation.Top)
                            )

    )
    );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
