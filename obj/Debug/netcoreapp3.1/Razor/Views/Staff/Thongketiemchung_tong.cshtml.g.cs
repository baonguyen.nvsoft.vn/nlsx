#pragma checksum "E:\WEBSITE\NLSX\nlsx\Views\Staff\Thongketiemchung_tong.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "500cdd0b154db143b3d3e8c2ec4c79a5f2eccdc2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Staff_Thongketiemchung_tong), @"mvc.1.0.view", @"/Views/Staff/Thongketiemchung_tong.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"500cdd0b154db143b3d3e8c2ec4c79a5f2eccdc2", @"/Views/Staff/Thongketiemchung_tong.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1141da4c9ba0da46695c5565cf3ad28e0e2b115b", @"/Views/_ViewImports.cshtml")]
    public class Views_Staff_Thongketiemchung_tong : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n    <h3 style=\"text-align:center\">THỐNG KÊ TỈ LỆ TIÊM VACXIN</h3>\r\n\r\n    ");
#nullable restore
#line 4 "E:\WEBSITE\NLSX\nlsx\Views\Staff\Thongketiemchung_tong.cshtml"
Write(Html.DevExtreme().TreeList<dbChange.Models.BaoCao.ThongKeMuiTiem_Theo_DonViTong>
    ()
    .DataSource(ds => ds.Mvc()
    .Controller("BaoCaoAPI1")
    .LoadAction("GetAll_ThongKeMuiTiem_Tong_TheoTaiKhoan")
    .UpdateAction("Put")
      .UpdateMethod("POST")
    .Key("Id")
    )
   .KeyExpr("Id")
    .ParentIdExpr("ParentId")
    .AutoExpandAll(true)
    .AllowColumnReordering(true)
    .RowAlternationEnabled(true)
    .ShowBorders(true)
    .FocusedRowEnabled(true)
    .SearchPanel(s => s
    .Visible(true)
    .HighlightCaseSensitive(true)
    )
    .WordWrapEnabled(false)
    .ShowColumnLines(true)
    .ShowRowLines(true)
    .RowAlternationEnabled(true)
    .AllowColumnResizing(true)

    .ColumnAutoWidth(true)
    .FilterRow(f => f.Visible(true))
    .HeaderFilter(h => h.Visible(true))


    .ColumnHidingEnabled(true)
    .ColumnResizingMode(ColumnResizingMode.Widget)
    .FocusedRowEnabled(true)
    .Columns(columns => {

        columns.AddFor(m => m.Id).Caption("#").HeaderCellTemplate(new JS("headerCellTemplate")).AllowEditing(false).Visible(false);
    


        columns.AddFor(m => m.ChiTietHienThi).Caption("Đơn Vị").HeaderCellTemplate(new JS("headerCellTemplate")).AllowEditing(false);


        columns.Add().Caption("Số lượng").Columns(c =>
        {

            c.AddFor(m => m.Mui1_DaTiem_SL).Caption("ĐÃ\r\nTIÊM\r\nMŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
            .Alignment(HorizontalAlignment.Center);

            c.AddFor(m => m.Mui2_DaTiem_SL).Caption("ĐÃ\r\nTIÊM\r\nMŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
          .Alignment(HorizontalAlignment.Center);
            c.AddFor(m => m.Mui1_ChuaTiem_SL).Caption("CHƯA\r\nTIÊM\r\nMŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
        .Alignment(HorizontalAlignment.Center);
            c.AddFor(m => m.Mui2_ChuaTiem_SL).Caption("CHƯA\r\nTIÊM\r\nMŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
        .Alignment(HorizontalAlignment.Center);


        }).HeaderCellTemplate(new JS("headerCellTemplate"));
        columns.Add().Caption("Tỉ lệ").Columns(c =>
        {
            c.AddFor(m => m.TiLe_Mui1_DaTiem_SL).Caption("ĐÃ\r\nTIÊM\r\nMŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
           .Alignment(HorizontalAlignment.Center).Format(Format.Percent);

            c.AddFor(m => m.TiLe_Mui2_DaTiem_SL).Caption("ĐÃ\r\nTIÊM\r\nMŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
          .Alignment(HorizontalAlignment.Center).Format(Format.Percent);
            c.AddFor(m => m.TiLe_Mui1_ChuaTiem_SL).Caption("CHƯA\r\nTIÊM\r\nMŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
        .Alignment(HorizontalAlignment.Center).Format(Format.Percent);
            c.AddFor(m => m.TiLe_Mui2_ChuaTiem_SL).Caption("CHƯA\r\nTIÊM\r\nMŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(90)
        .Alignment(HorizontalAlignment.Center).Format(Format.Percent);
        }).HeaderCellTemplate(new JS("headerCellTemplate"));
        columns.Add().Caption("Số lượng").Columns(c =>
        {
            c.AddFor(m => m.SoLuongF0).Caption("F0").HeaderCellTemplate(new JS("headerCellTemplate")).Width(70)
           .Alignment(HorizontalAlignment.Center);


        }).HeaderCellTemplate(new JS("headerCellTemplate"));
        columns.Add().Caption("Kế hoạch tiêm chủng tiếp theo").Columns(c =>
        {
            c.AddFor(m => m.Mui1_KeHoachTiemTiepTheo).Caption("MŨI 1").HeaderCellTemplate(new JS("headerCellTemplate")).Width(120)
          .Alignment(HorizontalAlignment.Center);
            c.AddFor(m => m.Mui2_KeHoachTiemTiepTheo).Caption("MŨI 2").HeaderCellTemplate(new JS("headerCellTemplate")).Width(120)
        .Alignment(HorizontalAlignment.Center);


        }).HeaderCellTemplate(new JS("headerCellTemplate"));


        columns.AddFor(m => m.GhiChu).Caption("Ghi chú").HeaderCellTemplate(new JS("headerCellTemplate")).Width(120);

    })
    .Editing(e => e.AllowUpdating(false).AllowDeleting(false).AllowAdding(false))


    );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
