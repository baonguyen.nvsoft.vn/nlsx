#pragma checksum "E:\WEBSITE\NLSX\nlsx\Views\ThongKeVungXanh\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f6abc9f52d747002d7ac9f663a0ba326fecec668"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ThongKeVungXanh_Index), @"mvc.1.0.view", @"/Views/ThongKeVungXanh/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "E:\WEBSITE\NLSX\nlsx\Views\ThongKeVungXanh\Index.cshtml"
using dbChange.Models.LIB;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f6abc9f52d747002d7ac9f663a0ba326fecec668", @"/Views/ThongKeVungXanh/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1141da4c9ba0da46695c5565cf3ad28e0e2b115b", @"/Views/_ViewImports.cshtml")]
    public class Views_ThongKeVungXanh_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "All", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "E:\WEBSITE\NLSX\nlsx\Views\ThongKeVungXanh\Index.cshtml"
  
    ViewData["Title"] = "Thống kê vùng dịch";
    Layout = "~/Views/Shared/_Layout.cshtml";

    var lsDonVi = ViewData["DonVi"] as List<DM_BoPhanChiTiet>;
    var lsBoPhan = ViewData["BoPhan"] as List<DonViChiTiet_BoPhan>;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<style>
    #pie {
        height: 440px;
    }

        #pie * {
            margin: 0 auto;
        }
</style>
<div class=""container"" style=""background-color:white;"">
    <div class=""row p-2"">
        <h4 style=""color:red;text-align:center;width: 100%; text-transform: uppercase; font-weight: bold;"">Thống kê vùng dịch cập nhật đến 08h00 ngày ");
#nullable restore
#line 20 "E:\WEBSITE\NLSX\nlsx\Views\ThongKeVungXanh\Index.cshtml"
                                                                                                                                                 Write(DateTime.Today.ToString("dd/MM/yyyy"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</h4>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-xl-2 col-sm-12 col-12 mb-2\">\r\n            \r\n        </div>\r\n        <div class=\"col-xl-4 col-sm-12 col-12 mb-4 \">\r\n            <label for=\"DonViChiTiet\">Đơn vị:</label>\r\n");
            WriteLiteral("            <div");
            BeginWriteAttribute("class", " class=\"", 1250, "\"", 1258, 0);
            EndWriteAttribute();
            WriteLiteral(@">
                <div id=""gridBox"" class=""form-control form-control-sm""></div>
            </div>

        </div>
        <div class=""col-xl-4 col-sm-12 col-12 mb-4"">
            <label for=""BoPhan"">Bộ phận:</label>
            <select class=""form-control form-control-sm"" name=""BoPhan"" id=""BoPhan"">
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f6abc9f52d747002d7ac9f663a0ba326fecec6685510", async() => {
                WriteLiteral(" Tất cả");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
            </select>
        </div>
        <div class=""col-xl-2 col-sm-12 col-12 mb-2""></div>
    </div>
    <div class=""row text-center"">
        <div class=""col-xl-6 col-sm-12 col-12 pb-5"">
            <div id=""pie"" style=""margin:0 auto""></div>
        </div>
        <div class=""col-xl-6 col-sm-12 col-12"">
            <div id=""chart-percent"" style=""margin:0 auto;font-size:15px;text-align:left;""></div>
        </div>
    </div>
    <div class=""row text-center pt-5 pb-5 pl-1 pr-1"">
        <h4>Danh sách chi tiết</h4>
        <div id=""gridContainer"" style=""    width: 100%;""></div>
    </div>
</div>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
