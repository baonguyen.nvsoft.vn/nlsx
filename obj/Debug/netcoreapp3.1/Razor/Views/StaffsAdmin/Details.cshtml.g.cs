#pragma checksum "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0fedf056f923a688dc09f836676d71c48b9a8b4a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_StaffsAdmin_Details), @"mvc.1.0.view", @"/Views/StaffsAdmin/Details.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0fedf056f923a688dc09f836676d71c48b9a8b4a", @"/Views/StaffsAdmin/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1141da4c9ba0da46695c5565cf3ad28e0e2b115b", @"/Views/_ViewImports.cshtml")]
    public class Views_StaffsAdmin_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dbChange.Models.Staff>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
  
    ViewData["Title"] = "Details";
    Layout = "~/Views/Shared/Admin/_Layout_Admin.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Details</h1>\r\n\r\n<div>\r\n    <h4>Staff</h4>\r\n    <hr />\r\n    <dl class=\"row\">\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 15 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.HoTen));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 18 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.HoTen));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 21 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.NgaySinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 24 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.NgaySinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 27 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.GioiTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 30 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.GioiTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 33 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.MaNhom));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 36 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MaNhom));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 39 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.DonVi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 42 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.DonVi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 45 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.DienThoai));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 48 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.DienThoai));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 51 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.CMND));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 54 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.CMND));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 57 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.BHYT));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 60 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.BHYT));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 63 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.TenTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 66 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.TenTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 69 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.MaTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 72 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MaTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 75 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.TenQuan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 78 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.TenQuan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 81 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.MaQuan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 84 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MaQuan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 87 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.TenPhuong));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 90 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.TenPhuong));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 93 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.MaPhuong));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 96 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MaPhuong));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 99 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.DiaChi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 102 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.DiaChi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 105 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui1_TenVacxin));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 108 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Mui1_TenVacxin));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 111 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui1_NgayTiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 114 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Mui1_NgayTiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 117 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui1_SoLo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 120 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Mui1_SoLo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 123 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui1_DiaDiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 126 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Mui1_DiaDiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 129 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui2_TenVacxin));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 132 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Mui2_TenVacxin));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 135 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui2_NgayTiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 138 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Mui2_NgayTiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 141 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui2_SoLo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 144 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Mui2_SoLo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 147 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui2_DiaDiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 150 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Mui2_DiaDiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 153 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.GhiChu));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 156 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.GhiChu));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 159 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.KetQuaImPort));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 162 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.KetQuaImPort));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 165 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.DonViChiTiet));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 168 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.DonViChiTiet));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 171 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.NgayCapNhat));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 174 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.NgayCapNhat));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 177 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.NguoiCapNhat));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 180 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.NguoiCapNhat));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 183 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.UrlHinhGiayXN));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 186 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.UrlHinhGiayXN));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 189 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.PhanNhom));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 192 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.PhanNhom));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 195 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.TinhTrang));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 198 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.TinhTrang));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 201 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.BiF0));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 204 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.BiF0));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 207 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.NgayXuatVien));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 210 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.NgayXuatVien));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 213 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.NgayS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 216 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.NgayS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 219 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.ThangS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 222 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.ThangS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 225 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.NamS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 228 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.NamS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 231 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.ISok));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 234 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.ISok));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 237 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Ma_ToKhuPho));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 240 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Ma_ToKhuPho));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 243 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Ten_ToKhuPho));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 246 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.Ten_ToKhuPho));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 249 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.VungCovid));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 252 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.VungCovid));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 255 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.BoPhanId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 258 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.BoPhanId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 261 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.TenBoPhan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 264 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.TenBoPhan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 267 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.IdWeb));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 270 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.IdWeb));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 273 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.LinkWeb));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 276 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.LinkWeb));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 279 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.CCCD));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 282 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.CCCD));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 285 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.MaQR));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 288 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MaQR));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 291 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.MaNS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 294 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.MaNS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 297 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.SSBH));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 300 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.SSBH));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 303 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.LoaiLD));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 306 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.LoaiLD));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 309 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.LinkImage));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 312 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.LinkImage));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 315 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.TrangThaiNhanSu));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 318 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
       Write(Html.DisplayFor(model => model.TrangThaiNhanSu));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n</div>\r\n<div>\r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0fedf056f923a688dc09f836676d71c48b9a8b4a34408", async() => {
                WriteLiteral("Edit");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 323 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Details.cshtml"
                           WriteLiteral(Model.Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(" |\r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0fedf056f923a688dc09f836676d71c48b9a8b4a36526", async() => {
                WriteLiteral("Back to List");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dbChange.Models.Staff> Html { get; private set; }
    }
}
#pragma warning restore 1591
