/* Toggle Expanded Menu Left - nnv */
if (!localStorage.getItem('expanded_menu_left')) {
    localStorage.setItem('expanded_menu_left', false);
}
var _settingUX = JSON.parse(localStorage.getItem('expanded_menu_left'));
console.log("expanded_menu_left: ",_settingUX);
if (_settingUX == true) {
    document.querySelector('.wrapper-menu').classList.add("open");
    document.querySelector('body').classList.add("sidebar-main");

} else {
    document.querySelector('.wrapper-menu').classList.remove("open");
    document.querySelector('body').classList.remove("sidebar-main");
}
document.querySelector('.wrapper-menu').addEventListener('click', function (e) {
    _settingUX = !_settingUX;
    localStorage.setItem('expanded_menu_left', _settingUX);
})