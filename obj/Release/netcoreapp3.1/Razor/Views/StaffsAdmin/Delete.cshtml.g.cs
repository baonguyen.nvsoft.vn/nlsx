#pragma checksum "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "25cc0c2b170d0f5fc2f663c370c5d16fac75ad26"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_StaffsAdmin_Delete), @"mvc.1.0.view", @"/Views/StaffsAdmin/Delete.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"25cc0c2b170d0f5fc2f663c370c5d16fac75ad26", @"/Views/StaffsAdmin/Delete.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1141da4c9ba0da46695c5565cf3ad28e0e2b115b", @"/Views/_ViewImports.cshtml")]
    public class Views_StaffsAdmin_Delete : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dbChange.Models.Staff>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "hidden", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
  
    ViewData["Title"] = "Delete";
    Layout = "~/Views/Shared/Admin/_Layout_Admin.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Delete</h1>\r\n\r\n<h3>Are you sure you want to delete this?</h3>\r\n<div>\r\n    <h4>Staff</h4>\r\n    <hr />\r\n    <dl class=\"row\">\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 16 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.HoTen));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 19 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.HoTen));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 22 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.NgaySinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 25 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.NgaySinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 28 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.GioiTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 31 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.GioiTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 34 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.MaNhom));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 37 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.MaNhom));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 40 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.DonVi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 43 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.DonVi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 46 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.DienThoai));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 49 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.DienThoai));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 52 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.CMND));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 55 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.CMND));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 58 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.BHYT));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 61 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.BHYT));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 64 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.TenTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 67 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.TenTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 70 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.MaTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 73 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.MaTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 76 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.TenQuan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 79 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.TenQuan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 82 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.MaQuan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 85 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.MaQuan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 88 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.TenPhuong));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 91 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.TenPhuong));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 94 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.MaPhuong));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 97 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.MaPhuong));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 100 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.DiaChi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 103 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.DiaChi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 106 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui1_TenVacxin));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 109 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Mui1_TenVacxin));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 112 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui1_NgayTiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 115 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Mui1_NgayTiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 118 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui1_SoLo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 121 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Mui1_SoLo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 124 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui1_DiaDiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 127 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Mui1_DiaDiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 130 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui2_TenVacxin));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 133 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Mui2_TenVacxin));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 136 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui2_NgayTiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 139 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Mui2_NgayTiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 142 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui2_SoLo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 145 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Mui2_SoLo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 148 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Mui2_DiaDiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 151 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Mui2_DiaDiem));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 154 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.GhiChu));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 157 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.GhiChu));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 160 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.KetQuaImPort));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 163 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.KetQuaImPort));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 166 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.DonViChiTiet));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 169 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.DonViChiTiet));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 172 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.NgayCapNhat));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 175 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.NgayCapNhat));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 178 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.NguoiCapNhat));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 181 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.NguoiCapNhat));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 184 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.UrlHinhGiayXN));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 187 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.UrlHinhGiayXN));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 190 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.PhanNhom));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 193 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.PhanNhom));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 196 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.TinhTrang));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 199 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.TinhTrang));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 202 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.BiF0));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 205 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.BiF0));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 208 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.NgayXuatVien));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 211 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.NgayXuatVien));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 214 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.NgayS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 217 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.NgayS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 220 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.ThangS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 223 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.ThangS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 226 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.NamS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 229 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.NamS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 232 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.ISok));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 235 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.ISok));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 238 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Ma_ToKhuPho));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 241 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Ma_ToKhuPho));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 244 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Ten_ToKhuPho));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 247 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Ten_ToKhuPho));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 250 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.VungCovid));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 253 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.VungCovid));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 256 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.BoPhanId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 259 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.BoPhanId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 262 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.TenBoPhan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 265 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.TenBoPhan));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 268 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.IdWeb));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 271 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.IdWeb));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 274 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.LinkWeb));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 277 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.LinkWeb));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 280 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.CCCD));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 283 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.CCCD));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 286 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.MaQR));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 289 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.MaQR));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 292 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.MaNS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 295 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.MaNS));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 298 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.SSBH));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 301 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.SSBH));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 304 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.LoaiLD));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 307 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.LoaiLD));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 310 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.LinkImage));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 313 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.LinkImage));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 316 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.TrangThaiNhanSu));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 319 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
       Write(Html.DisplayFor(model => model.TrangThaiNhanSu));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n    \r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "25cc0c2b170d0f5fc2f663c370c5d16fac75ad2635047", async() => {
                WriteLiteral("\r\n        ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "25cc0c2b170d0f5fc2f663c370c5d16fac75ad2635314", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
#nullable restore
#line 324 "E:\WEBSITE\NLSX\nlsx\Views\StaffsAdmin\Delete.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Id);

#line default
#line hidden
#nullable disable
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n        <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\" /> |\r\n        ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "25cc0c2b170d0f5fc2f663c370c5d16fac75ad2637075", async() => {
                    WriteLiteral("Back to List");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dbChange.Models.Staff> Html { get; private set; }
    }
}
#pragma warning restore 1591
