#pragma checksum "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "95da11ac0ffbe35fc270330eb66a41fc8cb9e446"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Excel_ExportExcel), @"mvc.1.0.razor-page", @"/Views/Excel/ExportExcel.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using dbChange.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\WEBSITE\NLSX\nlsx\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"95da11ac0ffbe35fc270330eb66a41fc8cb9e446", @"/Views/Excel/ExportExcel.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1141da4c9ba0da46695c5565cf3ad28e0e2b115b", @"/Views/_ViewImports.cshtml")]
    public class Views_Excel_ExportExcel : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
  
    ViewBag.Title = "List";

#line default
#line hidden
#nullable disable
            WriteLiteral("<table class=\"table table-striped table-bordered\">\r\n    <tr>\r\n        <th>\r\n            ");
#nullable restore
#line 9 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
       Write(Html.DisplayNameFor(model => model.HoTen));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </th>\r\n        <th>\r\n            ");
#nullable restore
#line 12 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
       Write(Html.DisplayNameFor(model => model.NamSinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </th>\r\n        <th>\r\n            ");
#nullable restore
#line 15 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
       Write(Html.DisplayNameFor(model => model.GioiTinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </th>\r\n        <th>\r\n            ");
#nullable restore
#line 18 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
       Write(Html.DisplayNameFor(model => model.DiaChi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </th>\r\n        <th>\r\n            ");
#nullable restore
#line 21 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
       Write(Html.DisplayNameFor(model => model.DienThoai));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </th>\r\n        <th>\r\n            ");
#nullable restore
#line 24 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
       Write(Html.DisplayNameFor(model => model.CMND));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </th>\r\n        <th>\r\n            ");
#nullable restore
#line 27 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
       Write(Html.DisplayNameFor(model => model.DonVi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </th>\r\n        <th>\r\n            ");
#nullable restore
#line 30 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
       Write(Html.DisplayNameFor(model => model.KetQuaXN));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </th>\r\n\r\n    </tr>\r\n\r\n");
#nullable restore
#line 35 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
     foreach (var item in Model)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("<tr>\r\n    <td>\r\n        ");
#nullable restore
#line 39 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
   Write(Html.DisplayFor(modelItem => item.HoTen));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </td>\r\n    <td>\r\n        ");
#nullable restore
#line 42 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
   Write(Html.DisplayFor(modelItem => item.NamSinh));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </td>\r\n    <td>\r\n        ");
#nullable restore
#line 45 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
   Write(Html.DisplayFor(modelItem => item.DiaChi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </td>\r\n    <td>\r\n        ");
#nullable restore
#line 48 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
   Write(Html.DisplayFor(modelItem => item.DienThoai));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </td>\r\n    <td>\r\n        ");
#nullable restore
#line 51 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
   Write(Html.DisplayFor(modelItem => item.CMND));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </td>\r\n    <td>\r\n        ");
#nullable restore
#line 54 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
   Write(Html.DisplayFor(modelItem => item.DonVi));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </td>\r\n    <td>\r\n        ");
#nullable restore
#line 57 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
   Write(Html.DisplayFor(modelItem => item.KetQuaXN));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </td>\r\n</tr>\r\n");
#nullable restore
#line 60 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"

    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</table>\r\n<div>\r\n    <a");
            BeginWriteAttribute("href", " href=\"", 1459, "\"", 1512, 1);
#nullable restore
#line 65 "E:\WEBSITE\NLSX\nlsx\Views\Excel\ExportExcel.cshtml"
WriteAttributeValue("", 1466, Url.Action("DownloadExcel","ExcelController"), 1466, 46, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Download Excel</a>\r\n</div>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<dbChange.Models.Employee>> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IEnumerable<dbChange.Models.Employee>> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IEnumerable<dbChange.Models.Employee>>)PageContext?.ViewData;
        public IEnumerable<dbChange.Models.Employee> Model => ViewData.Model;
    }
}
#pragma warning restore 1591
