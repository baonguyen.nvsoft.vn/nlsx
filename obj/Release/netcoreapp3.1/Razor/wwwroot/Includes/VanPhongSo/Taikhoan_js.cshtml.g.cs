#pragma checksum "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\VanPhongSo\Taikhoan_js.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "688ccedbd09af5a416536020afa40278e7cb5000"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.wwwroot_Includes_VanPhongSo_Taikhoan_js), @"mvc.1.0.view", @"/wwwroot/Includes/VanPhongSo/Taikhoan_js.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"688ccedbd09af5a416536020afa40278e7cb5000", @"/wwwroot/Includes/VanPhongSo/Taikhoan_js.cshtml")]
    public class wwwroot_Includes_VanPhongSo_Taikhoan_js : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/plugins/select2/js/select2.full.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("text/javascript"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/plugins/jquery-validation/jquery.validate.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/plugins/jquery-validation/additional-methods.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "688ccedbd09af5a416536020afa40278e7cb50004137", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "688ccedbd09af5a416536020afa40278e7cb50005259", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "688ccedbd09af5a416536020afa40278e7cb50006381", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

<!-- Plugin Search Select2 -->
<script type=""text/javascript"">
    $(document).ready(function () {
        $('.select2me').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
                closeOnSelect: !$(this).attr('multiple'),
            });
        });
    });
</script>

<!-- User List -->
<script type=""text/javascript"">
    var _height_Window = $(window).height();
    var _height_Header = $('.iq-top-navbar').height();
    var _height_Title = $('#box_title_table').height();
    var _height_Footer = $('.iq-footer').height();

    // console.log(_height_Window);
    // console.log(_height_Header);
    // console.log(_height_Title);
    // console.log(_height_Footer);
    /* Config scroll table - Xoá comm");
            WriteLiteral(@"ent để kích hoạt */
    var flag_fixed = false;
    if ($(window).width() > 760) {
        $('#gridContainer').height(_height_Window - _height_Header - _height_Title - _height_Footer - 76 - 25);
        flag_fixed = true;
    } else {
        $('#gridContainer').height(_height_Window - _height_Header - 15);
        if ($(window).width() > 375) {
            flag_fixed = true;
        } else {
            flag_fixed = false;
        }
    }


    function load_danhsach_user() {
        $.ajax({
            url: '");
#nullable restore
#line 48 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\VanPhongSo\Taikhoan_js.cshtml"
             Write(Url.Action("GetDanhSachTaiKhoan", "VanPhongSo"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
            type: 'GET',
            dataType: 'json'
        })
            .done(function (response) {
                
                $('#gridContainer').dxDataGrid({
                    dataSource: response,
                    keyExpr: 'Id',
                    showBorders: true,
                    showRowLines: true,
                    paging: {
                        enabled: false
                    },
                columnAutoWidth: true,
                columnHidingEnabled: true,
                allowColumnResizing: true,
                    columnFixing: {
                        enabled: true
                    },
                    filterRow: {
                        visible: true,
                        applyFilter: 'auto',
                    },
                    // filterPanel: { visible: true },
                    headerFilter: { visible: true },
                    columns:
                        [
                            {
                  ");
            WriteLiteral(@"              headerCellTemplate: function (container) {
                                    container.text('').parent().css({ fontWeight: 'bold', textAlign: 'center', backgroundColor: '#c5d3dd' });
                                },
                                width: 70,
                                dataField: """",
                                alignment: ""center"",
                                fixed: true,
                                cellTemplate: function (container, options) {
                                    //console.log(options);
                                    let text_Color_TrangThai = `
                                    <button type=""button"" class=""btn btn-sm btn-outline-primary rounded-pill""  data-toggle=""modal"" data-target=""#modal_User_Edit"" data-id=""${options.data.Id}"">
                                        <i class=""fa fa-pencil-square-o""></i>Sửa
                                    </button>
                                `;

                             ");
            WriteLiteral(@"       $(container).css({ overflow: 'initial' }).html(text_Color_TrangThai);
                                }
                            },
                            {
                                headerCellTemplate: function (container) {
                                    container.text('STT').parent().css({ fontWeight: 'bold', backgroundColor: '#c5d3dd' });
                                },
                                width: 50,
                                //dataField: ""STT"",
                                alignment: ""center"",
                                cellTemplate: function (container, options) {
                                    let _stt = options.rowIndex + 1;
                                    $(container).html(_stt);
                                }
                            },
                            {
                                headerCellTemplate: function (container) {
                                    container.text('Tên đăng nhập').parent(");
            WriteLiteral(@").css({ fontWeight: 'bold', backgroundColor: '#c5d3dd' });
                                },
                                dataField: ""TenDangNhap"",
                                alignment: ""center"",
                                minWidth: 120,
                                cellTemplate: function (container, options) {
                                    $(container).html(options.value);
                                },
                                caption : ""Tên đăng nhập""
                            },
                            {
                                headerCellTemplate: function (container) {
                                    container.text('Email').parent().css({ fontWeight: 'bold', backgroundColor: '#c5d3dd' });
                                },
                                dataField: ""Email"",
                                cellTemplate: function (container, options) {
                                    $(container).html(options.value);
                 ");
            WriteLiteral(@"               }
                            },
                            {
                                headerCellTemplate: function (container) {
                                    container.text('Mã nhân viên').parent().css({ fontWeight: 'bold', backgroundColor: '#c5d3dd' });
                                },
                                dataField: ""MaNhanVien"",
                                cellTemplate: function (container, options) {
                                    $(container).html(options.value);
                                },
                                caption : ""Mã nhân viên""
                            },
                            {
                                headerCellTemplate: function (container) {
                                    container.text('Đơn vị').parent().css({ fontWeight: 'bold', textAlign: 'center', backgroundColor: '#c5d3dd' });
                                },
                                dataField: ""DonViChiTiet"",
           ");
            WriteLiteral(@"                     cellTemplate: function (container, options) {
                                    $(container).html(options.value);
                                },
                                caption : ""Đơn vị""
                            },
                            {
                                headerCellTemplate: function (container) {
                                    container.text('VPS').parent().css({ fontWeight: 'bold', textAlign: 'center', backgroundColor: '#c5d3dd' });
                                },
                                width: 100,
                                dataField: ""isVPS"",
                                alignment: ""center"",
                                cellTemplate: function (container, options) {
                                    //console.log(options);
                                    let text_isVPS = """";
                                    if (options.data.isVPS) {
                                        text_isVPS = `
       ");
            WriteLiteral(@"                             <div class=""custom-control custom-checkbox"">
                                        <input checked=""checked"" class=""custom-control-input"" disabled=""disabled"" type=""checkbox"">
                                        <label class=""custom-control-label""></label>
                                    </div>
                                    `;
                                    } else {
                                        text_isVPS = `
                                    <div class=""custom-control custom-checkbox"">
                                        <input class=""custom-control-input"" disabled=""disabled"" type=""checkbox"">
                                        <label class=""custom-control-label""></label>
                                    </div>
                                    `;
                                    }
                                    $(container).html(""<h5>"" + text_isVPS + ""</h5>"");
                                }
                  ");
            WriteLiteral(@"          },
                            {
                                headerCellTemplate: function (container) {
                                    container.text('Scan').parent().css({ fontWeight: 'bold', textAlign: 'center', backgroundColor: '#c5d3dd' });
                                },
                                width: 100,
                                dataField: ""isScan"",
                                alignment: ""center"",
                                cellTemplate: function (container, options) {
                                    //console.log(options);
                                    let text_isScan = """";
                                    if (options.data.isScan) {
                                        text_isScan = `
                                    <div class=""custom-control custom-checkbox"">
                                        <input checked=""checked"" class=""custom-control-input"" disabled=""disabled"" type=""checkbox"">
                                 ");
            WriteLiteral(@"       <label class=""custom-control-label""></label>
                                    </div>
                                    `;
                                    } else {
                                        text_isScan = `
                                    <div class=""custom-control custom-checkbox"">
                                        <input class=""custom-control-input"" disabled=""disabled"" type=""checkbox"">
                                        <label class=""custom-control-label""></label>
                                    </div>
                                    `;
                                    }
                                    $(container).html(""<h5>"" + text_isScan + ""</h5>"");
                                }
                            },
                            {
                                headerCellTemplate: function (container) {
                                    container.text('Trưởng phòng').parent().css({ fontWeight: 'bold', textAlign: 'cen");
            WriteLiteral(@"ter', backgroundColor: '#c5d3dd' });
                                },
                                width: 100,
                                dataField: ""isTruongPhong"",
                                alignment: ""center"",
                                cellTemplate: function (container, options) {
                                    //console.log(options);
                                    let text_isTruongPhong = '';
                                    if (options.data.isTruongPhong) {
                                        text_isTruongPhong = `
                                    <div class=""custom-control custom-checkbox"">
                                        <input checked=""checked"" class=""custom-control-input"" disabled=""disabled"" type=""checkbox"">
                                        <label class=""custom-control-label""></label>
                                    </div>
                                    `;
                                    } else {
                   ");
            WriteLiteral(@"                     text_isTruongPhong = `
                                    <div class=""custom-control custom-checkbox"">
                                        <input class=""custom-control-input"" disabled=""disabled"" type=""checkbox"">
                                        <label class=""custom-control-label""></label>
                                    </div>
                                    `;
                                    }
                                    $(container).html(""<h5>"" + text_isTruongPhong + ""</h5>"");
                                }
                            }
                        ],
                    summary: {
                        totalItems:
                            [
                                {
                                    column: 'TenDangNhap',
                                    summaryType: 'count',
                                }
                            ],
                    }
                });
            })
    ");
            WriteLiteral(@"        .fail(function (error) {
                console.log(""error"", error);
            });
    };
    load_danhsach_user();



    /* Xoá tài khoản */
    $(""#gridContainer"").on('click', '.action_user_delete', function (e) {
        let _id = $(e.target).attr('data-id');
        let _TenDangNhap = $(e.target).attr('data-tendangnhap');
        console.log(_id);
        if (_id) {
            if (confirm(`Bạn muốn xoá tài khoản?\nID: ${_id}\nTên đăng nhập: ${_TenDangNhap}`)) {
                alert('đã xoá');
            }
        }
        // alert(message?: DOMString);
        /* Act on the event */
    });
</script>


<!-- User Add -->
<script type=""text/javascript"">
    var _id_element_Form_User_Add = '#form_User_Add';
    var _Form_User_Add = $(_id_element_Form_User_Add);
    var error = $('.alert-danger', _Form_User_Add);
    var success = $('.alert-success', _Form_User_Add);
    console.log(_Form_User_Add);
    _Form_User_Add.validate({
        errorElement: 'span', //d");
            WriteLiteral(@"efault input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: """", // validate all fields including form hidden input
        rules: {
            TenDangNhap: {
                required: true,
                remote: {
                    url: '");
#nullable restore
#line 284 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\VanPhongSo\Taikhoan_js.cshtml"
                     Write(Url.Action("CheckTenDangNhap", "VanPhongSo"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"', // hàm check trả về : die(json_encode(false/true));
                    type: ""post"",
                    data: {
                        TenDangNhap: function () {
                            return $(_id_element_Form_User_Add + "" .TenDangNhap"").val();
                        }
                    }
                }
            },
            MatKhau: {
                required: true
            },
            Email: {
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
            TenDangNhap: {
                required: ""Vui lòng nhập Tên đăng nhập"",
                remote: ""Tên đăng nhập đã tồn tại""
            },
            MatKhau: {
                required: ""Vui lòng nhập Mật khẩu""
            },
            Email: {
                required: ""Vui lòng nhập Email""
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
    ");
            WriteLiteral(@"        if (element.parent("".input-group"").size() > 0) {
                error.insertAfter(element.parent("".input-group""));
            } else if (element.attr(""data-error-container"")) {
                error.appendTo(element.attr(""data-error-container""));
            } else if (element.parents('.radio-list').size() > 0) {
                error.appendTo(element.parents('.radio-list').attr(""data-error-container""));
            } else if (element.parents('.radio-inline').size() > 0) {
                error.appendTo(element.parents('.radio-inline').attr(""data-error-container""));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr(""data-error-container""));
            } else if (element.parents('.checkbox-inline').size() > 0) {
                error.appendTo(element.parents('.checkbox-inline').attr(""data-error-container""));
            } else {
                error.insertAfter(element); // for other inputs, just");
            WriteLiteral(@" perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit
            success.hide();
            error.show();
            $(_id_element_Form_User_Add + "" .modal-body"").animate({ scrollTop: 10 });
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
    ");
            WriteLiteral(@"        $(_id_element_Form_User_Add + "" .ok"").hide();
            $(_id_element_Form_User_Add + "" .modal-body"").animate({ scrollTop: 10 });
            //$('#cancel').hide();
            //_Form_User_Add[0].submit();

            /* Ajax User Add */
            console.log('gọi ajax');
            let _data = {
                MaNhanVien: $(_id_element_Form_User_Add + "" .MaNhanVien"").val(),
                TenDangNhap: $(_id_element_Form_User_Add + "" .TenDangNhap"").val(),
                MatKhau: $(_id_element_Form_User_Add + "" .MatKhau"").val(),
                Email: $(_id_element_Form_User_Add + "" .Email"").val(),
                HoTen: $(_id_element_Form_User_Add + "" .HoTen"").val(),
                //TaiKhoanVPS: 1
            };
            console.log(_data);

            $.ajax({
                url: '");
#nullable restore
#line 374 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\VanPhongSo\Taikhoan_js.cshtml"
                 Write(Url.Action("CreateTaiKhoan", "VanPhongSo"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"', // hàm check trả về : die(json_encode(false/true));
                type: 'POST',
                dataType: 'json',
                data: _data,
            })
                .done(function () {
                    console.log(""success"");
                    success.hide();
                    $(_id_element_Form_User_Add + "" .btn_reset"").trigger('click');
                    $(_id_element_Form_User_Add + "" .ok"").show();
                    $(""#modal_User_Add"").modal('hide');
                    load_danhsach_user();
                    doNotify(""Tạo tài khoản thành công"", ""success"");
                })
                .fail(function () {
                    console.log(""error"");
                })
                .always(function () {
                    console.log(""complete"");
                });
        }

    });

    //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
    $('.LoaiTaiKhoan', _Form_User_Add).change(function () ");
            WriteLiteral(@"{
        _Form_User_Add.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
</script>

<!-- User Info - User Edit -->
<script type=""text/javascript"">

    /* Lấy thông tin nhân viên */
    $('#modal_User_Edit').on('show.bs.modal', function (event) {
        //$('#modal_User_Edit .modal-body').html('');
        var button = $(event.relatedTarget);
        var _id = button.data('id');

        var modal = $(this);
        //console.log(_id, _tieude, _maqr);
        $.ajax({
            type: 'POST',
            dataType: 'text',
            url: '");
#nullable restore
#line 418 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\VanPhongSo\Taikhoan_js.cshtml"
             Write(Url.Action("GetTaiKhoan", "VanPhongSo"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"',
            data: {
                Id: _id,
            },
            success: function (data) {
                var response = JSON.parse(data);
                console.log(response);
                if (response) {
                    $(_id_element_Form_User_Edit + "" .Id"").val(response.Id),
                    $(_id_element_Form_User_Edit + "" .TenDangNhap"").val(response.TenDangNhap);
                    $(_id_element_Form_User_Edit + "" .Email"").val(response.Email);
                    $(_id_element_Form_User_Edit + "" .MaNhanVien"").val(response.MaNhanVien);
                    $(_id_element_Form_User_Edit + "" .HoTen"").val(response.HoTen);
                    $(_id_element_Form_User_Edit + "" .LoaiTaiKhoan"").val(response.LoaiTaiKhoan).trigger('change');
                }

            },
            error: function () {
                alert('Có lỗi xảy ra trong quá trình xử lý');
            }
        });
    });
    /* Reset Form User Edit*/
    $('#modal_User_Edit').on('hide.bs.mo");
            WriteLiteral(@"dal', function (event) {
        $(""#modal_User_Edit .btn_reset"").trigger('click');
        //alert('Đóng modal nè');
        _Form_User_Edit.validate();
    });



    /* Submit Form User Edit */
    var _id_element_Form_User_Edit = '#form_User_Edit';
    var _Form_User_Edit = $(_id_element_Form_User_Edit);
    var error = $('.alert-danger', _Form_User_Edit);
    var success = $('.alert-success', _Form_User_Edit);
    console.log(_Form_User_Edit);
    _Form_User_Edit.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: """", // validate all fields including form hidden input
        rules: {
            Id: {
                required: true
            },
            TenDangNhap: {
                required: true,
");
            WriteLiteral(@"            },
            Email: {
                required: true
            }
        },

        messages: { // custom messages for radio buttons and checkboxes
            Id: {
                required: ""Không lấy được Id tài khoản""
            },
            TenDangNhap: {
                required: ""Vui lòng nhập Tên đăng nhập"",
                remote: ""Tên đăng nhập đã tồn tại""
            },
            Email: {
                required: ""Vui lòng nhập Email""
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.parent("".input-group"").size() > 0) {
                error.insertAfter(element.parent("".input-group""));
            } else if (element.attr(""data-error-container"")) {
                error.appendTo(element.attr(""data-error-container""));
            } else if (element.parents('.radio-list').size() > 0) {
                error.appendTo(element.parents('.radio-list').attr(""data-");
            WriteLiteral(@"error-container""));
            } else if (element.parents('.radio-inline').size() > 0) {
                error.appendTo(element.parents('.radio-inline').attr(""data-error-container""));
            } else if (element.parents('.checkbox-list').size() > 0) {
                error.appendTo(element.parents('.checkbox-list').attr(""data-error-container""));
            } else if (element.parents('.checkbox-inline').size() > 0) {
                error.appendTo(element.parents('.checkbox-inline').attr(""data-error-container""));
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit
            success.hide();
            error.show();
            $(_id_element_Form_User_Edit + "" .modal-body"").animate({ scrollTop: 10 });
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
     ");
            WriteLiteral(@"           .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            $(_id_element_Form_User_Edit + "" .ok"").hide();
            $(_id_element_Form_User_Edit + "" .modal-body"").animate({ scrollTop: 10 });
            //$('#cancel').hide();
            //_Form_User_Edit[0].submit();

            /* Ajax User Add */
            console.log('gọi ajax');
            let _data = {
                Id: $(_id_element_Form_User_Edit + "" .Id"").val(),
           ");
            WriteLiteral(@"     MaNhanVien: $(_id_element_Form_User_Edit + "" .MaNhanVien"").val(),
                TenDangNhap: $(_id_element_Form_User_Edit + "" .TenDangNhap"").val(),
                Email: $(_id_element_Form_User_Edit + "" .Email"").val(),
                HoTen: $(_id_element_Form_User_Edit + "" .HoTen"").val()
                //TaiKhoanVPS: 1
            };
            console.log(_data);

            $.ajax({
                url: '");
#nullable restore
#line 554 "E:\WEBSITE\NLSX\nlsx\wwwroot\Includes\VanPhongSo\Taikhoan_js.cshtml"
                 Write(Url.Action("UpdateTaiKhoan", "VanPhongSo"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"', // hàm check trả về : die(json_encode(false/true));
                type: 'POST',
                dataType: 'json',
                data: _data,
            })
                .done(function () {
                    console.log(""success"");
                    success.hide();
                    $(_id_element_Form_User_Edit + "" .btn_reset"").trigger('click');
                    $(_id_element_Form_User_Edit + "" .ok"").show();
                    $(""#modal_User_Edit"").modal('hide');
                    load_danhsach_user();
                    doNotify(""Cập nhật tài khoản thành công"", ""success"");
                })
                .fail(function () {
                    console.log(""error"");
                })
                .always(function () {
                    console.log(""complete"");
                });
        }

    });

    //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
    $('.LoaiTaiKhoan', _Form_User_Edit).change(fun");
            WriteLiteral("ction () {\r\n        _Form_User_Edit.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input\r\n    });\r\n</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
